package walkingalgorithm;

import kinematics.Joint;
import kinematics.KinematicModel;

import com.spookengine.maths.Vec2;
import com.spookengine.maths.Vec3;

public class WalkingAlgorithm {
	private Joint contactJoint;
	private Vec2 contact2D;
	private Vec2 hips2D;
	private Vec2 diff2D;

	public WalkingAlgorithm() {
		super();
		this.contactJoint = null;
		this.contact2D = new Vec2();
		this.hips2D = new Vec2();
		this.diff2D = new Vec2();
	}

	public Joint findContactJoint(Joint joint) {
		joint.isContact = false;

		Joint lowest = joint;
		for (int i = 0; i < joint.getChildren().size(); ++i) {
			Joint child = (Joint) joint.getChildren().get(i);

			Joint low = findContactJoint(child);

			if (low.getWorldTransform().getPosition().z() < lowest
					.getWorldTransform().getPosition().z()) {
				lowest = low;
			}
		}
		return lowest;
	}

	public void walk(KinematicModel skeleton) {
		Joint currContactJoint = findContactJoint(skeleton.getRoot());
		currContactJoint.isContact = true;

		if ((this.contactJoint == null)
				|| (!(this.contactJoint.equals(currContactJoint)))) {
			this.contactJoint = currContactJoint;

			Vec3 contact3D = this.contactJoint.getWorldTransform()
					.getPosition();
			this.contact2D.setTo(this.hips2D).add(contact3D.x(), contact3D.y());

			skeleton.getRoot().getLocalTransform()
					.moveTo(this.hips2D.x(), this.hips2D.y(), -contact3D.z())
					.update();
		} else {
			Vec3 contact3D = currContactJoint.getWorldTransform().getPosition();
			this.diff2D.setTo(this.contact2D).sub(this.hips2D)
					.sub(contact3D.x(), contact3D.y());

			this.hips2D.add(this.diff2D);
			skeleton.getRoot().getLocalTransform()
					.moveTo(this.hips2D.x(), this.hips2D.y(), -contact3D.z())
					.update();
		}
		skeleton.getRoot().localToWorldTree();
	}
}
