package streamservice;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import kinematics.KinematicModel;
import kinematics.KinematicModelProvider;
import kinematics.KinematicModelService;
import kinematics.models.TCPUpperSuitWithGloves;
import kinematics.models.ioutils.DistanceDataParser;
import main.GlobalSettingsAndVariables;

import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import streamservice.actions.CloseTrackAction;
import streamservice.stream.StreamService;
import streamservice.stream.events.TrackActorMapEvent;
import streamservice.tracks.AnimationTrack;
import streamservice.tracks.SuitTrack;
import streamservice.tracks.Track;
import eventhandling.EventBus;
import fileio.export.ExportService;

public class TrackPanel extends JPanel implements ActionListener, MouseListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2543787587256572009L;
	
	private class SuitScalingFactoEditorDialog extends JDialog implements ActionListener, ChangeListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2172211460911070195L;

		private static final double UPPER_BOUND = 10.0d;
		private static final double LOWER_BOUND = 0.0d;
		private static final double INCREMENT = 0.01d;
		
		private JSpinner handSpinner;
		private JSpinner armSpinner;
		private JSpinner upperBodySpinner;
		private JSpinner legSpinner;
		
		private SuitScalingFactoEditorDialog() {
			super();
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
			this.setTitle("Edit Scaling Factors");
			this.setAlwaysOnTop(true);
			this.setupContentPane();
			
			Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			Point upperLeft = new Point(0, 0);
			
			this.pack();
			Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
					+ d.height / 2);
			this.setLocation(new Point(center.x - this.getWidth() / 2, center.y
					- this.getHeight() / 2));
			
			this.setVisible(true);
		}
		/**
		 * builds the content pane of the dialog and places components.
		 */
		private void setupContentPane() {
			SpinnerModel handModel = new SpinnerNumberModel(
					TCPUpperSuitWithGloves.baseHandScalingFactor,
					TrackPanel.SuitScalingFactoEditorDialog.LOWER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.UPPER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.INCREMENT);
			this.handSpinner = new JSpinner(handModel);
			this.handSpinner.addChangeListener(this);
			JLabel handLabel = new JLabel("hand scaling factor:");
			
			SpinnerModel armModel = new SpinnerNumberModel(TCPUpperSuitWithGloves.baseArmScalingFactor,
					TrackPanel.SuitScalingFactoEditorDialog.LOWER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.UPPER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.INCREMENT);
			this.armSpinner = new JSpinner(armModel);
			this.armSpinner.addChangeListener(this);
			JLabel armLabel = new JLabel("arm scaling factor:");
			
			SpinnerModel upperBodyModel = new SpinnerNumberModel(TCPUpperSuitWithGloves.baseUpperBodyScalingFactor,
					TrackPanel.SuitScalingFactoEditorDialog.LOWER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.UPPER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.INCREMENT);
			this.upperBodySpinner = new JSpinner(upperBodyModel);
			this.upperBodySpinner.addChangeListener(this);
			JLabel upperBodyLabel = new JLabel("upper body scaling factor:");
			
			SpinnerModel legModel = new SpinnerNumberModel(TCPUpperSuitWithGloves.baseLegScalingFactor,
					TrackPanel.SuitScalingFactoEditorDialog.LOWER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.UPPER_BOUND,
					TrackPanel.SuitScalingFactoEditorDialog.INCREMENT);
			this.legSpinner = new JSpinner(legModel);
			this.legSpinner.addChangeListener(this);
			JLabel legLabel = new JLabel("leg scaling factor:");
			
			JPanel contentPanel = new JPanel();
			GroupLayout mainLayout = new GroupLayout(contentPanel);
			
			mainLayout.setAutoCreateContainerGaps(true);
			mainLayout.setAutoCreateGaps(true);
			
			mainLayout.linkSize(handLabel, armLabel, upperBodyLabel, legLabel, this.handSpinner, this.armSpinner,
					this.upperBodySpinner, this.legSpinner);
			
			JComponent[][] components = new JComponent[][] {
					{handLabel, this.handSpinner},
					{armLabel, this.armSpinner},
					{upperBodyLabel, this.upperBodySpinner},
					{legLabel, this.legSpinner}
			};
			
			ParallelGroup horizontalGroup = mainLayout.createParallelGroup();
			SequentialGroup verticalGroup = mainLayout.createSequentialGroup();
			
			for (JComponent[] comps : components) {
				horizontalGroup.addGroup(mainLayout.createSequentialGroup()
					.addComponent(comps[0])
					.addComponent(comps[1])
				);
				verticalGroup.addGroup(mainLayout.createParallelGroup()
					.addComponent(comps[0])
					.addComponent(comps[1])
				);
			}
			
			JButton applyButton = new JButton("update model");
			applyButton.setActionCommand("propotion_dialog_apply_button");
			applyButton.addActionListener(this);
			horizontalGroup.addGroup(mainLayout.createSequentialGroup()
				.addGap(0, 100, Short.MAX_VALUE)
				.addComponent(applyButton)
			);
			verticalGroup.addComponent(applyButton);
			
			mainLayout.setHorizontalGroup(horizontalGroup);
			mainLayout.setVerticalGroup(verticalGroup);
			
			contentPanel.setLayout(mainLayout);
			contentPanel.setBorder(BorderFactory.createTitledBorder("Editing Options"));
			this.setContentPane(contentPanel);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand().equals("propotion_dialog_apply_button")) {
				KinematicModelService model = KinematicModelProvider.getServiceByName(TCPUpperSuitWithGloves.modelName);
				if (model != null) {
					KinematicModel actor = model.create();
					TrackPanel.this.track.setActor(actor);
					EventBus.getInstance().removeAll(TrackActorMapEvent.class);
					EventBus.getInstance()
							.add(new TrackActorMapEvent(TrackPanel.this.track,
									actor));
				}
			}
		}
		
		@Override
		public void stateChanged(ChangeEvent ce) {
			if (ce.getSource().equals(this.handSpinner)) {
				float handScalingFactor = ((SpinnerNumberModel) this.handSpinner.getModel()).getNumber().floatValue();
				TCPUpperSuitWithGloves.baseHandScalingFactor = handScalingFactor;
			} else if (ce.getSource().equals(this.armSpinner)) {
				float armScalingFactor = ((SpinnerNumberModel) this.armSpinner.getModel()).getNumber().floatValue();
				TCPUpperSuitWithGloves.baseArmScalingFactor = armScalingFactor;
			} else if (ce.getSource().equals(this.upperBodySpinner)) {
				float upperBodyScalingFactor = ((SpinnerNumberModel) this.upperBodySpinner.getModel()).getNumber().floatValue();
				TCPUpperSuitWithGloves.baseUpperBodyScalingFactor = upperBodyScalingFactor;
			} else if (ce.getSource().equals(this.legSpinner)) {
				float legScalingFactor = ((SpinnerNumberModel) this.legSpinner.getModel()).getNumber().floatValue();
				TCPUpperSuitWithGloves.baseLegScalingFactor = legScalingFactor;
			}
		}
	}
	
	private class OnTrackActorMap implements LookupListener {
		private Lookup.Result<TrackActorMapEvent> result;

		public void listen() {
			this.result = EventBus.getInstance().getLookup()
					.lookupResult(TrackActorMapEvent.class);
			this.result.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TrackActorMapEvent> it = this.result.allInstances().iterator();

			while (it.hasNext()) {
				TrackActorMapEvent evt = (TrackActorMapEvent) it.next();

				if (evt.track == TrackPanel.this.track) {
					TrackPanel.this.actorLabel.setText(evt.actor.name);
					TrackPanel.this.track.setActor(evt.actor);
				}
			}
		}
	}
	
	private class ContextMenuMouseAdapter extends MouseAdapter {
		private JPopupMenu popup;
		
		private ContextMenuMouseAdapter(JPopupMenu popup) {
			super();
			this.popup = popup;
		}
		
		public void mousePressed(MouseEvent evt) {
			if (evt.isPopupTrigger())
				this.popup.show(evt.getComponent(), evt.getX(),
						evt.getY());
		}

		public void mouseReleased(MouseEvent evt) {
			if (evt.isPopupTrigger())
				this.popup.show(evt.getComponent(), evt.getX(),
						evt.getY());
		}
	}
	
	private final StreamService playback;
	private boolean isHighlighted;
	private final Track track;
	private Color color;
	private Stroke thickStroke;
	private JLabel actorLabel;
	private final OnTrackActorMap onTrackActorMap;

	public TrackPanel(Track track) {
		super();
		this.playback = GlobalSettingsAndVariables.defaultStreamService;

		this.track = track;
		initTrackPanel(track);

		initPopupMenu();

		addMouseListener(this);

		this.onTrackActorMap = new OnTrackActorMap();
		this.onTrackActorMap.listen();
	}

	public void setBounds(Rectangle r) {
		super.setBounds(r);

		removeAll();
		initTrackPanel(this.track);
	}

	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);

		removeAll();
		initTrackPanel(this.track);
	}

	public Color getColor() {
		return this.color;
	}

	public Track getTrack() {
		return this.track;
	}

	private void initTrackPanel(Track track) {
		setLayout(null);

		JLabel nameLabel = new JLabel(track.getName());
		add(nameLabel);
		if (track instanceof AnimationTrack) {
			this.color = new Color(0.6F, 0.6F, 0.9F);
			nameLabel.setBounds(20, 5, 160, 20);
		} else if (track instanceof SuitTrack) {
			this.color = new Color(0.6F, 0.9F, 0.6F);
			nameLabel.setBounds(20, 0, 160, 20);

			this.actorLabel = new JLabel(track.getActor().name);
			this.actorLabel.setBounds(20, 14, 160, 20);
			add(this.actorLabel);
		}
		this.thickStroke = new BasicStroke(6.0F);
		setBackground(this.color);
	}

	private void initPopupMenu() {
		JPopupMenu contextMenu = new JPopupMenu();

		if (this.track instanceof SuitTrack) {
			JMenu kinematicModelsMenu = new JMenu("Kinematic Models");
			
			for (String modelName : KinematicModelProvider.availableModelNames) {
				JMenuItem modelItem = new JMenuItem(modelName);
				modelItem.setActionCommand("model_change:" + modelName);
				modelItem.addActionListener(this);
				kinematicModelsMenu.add(modelItem);
			}
			contextMenu.add(kinematicModelsMenu);
			
			JMenuItem scalingFactorMenu = new JMenuItem("Adjust Proportions");
			scalingFactorMenu.setActionCommand("adjust_propotions");
			scalingFactorMenu.addActionListener(this);
//			if (this.track.getActor().name.startsWith(TCPUpperSuitWithGloves.modelName)) {
//			if (this.checkForSuitWithGloves()) {
//				scalingFactorMenu.setEnabled(true);
//			} else {
//				scalingFactorMenu.setEnabled(false);
//			}
			contextMenu.add(scalingFactorMenu);
			
			JMenuItem kinematicDataMenuItem = new JMenuItem("Load Kinematic Data");
			kinematicDataMenuItem.setActionCommand("load_kinematic_data");
			kinematicDataMenuItem.addActionListener(this);
			contextMenu.add(kinematicDataMenuItem);
			
//			JMenuItem trackOffset = new JMenuItem(new AbstractAction() {
//				public void actionPerformed(ActionEvent e) {
//					KinematicOffsetSettings offsetSettings = new KinematicOffsetSettings(
//							TrackPanel.this.track);
//				}
//			});
			JMenuItem trackOffset = new JMenuItem("Set Kinematic Offset");
			contextMenu.add(trackOffset);
			
			JMenuItem saveDataItem = new JMenuItem("Export Data");
			saveDataItem.setActionCommand("savedata");
			saveDataItem.addActionListener(this);
			contextMenu.add(saveDataItem);
		} else {
			JMenuItem closeTrackItem = new JMenuItem(new CloseTrackAction(
					this.track));
			closeTrackItem.setText("Close Track");
			contextMenu.add(closeTrackItem);
		}

		addMouseListener(new ContextMenuMouseAdapter(contextMenu));
	}

	private boolean checkForSuitWithGloves() {
		// hmm quite stupid...
		if (this.track.getActor() != null) {
			if (this.track.getActor().findChild("Hips") == null) {
				return false;
			}
			if (this.track.getActor().findChild("RightLeg") == null) {
				return false;
			}
			if (this.track.getActor().findChild("HeadTop") == null) {
				return false;
			}
			if (this.track.getActor().findChild("LeftLittleKnuckle1") == null) {
				return false;
			}
			if (this.track.getActor().findChild("RightThumbKnuckle2") == null) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		Stroke normalStroke = g2d.getStroke();

		if ((this.track.isSelected) || (this.isHighlighted)) {
			if (this.track.isSelected)
				g2d.setColor(this.color.darker().darker());
			if (this.isHighlighted) {
				g2d.setColor(this.color.darker());
			}
			g2d.setStroke(this.thickStroke);
			g2d.drawRect(0, 0, getBounds().width, getBounds().height);
		}

		g2d.setColor(this.color);
		g2d.setStroke(normalStroke);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		String actionCommand = ae.getActionCommand();
		
		if (actionCommand.startsWith("model_change:")) {
			String modelName = actionCommand.split("model_change:")[1];
			
			KinematicModelService model = KinematicModelProvider.getServiceByName(modelName);
			if (model != null) {
				KinematicModel actor = model.create();
				TrackPanel.this.track.setActor(actor);
				EventBus.getInstance().removeAll(TrackActorMapEvent.class);
				EventBus.getInstance()
						.add(new TrackActorMapEvent(TrackPanel.this.track,
								actor));
			}
		} else if (actionCommand.equals("savedata")) {
			Object[] data = ExportService.getFile(true, GlobalSettingsAndVariables.defaultOutputPath);
			if (data != null) {
				ExportService.runSaveService(data, this.track);
			}
		} else if (actionCommand.equals("load_kinematic_data")) {
			// TODO
			File file = DistanceDataParser.getFile(true, GlobalSettingsAndVariables.defaultKinematicDataPath);
			if (file != null) {
				String modelName = TrackPanel.this.track.getActor().name;
				String baseModelName = null;
				for (String name : KinematicModelProvider.availableModelNames) {
					if (modelName.startsWith(name)) {
						baseModelName = name;
					}
				}
				
				if (baseModelName == null) {
					System.err.println("unable to fetch basemodel name for: " + modelName + "...");
					return;
				}
				
				KinematicModelService model = KinematicModelProvider.getServiceByName(baseModelName);
				if (model != null) {
					KinematicModel actor = model.create(file);
					TrackPanel.this.track.setActor(actor);
					EventBus.getInstance().removeAll(TrackActorMapEvent.class);
					EventBus.getInstance()
							.add(new TrackActorMapEvent(TrackPanel.this.track,
									actor));
				}
			}
		} else if (actionCommand.equals("adjust_propotions")) {
			if (this.checkForSuitWithGloves()) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						new SuitScalingFactoEditorDialog();
					}
				});
			}
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (e.isShiftDown()) {
			this.playback.selectTrack(this.track);
		} else {
			this.playback.deselectAllTracks();
			this.playback.selectTrack(this.track);
		}
	}

	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {
		this.isHighlighted = true;
	}

	public void mouseExited(MouseEvent e) {
		this.isHighlighted = false;
	}
}
