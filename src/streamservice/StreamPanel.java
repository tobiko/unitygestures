package streamservice;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import kinematics.KinematicModel;
import kinematics.KinematicModelProvider;
import kinematics.KinematicModelService;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import network.connections.SuitConnection;
import network.connections.tcp.events.SuitsConnectedEvent;
import network.connections.tcp.events.SuitsDisconnectedEvent;

import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import streamservice.frames.FrameBuffer;
import streamservice.frames.FrameBufferFactory;
import streamservice.stream.StreamService;
import streamservice.stream.events.TracksCreatedEvent;
import streamservice.stream.events.TracksDestroyedEvent;
import streamservice.stream.events.TracksSelectedEvent;
import streamservice.tracks.AnimationTrack;
import streamservice.tracks.SuitTrack;
import streamservice.tracks.Track;
import eventhandling.EventBus;

public class StreamPanel extends JPanel implements MouseListener,
		MouseMotionListener, MouseWheelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1538586951064713204L;
	
	private static enum DragMode {
		TIMELINE_MOVE, SELECTION_MOVE, SELECTION_RESIZE_BACK, SELECTION_RESIZE_FRONT;
	}

	private class OnSuitsConnected implements LookupListener {
		private Lookup.Result<SuitsConnectedEvent> results;

		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(SuitsConnectedEvent.class);
			this.results.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends SuitsConnectedEvent> it = this.results
					.allInstances().iterator();

			if (it.hasNext()) {
				SuitsConnectedEvent evt = it.next();

				List<Track> createdTracks = new ArrayList<Track>();
				for (SuitConnection suit : evt.suits) {
					KinematicModel actor = null;

					String defaultKinematicModelName = GlobalSettingsAndVariables.defaultKinematicModelName;

					KinematicModelService model = KinematicModelProvider.getServiceByName(defaultKinematicModelName);
					
					if (model != null) {
						actor = model.create();
					}
					
					if (actor == null) {
						System.out.println("WTF?!");
						actor = GlobalSettingsAndVariables.defaultKinematicModelService.create();
					}

					Track track = new SuitTrack(suit.getName(), actor, suit);
					createdTracks.add(track);
				}

				EventBus.getInstance().removeAll(TracksCreatedEvent.class);
				EventBus.getInstance().add(
						new TracksCreatedEvent(createdTracks));
			}
		}
	}

	private class OnSuitsDisconnected implements LookupListener {
		private Lookup.Result<SuitsDisconnectedEvent> results;

		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(SuitsDisconnectedEvent.class);
			this.results.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends SuitsDisconnectedEvent> it = this.results.allInstances().iterator();

			if (it.hasNext()) {
				SuitsDisconnectedEvent evt = (SuitsDisconnectedEvent) it.next();

				List<Track> destroyTracks = new ArrayList<Track>();
				Iterator<? extends SuitConnection> iterator = evt.suits
						.iterator();
				while (iterator.hasNext()) {
					SuitConnection suit = iterator.next();

					for (TrackPanel jTrack : StreamPanel.this.trackPanels)
						if (jTrack.getTrack() instanceof SuitTrack) {
							SuitTrack suitTrack = (SuitTrack) jTrack.getTrack();

							if (suitTrack.getSuit().equals(suit)) {
								destroyTracks.add(suitTrack);
								StreamPanel.this.playback
										.deselectTrack(suitTrack);
							}
						}
				}

				EventBus.getInstance().removeAll(TracksDestroyedEvent.class);
				EventBus.getInstance().add(
						new TracksDestroyedEvent(destroyTracks));
			}
		}
	}

	private class OnTracksCreated implements LookupListener {
		private Lookup.Result<TracksCreatedEvent> results;

		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(TracksCreatedEvent.class);
			this.results.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksCreatedEvent> it = this.results.allInstances().iterator();

			if (it.hasNext()) {
				TracksCreatedEvent evt = (TracksCreatedEvent) it.next();

				for (Track track : evt.tracks) {
					TrackPanel newTrackPanel = new TrackPanel(track);
					newTrackPanel.setBounds(0, StreamPanel.this.topMargin
							+ StreamPanel.this.getTrackCount() * 35 + 3, 200,
							32);
					StreamPanel.this.trackPanels.add(newTrackPanel);
					StreamPanel.this.add(newTrackPanel);
				}

				if (StreamPanel.this.trackPanels.size() == 1) {
					StreamPanel.this.playback.deselectAllTracks();
					StreamPanel.this.playback
							.selectTrack(((TrackPanel) StreamPanel.this.trackPanels
									.get(0)).getTrack());
				}
			}
		}
	}

	private class OnTracksDestroyed implements LookupListener {
		private Lookup.Result<TracksDestroyedEvent> results;

		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(TracksDestroyedEvent.class);
			this.results.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksDestroyedEvent> it = this.results
					.allInstances().iterator();

			if (it.hasNext()) {
				TracksDestroyedEvent evt = (TracksDestroyedEvent) it.next();

				for (Track track : evt.tracks) {
					for (FrameBuffer buffer : StreamPanel.this.frameBufferFactory
							.getFrameBuffers()) {
						buffer.removeTrack(track);
					}

					StreamPanel.TrackSavable savable = StreamPanel.this
							.getSavable(track);
					if (savable != null) {
						savable.askSave();
					}

					for (int i = 0; i < StreamPanel.this.trackPanels.size(); ++i) {
						TrackPanel trackPanel = (TrackPanel) StreamPanel.this.trackPanels
								.get(i);
						if (trackPanel.getTrack() == track) {
							StreamPanel.this.trackPanels.remove(trackPanel);
							StreamPanel.this.remove(trackPanel);
							break;
						}

					}

					for (int i = 0; i < StreamPanel.this.trackPanels.size(); ++i) {
						((TrackPanel) StreamPanel.this.trackPanels.get(i))
								.setBounds(0, StreamPanel.this.topMargin + i
										* 35 + 3, 200, 32);
					}
				}
			}
		}
	}

	private class OnTracksSelected implements LookupListener {
		private Lookup.Result<TracksSelectedEvent> results;

		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(TracksSelectedEvent.class);
			this.results.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksSelectedEvent> it = this.results.allInstances().iterator();

			if (it.hasNext()) {
				TracksSelectedEvent evt = (TracksSelectedEvent) it.next();

				for (StreamPanel.TrackSavable savable : StreamPanel.this.savables) {
					if (savable.track instanceof AnimationTrack)
						savable.doUnregister();
					else if (!(((SuitTrack) savable.track).unsavedRecording)) {
						savable.doUnregister();
					}
				}

				for (Track track : evt.tracks) {
					StreamPanel.this.savables.add(new StreamPanel.TrackSavable(
							track));
				}
			}
		}
	}

	public class PlaybackTransferHandler extends TransferHandler {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4031941365669066494L;
//		public boolean canImport(TransferHandler.TransferSupport info) {
//			DataFlavor[] flavors = info.getDataFlavors();
//			for (DataFlavor flavor : flavors) {
//				if (flavor.getHumanPresentableName()
//						.equals("Animation_Flavour")) {
//					return true;
//				}
//			}
//			return false;
//		}
//
//		private boolean trackExists(String name) {
//			for (TrackPanel panel : StreamPanel.this.trackPanels) {
//				if (panel.getTrack().getName().equals(name)) {
//					return true;
//				}
//			}
//
//			return false;
//		}
//
//		public boolean importData(TransferHandler.TransferSupport info) {
//			if (!(info.isDrop()))
//				return false;
//			try {
//				for (DataFlavor flavour : info.getDataFlavors()) {
//					Transferable data = info.getTransferable();
//
//					String str = flavour.getHumanPresentableName();
//					int k = -1;
//					switch (str.hashCode()) {
//					case -1093988362:
//						if (str.equals("Animation_Flavour"))
//							k = 0;
//					}
//					switch (k) {
//					case 0:
//						AnimationDataObject animFile = (AnimationDataObject) data
//								.getTransferData(flavour);
//						Future future = animFile.loadTrack();
//
//						if (trackExists(animFile.getName()))
//							continue;
//						new Thread(new Runnable(future, animFile) {
//							public void run() {
//								try {
//									Track track = (Track) this.val$future.get();
//
//									TrackPanel newTrackPanel = new TrackPanel(track);
//									newTrackPanel.setBounds(
//											0,
//											StreamPanel.this.topMargin
//													+ StreamPanel.this
//															.getTrackCount()
//													* 35 + 3, 200, 32);
//									StreamPanel.this.trackPanels
//											.add(newTrackPanel);
//									StreamPanel.this.add(newTrackPanel);
//								} catch (java.util.concurrent.ExecutionException ex) {
//									MainFrame.defaultConsole.write("cannot load data from file..." + System.getProperty("line.separator"));
////									PlaybackTopComponent.logger.log(
////											Level.SEVERE,
////											"Unable to load animation {0}",
////											this.val$animFile.toString());
//								}
//							}
//						}).start();
//					}
//				}
//			} catch (NumberFormatException nfe) {
//				MainFrame.defaultConsole.write(nfe.getLocalizedMessage() + System.getProperty("line.separator"));
////				PlaybackTopComponent.logger.log(Level.WARNING,
////						"Number format exception\n{0}",
////						ex.getLocalizedMessage());
//			} catch (IOException ioe) {
//				MainFrame.defaultConsole.write(ioe.getLocalizedMessage() + System.getProperty("line.separator"));
////				PlaybackTopComponent.logger.log(Level.WARNING,
////						"IO exception \n{0}", ex.getLocalizedMessage());
//			} catch (UnsupportedFlavorException ex) {
//				MainFrame.defaultConsole.write("cannot load data from file..." + System.getProperty("line.separator"));
////				PlaybackTopComponent.logger.log(Level.WARNING,
////						"Unsupported flavour \n{0}", ex.getLocalizedMessage());
//			}
//
//			return true;
//		}
	}

	private class TrackSavable {
		// extends AbstractSavable {
		protected Track track;

		public TrackSavable(Track track) {
			this.track = track;

			// boolean canRegister = StreamPanel.this.getSavable(track) == null;
			// if (canRegister)
			// register();
		}

		public void doUnregister() {
			// unregister();
		}

		public void askSave() {
			// int result = JOptionPane
			// .showConfirmDialog(
			// null,
			// "Track contains unsaved recording.\n\nDo you want to save now?",
			// "Save Now", 0);
			// if (result != 0)
			// return;
			// try {
			// handleSave();
			// } catch (IOException ex) {
			// PlaybackTopComponent.logger.log(Level.WARNING,
			// "Error saving track.\n{0}", ex.getMessage());
			// }
		}

//		protected String findDisplayName() {
//			return this.track.getName();
//		}

//		protected void handleSave() {
			// throws IOException {
			// Collection saveServices = Lookup.getDefault().lookupAll(
			// SaveService.class);
			//
			// JFileChooser fileChooser = new JFileChooser();
			// fileChooser.setDialogTitle("Save " + this.track.getName());
			//
			// if ((this.track instanceof SuitTrack)
			// && (((SuitTrack) this.track).getRawFrameCount() > 0))
			// PlaybackTopComponent.access$1102(StreamPanel.this,
			// true);
			// else {
			// PlaybackTopComponent.access$1102(StreamPanel.this,
			// false);
			// }
			//
			// fileChooser.setAcceptAllFileFilterUsed(false);
			// for (SaveService saveService : saveServices) {
			// if ((StreamPanel.this.raw)
			// || (!(Arrays.asList(saveService.getExtensions())
			// .contains(".raw")))) {
			// fileChooser.addChoosableFileFilter(new FileFilter(
			// saveService) {
			// public boolean accept(File f) {
			// for (String ext : this.val$saveService
			// .getExtensions()) {
			// String filename = f.getName();
			// if ((filename.contains(ext))
			// || (f.isDirectory()))
			// return true;
			// }
			// return false;
			// }
			//
			// public String getDescription() {
			// return this.val$saveService.getName();
			// }
			// });
			// }
			// }
			//
			// int result = fileChooser.showSaveDialog(null);
			// if (result != 0)
			// return;
			// FileFilter filter = fileChooser.getFileFilter();
			//
			// File file = fileChooser.getSelectedFile();
			//
			// int extIndex = file.getName().lastIndexOf(".");
			// if (extIndex == -1)
			// extIndex = file.getName().length();
			//
			// String filename = file.getName().substring(0, extIndex)
			// + filter.getDescription();
			// String path = file.getPath().substring(0,
			// file.getPath().lastIndexOf(File.separator) + 1)
			// + filename;
			//
			// int startIndex = 0;
			// int filesSaved = 0;
			//
			// for (int i = 0; i < this.track.getFrameCount(); ++i)
			// if ((this.track.getFrame(i).isTagged())
			// || (i == this.track.getFrameCount() - 1)) {
			// if (filesSaved > 0)
			// file = new File(path.substring(0, path.length() - 4)
			// .concat("_"
			// + filesSaved
			// + path.substring(path.length() - 4,
			// path.length())));
			// else {
			// file = new File(path);
			// }
			//
			// if (!(file.exists())) {
			// file.createNewFile();
			// }
			//
			// SaveService saveService = null;
			// for (SaveService service : saveServices) {
			// for (String ext : service.getExtensions()) {
			// if (file.getName().contains(ext)) {
			// saveService = service;
			// break;
			// }
			// }
			//
			// if (saveService != null) {
			// break;
			// }
			// }
			// if (saveService != null) {
			// if (this.track instanceof SuitTrack) {
			// if (((SuitTrack) this.track).getRawFrameCount() == ((SuitTrack)
			// this.track)
			// .getFrameCount()) {
			// ((SuitTrack) this.track).cropRawTagged(
			// startIndex, i);
			// saveService.save((SuitTrack) this.track, file);
			// } else {
			// this.track.cropTagged(startIndex, i);
			// saveService.save(this.track, file);
			// }
			// } else {
			// this.track.cropTagged(startIndex, i);
			// saveService.save(this.track, file);
			// }
			// }
			// startIndex = i + 1;
			// ++filesSaved;
			// while ((StreamPanel.this.playback.kinematicTorrent != null)
			// && (StreamPanel.this.playback.kinematicTorrent
			// .isAlive()))
			// ;
			// while ((StreamPanel.this.playback.rawTorrent != null)
			// && (StreamPanel.this.playback.rawTorrent
			// .isAlive()))
			// ;
			// if (this.track instanceof SuitTrack)
			// ((SuitTrack) this.track).toRawNormal();
			// else
			// this.track.toNormal();
			// }
//		}

		public boolean equals(Object otherTrackSaveable) {
			if (otherTrackSaveable instanceof TrackSavable) {
				return (this == (TrackSavable) otherTrackSaveable);
			}
			return false;
		}

		public int hashCode() {
			return this.track.getName().hashCode();
		}
	}

	private DragMode dragMode = DragMode.TIMELINE_MOVE;
	private StreamService playback;
	private final OnTracksCreated onTracksCreated = new OnTracksCreated();
	private final OnTracksDestroyed onTracksDestroyed = new OnTracksDestroyed();
	private final OnTracksSelected onTracksSelected = new OnTracksSelected();
	private final OnSuitsConnected onSuitsConnected = new OnSuitsConnected();
	private final OnSuitsDisconnected onSuitsDisconnected = new OnSuitsDisconnected();
	private Timer timer;
	private List<TrackPanel> trackPanels = new ArrayList<TrackPanel>();

	private int topMargin = 28;
	private int leftMargin = 200;
	private int framesPerPixel = 1;
	private boolean mouseOver;
//	private boolean progressBarDrawn = false;
	private int currX = 0;
	private int scrollFrames = 0;
	private int scrollOffset = 0;
	private FrameBufferFactory frameBufferFactory;
	private int highlightedFrame;
	private int[] selectedFrames = new int[2];

	private Color anthra = new Color(68, 78, 87);
	private Color karmin = new Color(172, 55, 66);
	private Color gold = new Color(187, 169, 118);
	
	private List<TrackSavable> savables = new ArrayList<TrackSavable>();

	public StreamPanel() {
		super();
		this.initComponents();
		this.frameBufferFactory = GlobalSettingsAndVariables.defaultFrameBufferFactory;
		this.playback = GlobalSettingsAndVariables.defaultStreamService;

		this.onTracksCreated.listen();
		this.onTracksDestroyed.listen();
		this.onTracksSelected.listen();
		this.onSuitsConnected.listen();
		this.onSuitsDisconnected.listen();

		this.setTransferHandler(new PlaybackTransferHandler());

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);

		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				StreamPanel.this.repaint();
			}
		}, 0L, 40L);
	}

	private void initComponents() {
		this.setAutoscrolls(true);

		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));

		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
	}

	private int toFrameNumber(int x) {
		return ((this.scrollFrames + x) * this.framesPerPixel);
	}

	private void scrollTimeline(int x) {
		this.scrollFrames += this.currX - x;
		this.currX = x;

		if (this.scrollFrames < 0)
			this.scrollFrames = 0;

		this.scrollOffset = (this.scrollFrames % 40);
	}

	private void moveSelection(int dx) {
		for (int i = 0; i < 2; ++i)
			this.selectedFrames[i] += this.framesPerPixel * dx;
	}

	private void resizeSelection(int i, int dw) {
		this.selectedFrames[i] += this.framesPerPixel * dw;

		if ((this.selectedFrames[0] >= this.selectedFrames[1])
				|| (this.selectedFrames[1] <= this.selectedFrames[0]))
			this.selectedFrames[i] -= this.framesPerPixel * dw;
	}

	private void drawRuler(Graphics2D g2d) {
		g2d.setColor(this.karmin);
		g2d.fillRect(this.leftMargin, 0, getWidth(), this.topMargin);

		int barCount = getWidth() / 40;
		for (int i = 0; i < barCount; ++i) {
			int x = this.leftMargin + 40 * i - this.scrollOffset;
			int frameNo = (40 * this.scrollFrames / 40 + 40 * i)
					* this.framesPerPixel;

			g2d.setColor(this.gold);
			if ((frameNo == 0) || (frameNo % this.framesPerPixel * 200 == 0)) {
				g2d.fillRect(x - 1, this.topMargin, 2, getHeight());
				g2d.drawString("" + frameNo, x + 3, 24);
			} else {
				g2d.drawLine(x, this.topMargin, x, getHeight());
			}
		}
	}

	private void drawHighlightedFrame(Graphics2D g2d) {
		int x = this.leftMargin + this.highlightedFrame / this.framesPerPixel
				- this.scrollFrames;

		g2d.setColor(this.anthra);
		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.drawRect(x - 1, 0, 2, getHeight());
		g2d.setComposite(AlphaComposite.getInstance(3, 0.4F));
		g2d.fillRect(x - 1, 0, 2, getHeight());

		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.setColor(this.gold);
		g2d.drawString("" + this.highlightedFrame, x + 3, 10);
	}

	private void drawSelectedFrame(Graphics2D g2d) {
		int x = this.leftMargin + this.selectedFrames[0] / this.framesPerPixel
				- this.scrollFrames;
		int w = this.leftMargin + this.selectedFrames[1] / this.framesPerPixel
				- this.scrollFrames - x;

		g2d.setColor(this.anthra);
		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.drawRect(x - 1, 0, w + 2, getHeight());
		g2d.setComposite(AlphaComposite.getInstance(3, 0.4F));
		g2d.fillRect(x - 1, 0, w + 2, getHeight());

		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.setColor(this.gold);
		g2d.drawString("" + this.selectedFrames[1], x + w + 3, 10);
		if (this.selectedFrames[0] < this.selectedFrames[1])
			g2d.drawString("" + this.selectedFrames[0], x - 24, 10);
	}

	private void drawCurrentFrame(Graphics2D g2d) {
		Iterator<Track> localIterator = this.playback.getSelectedTracks().iterator();
		if (!(localIterator.hasNext()))
			return;
		Track track = (Track) localIterator.next();
		int x = this.leftMargin + track.frameNo / this.framesPerPixel
				- this.scrollFrames;

		switch (track.state) {
		case IDLE:
			g2d.setColor(Color.GREEN);
			break;
		case LIVE:
			g2d.setColor(Color.GREEN);
			break;
		case PLAYING:
			g2d.setColor(Color.RED);
			break;
		case RECORDING:
			g2d.setColor(Color.YELLOW);
		}

		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.drawRect(x - 1, 0, 2, getHeight());
		g2d.setComposite(AlphaComposite.getInstance(3, 0.5F));
		g2d.fillRect(x - 1, 0, 2, getHeight());
//		this.progressBarDrawn = true;

		g2d.setColor(this.gold);
		g2d.setComposite(AlphaComposite.getInstance(3, 0.9F));
		g2d.drawString("" + track.frameNo, x + 3, 10);
	}

	private void drawTrackList(Graphics2D g2d) {
		g2d.setComposite(AlphaComposite.getInstance(3, 1.0F));

		g2d.setColor(this.karmin);
		g2d.fillRect(0, 0, this.leftMargin, this.topMargin);
		g2d.setColor(this.gold);
		g2d.drawString("Track List", 7, 24);

		g2d.setColor(this.anthra);
		g2d.fillRect(0, this.topMargin, this.leftMargin, getHeight());

		g2d.setColor(Color.BLACK);
		g2d.drawLine(this.leftMargin, 0, this.leftMargin, getHeight());
		g2d.drawLine(0, this.topMargin, this.getWidth(), this.topMargin);
	}

	private void drawTracks(Graphics2D g2d) {
		for (Component component : getComponents())
			if (component instanceof TrackPanel) {
				TrackPanel trackPanel = (TrackPanel) component;

				int y = trackPanel.getBounds().y;
				int frameCount = trackPanel.getTrack().getFrameCount() - 1;
				int height = trackPanel.getBounds().height - 1;

				g2d.setColor(trackPanel.getBackground());
				g2d.setComposite(AlphaComposite.getInstance(3, 0.8F));
				g2d.drawRect(this.leftMargin - this.scrollFrames, y, frameCount
						/ this.framesPerPixel, height);
				g2d.setComposite(AlphaComposite.getInstance(3, 0.4F));
				g2d.fillRect(this.leftMargin - this.scrollFrames, y, frameCount
						/ this.framesPerPixel, height);

				Track track = trackPanel.getTrack();

				if ((track.state != Track.State.RECORDING) && (frameCount > 0)) {
					g2d.setColor(this.gold);
					g2d.setComposite(AlphaComposite.getInstance(3, 0.8F));
					g2d.drawString("" + frameCount, this.leftMargin
							- this.scrollFrames + frameCount
							/ this.framesPerPixel + 4, y + height / 2 + 4);
				}

				for (int i = 0; i < track.getFrameCount(); ++i)
					if (track.getFrame(i).isTagged()) {
						g2d.setColor(Color.RED);
						g2d.fillRect(this.leftMargin - this.scrollFrames + i
								/ this.framesPerPixel, y, 4, height);
					}
			}
	}

	public void paintComponent(Graphics g) {
		setLayout(null);
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setColor(this.anthra);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.setFont(new Font(MainFrame.defaultFontName, Font.BOLD, 10));

		drawRuler(g2d);

		drawTracks(g2d);

		if (this.mouseOver) {
			drawHighlightedFrame(g2d);
		}
		drawSelectedFrame(g2d);
		drawCurrentFrame(g2d);

		drawTrackList(g2d);
	}

	// FIXME: do we still need this?
	public void mouseClicked(MouseEvent me) {
		if ((SwingUtilities.isRightMouseButton(me))
				&& (this.selectedFrames[0] != 0)
				&& (this.selectedFrames[1] != 0)) {
			JPopupMenu contextMenu = new JPopupMenu();
			JMenuItem cropTrack = new JMenuItem(new AbstractAction() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -3499413739388014498L;

				public void actionPerformed(ActionEvent e) {
					for (Track track : StreamPanel.this.playback
							.getSelectedTracks()) {
						track.cropFrames(
								StreamPanel.this.selectedFrames[0] - 1,
								StreamPanel.this.selectedFrames[1]);
						track.frameNo = track.getFrameCount();
						if (track instanceof SuitTrack)
							((SuitTrack) track).cropRawFrames(
									StreamPanel.this.selectedFrames[0] - 1,
									StreamPanel.this.selectedFrames[1]);
					}
				}
			});
			cropTrack.setText("Crop Tracks");
			contextMenu.add(cropTrack);

			JMenuItem removeTags = new JMenuItem(new AbstractAction() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -7674067678480090517L;

				public void actionPerformed(ActionEvent e) {
					for (Track track : StreamPanel.this.playback
							.getSelectedTracks())
						track.removeTags(StreamPanel.this.selectedFrames[0],
								StreamPanel.this.selectedFrames[1]);
				}
			});
			removeTags.setText("Remove Tags");
			contextMenu.add(removeTags);
			contextMenu.show(this, me.getX(), me.getY());
			contextMenu.show(this, me.getX(), me.getY());
		}
	}

	public void mousePressed(MouseEvent me) {
		this.currX = me.getX();

		if (!(me.isControlDown())) {
			if ((me.getModifiers() & 0x10) == 16) {
				this.selectedFrames[0] = (this.framesPerPixel * (this.scrollFrames
						+ me.getX() - this.leftMargin));
				if (this.selectedFrames[0] < 1)
					this.selectedFrames[0] = 0;
				this.selectedFrames[1] = this.selectedFrames[0];
			}
		} else {
			int frameNo = toFrameNumber(this.currX - this.leftMargin);

			if (Math.abs(frameNo - this.selectedFrames[0]) <= 5 * this.framesPerPixel)
				this.dragMode = DragMode.SELECTION_RESIZE_BACK;
			else if (Math.abs(frameNo - this.selectedFrames[1]) <= 5 * this.framesPerPixel)
				this.dragMode = DragMode.SELECTION_RESIZE_FRONT;
			else if ((frameNo > this.selectedFrames[0])
					&& (frameNo < this.selectedFrames[1]))
				this.dragMode = DragMode.SELECTION_MOVE;
			else
				this.dragMode = DragMode.TIMELINE_MOVE;
		}
	}

	public void mouseReleased(MouseEvent me) {
		this.playback.selectFrames(this.selectedFrames[0],
				this.selectedFrames[1]);
	}

	public void mouseEntered(MouseEvent me) {
		this.mouseOver = true;
	}

	public void mouseExited(MouseEvent me) {
		this.mouseOver = false;
		this.highlightedFrame = 0;
	}

	public void mouseDragged(MouseEvent me) {
		int x = me.getX();

		if ((me.isControlDown()) && (x >= this.leftMargin)) {
			switch (this.dragMode) {
			case TIMELINE_MOVE:
				scrollTimeline(x);
				break;
			case SELECTION_MOVE:
				moveSelection(x - this.currX);
				break;
			case SELECTION_RESIZE_BACK:
				resizeSelection(0, x - this.currX);
				break;
			case SELECTION_RESIZE_FRONT:
				resizeSelection(1, x - this.currX);
			}
		} else if ((me.getModifiers() & 0x10) == 16) {
			this.selectedFrames[1] = (this.framesPerPixel * (this.scrollFrames
					+ x - this.leftMargin));
			if (this.selectedFrames[1] < this.selectedFrames[0]) {
				this.selectedFrames[1] = this.selectedFrames[0];
			}
		}

		this.currX = x;
	}

	public void mouseMoved(MouseEvent me) {
		int x = me.getX();

		if (x >= this.leftMargin) {
			this.highlightedFrame = (this.framesPerPixel * (this.scrollFrames
					+ x - this.leftMargin));
		}
		if (me.isControlDown()) {
			int frameNo = toFrameNumber(x - this.leftMargin);

			if (Math.abs(frameNo - this.selectedFrames[0]) <= 5 * this.framesPerPixel)
				this.setCursor(Cursor.getPredefinedCursor(10));
			else if (Math.abs(frameNo - this.selectedFrames[1]) <= 5 * this.framesPerPixel)
				this.setCursor(Cursor.getPredefinedCursor(11));
			else if ((frameNo > this.selectedFrames[0])
					&& (frameNo < this.selectedFrames[1]))
				this.setCursor(Cursor.getPredefinedCursor(13));
		} else {
			this.setCursor(Cursor.getPredefinedCursor(0));
		}
	}

	public void mouseWheelMoved(MouseWheelEvent mwe) {
		int zoom;
		if ((this.framesPerPixel == 1) && (mwe.getWheelRotation() == 1))
			zoom = 1;
		else {
			zoom = 2 * mwe.getWheelRotation();
		}
		this.framesPerPixel += zoom;
		if (this.framesPerPixel <= 0)
			this.framesPerPixel = 1;
	}

	public int getTrackCount() {
		int i = 0;
		for (Component component : getComponents()) {
			if (component instanceof TrackPanel) {
				++i;
			}
		}
		return i;
	}

	public TrackSavable getSavable(Track track) {
		return null;
	}
}
