package streamservice.stream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import praktikum.preprocessing.GestureInputProcessor;
import kinematics.Joint;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import network.connections.SuitConnection;
import streamservice.frames.DataFrame;
import streamservice.frames.FrameBuffer;
import streamservice.frames.FrameBufferFactory;
import streamservice.frames.KinematicFrame;
import streamservice.frames.RawFrame;
import streamservice.stream.events.TracksSelectedEvent;
import streamservice.torrents.KinematicTorrent;
import streamservice.torrents.RawTorrent;
import streamservice.tracks.SuitTrack;
import streamservice.tracks.Track;
import walkingalgorithm.WalkingAlgorithm;
import eventhandling.EventBus;

public class StreamService {
	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private final Lock readLock = this.readWriteLock.readLock();
	private final Lock writeLock = this.readWriteLock.writeLock();
	private final List<Track> selectedTracks = new ArrayList<Track>();
	private final List<Track> selectedTracksSnapshot = new ArrayList<Track>();
	
	private GestureInputProcessor gestureInputProcessor = new GestureInputProcessor();

	private class AnimationTask implements Runnable {
		WalkingAlgorithm walkingAlgorithm;
		KinematicFrame currFrame;

		private AnimationTask() {
			super();
			this.walkingAlgorithm = new WalkingAlgorithm();
		}

		public void run() {
			while (true) {
				StreamService.this.readLock.lock();
				try {
					for (Track track : StreamService.this.selectedTracks) {
						switch (track.state) {
							case RECORDING:
							case LIVE:
							case IDLE:
								this.walkingAlgorithm = new WalkingAlgorithm();
	
								synchronized (StreamService.this) {
									if (StreamService.this.selectedTracks
											.isEmpty())
										try {
											StreamService.this.wait();
										} catch (InterruptedException ex) {
											ex.printStackTrace();
										}
								}
								break;
							case PLAYING:
								int frameNo;
								synchronized (StreamService.this) {
									if (StreamService.this.singleStep) {
										frameNo = track.frameNo + track.frameDelta;
										if ((frameNo >= 0)
												&& (frameNo < track.getFrameCount()))
											track.frameNo = frameNo;
										StreamService.this.stop(track);
									} else {
										frameNo = track.frameNo + track.frameDelta;
										if (StreamService.this.loop) {
											track.frameNo = frameNo;
											if (StreamService.this.selectedFrames[0] >= StreamService.this.selectedFrames[1]) {
												if (frameNo > track.getFrameCount())
													track.frameNo = 0;
											} else if ((frameNo > track
													.getFrameCount())
													|| (frameNo > StreamService.this.selectedFrames[1]))
												track.frameNo = StreamService.this.selectedFrames[0];
										} else {
											if (frameNo == StreamService.this.selectedFrames[0]) {
												track.frameNo = frameNo;
												StreamService.this.stop(track);
											} else if (frameNo == StreamService.this.selectedFrames[1]) {
												track.frameNo = frameNo;
												StreamService.this.stop(track);
											}
	
											if (frameNo > track.getFrameCount() - 1)
												StreamService.this.stop(track);
											else {
												track.frameNo = frameNo;
											}
	
										}
	
									}
	
									if (track.frameNo < track.getFrameCount()) {
										this.currFrame = new KinematicFrame(
												track.frameNo, track.getActor(),
												track.getFrame(track.frameNo));
	
										if ((track instanceof SuitTrack)
												&& (StreamService.this.performWalkingAlgorithm)) {
											this.walkingAlgorithm
													.walk(this.currFrame.skeleton);
										}
	
										for (FrameBuffer buffer : StreamService.this.frameBufferFactory
												.getFrameBuffers()) {
											buffer.push(track, this.currFrame);
										}
									}
								}
							default:
								break;
						}

					}

					if (!(StreamService.this.selectedTracks.isEmpty()))
						try {
							Thread.sleep(1000 / ((Track) StreamService.this.selectedTracks
									.get(0)).getFps());
						} catch (InterruptedException ex) {
							ex.printStackTrace();
						}
				} finally {
					StreamService.this.readLock.unlock();
				}
			}
		}
	}
	
	public boolean performWalkingAlgorithm = false;
	public KinematicTorrent kinematicTorrent;
	public RawTorrent rawTorrent;
	private boolean loop = true;

	private int[] selectedFrames = new int[2];
	private boolean singleStep;
	KinematicFrame kinematicFrame;
	Joint lowest;
	private FrameBufferFactory frameBufferFactory;
	RawFrame RF = new RawFrame();
	

	public StreamService() {
		super();
		this.frameBufferFactory = GlobalSettingsAndVariables.defaultFrameBufferFactory;

		Thread animationThread = new Thread(new AnimationTask());
		animationThread.start();
	}
	
	public List<Track> getSelectedTracks() {
		this.readLock.lock();

		try {
			this.selectedTracksSnapshot.clear();
			this.selectedTracksSnapshot.addAll(this.selectedTracks);
		} finally {
			this.readLock.unlock();
		}
		return this.selectedTracksSnapshot;
	}

	public boolean selectTrack(Track track) {
		this.writeLock.lock();
		try {
			if ((!(this.selectedTracks.isEmpty()))
					&& (((this.selectedTracks.isEmpty())
							|| (this.selectedTracks.contains(track)) || (!(((Track) this.selectedTracks
								.get(0)).getClass().equals(track.getClass())))))) {
				return false;
			}
			track.isSelected = true;
			track.state = Track.State.IDLE;
			this.selectedTracks.add(track);

			EventBus.getInstance().removeAll(TracksSelectedEvent.class);
			EventBus.getInstance().add(
					new TracksSelectedEvent(this.selectedTracks));

			System.out.println(String.valueOf(this.selectedTracks.size())
					+ " tracks selected...");
		} finally {
			this.writeLock.unlock();
		}

		return true;
	}

	public void deselectTrack(Track track) {
		this.writeLock.lock();
		try {
			track.isSelected = false;
			this.selectedTracks.remove(track);
		} finally {
			this.writeLock.unlock();
		}
	}

	public void selectFrames(int start, int end) {
		if (start <= end) {
			this.selectedFrames[0] = start;
			this.selectedFrames[1] = end;
		}
	}

	public void deselectAllTracks() {
		this.writeLock.lock();
		try {
			for (Track track : this.selectedTracks) {
				track.isSelected = false;
			}
			this.selectedTracks.clear();
		} finally {
			this.writeLock.unlock();
		}
	}

	public void toggleLive() {
		this.readLock.lock();
		try {
			if ((!(this.selectedTracks.isEmpty()))
					&& (this.selectedTracks.get(0) instanceof SuitTrack)) {
				boolean fireSelectedEvent = false;
				for (Track track : this.selectedTracks) {
//					XXX
					track.frameNo = 0;
					switch (track.state) {
						case IDLE:
							track.frameDelta = 1;
							track.state = Track.State.LIVE;
							break;
						case LIVE:
							track.frameDelta = 0;
							track.state = Track.State.IDLE;
							break;
						case RECORDING:
							track.state = Track.State.LIVE;
	
							((SuitTrack) track).unsavedRecording = true;
	
							track.frameDelta = 0;
							fireSelectedEvent = true;
							break;
						default:
							break;
					}

				}

				if (fireSelectedEvent) {
					EventBus.getInstance().removeAll(TracksSelectedEvent.class);
					EventBus.getInstance().add(
							new TracksSelectedEvent(this.selectedTracks));
				}
			} else {
				throw new IllegalStateException(
						"Live action only operates when a suit track is selected!");
			}
		} finally {
			this.readLock.unlock();
		}
	}

	public void toggleRecord() {
		this.readLock.lock();
		try {
			if ((!(this.selectedTracks.isEmpty()))
					&& (this.selectedTracks.get(0) instanceof SuitTrack)) {
				boolean fireSelectedEvent = false;
				for (Track track : this.selectedTracks) {
					if (track.getFrameCount() - 1 < 0)
						track.frameNo = 0;
					else
						track.frameNo = (track.getFrameCount() - 1);
					
					switch (track.state) {
						case IDLE:
						case LIVE:
						case PLAYING:
							track.state = Track.State.RECORDING;
							break;
						case RECORDING:
							track.state = Track.State.IDLE;
	
							((SuitTrack) track).unsavedRecording = true;
							fireSelectedEvent = true;
					}

				}

				if (fireSelectedEvent) {
					EventBus.getInstance().removeAll(TracksSelectedEvent.class);
					EventBus.getInstance().add(
							new TracksSelectedEvent(this.selectedTracks));
				}
			} else {
				throw new IllegalStateException(
						"Record action only operates when a suit track is selected!");
			}
		} finally {
			this.readLock.unlock();
		}
	}

	public void empty() {
		for (Track selectedTrack : this.selectedTracks) {
			((SuitTrack) selectedTrack).clear();
			((SuitTrack) selectedTrack).clearRaw();
			selectedTrack.frameDelta = 0;
		}
	}

	public int nextPOI(Track track) {
		int selectionStartDiff = this.selectedFrames[0] - track.frameNo;
		if ((selectionStartDiff > 0) && (this.selectedFrames[0] > 0)
				&& (this.selectedFrames[0] < track.getFrameCount())) {
			return this.selectedFrames[0];
		}

		int selectionEndDiff = this.selectedFrames[1] - track.frameNo;
		if ((selectionEndDiff > 0) && (this.selectedFrames[1] > 0)
				&& (this.selectedFrames[1] < track.getFrameCount())) {
			return this.selectedFrames[1];
		}

		return (track.getFrameCount() - 1);
	}

	public int prevPOI(Track track) {
		int selectionEndDiff = this.selectedFrames[1] - track.frameNo;
		if (selectionEndDiff < 0) {
			return this.selectedFrames[1];
		}

		int selectionStartDiff = this.selectedFrames[0] - track.frameNo;
		if (selectionStartDiff < 0) {
			return this.selectedFrames[0];
		}

		return 0;
	}

	public void rewind(Track track) {
		switch (track.state) {
			case IDLE:
				this.singleStep = true;
				step(track, prevPOI(track) - track.frameNo);
	
				synchronized (this) {
					super.notifyAll();
				}
				break;
			default:
				break;
		}
	}

	public void fastForward(Track track) {
		switch (track.state) {
			case IDLE:
				this.singleStep = true;
				step(track, nextPOI(track) - track.frameNo);
	
				synchronized (this) {
					super.notifyAll();
				}
				break;
			default:
				break;
		}
	}

	public void step(Track track, int delta) {
		switch (track.state) {
			case IDLE:
				this.singleStep = true;
				track.frameDelta = delta;
				track.state = Track.State.PLAYING;
	
				synchronized (this) {
					super.notifyAll();
				}
				break;
			default:
				break;
		}
	}

	public void play(Track track) {
		switch (track.state) {
			case IDLE:
				this.singleStep = false;
				track.frameDelta = 1;
				track.state = Track.State.PLAYING;
	
				synchronized (this) {
					super.notifyAll();
				}
				System.out.println("replay started...");
				break;
			default:
				break;
		}
	}

	public void stop(Track track) {
		switch (track.state) {
			case RECORDING:
				track.state = Track.State.IDLE;
				break;
			default:
				break;
		}
	}

	public void toggleLoop() {
		this.loop = (!(this.loop));
	}

	public boolean isLooping() {
		return this.loop;
	}

	public void setRawFrame(RawFrame RF) {
		this.RF = RF;
		// TODO: the static shit here is really unstyle, it would be nice if this would work via the EventBus...
//		if (MainFrame.unityServerActive) {
//			MainFrame.unityServerConnector.sendFrame(this.RF.toShortString());
//		}
	}

	public void onTick(SuitConnection suit, DataFrame dataFrame) {
		this.readLock.lock();

		SuitTrack suitTrack = null;
		try {
			for (Track track : this.selectedTracks)
				if ((track instanceof SuitTrack)
						&& (((SuitTrack) track).getSuit().equals(suit)))
					suitTrack = (SuitTrack) track;
		} finally {
			this.readLock.unlock();
		}

		if (suitTrack == null) {
			return;
		}
		
//		KinematicModel actor = suitTrack.getActor();
//		
		if (suitTrack.state == Track.State.LIVE) {
			if (GlobalSettingsAndVariables.defaultDataManipulator != null) {
//				GlobalSettingsAndVariables.defaultDataManipulator.applyManiputlation(actor);
//				GlobalSettingsAndVariables.defaultDataManipulator.applyManiputlation(this.RF, dataFrame);
			}
		}
//		
//		this.kinematicFrame = new KinematicFrame(suitTrack.frameNo,
//				actor, dataFrame);
		
		this.kinematicFrame = new KinematicFrame(suitTrack.frameNo,
				suitTrack.getActor(), dataFrame);

		if (this.performWalkingAlgorithm) {
			suitTrack.getWalkingAlgorithm().walk(this.kinematicFrame.skeleton);
		} else {
			this.lowest = suitTrack.getWalkingAlgorithm().findContactJoint(
					suitTrack.getActor().getRoot());
			this.kinematicFrame.skeleton
					.getRoot()
					.getLocalTransform()
					.moveTo(0.0F, 0.0F,
							-this.lowest.getWorldTransform().getPosition().z())
					.update();
			this.kinematicFrame.skeleton.localToWorldTree();
		}
		
		if (suitTrack.state == Track.State.LIVE) {
			if (GlobalSettingsAndVariables.defaultDataManipulator != null) {
				GlobalSettingsAndVariables.defaultDataManipulator.applyManiputlation(this.kinematicFrame.skeleton);
			}
		}
		
		//TODO: unstyle
		if (MainFrame.unityServerActive) {
			MainFrame.unityServerConnector.sendFrame(this.kinematicFrame.skeleton.getQuaternionString(this.kinematicFrame.skeleton.getRoot()));
		}
		
		gestureInputProcessor.update(this.kinematicFrame);
		
		switch (suitTrack.state) {
			case IDLE:
			case PLAYING:
				break;
			case LIVE:
				suitTrack.frameNo += 1;
	
				for (Iterator<FrameBuffer> iter = this.frameBufferFactory.getFrameBuffers()
						.iterator(); iter.hasNext();) {
					FrameBuffer buffer = (FrameBuffer) iter.next();
					buffer.push(suitTrack, this.kinematicFrame);
				}
				break;
			case RECORDING:
				suitTrack.addFrame(dataFrame);
				if (this.RF.containsData)
					suitTrack.addRawFrame(this.RF);
				suitTrack.frameNo += 1;
	
				for (Iterator<FrameBuffer> iter = this.frameBufferFactory.getFrameBuffers()
						.iterator(); iter.hasNext();) {
					FrameBuffer buffer = (FrameBuffer) iter.next();
					buffer.push(suitTrack, this.kinematicFrame);
				}
				break;
		}
	}

	public void startKinematicTorrent(Track track,
			KinematicTorrent.Listener listener) {
		this.readLock.lock();
		try {
			this.kinematicTorrent = new KinematicTorrent(listener, track, this);
			this.kinematicTorrent.start();
		} finally {
			this.readLock.unlock();
		}
	}

	public void startRawTorrent(Track track, RawTorrent.Listener listener) {
		this.readLock.lock();
		try {
			this.rawTorrent = new RawTorrent(listener, (SuitTrack) track);
			this.rawTorrent.start();
		} finally {
			this.readLock.unlock();
		}
	}
}
