package streamservice.stream;

import kinematics.KinematicModel;
// FIXME: currently we are not using this interface...
public abstract interface StreamListener {
	public abstract void onPlayFrame(int paramInt,
			KinematicModel paramKinematicModel);

	public abstract void onRecordFrame(int paramInt,
			KinematicModel paramKinematicModel);

	public abstract void onLiveFrame(int paramInt,
			KinematicModel paramKinematicModel);
}
