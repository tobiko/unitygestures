package streamservice.stream.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import streamservice.tracks.Track;

public class TracksDestroyedEvent {
	public List<Track> tracks;

	public TracksDestroyedEvent() {
		super();
		this.tracks = new ArrayList<Track>();
	}

	public TracksDestroyedEvent(Track[] tracks) {
		this.tracks = Arrays.asList(tracks);
	}

	public TracksDestroyedEvent(List<Track> tracks) {
		this.tracks = tracks;
	}
}
