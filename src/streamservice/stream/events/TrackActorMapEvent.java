package streamservice.stream.events;

import kinematics.KinematicModel;
import streamservice.tracks.Track;

public class TrackActorMapEvent {
	public Track track;
	public KinematicModel actor;

	public TrackActorMapEvent(Track track, KinematicModel actor) {
		super();
		this.track = track;
		this.actor = actor;
	}
}
