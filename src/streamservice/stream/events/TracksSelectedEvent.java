package streamservice.stream.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import streamservice.tracks.Track;

public class TracksSelectedEvent {
	public List<Track> tracks;

	public TracksSelectedEvent() {
		super();
		this.tracks = new ArrayList<Track>();
	}

	public TracksSelectedEvent(Track[] tracks) {
		this.tracks = Arrays.asList(tracks);
	}

	public TracksSelectedEvent(List<Track> tracks) {
		this.tracks = tracks;
	}
}
