package streamservice.stream.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import streamservice.tracks.Track;

public class TracksCreatedEvent {
	public List<Track> tracks;

	public TracksCreatedEvent() {
		super();
		this.tracks = new ArrayList<Track>();
	}

	public TracksCreatedEvent(Track[] tracks) {
		this.tracks = Arrays.asList(tracks);
	}

	public TracksCreatedEvent(List<Track> tracks) {
		this.tracks = tracks;
	}
}
