package streamservice.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import eventhandling.EventBus;
import streamservice.stream.events.TracksDestroyedEvent;
import streamservice.tracks.Track;

public class CloseTrackAction extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7109327818169374009L;

	private Track track;

	public CloseTrackAction(Track track) {
		super();
		this.track = track;
	}

	public void actionPerformed(ActionEvent e) {
		EventBus.getInstance().removeAll(TracksDestroyedEvent.class);
		EventBus.getInstance().add(
				new TracksDestroyedEvent(new Track[] { this.track }));
	}
}