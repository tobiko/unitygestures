package streamservice.frames;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import com.spookengine.maths.Mat3;
import com.spookengine.maths.Vec3;

public class DataFrame {
	public final LocalOrWorld localOrWorld;
	protected Vec3 position = new Vec3();
	public Map<Integer, Mat3> rotations = new TreeMap<Integer, Mat3>();
	private boolean tagged = false;

	public DataFrame(LocalOrWorld low) {
		super();
		this.localOrWorld = low;
	}

	public DataFrame(LocalOrWorld low, Vec3 position) {
		this.localOrWorld = low;
		this.position.setTo(position);
	}

	public void setPosition(Vec3 position) {
		this.position.setTo(position);
	}

	public Vec3 getPosition() {
		return this.position;
	}

	public void addRotation(int index, Mat3 rotation) {
		this.rotations.put(Integer.valueOf(index), rotation);
	}

	public void tag() {
		this.tagged = true;
	}

	public void untag() {
		this.tagged = false;
	}

	public boolean isTagged() {
		return this.tagged;
	}

	public Mat3 getRotation(int index) {
		return ((Mat3) this.rotations.get(Integer.valueOf(index)));
	}

	public Collection<Mat3> getRotations() {
		return this.rotations.values();
	}

	public static enum LocalOrWorld {
		LOCAL, WORLD;
	}
}
