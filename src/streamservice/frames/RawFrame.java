package streamservice.frames;

import java.util.ArrayList;

public class RawFrame {
	private ArrayList<RawReading> readings;
	private StringBuilder sb;
	public boolean containsData = false;

	public RawFrame() {
		super();
		this.readings = new ArrayList<RawReading>();
	}

	public RawFrame(RawFrame frame) {
		this.readings = frame.readings;
	}

	public void fromPacket(byte[] packet, int sId, int sC, int fC) {
		this.readings.add(new RawReading(packet, sId, sC, fC));
		this.containsData = true;
	}

	public void addReading(RawReading reading) {
		this.readings.add(reading);
		this.containsData = true;
	}
	
	public RawReading getReadingForSensorIndex(int sensorIndex) {
		RawReading reading = null;
		for (RawReading cReading : this.readings) {
			if (cReading.sensorId == sensorIndex) {
				reading = cReading;
				break;
			}
		}
		
		return reading;
	}
	
	public String toString() {
		this.sb = new StringBuilder();
		for (RawReading reading : this.readings)
			this.sb.append(reading.toString());
		return this.sb.toString();
	}
	
	public String toShortString() {
		this.sb = new StringBuilder();
		for (RawReading reading : this.readings)
			this.sb.append(reading.toShortString() + "nn");
		return this.sb.toString();
	}
}
