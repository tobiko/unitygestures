package streamservice.frames;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import streamservice.tracks.Track;

public class FrameBuffer {
	private int capacity;
	private Map<Track, Integer> size;
	private Map<Track, LinkedList<KinematicFrame>> frames;

	public FrameBuffer(int capacity) {
		super();
		this.capacity = capacity;
		this.size = new HashMap<Track, Integer>();
		this.frames = new HashMap<Track, LinkedList<KinematicFrame>>();
	}

	public synchronized Set<Track> getTracks() {
		return this.frames.keySet();
	}

	public synchronized void removeTrack(Track track) {
		this.size.remove(track);
		this.frames.remove(track);
	}

	public synchronized void push(Track track, KinematicFrame frame) {
		if (!(this.frames.containsKey(track))) {
			this.frames.put(track, new LinkedList<KinematicFrame>());
			this.size.put(track, Integer.valueOf(0));
		}

		if (((Integer) this.size.get(track)).intValue() == this.capacity) {
			((LinkedList<KinematicFrame>) this.frames.get(track)).removeLast();
			((LinkedList<KinematicFrame>) this.frames.get(track)).push(frame);
		} else {
			this.size.put(track, Integer.valueOf(((Integer) this.size
					.get(track)).intValue() + 1));
			((LinkedList<KinematicFrame>) this.frames.get(track)).push(frame);
		}

		super.notifyAll();
	}

	public synchronized KinematicFrame pop(Track track, boolean wait) {
		if (wait)
			try {
				super.wait();
			} catch (InterruptedException ex) {
				System.err.println("interrupt in FramBuffer...");
				ex.printStackTrace();
			}

		if (this.size.containsKey(track)) {
			if (((Integer) this.size.get(track)).intValue() == 0) {
				return null;
			}
			this.size.put(track, Integer.valueOf(((Integer) this.size
					.get(track)).intValue() - 1));
			return ((KinematicFrame) ((LinkedList<KinematicFrame>) this.frames.get(track))
					.pop());
		}

		return null;
	}
}
