package streamservice.frames;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FrameBufferFactoryImplementation implements FrameBufferFactory {
	private Map<String, FrameBuffer> frameBuffers;

	public FrameBufferFactoryImplementation() {
		super();
		this.frameBuffers = new HashMap<String, FrameBuffer>();
	}

	public Collection<FrameBuffer> getFrameBuffers() {
		return this.frameBuffers.values();
	}

	public FrameBuffer getFrameBuffer(String name) {
		return ((FrameBuffer) this.frameBuffers.get(name));
	}

	public FrameBuffer createFrameBuffer(String name, int size) {
		if (this.frameBuffers.containsKey(name)) {
			return ((FrameBuffer) this.frameBuffers.get(name));
		}
		FrameBuffer frameBuffer = new FrameBuffer(size);
		this.frameBuffers.put(name, frameBuffer);

		return frameBuffer;
	}
}