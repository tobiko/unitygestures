package streamservice.frames;

import kinematics.Joint;
import kinematics.KinematicModel;

import com.spookengine.maths.Mat3;
import com.spookengine.maths.Vec3;
import com.spookengine.scenegraph.Node;

public class KinematicFrame {
	public final int frameNumber;
	public KinematicModel skeleton;
	private Node parent;
	private Vec3 axis;

	public KinematicFrame(int frameNumber, KinematicModel actor, DataFrame data) {
		super();
		this.frameNumber = frameNumber;
		this.skeleton = actor.cloneTree(null);

		switch (data.localOrWorld) {
		case LOCAL:
			doLocalTrfm(this.skeleton.getRoot(), data);
			break;
		case WORLD:
			doWorldTrfm(this.skeleton.getRoot(), data);
		}

		interpolate(this.skeleton.getRoot(), data);
	}

	private void doLocalTrfm(Joint joint, DataFrame data) {
		if (!(data.rotations.containsKey(Integer.valueOf(joint.sensorId)))) {
			joint.sensorId = -1;
		}

		this.parent = joint.getParent();
		Mat3 rotation;
		if (this.parent instanceof KinematicModel) {
			if (data.getPosition().length() > 0.0F) {
				joint.getLocalTransform().moveTo(data.getPosition());
			}
			rotation = data.getRotation(joint.sensorId);
			if (rotation != null) {
				joint.getLocalTransform().rotateTo(rotation);
			}

		} else {
			rotation = data.getRotation(joint.sensorId);
			if (rotation != null) {
				joint.getLocalTransform().rotateTo(rotation);
			}
		}
		joint.getLocalTransform().update();
		joint.localToWorld();

		for (Node child : joint.getChildren())
			doLocalTrfm((Joint) child, data);
	}

	private void doWorldTrfm(Joint joint, DataFrame data) {
		if (!(data.rotations.containsKey(Integer.valueOf(joint.sensorId))))
			joint.sensorId = -1;
		this.parent = joint.getParent();
		Mat3 rotation;
		if (this.parent instanceof KinematicModel) {
			Vec3 dataPos = data.getPosition();
			if (dataPos.length() > 0.0F) {
				joint.getLocalTransform().moveTo(data.getPosition());
			}
			rotation = data.getRotation(joint.sensorId);
			if (rotation != null)
				joint.getLocalTransform().rotateTo(rotation);
		} else {
			rotation = data.getRotation(joint.sensorId);
			if (rotation == null) {
				joint.getWorldTransform()
						.rotateTo(
								((Joint) this.parent).getWorldTransform()
										.getRotation());
			} else {
				joint.getWorldTransform().rotateTo(rotation);
				joint.getLocalTransform()
						.getRotation()
						.setTo(((Joint) this.parent).getWorldTransform()
								.getRotation()).invert();
				joint.getLocalTransform().getRotation().mult(rotation);
			}
		}
		joint.getLocalTransform().update();
		joint.localToWorld();

		for (Node child : joint.getChildren())
			doWorldTrfm((Joint) child, data);
	}

	private void interpolate(Joint joint, DataFrame data) {
		if (!(data.rotations.containsKey(Integer.valueOf(joint.sensorId)))) {
			joint.sensorId = -1;
		}
		this.parent = joint.getParent();

		if ((joint.interpolationFactor > 0.0F) && (joint.sensorId == -1)) {
			this.axis = new Vec3();
			float angle = ((Joint) this.parent).getLocalTransform()
					.getRotationAA(this.axis);

			joint.getLocalTransform()
					.rotateTo(angle * joint.interpolationFactor, this.axis)
					.update();
			joint.localToWorld();
		}
		float angle;
		if ((joint.extrapolationFactor > 0.0F)
				&& (((Joint) this.parent).sensorId == -1)) {
			this.axis = new Vec3();
			angle = joint.getLocalTransform().getRotationAA(this.axis);

			((Joint) this.parent).getLocalTransform()
					.rotateTo(angle * joint.extrapolationFactor, this.axis)
					.update();
			((Joint) this.parent).localToWorld();
		}

		for (Node child : joint.getChildren())
			if (child instanceof Joint)
				interpolate((Joint) child, data);
	}
}
