package streamservice.frames;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

public class RawReading {
	private DecimalFormat decimalFormat;// = new DecimalFormat("###0.00000")
	public int sensorId;
	public int sensorCount;
	public int frameCount;
	public int s;
	public float accelerationX;
	public float accelerationY;
	public float accelerationZ;
	public float angularRateX;
	public float angularRateY;
	public float angularRateZ;
	public float magneticX;
	public float magneticY;
	public float magneticZ;
	public float rawQuatX;
	public float rawQuatY;
	public float rawQuatZ;
	public float rawQuatW;
	public float qx;
	public float qy;
	public float qz;
	public float qw;
	public float qErrorX;
	public float qErrorY;
	public float qErrorZ;
	public float qErrorW;
	public float qAzimuthError;
	public float nMag;
	public float nMagZero;
	public float alpha;
	public float state;

	public RawReading(byte[] packet, int sId, int sC, int fC) {
		super();
		this.decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		this.decimalFormat.applyPattern("###0.00000");
		this.sensorId = sId;
		this.sensorCount = sC;
		this.frameCount = fC;
		this.s = (this.sensorCount * 104);

		this.accelerationX = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.accelerationY = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.accelerationZ = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.angularRateX = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.angularRateY = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.angularRateZ = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.magneticX = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.magneticY = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.magneticZ = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.rawQuatX = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.rawQuatY = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.rawQuatZ = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.rawQuatW = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.qx = bytesToFloat(Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qy = bytesToFloat(Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qz = bytesToFloat(Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qw = bytesToFloat(Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.qErrorX = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qErrorY = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qErrorZ = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.qErrorW = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);

		this.qAzimuthError = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.nMag = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.nMagZero = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.alpha = bytesToFloat(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
		this.state = (float) bytesToUint(
				Arrays.copyOfRange(packet, this.s, this.s += 4),
				ByteOrder.LITTLE_ENDIAN);
	}
	
	public RawReading(int sensorId, int sensorCount, int frameCount,
			float accelerationX, float accelerationY, float accelerationZ,
			float angularRateX, float angularRateY, float angularRateZ,
			float magneticX, float magneticY, float magneticZ, float rawQuatX,
			float rawQuatY, float rawQuatZ, float rawQuatW, float qx, float qy,
			float qz, float qw, float qErrorX, float qErrorY, float qErrorZ,
			float qErrorW, float qAzimuthError, float nMag, float nMagZero,
			float alpha, float state) {
		super();
		this.decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		this.decimalFormat.applyPattern("###0.00000");
		this.sensorId = sensorId;
		this.sensorCount = sensorCount;
		this.frameCount = frameCount;
		this.accelerationX = accelerationX;
		this.accelerationY = accelerationY;
		this.accelerationZ = accelerationZ;
		this.angularRateX = angularRateX;
		this.angularRateY = angularRateY;
		this.angularRateZ = angularRateZ;
		this.magneticX = magneticX;
		this.magneticY = magneticY;
		this.magneticZ = magneticZ;
		this.rawQuatX = rawQuatX;
		this.rawQuatY = rawQuatY;
		this.rawQuatZ = rawQuatZ;
		this.rawQuatW = rawQuatW;
		this.qx = qx;
		this.qy = qy;
		this.qz = qz;
		this.qw = qw;
		this.qErrorX = qErrorX;
		this.qErrorY = qErrorY;
		this.qErrorZ = qErrorZ;
		this.qErrorW = qErrorW;
		this.qAzimuthError = qAzimuthError;
		this.nMag = nMag;
		this.nMagZero = nMagZero;
		this.alpha = alpha;
		this.state = state;
	}



	public String toString() {
		return "" + this.sensorId + "\t" + this.frameCount + '\t'
				+ this.decimalFormat.format(this.accelerationX) + '\t'
				+ this.decimalFormat.format(this.accelerationY) + '\t'
				+ this.decimalFormat.format(this.accelerationZ) + '\t'
				+ this.decimalFormat.format(this.angularRateX) + '\t'
				+ this.decimalFormat.format(this.angularRateY) + '\t'
				+ this.decimalFormat.format(this.angularRateZ) + '\t'
				+ this.decimalFormat.format(this.magneticX) + '\t'
				+ this.decimalFormat.format(this.magneticY) + '\t'
				+ this.decimalFormat.format(this.magneticZ) + '\t'
				+ this.decimalFormat.format(this.rawQuatX) + '\t'
				+ this.decimalFormat.format(this.rawQuatY) + '\t'
				+ this.decimalFormat.format(this.rawQuatZ) + '\t'
				+ this.decimalFormat.format(this.rawQuatW) + '\t' + this.decimalFormat.format(this.qx)
				+ '\t' + this.decimalFormat.format(this.qy) + '\t' + this.decimalFormat.format(this.qz)
				+ '\t' + this.decimalFormat.format(this.qw) + '\t'
				+ this.decimalFormat.format(this.qErrorX) + '\t'
				+ this.decimalFormat.format(this.qErrorY) + '\t'
				+ this.decimalFormat.format(this.qErrorZ) + '\t'
				+ this.decimalFormat.format(this.qErrorW) + '\t'
				+ this.decimalFormat.format(this.qAzimuthError) + '\t'
				+ this.decimalFormat.format(this.nMag) + '\t'
				+ this.decimalFormat.format(this.nMagZero) + '\t'
				+ this.decimalFormat.format(this.alpha) + '\t' + this.decimalFormat.format(this.state)
				+ System.getProperty("line.separator");
	}
	
	public String toShortString() {
		return "" + this.sensorId
				+ "\t" + this.decimalFormat.format(this.qx)
				+ '\t' + this.decimalFormat.format(this.qy)
				+ '\t' + this.decimalFormat.format(this.qz)
				+ '\t' + this.decimalFormat.format(this.qw);
	}

	protected int bytesToInt(byte[] buf, ByteOrder endianness) {
		return ByteBuffer.wrap(buf).order(endianness).asIntBuffer().get();
	}

	protected long bytesToUint(byte[] buf, ByteOrder endianness) {
		return (0xFFFFFFFF & ByteBuffer.wrap(buf).order(endianness)
				.asIntBuffer().get());
	}

	protected float bytesToFloat(byte[] buf, ByteOrder endianess) {
		return ByteBuffer.wrap(buf).order(endianess).asFloatBuffer().get();
	}
}
