package streamservice.frames;

import java.util.Collection;

public abstract interface FrameBufferFactory {
	public abstract Collection<FrameBuffer> getFrameBuffers();

	public abstract FrameBuffer getFrameBuffer(String paramString);

	public abstract FrameBuffer createFrameBuffer(String paramString,
			int paramInt);
}
