package streamservice.torrents;

import streamservice.frames.RawFrame;
import streamservice.tracks.SuitTrack;

public class RawTorrent extends Thread {
	private Listener listener;
	private SuitTrack track;

	public RawTorrent(Listener listener, SuitTrack track) {
		super();
		this.listener = listener;
		this.track = track;
	}

	public void run() {
		this.listener.start(this.track.getRawFrameCount(), this.track.getFps());
		for (int i = 0; i < this.track.getRawFrameCount(); ++i)
			this.listener.next(new RawFrame(this.track.getRawFrame(i)));
		this.listener.finished();
	}

	public static abstract interface Listener {
		public abstract void start(int paramInt1, int paramInt2);

		public abstract void next(RawFrame paramRawFrame);

		public abstract void finished();
	}
}
