package streamservice.torrents;

import kinematics.Joint;
import streamservice.frames.KinematicFrame;
import streamservice.stream.StreamService;
import streamservice.tracks.Track;
import walkingalgorithm.WalkingAlgorithm;

public class KinematicTorrent extends Thread {
	private Listener listener;
	private Track track;
	private WalkingAlgorithm walkingAlgorithm;
	private StreamService playback;

	public KinematicTorrent(Listener listener, Track track, StreamService playback) {
		super();
		this.listener = listener;
		this.track = track;
		this.walkingAlgorithm = new WalkingAlgorithm();

		this.playback = playback;
	}

	public void run() {
		this.listener.start(this.track);
		for (int i = 0; i < this.track.getFrameCount(); ++i) {
			KinematicFrame frame = new KinematicFrame(i, this.track.getActor(),
					this.track.getFrame(i));

			if (this.playback.performWalkingAlgorithm) {
				this.walkingAlgorithm.walk(frame.skeleton);
			} else {
				Joint lowest = this.walkingAlgorithm
						.findContactJoint(this.track.getActor().getRoot());
				frame.skeleton
						.getRoot()
						.getLocalTransform()
						.moveTo(0.0F, 0.0F,
								-lowest.getWorldTransform().getPosition().z())
						.update();
				frame.skeleton.localToWorldTree();
			}

			this.listener.next(this.track, frame);
		}

		this.listener.finished(this.track);
	}

	public static abstract interface Listener {
		public abstract void start(Track paramTrack);

		public abstract void next(Track paramTrack,
				KinematicFrame paramKinematicFrame);

		public abstract void finished(Track paramTrack);
	}
}