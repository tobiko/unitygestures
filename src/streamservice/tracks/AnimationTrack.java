package streamservice.tracks;

import kinematics.KinematicModel;

public class AnimationTrack extends Track {
	private int fps;

	public AnimationTrack(String name, KinematicModel actor, int fps) {
		super(name, actor);
		this.fps = fps;
	}

	public int getFps() {
		return this.fps;
	}
}
