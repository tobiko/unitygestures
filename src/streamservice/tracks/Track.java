package streamservice.tracks;

import java.util.ArrayList;
import java.util.List;

import streamservice.frames.DataFrame;
import kinematics.KinematicModel;

public abstract class Track {
	public static enum State {
		IDLE, LIVE, PLAYING, RECORDING;
	}
	
	public State state = State.IDLE;
	public boolean isSelected;
	protected String name;
	protected KinematicModel actor;
	public int frameNo;
	public int frameDelta;
	private List<DataFrame> frames;
	private List<DataFrame> clonedFrames;

	public Track(String name, KinematicModel actor) {
		super();
		this.name = name;
		this.actor = actor;

		this.frames       = new ArrayList<DataFrame>();
		this.clonedFrames = new ArrayList<DataFrame>();
	}

	public String getName() {
		return this.name;
	}

	public void setActor(KinematicModel actor) {
		this.actor = actor;
	}

	public KinematicModel getActor() {
		return this.actor;
	}

	public abstract int getFps();

	public int getFrameCount() {
		return this.frames.size();
	}

	public void setFrame(int i, DataFrame frame) {
		this.frames.set(i, frame);
	}

	public void addFrame(DataFrame frame) {
		this.frames.add(frame);
	}

	public void cropTagged(int from, int to) {
		this.clonedFrames = this.frames;
		this.frames = this.frames.subList(from, to);
	}

	public void toNormal() {
		this.frames = this.clonedFrames;
	}

	public DataFrame getFrame(int i) {
		return ((DataFrame) this.frames.get(i));
	}

	public List<DataFrame> getFrames() {
		return this.frames;
	}

	public void clear() {
		this.frames = new ArrayList<DataFrame>();
		this.frameNo = 0;
		System.gc();
	}

	public String toString() {
		return this.name;
	}

	public void cropFrames(int from, int to) {
		if ((this.frames.size() > 0) && (from < this.frames.size())) {
			if (to > this.frames.size())
				to = this.frames.size() - 1;
			for (int i = from; i < to; ++i)
				this.frames.remove(from);
		}
	}

	public void removeTags(int from, int to) {
		if ((this.frames.size() > 0) && (from < this.frames.size())) {
			if (to > this.frames.size())
				to = this.frames.size() - 1;
			for (int i = from; i < to; ++i)
				((DataFrame) this.frames.get(i)).untag();
		}
	}
	
	public static void main(String[] args) {
		for (State state : State.values()) {
			System.out.println(state + " : " + state.ordinal());
		}
	}
}
