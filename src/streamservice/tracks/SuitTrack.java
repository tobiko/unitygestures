package streamservice.tracks;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;
import kinematics.KinematicModel;
import streamservice.frames.RawFrame;
import walkingalgorithm.WalkingAlgorithm;

public class SuitTrack extends Track {
	
	public boolean unsavedRecording;
	private SuitConnection suit;
	private WalkingAlgorithm walkingAlgorithm;
	private List<RawFrame> rawFrames;
	private List<RawFrame> clonedRawFrames;

	public SuitTrack(String name, KinematicModel actor, SuitConnection suit) {
		super(name, actor);

		this.unsavedRecording = false;
		this.suit = suit;
		this.walkingAlgorithm = new WalkingAlgorithm();

		this.rawFrames       = new ArrayList<RawFrame>();
		this.clonedRawFrames = new ArrayList<RawFrame>();
	}

	public WalkingAlgorithm getWalkingAlgorithm() {
		return this.walkingAlgorithm;
	}

	public void setSuit(SuitConnection suit) {
		this.suit = suit;
	}

	public SuitConnection getSuit() {
		return this.suit;
	}

	public int getFps() {
		return this.suit.getFps();
	}

	public void clearRaw() {
		this.rawFrames = new ArrayList<RawFrame>();
		System.gc();
	}

	public int getRawFrameCount() {
		return this.rawFrames.size();
	}

	public void setRawFrame(int i, RawFrame frame) {
		this.rawFrames.set(i, frame);
	}

	public void addRawFrame(RawFrame frame) {
		this.rawFrames.add(frame);
	}

	public RawFrame getRawFrame(int i) {
		return ((RawFrame) this.rawFrames.get(i));
	}

	public List<RawFrame> getRawFrames() {
		return this.rawFrames;
	}

	public void cropRawTagged(int from, int to) {
		super.cropTagged(from, to);
		this.clonedRawFrames = this.rawFrames;
		this.rawFrames = this.rawFrames.subList(from, to);
	}

	public void toRawNormal() {
		super.toNormal();
		this.rawFrames = this.clonedRawFrames;
	}

	public void cropRawFrames(int from, int to) {
		if ((this.rawFrames.size() > 0) && (from < this.rawFrames.size())) {
			if (to > this.rawFrames.size())
				to = this.rawFrames.size() - 1;
			for (int i = from; i < to; ++i)
				this.rawFrames.remove(from);
		}
	}
}