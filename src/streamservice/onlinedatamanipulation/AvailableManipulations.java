package streamservice.onlinedatamanipulation;

public class AvailableManipulations {

	// would be cooler if this would be done via reflection...
	public static String[] availableManipulators = new String[] {
		"NONE",
		WristDrift.name,
		ExtWristDrift.name
	};
	
	public static String[] availableManipulatorLabels = new String[] {
		"none",
		WristDrift.label,
		ExtWristDrift.label
	};
	
	public static String[] availableManipulatorDescriptions = new String[] {
		"no manipulation",
		WristDrift.description,
		ExtWristDrift.description
	};
	
	public static IDataManipulator getManipulatorInstanceByName(String name) {
		if (name.equals("NONE")) {
			return null;
		} else if (name.equals(WristDrift.name)) {
			return new WristDrift();
		} else if (name.equals(ExtWristDrift.name)) {
			return new ExtWristDrift();
		}
		
		return null;
	}
	
}
