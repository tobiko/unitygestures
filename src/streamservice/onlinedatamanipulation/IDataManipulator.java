package streamservice.onlinedatamanipulation;

import scene.Scene;
import streamservice.frames.DataFrame;
import streamservice.frames.RawFrame;
import kinematics.KinematicModel;

public interface IDataManipulator {

//	public void applyManiputlation(KinematicFrame frame);
	public void applyManiputlation(KinematicModel actor);
	
	public void applyManiputlation(RawFrame rawFrame, DataFrame dataFrame);
	
	public void setScene(Scene scene);
	
	public void shutDown();
	
	public String getDescription();
	
	public String getName();
	
	public String getLabel();
	
}
