package streamservice.onlinedatamanipulation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import kinematics.Joint;
import kinematics.KinematicModel;
import scene.Scene;
import streamservice.frames.DataFrame;
import streamservice.frames.RawFrame;
import streamservice.frames.RawReading;

import com.jogamp.graph.math.Quaternion;
import com.spookengine.core.Geom;
import com.spookengine.core.Trimesh;
import com.spookengine.core.Trimesh.DrawMode;
import com.spookengine.maths.Euler;
import com.spookengine.maths.Mat3;
import com.spookengine.maths.Vec2;
import com.spookengine.maths.Vec3;
import com.spookengine.scenegraph.Node;
import com.spookengine.scenegraph.Spatial;
import com.spookengine.scenegraph.Visual;
import com.spookengine.scenegraph.appearance.App;
import com.spookengine.scenegraph.appearance.Colour;
import com.spookengine.scenegraph.appearance.LineAtt;

public class ExtWristDriftBCK290914 implements IDataManipulator {

	public final static String name = "EXT_WRIST_DRIFT";
	
	public final static String label = "wrist drift (relative to each other)";
	
	public final static String description = "Applies a drift to the left and right wrist towards each other on their current axes."
			+ "Compared to the simplified version, the drift is calculated within the local frame of reference.";
	
	public final static float defaultDistance = 16.0f;
	
	public static double MAX_DRIFT = Math.toRadians(30);//Math.PI * .5d;
	
	public static float DEFAULT_INCREMENT = 0.02f;//.1f;
	
	private static class WristDriftEditorDialog extends JDialog implements ChangeListener {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8638552271982103793L;

		private static final double UPPER_DRIFT_BOUND = 120.0d;
		
		private static final double UPPER_INCREMENT_BOUND = .5d;
		
		private JSpinner incrementSpinner;
		
		private JSpinner maxDriftSpinner;
		
		private WristDriftEditorDialog() {
			super();
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
			this.setTitle("Edit Wrist Drift Parameter");
			this.setAlwaysOnTop(true);
			this.setupContentPane();
			
			Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			Point upperLeft = new Point(0, 0);
			
			this.pack();
			Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
					+ d.height / 2);
			this.setLocation(new Point(center.x - this.getWidth() / 2, center.y
					- this.getHeight() / 2));
			
			this.setVisible(true);
		}
		/**
		 * builds the content pane of the dialog and places components.
		 */
		private void setupContentPane() {
			SpinnerModel incrementModel = new SpinnerNumberModel(ExtWristDriftBCK290914.DEFAULT_INCREMENT, 0.0d, ExtWristDriftBCK290914.WristDriftEditorDialog.UPPER_INCREMENT_BOUND, .01d);
			this.incrementSpinner = new JSpinner(incrementModel);
			this.incrementSpinner.addChangeListener(this);
			
			JLabel incrementLabel = new JLabel("drift increment:");
			
			SpinnerModel driftModel = new SpinnerNumberModel(Math.toDegrees(ExtWristDriftBCK290914.MAX_DRIFT), 0.0d, ExtWristDriftBCK290914.WristDriftEditorDialog.UPPER_DRIFT_BOUND, 1.0d);
			this.maxDriftSpinner = new JSpinner(driftModel);
			this.maxDriftSpinner.addChangeListener(this);
			
			JLabel driftLabel = new JLabel("max drift in [ang. deg.]:");
			
			JPanel contentPanel = new JPanel();
			GroupLayout mainLayout = new GroupLayout(contentPanel);
			
			mainLayout.setAutoCreateContainerGaps(true);
			mainLayout.setAutoCreateGaps(true);
			
			mainLayout.linkSize(incrementLabel, driftLabel, this.incrementSpinner, this.maxDriftSpinner);
			
			mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
				.addGroup(mainLayout.createSequentialGroup()
					.addComponent(incrementLabel)
					.addComponent(this.incrementSpinner)
				)
				.addGroup(mainLayout.createSequentialGroup()
					.addComponent(driftLabel)
					.addComponent(this.maxDriftSpinner)
				)
			);
			
			mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
				.addGroup(mainLayout.createParallelGroup()
					.addComponent(incrementLabel)
					.addComponent(this.incrementSpinner)
				)
				.addGroup(mainLayout.createParallelGroup()
					.addComponent(driftLabel)
					.addComponent(this.maxDriftSpinner)
				)
			);
			
			contentPanel.setLayout(mainLayout);
			contentPanel.setBorder(BorderFactory.createTitledBorder("Editing Options"));
			this.setContentPane(contentPanel);
		}

		@Override
		public void stateChanged(ChangeEvent ce) {
			if (ce.getSource().equals(this.maxDriftSpinner)) {
				double drift = Math.toRadians(((SpinnerNumberModel) this.maxDriftSpinner.getModel()).getNumber().doubleValue());
				ExtWristDriftBCK290914.MAX_DRIFT = drift;
			} else if (ce.getSource().equals(this.incrementSpinner)) {
				float increment = ((SpinnerNumberModel) this.incrementSpinner.getModel()).getNumber().floatValue();
				ExtWristDriftBCK290914.DEFAULT_INCREMENT = increment;
			}
		}
		
	}
	
	private float leftXDrift;
	
	private float rightXDrift;
	
	private float increment;
	
	private Scene scene;
	
//	private boolean started;
	private boolean useTimeConstraints;
	private JProgressBar progBar;
	private long refTime;
	private long timeToRun;
	private long delay;
	private float cubeWidth;
	
//	private float initialLeftOrientation
	
	public ExtWristDriftBCK290914() {
		super();
		this.leftXDrift = 0.0f;
		this.rightXDrift = 0.0f;
		this.increment = ExtWristDriftBCK290914.DEFAULT_INCREMENT;
//		this.started = false;
		this.useTimeConstraints = false;
		this.cubeWidth = 5.0f;
	}
	
	public void initTimedControl(JProgressBar progBar, long timeToRun, long delay) {
		this.progBar = progBar;
		this.timeToRun = timeToRun;
		this.delay = delay;
		this.useTimeConstraints = true;
		this.refTime = System.currentTimeMillis();
	}
	
//	private void getInitialOrientations(KinematicModel actor) {
//		Joint hip = (Joint) actor.getRoot().findChild("Hips");
//		Vec3 hipPosition = hip.getLocalTransform().getPosition();
//		Vec3 axis = new Vec3(hipPosition.x(), hipPosition.y(), hipPosition.z());
//		axis = axis.cross(1, 1, 1.0f).norm();
//		Joint leftForeArm = (Joint) actor.getRoot().findChild("LeftForeArm");
//		leftForeArm.getLocalTransform().getRotationAA(axis);
//		
//		axis = new Vec3(hipPosition.x(), hipPosition.y(), -hipPosition.z());
//		axis = axis.cross(1, 1, -1.0f).norm();
//		axis = new Vec3(-axis.x(), axis.y(), axis.z());
//		
//		Joint rightForeArm = (Joint) actor.getRoot().findChild("RightForeArm");
//		rightForeArm.getLocalTransform().getRotationAA(axis);
//	}

	private boolean checkTiming() {
		long cTime = System.currentTimeMillis();
		long diff = cTime - this.refTime;
		
		double ratio = diff / ((double) this.timeToRun);
		if (this.progBar != null) {
			int progress = (int) Math.rint(ratio * 100.0d);
			this.progBar.setValue(progress < 100 ? progress : 100);
		}
		
		if (diff > this.delay) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void applyManiputlation(KinematicModel actor) {
//		if (!this.started) {
//			
//			this.started = true;
//		}
		boolean increment = true;
		if (this.useTimeConstraints) {
			increment = this.checkTiming();
		}
		if (Math.abs(Math.toRadians(this.leftXDrift)) >= ExtWristDriftBCK290914.MAX_DRIFT) {
			increment = false;
		}
		
		// XXX: not used
		Joint hip = (Joint) actor.getRoot().findChild("Hips");
		Vec3 hipPosition = hip.getLocalTransform().getPosition();
		Vec3 axis = new Vec3(hipPosition.x(), hipPosition.y(), hipPosition.z());
		axis = axis.cross(1, 1, 1.0f).norm();
//		System.out.println(axis.x() + " : " + axis.y() + " : " + axis.z());
		Joint leftForeArm = (Joint) actor.getRoot().findChild("LeftForeArm");
		float iAngle = leftForeArm.getLocalTransform().getRotationAA(axis);
//		leftForeArm.getLocalTransform().rotateTo((float) Math.toRadians(this.leftXDrift), axis).update();
		leftForeArm.getLocalTransform().rotateTo(iAngle + (float) Math.toRadians(this.leftXDrift), axis).update();
		if (increment) {
			this.leftXDrift += this.increment;
//			leftForeArm.getLocalTransform().rotateTo(iAngle + (float) Math.toRadians(this.leftXDrift), axis).update();
		}
		
		axis = new Vec3(hipPosition.x(), hipPosition.y(), -hipPosition.z());
		axis = axis.cross(1, 1, -1.0f).norm();
		axis = new Vec3(-axis.x(), axis.y(), axis.z());
		
		Joint rightForeArm = (Joint) actor.getRoot().findChild("RightForeArm");
		iAngle = rightForeArm.getLocalTransform().getRotationAA(axis);
//		rightForeArm.getLocalTransform().rotateTo((float) Math.toRadians(this.rightXDrift), axis).update();
		rightForeArm.getLocalTransform().rotateTo(iAngle + (float) Math.toRadians(this.rightXDrift), axis).update();
		if (increment) {
			this.rightXDrift += this.increment;
//			rightForeArm.getLocalTransform().rotateTo(iAngle + (float) Math.toRadians(this.rightXDrift), axis).update();
		}
		
		if (this.scene != null) {
			Visual root = this.scene.getRoot();
			Spatial rightHand = (Spatial) root.findChild("RightHand");
			Spatial leftHand = (Spatial) root.findChild("LeftHand");
			if (rightHand != null && leftHand != null) {
//				float distance = Math.abs(leftHand.getWorldTransform()
//						.getPosition()
//						.dist(rightHand.getWorldTransform().getPosition())
//						- ExtWristDrift.defaultDistance)
//						/ (ExtWristDrift.defaultDistance * .75f);
				float distance = Math.abs((leftHand.getWorldTransform()
						.getPosition()
						.dist(rightHand.getWorldTransform().getPosition())
						- ExtWristDriftBCK290914.defaultDistance))
						/ (ExtWristDriftBCK290914.defaultDistance * .75f);
				float sign = distance > ExtWristDriftBCK290914.defaultDistance ? -1.0f : 1.0f;
				distance = distance > 1.0f ? 1.0f : distance;
				float hue = (120.0f - 120.0f * distance) / 360.0f;
				Color color = new Color(Color.HSBtoRGB(hue, 1.0f, 1.0f));
//				System.out.println("distance: " + leftHand.getWorldTransform().getPosition().dist(rightHand.getWorldTransform().getPosition()));
				Node distanceIndicator = root.findChild("distance_indicator");
				if (distanceIndicator != null) {
					root.detachChild(distanceIndicator);
					distanceIndicator = null;
				}
				
				Geom geom = new Geom("distance_indicator", Trimesh.Line(60,
						Trimesh.DrawMode.LINES, new Vec3[] {
								leftHand.getWorldTransform().getPosition(),
								rightHand.getWorldTransform().getPosition() }));
				geom.getLocalAppearance().override(App.COLOUR);
				geom.getLocalAppearance().override(App.LINE_ATT);
				geom.getLocalAppearance().colour = new Colour(
						color.getRed() / 255.0f, color.getGreen() / 255.0f,
						color.getBlue() / 255.0f);
				geom.getLocalAppearance().lineAtt = new LineAtt();
				geom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(geom);
				
				Node distanceDirectionIndicatorLeft = root.findChild("distance_direction_indicator_l");
				if (distanceDirectionIndicatorLeft != null) {
					root.detachChild(distanceDirectionIndicatorLeft);
					distanceDirectionIndicatorLeft = null;
				}
				
				axis = new Vec3(leftHand.getWorldTransform().getPosition()).cross(rightHand.getWorldTransform().getPosition());
				double axisAngle = Math.acos(
						(new Vec3(leftHand.getWorldTransform().getPosition()).dot(rightHand.getWorldTransform().getPosition())) /
						(leftHand.getWorldTransform().getPosition().length() * rightHand.getWorldTransform().getPosition().length()));
				
				Euler angle = this.fromAxisAngle(axis, axisAngle);
				Geom lGeom = new Geom("distance_direction_indicator_l", this.generateTriangle(5.0f * sign, 5.0f));
				lGeom.getLocalTransform().moveTo(leftHand.getWorldTransform().getPosition()).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
				lGeom.getLocalAppearance().override(App.COLOUR);
				lGeom.getLocalAppearance().override(App.LINE_ATT);
				lGeom.getLocalAppearance().colour = new Colour(
						color.getRed() / 255.0f, color.getGreen() / 255.0f,
						color.getBlue() / 255.0f);
				lGeom.getLocalAppearance().lineAtt = new LineAtt();
				lGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(lGeom);
				
				Node distanceDirectionIndicatorRight = root.findChild("distance_direction_indicator_r");
				if (distanceDirectionIndicatorRight != null) {
					root.detachChild(distanceDirectionIndicatorRight);
					distanceDirectionIndicatorRight = null;
				}
				
				axis = new Vec3(leftHand.getWorldTransform().getPosition()).cross(rightHand.getWorldTransform().getPosition());
				axisAngle = Math.acos(
						(new Vec3(leftHand.getWorldTransform().getPosition()).dot(rightHand.getWorldTransform().getPosition())) /
						(leftHand.getWorldTransform().getPosition().length() * rightHand.getWorldTransform().getPosition().length()));
				
				angle = this.fromAxisAngle(axis, axisAngle);
				Geom rGeom = new Geom("distance_direction_indicator_r", this.generateTriangle(5.0f * -sign, 5.0f));
				rGeom.getLocalTransform().moveTo(rightHand.getWorldTransform().getPosition()).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
				rGeom.getLocalAppearance().override(App.COLOUR);
				rGeom.getLocalAppearance().override(App.LINE_ATT);
				rGeom.getLocalAppearance().colour = new Colour(
						color.getRed() / 255.0f, color.getGreen() / 255.0f,
						color.getBlue() / 255.0f);
				rGeom.getLocalAppearance().lineAtt = new LineAtt();
				rGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(rGeom);
				
				//distance indicator
				Vec3 lPos = new Vec3(leftHand.getWorldTransform().getPosition());
				Vec3 rPos = new Vec3(rightHand.getWorldTransform().getPosition());
				Vec3 cVec = new Vec3(lPos.x() - rPos.x(), lPos.y() - rPos.y(), lPos.z() - rPos.z());
				Vec3 dVec = rPos.add(cVec.x() * .5f, cVec.y() * .5f, cVec.z() * .5f);
//				Vec3 rVec = new Vec3(rightHand.getWorldTransform().getPosition()).add(cVec.x() * 1.5f, cVec.y() * 1.5f, cVec.z() * 1.5f);
//				Vec3 lVec = new Vec3(leftHand.getWorldTransform().getPosition()).add(cVec.x() * -1.5f, cVec.y() * -1.5f, cVec.z() * -1.5f);
				Vec3 rVec = new Vec3(rightHand.getWorldTransform().getPosition()).add(cVec.x() * 1.5f, cVec.y() * 1.5f, cVec.z() * 1.5f);
				Vec3 lVec = new Vec3(leftHand.getWorldTransform().getPosition()).add(cVec.x() * -1.5f, cVec.y() * -1.5f, cVec.z() * -1.5f);
				
				Node growingDistanceIndicator = root.findChild("distance_cube_c");
				if (growingDistanceIndicator != null) {
					root.detachChild(growingDistanceIndicator);
					growingDistanceIndicator = null;
				}
				Geom dGeom = new Geom("distance_cube_c", this.generateBox(this.cubeWidth));
				dGeom.getLocalTransform().moveTo(dVec).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
				dGeom.getLocalAppearance().override(App.COLOUR);
				dGeom.getLocalAppearance().override(App.LINE_ATT);
				dGeom.getLocalAppearance().colour = new Colour(
						15.0f / 255.0f, 15.0f / 255.0f,
						205.0f / 255.0f);
				dGeom.getLocalAppearance().lineAtt = new LineAtt();
				dGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(dGeom);
				
				growingDistanceIndicator = root.findChild("distance_cube_r");
				if (growingDistanceIndicator != null) {
					root.detachChild(growingDistanceIndicator);
					growingDistanceIndicator = null;
				}
//				Geom rdGeom = new Geom("distance_cube_r", this.generateBox(this.cubeWidth));
//				rdGeom.getLocalTransform().moveTo(rVec).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
//				rdGeom.getLocalAppearance().override(App.COLOUR);
//				rdGeom.getLocalAppearance().override(App.LINE_ATT);
//				rdGeom.getLocalAppearance().colour = new Colour(
//						15.0f / 255.0f, 15.0f / 255.0f,
//						205.0f / 255.0f);
//				rdGeom.getLocalAppearance().lineAtt = new LineAtt();
//				rdGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
//				root.attachChild(rdGeom);
				Geom rdGeom = new Geom("distance_cube_r", Trimesh.Ellipse(ExtWristDriftBCK290914.defaultDistance, ExtWristDriftBCK290914.defaultDistance, 30, false));
				rdGeom.getLocalTransform().moveTo(dVec).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
				rdGeom.getLocalAppearance().override(App.COLOUR);
				rdGeom.getLocalAppearance().override(App.LINE_ATT);
				rdGeom.getLocalAppearance().colour = new Colour(
						15.0f / 255.0f, 15.0f / 255.0f,
						205.0f / 255.0f);
				rdGeom.getLocalAppearance().lineAtt = new LineAtt();
				rdGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(rdGeom);
				
				growingDistanceIndicator = root.findChild("distance_cube_l");
				if (growingDistanceIndicator != null) {
					root.detachChild(growingDistanceIndicator);
					growingDistanceIndicator = null;
				}
				Geom ldGeom = new Geom("distance_cube_l", this.generateBox(this.cubeWidth));
				ldGeom.getLocalTransform().moveTo(lVec).rotateToRPY(0.0f, angle.pitch, angle.yaw).update();
				ldGeom.getLocalAppearance().override(App.COLOUR);
				ldGeom.getLocalAppearance().override(App.LINE_ATT);
				ldGeom.getLocalAppearance().colour = new Colour(
						15.0f / 255.0f, 15.0f / 255.0f,
						205.0f / 255.0f);
				ldGeom.getLocalAppearance().lineAtt = new LineAtt();
				ldGeom.getLocalAppearance().lineAtt.setLineWidth(5.0f);
				root.attachChild(ldGeom);
				
				this.cubeWidth += this.cubeWidth < ExtWristDriftBCK290914.defaultDistance ? this.increment * .1f : 0.0f;
			}
		}
	}
	
//	private Trimesh generateTriangle(float width, float height) {
//		float halfWidth = width / 2.0F;
//		float halfHeight = height / 2.0F;
//
//		Vec3[] vertexArray = { new Vec3(1.0F * halfWidth, 0.0F, 1.0F * halfHeight),
//				new Vec3(-1.0F * halfWidth, 0.0F, 0.0F),
//				new Vec3(1.0F * halfWidth, 0.0F, -1.0F * halfHeight)};
//
//		Vec3[] normalArray = { new Vec3(0.0F, -1.0F, 0.0F),
//				new Vec3(0.0F, -1.0F, 0.0F), new Vec3(0.0F, -1.0F, 0.0F) };
//
//		Vec2[] texCoordArray = { new Vec2(1.0f / 2.0F, 0.0F),
//				new Vec2(0.0F, 1.0f), new Vec2(1.0f, 1.0f) };
//
//		Trimesh triangle = new Trimesh(DrawMode.TRIANGLES, vertexArray, normalArray);
//		triangle.addTexCoords(texCoordArray);
//		return triangle;
//	}
	
	private Trimesh generateTriangle(float width, float height) {
		float halfWidth = width / 2.0F;
		float halfHeight = height / 2.0F;

		Vec3[] vertexArray = { new Vec3(1.0F * halfWidth, 0.0F, 1.0F * halfHeight),
				new Vec3(-1.0F * halfWidth, 0.0F, 0.0F),
				new Vec3(1.0F * halfWidth, 0.0F, -1.0F * halfHeight),
				new Vec3(1.0F * halfWidth,  1.0F * halfHeight, 0.0F),
				new Vec3(-1.0F * halfWidth, 0.0F, 0.0F),
				new Vec3(1.0F * halfWidth, -1.0F * halfHeight, 0.0F),
				new Vec3(1.0F * halfWidth, 0.0F, -1.0F * halfHeight) };

		Vec3[] normalArray = { new Vec3(0.0F, -1.0F, 0.0F),
				new Vec3(0.0F, -1.0F, 0.0F), new Vec3(0.0F, -1.0F, 0.0F) };

		Vec2[] texCoordArray = { new Vec2(1.0f / 2.0F, 0.0F),
				new Vec2(0.0F, 1.0f), new Vec2(1.0f, 1.0f) };

		Trimesh triangle = new Trimesh(DrawMode.TRIANGLE_FAN, vertexArray, normalArray);
		triangle.addTexCoords(texCoordArray);
		return triangle;
	}
	
	private Trimesh generateBox(float width) {
		return Trimesh.Cube(width, width, width);
	}
	
	private Euler fromAxisAngle(Vec3 axis, double angle) {
		float heading = (float) Math.atan2(
				axis.y() * Math.sin(angle) - axis.x() * axis.z()
						* (1.0d - Math.cos(angle)), 1.0d
						- (axis.y() * axis.y() + axis.z() * axis.z())
						* (1.0d - Math.cos(angle)));
		float attitude = (float) Math.asin(axis.x() * axis.y()
				* (1.0d - Math.cos(angle)) + axis.z() * Math.sin(angle));
		float bank = (float) Math.atan2(axis.x() * Math.sin(angle) - axis.y()
				* axis.z() * (1.0d - Math.cos(angle)),
				1.0d - (axis.x() * axis.x() + axis.z() * axis.z())
						* (1.0d - Math.cos(angle)));
		return new Euler(heading, attitude, bank);
	}

	public void applyManiputlation(RawFrame rawFrame, DataFrame dataFrame) {
		int leftForeArmIndex = 23;
		int rightForeArmIndex = 26;
		int hipIndex = 31;
		
		RawReading leftForeArm = rawFrame.getReadingForSensorIndex(leftForeArmIndex);
		if (leftForeArm == null) {
			leftForeArm = new RawReading(leftForeArmIndex, 0,
					0, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		}
		RawReading rightForeArm = rawFrame.getReadingForSensorIndex(rightForeArmIndex);
		if (rightForeArm == null) {
			rightForeArm = new RawReading(rightForeArmIndex, 0,
					0, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		}
		RawReading hipReading = rawFrame.getReadingForSensorIndex(hipIndex);
		if (hipReading == null) {
			hipReading = new RawReading(hipIndex, 0,
					0, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		}
		
		Quaternion qLeft  = new Quaternion(leftForeArm.qx, leftForeArm.qy, leftForeArm.qz, leftForeArm.qw);
		qLeft.inverse();
		Quaternion qRight = new Quaternion(rightForeArm.qx, rightForeArm.qy, rightForeArm.qz, rightForeArm.qw);
//		qRight.inverse();
		Quaternion qHip   = new Quaternion(hipReading.qx, hipReading.qy, hipReading.qz, hipReading.qw);
		
		qHip.mult(qLeft);
		
		float qx = qHip.getX();
		float qy = qHip.getY();
		float qz = qHip.getZ();
		float qw = qHip.getW();
		
		dataFrame.addRotation(leftForeArmIndex, new Mat3(1.0F
				- (2.0F * qy * qy)
				- (2.0F * qz * qz), 2.0F
				* qx * qy
				- (2.0F * qz * qw), 2.0F
				* qx * qz + 2.0F * qy
				* qw, 2.0F * qx * qy
				+ 2.0F * qz * qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qz * qz), 2.0F
				* qy * qz
				- (2.0F * qx * qw), 2.0F
				* qx * qz
				- (2.0F * qy * qw), 2.0F
				* qy * qz + 2.0F * qx
				* qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qy * qy)));
		
		qHip = new Quaternion(hipReading.qx, hipReading.qy, hipReading.qz, hipReading.qw);
		qHip.inverse();
		qRight.mult(qHip);
		
		qx = qRight.getX();
		qy = qRight.getY();
		qz = qRight.getZ();
		qw = qRight.getW();
		
		dataFrame.addRotation(rightForeArmIndex, new Mat3(1.0F
				- (2.0F * qy * qy)
				- (2.0F * qz * qz), 2.0F
				* qx * qy
				- (2.0F * qz * qw), 2.0F
				* qx * qz + 2.0F * qy
				* qw, 2.0F * qx * qy
				+ 2.0F * qz * qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qz * qz), 2.0F
				* qy * qz
				- (2.0F * qx * qw), 2.0F
				* qx * qz
				- (2.0F * qy * qw), 2.0F
				* qy * qz + 2.0F * qx
				* qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qy * qy)));
	}
	
	@Override
	public void setScene(Scene scene) {
		this.scene = scene;
	}
	
	@Override
	public void shutDown() {
		// remove the marker
		if (this.scene != null) {
			Visual root = this.scene.getRoot();
			Node distanceIndicator = root.findChild("distance_indicator");
			if (distanceIndicator != null) {
				root.detachChild(distanceIndicator);
				System.out.println("detach child...");
				distanceIndicator = null;
			}
			Node distanceDirectionIndicatorLeft = root.findChild("distance_direction_indicator_l");
			if (distanceDirectionIndicatorLeft != null) {
				root.detachChild(distanceDirectionIndicatorLeft);
				distanceDirectionIndicatorLeft = null;
			}
			Node distanceDirectionIndicatorRight = root.findChild("distance_direction_indicator_r");
			if (distanceDirectionIndicatorRight != null) {
				root.detachChild(distanceDirectionIndicatorRight);
				distanceDirectionIndicatorRight = null;
			}
			Node growingDistanceIndicator = root.findChild("distance_cube_c");
			if (growingDistanceIndicator != null) {
				root.detachChild(growingDistanceIndicator);
				growingDistanceIndicator = null;
			}
			Node growingDistanceIndicatorRight = root.findChild("distance_cube_r");
			if (growingDistanceIndicatorRight != null) {
				root.detachChild(growingDistanceIndicatorRight);
				growingDistanceIndicatorRight = null;
			}
			Node growingDistanceIndicatorLeft = root.findChild("distance_cube_l");
			if (growingDistanceIndicatorLeft != null) {
				root.detachChild(growingDistanceIndicatorLeft);
				growingDistanceIndicatorLeft = null;
			}
		}
	}
	
	@Override
	public String getDescription() {
		return ExtWristDriftBCK290914.description;
	}

	@Override
	public String getName() {
		return ExtWristDriftBCK290914.name;
	}

	@Override
	public String getLabel() {
		return ExtWristDriftBCK290914.label;
	}

	public static void showEditorDialog() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new ExtWristDriftBCK290914.WristDriftEditorDialog();
			}
		});
	}
}
