package streamservice.onlinedatamanipulation;

import kinematics.Joint;
import kinematics.KinematicModel;

import com.jogamp.graph.math.Quaternion;
import com.spookengine.maths.Mat3;
import com.spookengine.maths.Vec3;
import com.spookengine.scenegraph.Node;

import scene.Scene;
import streamservice.frames.DataFrame;
import streamservice.frames.RawFrame;
import streamservice.frames.RawReading;

public class WristDrift implements IDataManipulator {

	public final static String name = "WRIST_DRIFT";
	
	public final static String label = "wrist drift";
	
	public final static String description = "Applies a constant drift to the left and right wrist towards each other on the x-axis."
			+ "This will only work on actor models which use the respective joints.";
	
	private float leftXDrift;
	
	private float rightXDrift;
	
	private float increment;
	
	public WristDrift() {
		super();
		this.leftXDrift = 0.0f;
		this.rightXDrift = 0.0f;
		this.increment = .1f;
	}
	
//	@Override
//	public void applyManiputlation(KinematicFrame frame) {
//		Node node = frame.skeleton.findChild("LeftHand");
//		if (node != null) {
//			if (node instanceof Joint) {
//				Joint joint = ((Joint) node);
//				Vec3 position = joint.getLocalTransform().getPosition();
//				joint.getLocalTransform().moveTo(position.x() - this.leftXDrift, position.y(), position.z())
//						.update();
//				this.leftXDrift += increment;
//				if (joint.getParent() instanceof Joint) {
//					((Joint) joint.getParent()).localToWorldTree();
//				}
//			}
//		}
//		
//		node = frame.skeleton.findChild("RightHand");
//		if (node != null) {
//			if (node instanceof Joint) {
//				Joint joint = ((Joint) node);
//				Vec3 position = joint.getLocalTransform().getPosition();
//				joint.getLocalTransform().moveTo(position.x() + this.rightXDrift, position.y(), position.z())
//						.update();
//				this.rightXDrift += increment;
//			}
//		}
//		
////		frame.skeleton.localToWorldTree();
//	}
	@Override
	public void applyManiputlation(KinematicModel actor) {
		Node node = actor.findChild("LeftHand");
		if (node != null) {
			if (node instanceof Joint) {
				Joint joint = ((Joint) node);
				Vec3 position = joint.getLocalTransform().getPosition();
//				joint.getLocalTransform().moveTo(position.x() - this.leftXDrift, position.y(), position.z())
//						.update();
				joint.getLocalTransform().moveTo(position.x() - this.leftXDrift, position.y(), position.z())
				.update();
				joint.getLocalTransform().update();
				joint.localToWorld();
				this.leftXDrift += increment;
				if (joint.getParent() instanceof Joint) {
					Vec3 jPos = joint.getLocalTransform().getPosition();
					Vec3 pPos = ((Joint) joint.getParent()).getLocalTransform().getPosition();
					Vec3 axis = pPos.cross(jPos);
					float angle = (float) Math.acos(pPos.dot(jPos) / jPos.length() / pPos.length());
					((Joint) joint.getParent()).getWorldTransform().rotateTo(angle, axis).update();;
//					((Joint) joint.getParent()).getLocalTransform().rotateTo(joint.getLocalTransform().getRotation()).update();
//					((Joint) joint.getParent()).getLocalTransform().rotateTo(joint.getLocalTransform().getPosition()).update();
					((Joint) joint.getParent()).localToWorld();
				}
			}
		}
		
		node = actor.findChild("RightHand");
		if (node != null) {
			if (node instanceof Joint) {
				Joint joint = ((Joint) node);
				Vec3 position = joint.getLocalTransform().getPosition();
				joint.getLocalTransform().moveTo(position.x() + this.rightXDrift, position.y(), position.z())
						.update();
				this.rightXDrift += increment;
			}
		}
		
//		frame.skeleton.localToWorldTree();
	}
	
//	public void applyManiputlation(RawFrame rawFrame, DataFrame dataFrame) {
//		RawReading leftForeArm = rawFrame.getReadingForSensorIndex(0);
//		RawReading rightForeArm = rawFrame.getReadingForSensorIndex(26);
//		
////		if (leftForeArm != null && rightForeArm != null) {
////			
////		}
//		
//		if (leftForeArm == null) {
//			leftForeArm = new RawReading(0, 0,
//					0, 0.0f, 0.0f, 0.0f,
//					0.0f, 0.0f, 0.0f,
//					0.0f, 0.0f, 0.0f,
//					0.0f, 0.0f, 0.0f, 0.0f,
//					0.0f, 0.0f, 0.0f, 1.0f,
//					0.0f, 0.0f, 0.0f, 0.0f,
//					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
//		}
//		
//		float angle = (float) Math.toRadians(this.leftXDrift);
//		
//		float result = (float) Math.sin(angle / 2.0);
//
//	    // Calculate the x, y and z of the quaternion
//	    float x = 1.0f * result;
//	    float y = 1.0f * result;
//	    float z = 0.0f * result;
//
//	    // Calcualte the w value by cos( theta / 2 )
//	    float w = (float) Math.cos( angle / 2.0 );
//		
//		Quaternion q1 = new Quaternion(x, y, z, w);
//		Quaternion q2 = new Quaternion(leftForeArm.qx, leftForeArm.qy, leftForeArm.qz, leftForeArm.qw);
//		q1.mult(q2);
//		
//		float qx = q1.getX();
//		float qy = q1.getY();
//		float qz = q1.getZ();
//		float qw = q1.getW();
//		
////		leftForeArm.qx += this.leftXDrift;
//		this.leftXDrift += this.increment;
//		
//		dataFrame.addRotation(0, new Mat3(1.0F
//				- (2.0F * qy * qy)
//				- (2.0F * qz * qz), 2.0F
//				* qx * qy
//				- (2.0F * qz * qw), 2.0F
//				* qx * qz + 2.0F * qy
//				* qw, 2.0F * qx * qy
//				+ 2.0F * qz * qw, 1.0F
//				- (2.0F * qx * qx)
//				- (2.0F * qz * qz), 2.0F
//				* qy * qz
//				- (2.0F * qx * qw), 2.0F
//				* qx * qz
//				- (2.0F * qy * qw), 2.0F
//				* qy * qz + 2.0F * qx
//				* qw, 1.0F
//				- (2.0F * qx * qx)
//				- (2.0F * qy * qy)));
//	}

	public void applyManiputlation(RawFrame rawFrame, DataFrame dataFrame) {
		int leftForeArmIndex = 23;
		int rightForeArmIndex = 26;
		
		RawReading leftForeArm = rawFrame.getReadingForSensorIndex(leftForeArmIndex);
		RawReading rightForeArm = rawFrame.getReadingForSensorIndex(rightForeArmIndex);
		
		// left arm
		if (leftForeArm == null) {
			leftForeArm = new RawReading(leftForeArmIndex, 0,
					0, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		}
		
		float angle = (float) Math.toRadians(this.leftXDrift);
		
		float result = (float) Math.sin(angle / 2.0);

	    // Calculate the x, y and z of the quaternion
	    float x = 1.0f * result;
	    float y = 0.0f * result;
	    float z = 0.0f * result;

	    // Calcualte the w value by cos( theta / 2 )
	    float w = (float) Math.cos( angle / 2.0 );
		
		Quaternion q1 = new Quaternion(x, y, z, w);
		Quaternion q2 = new Quaternion(leftForeArm.qx, leftForeArm.qy, leftForeArm.qz, leftForeArm.qw);
		q1.mult(q2);
		
		float qx = q1.getX();
		float qy = q1.getY();
		float qz = q1.getZ();
		float qw = q1.getW();
		
		this.leftXDrift += this.increment;
		
		dataFrame.addRotation(leftForeArmIndex, new Mat3(1.0F
				- (2.0F * qy * qy)
				- (2.0F * qz * qz), 2.0F
				* qx * qy
				- (2.0F * qz * qw), 2.0F
				* qx * qz + 2.0F * qy
				* qw, 2.0F * qx * qy
				+ 2.0F * qz * qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qz * qz), 2.0F
				* qy * qz
				- (2.0F * qx * qw), 2.0F
				* qx * qz
				- (2.0F * qy * qw), 2.0F
				* qy * qz + 2.0F * qx
				* qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qy * qy)));
		
		// right arm
		if (rightForeArm == null) {
			rightForeArm = new RawReading(rightForeArmIndex, 0,
					0, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f,
					0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
		}
		
		angle = (float) Math.toRadians(this.rightXDrift);
		
		result = (float) Math.sin(angle / 2.0);

	    // Calculate the x, y and z of the quaternion
	    x = 1.0f * result;
	    y = 0.0f * result;
	    z = 0.0f * result;

	    // Calcualte the w value by cos( theta / 2 )
	    w = (float) Math.cos( angle / 2.0 );
		
		q1 = new Quaternion(x, y, z, w);
		q2 = new Quaternion(rightForeArm.qx, rightForeArm.qy, rightForeArm.qz, rightForeArm.qw);
		q1.mult(q2);
		
		qx = q1.getX();
		qy = q1.getY();
		qz = q1.getZ();
		qw = q1.getW();
		
		this.rightXDrift -= this.increment;
		
		dataFrame.addRotation(rightForeArmIndex, new Mat3(1.0F
				- (2.0F * qy * qy)
				- (2.0F * qz * qz), 2.0F
				* qx * qy
				- (2.0F * qz * qw), 2.0F
				* qx * qz + 2.0F * qy
				* qw, 2.0F * qx * qy
				+ 2.0F * qz * qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qz * qz), 2.0F
				* qy * qz
				- (2.0F * qx * qw), 2.0F
				* qx * qz
				- (2.0F * qy * qw), 2.0F
				* qy * qz + 2.0F * qx
				* qw, 1.0F
				- (2.0F * qx * qx)
				- (2.0F * qy * qy)));
	}
	
	@Override
	public void setScene(Scene scene) {}
	
	@Override
	public void shutDown() {}
	
	@Override
	public String getDescription() {
		return WristDrift.description;
	}

	@Override
	public String getName() {
		return WristDrift.name;
	}

	@Override
	public String getLabel() {
		return WristDrift.label;
	}

}
