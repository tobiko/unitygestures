package praktikum.preprocessing;

import java.util.HashMap;





public class MarkFrames {

	
	public static final int noiseID = -1;
	public static int frameLength = 0;
	
	private static boolean active = false;
	
	//hier werden die Abschnitte der jeweiligen id gespeichert, als key der hashmap wird die Annotation verwendet (also zb -1 f�r noise)
	private static HashMap<Integer, TimeAnno[]> annoData = new HashMap<Integer, TimeAnno[]>();



	
	public static void init(String filename, int frameLength){
		MarkFrames.frameLength = frameLength;
		
		if(filename.contains("movement")){
			System.out.println("movement edit");

			//F�lle Hashmap
			annoData.clear();
			annoData.put(noiseID, new TimeAnno[]{ new TimeAnno( 380, 530),  new TimeAnno( 880, 1120) , new TimeAnno( 1470, 1630) ,  new TimeAnno(2660, 2760),  new TimeAnno( 3200, 3320) , new TimeAnno(3800, 3892)});
			active = true;

		} else 	if(filename.contains("rotation")){
			System.out.println("rotation edit");

			//F�lle Hashmap
			annoData.clear();
			annoData.put(noiseID, new TimeAnno[]{ new TimeAnno( 0, 50),  new TimeAnno( 260, 335) , new TimeAnno( 575, 630) ,  new TimeAnno(910, 990),  new TimeAnno( 1690, 1787) , new TimeAnno(2060, 2150), new TimeAnno(2480, 2590)});
			active = true;

		}
	}
	
	public static int getStatus(int frameNumber){
		//System.out.println(frame.toString());
		int status = 0; 
		
		if(active){

			for (Integer key : annoData.keySet()) {
				int annotation = key;
				TimeAnno[] an = annoData.get(key);
				for (TimeAnno timeAnno : an) {
					if(timeAnno.begin <= frameNumber && frameNumber <= timeAnno.end){
						status = annotation;
					}

				}	

			}
		}
		
		return status;
		

	}
	
	
}
