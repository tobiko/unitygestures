package praktikum.preprocessing;


import streamservice.frames.KinematicFrame;

public class GestureInputProcessor {
	

	private int countFrames = 0;
	
	public GestureInputProcessor(){
	
		
	}
	
	
	public void update(KinematicFrame kinematicFrame){
		//status of the frame, for example if its noise eg...
		int status = MarkFrames.getStatus(countFrames);
		
		Runnable r = new ComputeDataAsynchronus(kinematicFrame, status);
		new Thread(r).start();
		
		countFrames++;
		if(countFrames >= MarkFrames.frameLength){
			countFrames = 0;
		}
	}
	
	
	
	

}
