package praktikum.preprocessing;

import kinematics.Joint;
import streamservice.frames.KinematicFrame;

import com.spookengine.maths.Vec3;
import com.spookengine.scenegraph.Node;

public class ComputeDataAsynchronus implements Runnable{
	/* All Hand Joints:
	 	0: LeftForeArm
		1: LeftHand
		2: LeftCarpometacarpal
		3: LeftThumbKnuckle1
		4: LeftThumbKnuckle2
		5: LeftThumbTip
		6: LeftIndexKnuckle1
		7: LeftIndexKnuckle2
		8: LeftIndexKnuckle3
		9: LeftIndexTip
		10: LeftMiddleKnuckle1
		11: LeftMiddleKnuckle2
		12: LeftMiddleKnuckle3
		13: LeftMiddleTip
		14: LeftRingKnuckle1
		15: LeftRingKnuckle2
		16: LeftRingKnuckle3
		17: LeftRingTip
		18: LeftLittleKnuckle1
		19: LeftLittleKnuckle2
		20: LeftLittleKnuckle3
		21: LeftLittleTip
		22: RightForeArm
		23: RightHand
		24: RightCarpometacarpal
		25: RightThumbKnuckle1
		26: RightThumbKnuckle2
		27: RightThumbTip
		28: RightIndexKnuckle1
		29: RightIndexKnuckle2
		30: RightIndexKnuckle3
		31: RightIndexTip
		32: RightMiddleKnuckle1
		33: RightMiddleKnuckle2
		34: RightMiddleKnuckle3
		35: RightMiddleTip
		36: RightRingKnuckle1
		37: RightRingKnuckle2
		38: RightRingKnuckle3
		39: RightRingTip
		40: RightLittleKnuckle1
		41: RightLittleKnuckle2
		42: RightLittleKnuckle3
		43: RightLittleTip
	 */

	/*
		used Orientations
		0: LeftForeArm
		1: LeftHand
		2: LeftCarpometacarpal
		3: LeftThumbKnuckle1
		4: LeftThumbKnuckle2
		6: LeftIndexKnuckle1
		10: LeftMiddleKnuckle1


		22: RightForeArm
		23: RightHand
		24: RightCarpometacarpal
		25: RightThumbKnuckle1
		26: RightThumbKnuckle2
		28: RightIndexKnuckle1
		32: RightMiddleKnuckle1

	 */
	private static final int[] usedOrientations = new int[]{0,1,2,3,4,6,10,22,23,24,25,26,28,32};

	private int status;
	private KinematicFrame frame;

	/* relevant Input Data for the MLP
	 * Array of Vec3 is structured like this:
	 * 	0: LeftForeArm
	 *  1: LeftHand
	 *  2: LeftCarpometacarpal
	 *  3: LeftThumbKnuckle1
	 *  4: LeftThumbKnuckle2
	 *  5: LeftIndexKnuckle1
	 *  6: LeftMiddleKnuckle1

	 *  7: RightForeArm
	 *  8: RightHand
	 *  9: RightCarpometacarpal
	 *  10: RightThumbKnuckle1
	 *  11: RightThumbKnuckle2
	 *  12: RightIndexKnuckle1
	 *  13: RightMiddleKnuckle1 
	 *  
	 *  14: ellbowLeftDirection
	 *  15: ellbowRightDirection 
	 * 
	*/
	public Vec3[] relevantInputData = new Vec3[usedOrientations.length + 2]; 
	
	
	//Alte joint Positionen, wird f�r velocity Berechnung ben�tigt
	public static Vec3[] oldDirections = new Vec3[usedOrientations.length]; 



	private int countUsed = 0;
	private int countPos = 0;
	//Gl�ttungsfaktor
	private float alpha = 0.3f;


	public ComputeDataAsynchronus(KinematicFrame frame, int status) {
		this.frame = frame;
		this.status = status;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub

		
		
		//Betrachte nur den Oberk�rper, deswegen kann gleich auf den spine joint zugegriffen werden, da er der Vaterknoten des oberk�rpers ist
		Joint spine = (Joint)frame.skeleton.getRoot().getChildren().get(2).getChildren().get(0);

		Joint leftArm = (Joint)spine.getChildren().get(1).getChildren().get(0).getChildren().get(0);
		Joint rightArm = (Joint)spine.getChildren().get(2).getChildren().get(0).getChildren().get(0);

		
		convertData(leftArm);
		convertData(rightArm);

		//Berechne die globale orientierung des linken und rechten elbogen

		Vec3 ellbowLeftDirection = new Vec3(0, 0, 1);
		leftArm.getWorldTransform().getAffineTransform().mult(ellbowLeftDirection);
		ellbowLeftDirection.norm();

		relevantInputData[relevantInputData.length -2] = ellbowLeftDirection;
		
		Vec3 ellbowRightDirection = new Vec3(0, 0, 1);
		rightArm.getWorldTransform().getAffineTransform().mult(ellbowRightDirection);
		ellbowRightDirection.norm();				
		
		relevantInputData[relevantInputData.length-1] = ellbowRightDirection;
		
		//for(int i = 0; i < relevantInputData.length; i++){
		//	System.out.println(relevantInputData[i]);
		//}
		//rotation.mult(directionVector);
	}


	private void convertData(Joint joint){
		//Kopf und Nacken werden nicht in betracht gezogen

		if(usedPositionContains(countPos)){
			calculateVelocity(joint, countUsed);
			//System.out.println(countPos + ": " + joint.name + "\t" + relevantInputData[countUsed]);
			countUsed++;

		}			
		countPos++;


		for (Node child : joint.getChildren()) {
			if (child instanceof Joint) convertData((Joint) child);
		}

	}


	private boolean usedPositionContains(int key){
		for(int i = 0; i < usedOrientations.length; i++){
			if(usedOrientations[i] == key){
				return true;
			}
		}
		return false;
	}

	private void calculateVelocity(Joint joint, int index){

		Vec3 direction = new Vec3(0, 0, 1);
		joint.getLocalTransform().getAffineTransform().mult(direction);
		direction.norm();


		if(oldDirections[index] != null){		
			Vec3 old = oldDirections[index];


			//Exponentielle-Gl�ttung
			Vec3 direction2 = new Vec3(direction.x() * alpha + old.x() * (1-alpha) , direction.y() * alpha + old.y() * (1-alpha), direction.z() * alpha + old.z() * (1-alpha));		
			//Velocity
			Vec3 velocity = old.sub(direction2).norm();
			relevantInputData[index] = velocity; 

			oldDirections[index] = direction2;

		} else {
			relevantInputData[index] = new Vec3(0, 0, 0); 
			oldDirections[index] = direction;		
		}


	}

	/*
	Vec3 vec = new Vec3(0, 0, 1);

	kinematicFrame.skeleton.getRoot().getLocalTransform().getAffineTransform().mult(vec);;

	vec.norm();

	System.out.println(vec);
	 */

}
