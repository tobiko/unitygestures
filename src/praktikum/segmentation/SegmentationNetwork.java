package praktikum.segmentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

/**
 * Implementation of Grossberg's segmentation network using winner takes all
 * principle
 * 
 * @author smihael
 *
 */

public class SegmentationNetwork {

	/**
	 * weights from a given input neuron (first dimension) to output neurons
	 * (second dimension)
	 */
	public double[][] weights;

	/**
	 * net input for a given output neuron
	 */
	public double[] net;

	/**
	 * activation of a given output neuron (pattern layer) TODO: dynamic with
	 * ArrayList
	 */
	public double[] output;

	/**
	 * number of neurons in the input layer (number of features multiplied by
	 * number of input vectors)
	 */
	private int numInputNeurons;

	/**
	 * number of neurons in the output layer (number of already learnt patterns
	 * + 1)
	 */
	private int numOutputNeurons;

	private Random random;

	/**
	 * Constructor to create a new Segmentation Network Object with a given
	 * number of input and output neurons
	 */
	public SegmentationNetwork(final int _numInputNeurons,
			final int _numOutputNeurons) {

		// initial settings
		this.numInputNeurons = _numInputNeurons;
		this.numOutputNeurons = _numOutputNeurons;

		// initialising arrays
		this.weights = new double[numInputNeurons][numOutputNeurons];
		this.net = new double[numInputNeurons];
		this.output = new double[numOutputNeurons];

		// seed random genarator
		this.random = new Random(System.currentTimeMillis());

	}

	/**
	 * Constructor which reads settings of the MLP from a file
	 */
	public SegmentationNetwork(String file) {
		importWM(file);
		this.net = new double[numInputNeurons];
		this.output = new double[numOutputNeurons];
	}

	/**
	 * Quantile function (inverse cdf) of the Cauchy distribution
	 * 
	 * @param mu
	 *            mean value
	 * @param sigma
	 *            scale
	 * @return cauchy distributed random variable
	 */
	private double cauchy(double mu, double sigma) {
		return mu + (sigma * Math.tan(Math.PI * (random.nextDouble() - 0.5)));
	}

	/**
	 * Gets instar vector and calculates its length using Euclidian norm
	 * 
	 * @param weights
	 *            weigth matrix
	 * @param outputNeuronIndex
	 *            determines desired instar vector
	 * @return instar vector length
	 */
	private double calcInstarVectorLength(double weights[][],
			double outputNeuronIndex) {

		// array holding instar vector parameters
		double instar[] = new double[this.output.length];

		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {
				if (j == outputNeuronIndex)
					instar[i] += weights[i][j];
			}
		}

		double sumOfSquares = 0;
		// |\vec{a}|=\sqrt{a_1^2+a_2^2+a_3^2}
		for (int i = 0; i < instar.length; i++) {
			sumOfSquares += Math.pow(instar[i], 2.);
		}

		return Math.sqrt(sumOfSquares);
	}

	/**
	 * Feeds neural network with input and generates output
	 * 
	 * @param input
	 *            input vectors' componets as doubles
	 * @return
	 */
	public double[] generateOutput(final double[] input) {

		double epsilon = 0.0025;
		double r = 1000.;

		// TODO:
		double theta = 0.;

		double gamma = Math.tan(epsilon * Math.PI) * (1. - theta) / (2. * r);
		double m = (theta + 1.) / (2. - (2. / r));

		for (int numNeuronCurr = 0; numNeuronCurr < output.length; ++numNeuronCurr) {
			double sum = 0.;
			// Calculates net input for a given output neuron
			for (int i = 0; i < numInputNeurons; i++)
				sum += (weights[i][numNeuronCurr] * input[i]);
			net[numNeuronCurr] = sum + cauchy(m / r, gamma);

			// TODO: verify
			output[numNeuronCurr] = calcActivation(net[numNeuronCurr], r,
					calcInstarVectorLength(weights, numNeuronCurr));
		}

		return output;
	}

	/**
	 * Supervised leraining of neural network using the winner takes all
	 * principle and Hebbian learning
	 * 
	 * @param trainInput
	 * @param teachingInput
	 * @param learningrate
	 */
	public void trainPattern(double[] trainInput,
			final double[] teachingInput, final double learningrate) {
		
		
		// TODO!!!
		
		// indirect winner takes all (update only weights of the winner)
		
		// if calcInstarVectorLength <= 1/r  then proceed (we are still learning new pattern)
		//                     else (>= 1/r)      add new output neuron to the network!        
		
		double[] out = generateOutput(trainInput);
				
		//Hebbian learning
		for (int fromNeuron = 0; fromNeuron < weights.length; fromNeuron++) {
			for (int toNeuron = 0; toNeuron < weights[fromNeuron].length; toNeuron++) {
				weights[fromNeuron][toNeuron] += (teachingInput[fromNeuron]-weights[fromNeuron][toNeuron]) * out[toNeuron];
			}
		}

	}

	/**
	 * Calculate activation
	 */
	private double calcActivation(double net, double r, double w_norm) {
		return 1. + net / (Math.max(1 / r, w_norm));
	}

	/**
	 * Export weights to a file
	 */
	public void exportWM(String file) {

		File data = new File(file);
		try {
			FileOutputStream f = new FileOutputStream(data, false);
			ObjectOutputStream o = new ObjectOutputStream(f);
			o.writeObject(this.weights);
			o.flush();
			o.close();

		} catch (IOException e) {
			System.out.println("Could not write Matrix to file.");
			e.printStackTrace();
		}

	}

	/**
	 * Import weights from a file and reconfigure the network
	 */
	private void importWM(String file) {

		File data = new File(file);
		if (data.exists()) {
			try {
				FileInputStream f = new FileInputStream(data);
				ObjectInputStream o = new ObjectInputStream(f);
				this.weights = (double[][]) (o.readObject());

				o.close();
			} catch (IOException | ClassNotFoundException e) {
				System.out.println("Could not read weight matrix.");
				e.printStackTrace();
			}
		} else {
			System.exit(-1);
		}

		this.numInputNeurons = this.weights.length;
		this.numOutputNeurons = this.weights[1].length;

	}

}
