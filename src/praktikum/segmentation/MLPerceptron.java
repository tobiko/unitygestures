package praktikum.segmentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

/**
 * Implementation of multilayer perceptron with one hidden layer
 * 
 * @author smihael
 *
 */

public class MLPerceptron {

	// weights (layer/from neuron/to neuron) 
	public double[][][] weights;

	public double[][] net;
	public double[][] output;
	public double[][] delta;

	// activation of onNeuron
	private final double onNeuron = 1.d;

	// total number of layers
	private final int numLayer = 3;

	private int numInputNeurons;
	private int numHiddenNeurons;
	private int numOutputNeurons;

	private Random random;

	/**
	 * Constructor to create a new MLP Object with a given number of input,
	 * hidden and output neurons
	 */
	public MLPerceptron(final int _numInputNeurons,
			final int _numHiddenNeurons, final int _numOutputNeurons) {
		// initial settings
		this.numInputNeurons = _numInputNeurons;
		this.numHiddenNeurons = _numHiddenNeurons;
		this.numOutputNeurons = _numOutputNeurons;

		// initialising arrays
		this.weights = new double[numLayer - 1][][];
		this.output = new double[numLayer][];
		this.delta = new double[numLayer][];
		this.net = new double[numLayer][];

		this.net[0] = new double[numInputNeurons];
		this.output[0] = new double[numInputNeurons];
		this.delta[0] = new double[numInputNeurons];

		this.net[1] = new double[numHiddenNeurons];
		this.output[1] = new double[numHiddenNeurons];
		this.delta[1] = new double[numHiddenNeurons];

		this.net[2] = new double[numOutputNeurons];
		this.output[2] = new double[numOutputNeurons];
		this.delta[2] = new double[numOutputNeurons];

		// initialising the weight matrix with an extra weight per layer for the
		// on-neuron
		this.weights[0] = new double[numInputNeurons + 1][numHiddenNeurons];
		this.weights[1] = new double[numHiddenNeurons + 1][numOutputNeurons];

		// random start weights
		this.random = new Random(System.currentTimeMillis());

		for (int layer = 0; layer < this.weights.length; layer++) {
			for (int i = 0; i < this.weights[layer].length; i++) {
				for (int j = 0; j < this.weights[layer][i].length; j++) {
					this.weights[layer][i][j] = (this.random.nextDouble() * 0.2) - 0.1;
				}
			}
		}
	}

	/**
	 * Constructor which reads settings of the MLP from a file
	 */
	public MLPerceptron(String file) {
		importWM(file);

		this.output = new double[numLayer][];
		this.delta = new double[numLayer][];
		this.net = new double[numLayer][];

		this.net[0] = new double[numInputNeurons];
		this.output[0] = new double[numInputNeurons];
		this.delta[0] = new double[numInputNeurons];

		this.net[1] = new double[numHiddenNeurons];
		this.output[1] = new double[numHiddenNeurons];
		this.delta[1] = new double[numHiddenNeurons];

		this.net[2] = new double[numOutputNeurons];
		this.output[2] = new double[numOutputNeurons];
		this.delta[2] = new double[numOutputNeurons];

	}

	public double[] generateOutput(final int[] inputAsInt) {
		
		// inputs as outputs of input neurons
		for (int neuronNum = 0; neuronNum < output[0].length; ++neuronNum) {
			output[0][neuronNum] = (double) (inputAsInt[neuronNum]);
		}

		double sum = 0.d;
		for (int layer = 1; layer < output.length; ++layer) {
			for (int numNeuronCurr = 0; numNeuronCurr < output[layer].length; ++numNeuronCurr) {
				sum = 0.d;
				for (int numNeuronPrev = 0; numNeuronPrev < output[layer - 1].length; ++numNeuronPrev) {
					sum += output[layer - 1][numNeuronPrev]
							* weights[layer - 1][numNeuronPrev][numNeuronCurr];
				}
				sum += onNeuron
						* weights[layer - 1][weights[layer - 1].length - 1][numNeuronCurr];
				net[layer][numNeuronCurr] = sum;
				output[layer][numNeuronCurr] = calcActivation(net[layer][numNeuronCurr]);
			}
		}

		return output[output.length - 1];
	}

	/**
	 * 
	 * Training MLP using the bacprogpagation rule
	 * 
	 * @param trainInput
	 * @param teachingInput
	 * @param learningrate
	 */
	public void trainPattern(final int[] trainInput, final double[] teachingInput, final double learningrate) {
		double[] out = generateOutput(trainInput);
		double sum = 0.d;

		// calculating delta for output neurons
		for (int numNeuron = 0; numNeuron < delta[2].length; ++numNeuron) {
			delta[2][numNeuron] = calcActivationDerivative(net[2][numNeuron])
					* (teachingInput[numNeuron] - out[numNeuron]);
		}

		// calculating delta for hidden neurons
		for (int numNeuron = 0; numNeuron < delta[1].length; ++numNeuron) {
			sum = 0;
			for (int numNeuronAbove = 0; numNeuronAbove < delta[2].length; ++numNeuronAbove) {
				sum += delta[2][numNeuronAbove]
						* weights[1][numNeuron][numNeuronAbove];
			}
			delta[1][numNeuron] = calcActivationDerivative(net[1][numNeuron])
					* sum;
		}

		// updating the weights
		for (int layer = 0; layer < numLayer - 1; ++layer) {
			final int numNeurons = output[layer].length;
			for (int currNeuron = 0; currNeuron < numNeurons; ++currNeuron) {
				for (int targetNeuron = 0; targetNeuron < delta[layer + 1].length; ++targetNeuron) {
					weights[layer][currNeuron][targetNeuron] += (double) (learningrate
							* output[layer][currNeuron] * delta[layer + 1][targetNeuron]);
				}
			}

			for (int targetNeurons = 0; targetNeurons < delta[layer + 1].length; ++targetNeurons) {
				weights[layer][numNeurons][targetNeurons] += (double) (learningrate
						* onNeuron * delta[layer + 1][targetNeurons]);
			}
		}
	}

	/**
	 * Calculate activation using the logistic function
	 */
	private double calcActivation(double net) {
		return (1. / (1. + Math.pow(Math.E, -net)));
	}

	/**
	 * Detivative of activation function
	 */
	private double calcActivationDerivative(double net) {
		double act = calcActivation(net);
		return act * (1. - act);
	}

	/**
	 * Export weights to a file
	 */
	public void exportWM(String file) {

		File data = new File(file);
		try {
			FileOutputStream f = new FileOutputStream(data, false);
			ObjectOutputStream o = new ObjectOutputStream(f);
			o.writeObject(this.weights);
			o.flush();
			o.close();

		} catch (IOException e) {
			System.out.println("Could not write Matrix to file.");
			e.printStackTrace();
		}

	}

	/**
	 * Import weights from a file and reconfigure the MLP
 	 */
	private void importWM(String file) {

		File data = new File(file);
		if (data.exists()) {
			try {
				FileInputStream f = new FileInputStream(data);
				ObjectInputStream o = new ObjectInputStream(f);
				this.weights = (double[][][]) (o.readObject());

				o.close();
			} catch (IOException | ClassNotFoundException e) {
				System.out.println("Could not read weight matrix.");
				e.printStackTrace();
			}
		} else {
			System.exit(-1);
		}

		this.numInputNeurons = this.weights[0][0].length - 1;
		this.numHiddenNeurons = this.weights[0][1].length;
		this.numOutputNeurons = this.weights[1][1].length;

	}

}
