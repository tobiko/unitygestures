package utils;

import java.awt.EventQueue;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JTextArea;

// many many thanks to stackoverflow:
// http://stackoverflow.com/questions/342990/create-java-console-inside-the-panel
public class TextAreaOutputStream extends OutputStream {

	private static class PrinterThread implements Runnable {
		
		private final static String LINE_SEPARATOR = System.getProperty("line.separator");
		
		// the text area where the text should be printed to
		private final JTextArea textArea;
		// the maximum number of lines in the text area
		private final int maxNumberOfLines;
		// length of the lines within the textarea
		private final LinkedList<Integer> lengths;
		// to be appended values, can be considered as buffer
		private final ArrayList<String> lines;

		// length of the current line
		private int     currentLineLength;
		// flag which indicates if the contents of the text area shall be erased
		private boolean clear;
		// flag which indicates if we have scheduled an instance of this thread on the event queue
		private boolean queue;
		
	    private PrinterThread(JTextArea textArea, int maxNumberOfLines) {
			this.textArea         = textArea;
			this.maxNumberOfLines = maxNumberOfLines;
			this.lengths          = new LinkedList<Integer>();
			this.lines            = new ArrayList<String>();

			this.currentLineLength = 0;
			this.clear             = false;
			this.queue             = true;
	    }
	    
		private synchronized void append(String val) {
			this.lines.add(val);
			if (this.queue) {
				this.queue = false;
				EventQueue.invokeLater(this);
			}
		}

		private synchronized void clear() {
			this.clear             = true;
			this.currentLineLength = 0;
			this.lengths.clear();
			this.lines.clear();
			if (this.queue) {
				this.queue = false;
				EventQueue.invokeLater(this);
			}
		}

		public synchronized void run() {
			if (clear) {
				textArea.setText("");
			}
			for (String val : this.lines) {
				this.currentLineLength += val.length();
				// if we are at a new line, we have to count
				if (val.endsWith(PrinterThread.LINE_SEPARATOR)) {
					if (this.lengths.size() >= this.maxNumberOfLines) {
						this.textArea.replaceRange("", 0,
								this.lengths.removeFirst());
					}
					this.lengths.addLast(this.currentLineLength);
					this.currentLineLength = 0;
				}
				this.textArea.append(val);
			}
			this.lines.clear();
			this.clear = false;
			this.queue = true;
		}
		
	}

	private PrinterThread printThread;             

	public TextAreaOutputStream(JTextArea textArea) {
		this(textArea, 1000);
	}

	public TextAreaOutputStream(JTextArea textArea, int maxNumberOfLines) {
		super();
		if (maxNumberOfLines < 1) {
			maxNumberOfLines = 10;
		}

		this.printThread = new PrinterThread(textArea, maxNumberOfLines);
	}

	public synchronized void clear() {
		if (this.printThread != null) {
			this.printThread.clear();
		}
	}

	public synchronized void close() {
		this.printThread = null;
	}

	public synchronized void flush() {}

	public synchronized void write(int value) {
		byte[] intByte = new byte[] {(byte) value};
		this.write(intByte, 0, 1);
	}

	public synchronized void write(byte[] ba) {
		this.write(ba, 0, ba.length);
	}
	
	public synchronized void write(String string) {
		byte[] stringBytes = string.getBytes();
		this.write(stringBytes, 0, stringBytes.length);
	}
	
	public synchronized void write(byte[] ba, int str, int len) {
		if (this.printThread != null) {
			this.printThread.append(new String(ba, str, len));
		}
	}

}
