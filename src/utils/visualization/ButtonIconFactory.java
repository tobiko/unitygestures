package utils.visualization;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ButtonIconFactory {
	public static ImageIcon ALIGN_ICON;
	public static ImageIcon FIND_ICON;
	public static ImageIcon DISCONNECT_ICON;
	public static ImageIcon START_LIVE_ICON;
	public static ImageIcon STOP_LIVE_ICON;
	public static ImageIcon START_RECORD_ICON;
	public static ImageIcon STOP_RECORD_ICON;
	public static ImageIcon CONNECT_SPEECH_SERVER;
	public static ImageIcon DISCONNECT_SPEECH_SERVER;
	public static ImageIcon CONNECT_UNITY_SERVER;
	public static ImageIcon DISCONNECT_UNITY_SERVER;
	public static ImageIcon PERSPECTIVE_OPTIONS;
	public static ImageIcon TOGGLE_VIEW_ICON;
	public static ImageIcon UNKNOWN_ICON;
	
	public static enum ButtonStates {
		ALIGN_STATE,
		FIND_STATE,
		DISCONNECT_STATE,
		START_LIVE_STATE,
		STOP_LIVE_STATE,
		START_RECORD_STATE,
		STOP_RECORD_STATE,
		CONNECT_TO_SPEECHSERVER,
		DISCONNECT_FROM_SPEECHSERVER,
		CONNECT_TO_UNITYSERVER,
		DISCONNECT_FROM_UNITYSERVER,
		PERSPECTIVE_OPTIONS,
		ENABLE_VIEW_STATE,
		DISABLE_VIEW_STATE;
	}
	
	public static String getToolTipForState(ButtonIconFactory.ButtonStates state) {
		switch (state) {
			case ALIGN_STATE:
				return "aligns the device.";
			case FIND_STATE:
				return "look for devices in the local network (can take some time).";
			case DISCONNECT_STATE:
				return "disconnects the device permanently, from this frame no further interaction can be started.";
			case START_LIVE_STATE:
				return "starts live streaming.";
			case STOP_LIVE_STATE:
				return "stops live streaming.";
			case START_RECORD_STATE:
				return "starts recording into a track.";
			case STOP_RECORD_STATE:
				return "stops recording into a track.";
			case CONNECT_TO_SPEECHSERVER:
				return "register this component at the speech server to receive speech commands.";
			case DISCONNECT_FROM_SPEECHSERVER:
				return "disconnect this component from the speech server to avoid receiving speech commands.";
			case CONNECT_TO_UNITYSERVER:
				return "start the unity server to forward kinematic data to unity.";
			case DISCONNECT_FROM_UNITYSERVER:
				return "shut down the unity server and stop sending kinematic data to unity.";
			case PERSPECTIVE_OPTIONS:
				return "change viewport or camera.";
			case ENABLE_VIEW_STATE:
				return "enable visualization.";
			case DISABLE_VIEW_STATE:
				return "disable visualization.";
			default:
				return "unknown state.";
		}
	}
	
	static {
		try {
			ButtonIconFactory.ALIGN_ICON               = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/align_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.FIND_ICON                = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/find_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.DISCONNECT_ICON          = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/disconnect_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.START_LIVE_ICON          = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/record_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.STOP_LIVE_ICON           = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/stop_record_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.START_RECORD_ICON        = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/start_print_to_file_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.STOP_RECORD_ICON         = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/stop_print_to_file_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.CONNECT_SPEECH_SERVER    = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/microphone.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.DISCONNECT_SPEECH_SERVER = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/no_microphone.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.CONNECT_UNITY_SERVER     = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/start_unity_server_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.DISCONNECT_UNITY_SERVER  = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/stop_unity_server_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.PERSPECTIVE_OPTIONS      = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/camera_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.TOGGLE_VIEW_ICON         = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/toggle_view_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ButtonIconFactory.UNKNOWN_ICON             = new ImageIcon((ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/unknown_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
//	static {
//		try {
//			ButtonIconFactory.ALIGN_ICON               = new ImageIcon(((BufferedImage) ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/align_button_icon.png")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
//			
//			ButtonIconFactory.FIND_ICON                = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/find_button_icon.png")));
//			ButtonIconFactory.DISCONNECT_ICON          = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/disconnect_button_icon.png")));
//			ButtonIconFactory.RECORD_ICON              = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/record_button_icon.png")));
//			ButtonIconFactory.STOP_RECORD_ICON         = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/stop_record_button_icon.png")));
//			ButtonIconFactory.START_SAVE_TO_FILE_ICON  = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/start_print_to_file_icon.png")));
//			ButtonIconFactory.STOP_SAVE_TO_FILE_ICON   = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/stop_print_to_file_icon.png")));
//			ButtonIconFactory.CONNECT_SPEECH_SERVER    = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/microphone.png")));
//			ButtonIconFactory.DISCONNECT_SPEECH_SERVER = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/no_microphone.png")));
//			ButtonIconFactory.PERSPECTIVE_OPTIONS      = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/camera_button_icon.png")));
//			ButtonIconFactory.UNKNOWN_ICON             = new ImageIcon(ImageIO.read(ButtonIconFactory.class.getResourceAsStream("/utils/visualization/icons/unknown_button_icon.png")));
//		} catch (IOException ioe) {
//			ioe.printStackTrace();
//		}
//	}
}
