package utils.visualization;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.JButton;

import main.GlobalSettingsAndVariables;

public class BinaryIconButton extends JButton implements ActionListener, PropertyChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 599376722902816365L;
	
	private ButtonIconFactory.ButtonStates selectedState;
	private ButtonIconFactory.ButtonStates deselectedState;
	private Color defaultBackgroundColor;
	private boolean selectionState;
	private Icon selectedIcon;
	private Icon deselectedIcon;
	private String propertyChangeCommand;
	
	private BinaryIconButton refButton;

	public BinaryIconButton(Icon selectedIcon,
			ButtonIconFactory.ButtonStates selectedState, Icon deselectedIcon,
			ButtonIconFactory.ButtonStates deselectedState,
			boolean selectionState) {
		super(null, selectionState ? selectedIcon : deselectedIcon);
		this.selectedState = selectedState;
		this.deselectedState = deselectedState;
		this.selectedIcon = selectedIcon;
		this.deselectedIcon = deselectedIcon;
		
		this.selectionState = selectionState;
		this.setToolTipText(ButtonIconFactory.getToolTipForState(this.getCurrentState()));
		
		this.defaultBackgroundColor = this.getBackground();
		
		this.addActionListener(this);
	}
	
	public ButtonIconFactory.ButtonStates getCurrentState() {
		return this.selectionState ? this.selectedState : this.deselectedState;
	}
	
	public Color getDefaultBackgroundColor() {
		return this.defaultBackgroundColor;
	}
	
	public boolean getSelectionState() {
		return this.selectionState;
	}
	
	public void setSelectionState(boolean selectionState) {
		this.selectionState = selectionState;
		if (this.selectionState) {
			this.setIcon(this.selectedIcon);
		} else {
			this.setIcon(this.deselectedIcon);
		}
		this.setToolTipText(ButtonIconFactory.getToolTipForState(this.getCurrentState()));
	}
	
	public String getPropertyChangeCommand() {
		return this.propertyChangeCommand;
	}
	
	public void setPropertyChangeCommand(String propertyChangeCommand) {
		this.propertyChangeCommand = propertyChangeCommand;
	}
	
	public void setReferenceButton(BinaryIconButton refButton) {
		this.refButton = refButton;
	}

	public ButtonIconFactory.ButtonStates getSelectedState() {
		return this.selectedState;
	}
	
	public ButtonIconFactory.ButtonStates getDeselectedState() {
		return this.deselectedState;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (this.propertyChangeCommand != null) {
			if (pce.getPropertyName().equals(this.propertyChangeCommand)) {
				boolean enabled = (Boolean) pce.getNewValue();
				// TODO: invoke action?
				if (!enabled) {
					this.setSelectionState(false);
				}
				this.setEnabled(enabled);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		switch (this.getCurrentState()) {
			case START_LIVE_STATE:
				GlobalSettingsAndVariables.defaultStreamService.toggleLive();
				this.setSelectionState(!this.selectionState);
				if (this.refButton != null) {
					this.refButton.setSelectionState(false);
				}
				break;
			case STOP_LIVE_STATE:
				GlobalSettingsAndVariables.defaultStreamService.toggleLive();
				this.setSelectionState(!this.selectionState);
				break;
			case START_RECORD_STATE:
				GlobalSettingsAndVariables.defaultStreamService.toggleRecord();
				this.setSelectionState(!this.selectionState);
				if (this.refButton != null) {
					this.refButton.setSelectionState(false);
				}
				break;
			case STOP_RECORD_STATE:
				GlobalSettingsAndVariables.defaultStreamService.toggleRecord();
				this.setSelectionState(!this.selectionState);
				break;
			case ENABLE_VIEW_STATE:
				GlobalSettingsAndVariables.animate = true;
				this.setSelectionState(!this.selectionState);
				break;
			case DISABLE_VIEW_STATE:
				GlobalSettingsAndVariables.animate = false;
				this.setSelectionState(!this.selectionState);
				break;
//			TODO: speechserver?
			default:
				break;
		}
	}
}