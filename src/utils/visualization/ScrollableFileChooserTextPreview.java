package utils.visualization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import main.MainFrame;

public class ScrollableFileChooserTextPreview extends JScrollPane implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8960359629619472282L;

	private static final int maximalPreviewLength = 200;
	
	private JTextArea previewArea;
	
	private File file = null;
	
	public ScrollableFileChooserTextPreview (JFileChooser fileChooser) {
		super();
		this.setPreferredSize(new Dimension(200, 100));
		
		this.previewArea = new JTextArea();
		this.previewArea.setEditable(false);
		this.setViewportView(this.previewArea);
		this.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.getVerticalScrollBar().setUnitIncrement(20);
		
		TitledBorder border = BorderFactory.createTitledBorder("Preview");
		border.setTitleFont(new Font(MainFrame.defaultFontName, Font.BOLD, 12));
		this.setBorder(border);
		
		fileChooser.addPropertyChangeListener(this);
	}
	
	private void loadText() {
		StringBuffer input = new StringBuffer();
		
		if (this.file != null) {
			if (!this.file.isDirectory()) {
				try {
					int counter = 0;
					
					BufferedReader filereader = new BufferedReader(new InputStreamReader
							(new FileInputStream(this.file)));
					
					while (filereader.ready()) {
						if (counter == ScrollableFileChooserTextPreview.maximalPreviewLength) {
							input.append("----- end of preview -----" + System.getProperty("line.separator"));
							break;
						}
						
						input.append(filereader.readLine() + System.getProperty("line.separator"));
						counter++;
					}

					filereader.close();
				
					filereader = null;
					
				} catch (NullPointerException npe) {	
					input.append("No file selected.");
					this.previewArea.setForeground(Color.BLACK);
				} catch (FileNotFoundException fnf) {
					input.append(fnf.getMessage());
				} catch (IOException ioe) {
					input.append(ioe.getMessage());
				}
			} else {
				input.append("Directory selected: " + this.file.getName());
			}
		} else {
			input.append("No file selected.");
		}
		
		this.previewArea.setText(input.toString());
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		String prop = pce.getPropertyName();

		if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
			this.file = null;
		} else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
			this.file = (File) pce.getNewValue();
		}

		if (this.isShowing()) {
			this.loadText();
			this.repaint();
		}
	}

}
