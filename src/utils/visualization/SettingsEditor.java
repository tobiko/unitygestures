package utils.visualization;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import kinematics.KinematicModelProvider;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import streamservice.onlinedatamanipulation.AvailableManipulations;
import streamservice.onlinedatamanipulation.ExtWristDrift;
import streamservice.onlinedatamanipulation.IDataManipulator;

public class SettingsEditor extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5436753109563687313L;

	private MainFrame master;
	
	public SettingsEditor(MainFrame master) {
		super();
		this.master = master;
		this.setTitle("settings");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		this.setAlwaysOnTop(true);
		
		this.initializeContentPane();
		
		Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		Point upperLeft = new Point(0, 0);
		
		this.setSize((int) (d.width * .5D), (int) (d.height * .25D));
		Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
				+ d.height / 2);
		this.setLocation(new Point(center.x - this.getWidth() / 2, center.y
				- this.getHeight() / 2));
		
		this.setVisible(true);
	}
	
	private void initializeContentPane() {
		JTabbedPane optionPane = new JTabbedPane();
		optionPane.addTab("data options", this.generateDataOptionPane());
		optionPane.addTab("model options", this.generateModelOptionPane());
		
		JPanel panel = new JPanel();
		GroupLayout mainLayout = new GroupLayout(panel);
		mainLayout.setAutoCreateContainerGaps(true);
		mainLayout.setAutoCreateGaps(true);
		
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
			.addComponent(optionPane)
		);
		
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
			.addComponent(optionPane)
		);
		
		panel.setLayout(mainLayout);
		
		this.setContentPane(panel);
	}
	
	private JScrollPane generateDataOptionPane() {
		JPanel buttonPanel = new JPanel();
		GroupLayout buttonLayout = new GroupLayout(buttonPanel);
		buttonLayout.setAutoCreateContainerGaps(true);
		buttonLayout.setAutoCreateGaps(true);
		
		ParallelGroup horizontalGroup = buttonLayout.createParallelGroup();
		SequentialGroup verticalGroup = buttonLayout.createSequentialGroup();

		JLabel infoLabel = new JLabel("default manipulation:");
		horizontalGroup.addComponent(infoLabel);
		verticalGroup.addComponent(infoLabel);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		for (int i = 0; i < AvailableManipulations.availableManipulatorLabels.length; i++) {
			JRadioButton button = new JRadioButton(AvailableManipulations.availableManipulatorLabels[i]);
			button.setToolTipText(AvailableManipulations.availableManipulatorDescriptions[i]);
			button.setActionCommand("dataManipulation:" + AvailableManipulations.availableManipulators[i]);
			if (GlobalSettingsAndVariables.defaultDataManipulatorName.equals(AvailableManipulations.availableManipulators[i])) {
				button.setSelected(true);
			} else {
				button.setSelected(false);
			}
			
			button.addActionListener(this);
			buttonGroup.add(button);
			
			if (AvailableManipulations.availableManipulatorLabels[i].equals(ExtWristDrift.label)) {
				JButton editButton = new JButton("edit");
				editButton.setActionCommand("run_ext_wrist_drift_settings");
				editButton.addActionListener(this);
				
				SequentialGroup sequentialGroup = buttonLayout.createSequentialGroup();
				sequentialGroup.addComponent(button);
				sequentialGroup.addComponent(editButton);
				
				ParallelGroup parallelGroup = buttonLayout.createParallelGroup(Alignment.CENTER);
				parallelGroup.addComponent(button);
				parallelGroup.addComponent(editButton);
				
				horizontalGroup.addGroup(sequentialGroup);
				verticalGroup.addGroup(parallelGroup);
			} else {
				horizontalGroup.addComponent(button);
				verticalGroup.addComponent(button);
			}
		}
		
		buttonLayout.setVerticalGroup(verticalGroup);
		buttonLayout.setHorizontalGroup(horizontalGroup);
		buttonPanel.setLayout(buttonLayout);
		
		buttonPanel.setBorder(BorderFactory.createTitledBorder("data options"));
		
		JScrollPane mainPane = new JScrollPane(buttonPanel);
		mainPane.getVerticalScrollBar().setUnitIncrement(20);
		
		return mainPane;
	}
	
	private JScrollPane generateModelOptionPane() {
		JPanel buttonPanel = new JPanel();
		GroupLayout buttonLayout = new GroupLayout(buttonPanel);
		buttonLayout.setAutoCreateContainerGaps(true);
		buttonLayout.setAutoCreateGaps(true);
		
		ParallelGroup horizontalGroup = buttonLayout.createParallelGroup();
		SequentialGroup verticalGroup = buttonLayout.createSequentialGroup();
		
		JLabel infoLabel = new JLabel("default model:");
		horizontalGroup.addComponent(infoLabel);
		verticalGroup.addComponent(infoLabel);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		for (int i = 0; i < KinematicModelProvider.availableModelNames.length; i++) {
			JRadioButton button = new JRadioButton(KinematicModelProvider.availableModelNames[i]);
			button.setActionCommand("modelSelection:" + KinematicModelProvider.availableModelNames[i]);
			if (GlobalSettingsAndVariables.defaultKinematicModelName.equals(KinematicModelProvider.availableModelNames[i])) {
				button.setSelected(true);
			} else {
				button.setSelected(false);
			}
			
			button.addActionListener(this);
			buttonGroup.add(button);
			
			horizontalGroup.addComponent(button);
			verticalGroup.addComponent(button);
		}
		
		buttonLayout.setVerticalGroup(verticalGroup);
		buttonLayout.setHorizontalGroup(horizontalGroup);
		buttonPanel.setLayout(buttonLayout);
		
		buttonPanel.setBorder(BorderFactory.createTitledBorder("available kinematic models"));
		
		JScrollPane mainPane = new JScrollPane(buttonPanel);
		mainPane.getVerticalScrollBar().setUnitIncrement(20);
		
		return mainPane;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand();
		if (command.startsWith("dataManipulation:")) {
			String modelName = command.split("dataManipulation:")[1];
			IDataManipulator model = AvailableManipulations.getManipulatorInstanceByName(modelName);
			if (model != null) {
				model.setScene(this.master.getScene());
			}
			if (GlobalSettingsAndVariables.defaultDataManipulator != null) {
				GlobalSettingsAndVariables.defaultDataManipulator.shutDown();
			}
			GlobalSettingsAndVariables.defaultDataManipulator = model;
			GlobalSettingsAndVariables.defaultDataManipulatorName = modelName;
		} else if (command.startsWith("modelSelection:")) {
			String modelName = command.split("modelSelection:")[1];
			GlobalSettingsAndVariables.defaultKinematicModelName = modelName;
		} else if (command.equals("run_ext_wrist_drift_settings")) {
			ExtWristDrift.showEditorDialog();
		}
	}

}
