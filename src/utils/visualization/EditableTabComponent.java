package utils.visualization;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIDefaults;

public abstract class EditableTabComponent extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8801559254213634714L;

	private class TabButtonMouseAdapter extends MouseAdapter {
		public void mouseEntered(MouseEvent me) {
			Component component = me.getComponent();
			if (component instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(true);
			}
		}

		public void mouseExited(MouseEvent me) {
			Component component = me.getComponent();
			if (component instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(false);
			}
		}
	}
	
	private class TabButton extends JButton {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4086191714107316547L;
		
		private boolean closeButton;
		
		private TabButton(boolean closeButton) {
			this.closeButton = closeButton;

			this.setActionCommand(this.closeButton ? "close" : "add");
			
			int size = 17;
			this.setPreferredSize(new Dimension(size, size));
			setToolTipText(this.closeButton ? "close this tab" : "add new stuff");
			// Make it transparent
			this.setContentAreaFilled(false);
			// No need to be focusable
			this.setFocusable(false);
			this.setBorder(BorderFactory.createEtchedBorder());
			this.setBorderPainted(false);
			// Making nice rollover effect
			// we use the same listener for all buttons
			this.addMouseListener(new TabButtonMouseAdapter());
			this.setRolloverEnabled(true);
			// Close the proper tab by clicking the button
			this.addActionListener(EditableTabComponent.this);
		}
		
		protected void paintComponent(Graphics g) {
			if (this.closeButton) {
				//cross
				super.paintComponent(g);
		        Graphics2D g2 = (Graphics2D) g.create();
		        //shift the image for pressed buttons
		        if (getModel().isPressed()) {
		            g2.translate(1, 1);
		        }
		        g2.setStroke(new BasicStroke(2));
		        g2.setColor(Color.BLACK);
		        if (getModel().isRollover()) {
		            g2.setColor(Color.RED);
		        }
		        int delta = 6;
		        g2.drawLine(delta, delta, this.getWidth() - delta - 1, this.getHeight() - delta - 1);
		        g2.drawLine(this.getWidth() - delta - 1, delta, delta, this.getHeight() - delta - 1);
		        g2.dispose();
			} else {
				//plus
				super.paintComponent(g);
		        Graphics2D g2 = (Graphics2D) g.create();
		        //shift the image for pressed buttons
		        if (getModel().isPressed()) {
		            g2.translate(1, 1);
		        }
		        g2.setStroke(new BasicStroke(2));
		        g2.setColor(Color.BLACK);
		        if (getModel().isRollover()) {
		            g2.setColor(Color.GREEN);
		        }
		        
				int delta = 6;
		        g2.drawLine((this.getWidth() - 1) / 2, delta, (this.getWidth() - 1) / 2, this.getHeight() - delta - 1);
		        g2.drawLine(this.getWidth() - delta - 1, this.getHeight() / 2, delta, this.getHeight() / 2);
		        g2.dispose();
			}
	    }
	}

	private class TitleLabel extends JLabel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6209176643385710469L;

		private TitleLabel(boolean costumeSkin) {
			if (costumeSkin) {
				this.applyCostumeSkin();
			}
		}
		
		public String getText() {
			try {
				int i = EditableTabComponent.this.getMasterPane().indexOfTabComponent(EditableTabComponent.this);
				if (i != -1) {
					return EditableTabComponent.this.getMasterPane().getTitleAt(i);
				}
				return null;
			} catch (NullPointerException npe) {
				return null;
			}
		}
		
		public void applyCostumeSkin() {
			UIDefaults labelDefaults = new UIDefaults();

			labelDefaults.put("Label.font", new Font("Avalon Quest", Font.BOLD, 12));

			this.putClientProperty("Nimbus.Overrides", labelDefaults);
			this.putClientProperty("Nimbus.Overrides.InheritDefaults", false);
		}
	}
	
	protected PopupEnabled master;
	
	public EditableTabComponent(PopupEnabled master) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		this.master = master;
		this.setOpaque(false);

		TitleLabel titleLabel = new TitleLabel(true);
		titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		
		this.add(titleLabel);
		
		this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
	}
	
	public EditableTabComponent(PopupEnabled master, boolean closeButton) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		this.master = master;
		this.setOpaque(false);

		TitleLabel titleLabel = new TitleLabel(true);
		titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		
		this.add(titleLabel);
		
		this.add(new TabButton(closeButton));
		
		this.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
	}
	
	public JTabbedPane getMasterPane() {
		try {
			return (JTabbedPane) this.master;
		} catch (ClassCastException cce) {
			return null;
		}
	}
	
	@Override
	public abstract void actionPerformed(ActionEvent ae);
	
}
