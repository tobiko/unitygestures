package utils.visualization;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JTabbedPane;

public class EditableTabbedPane extends JTabbedPane implements PopupEnabled {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7132831575958510196L;

	protected class PlotPaneTabComponent extends EditableTabComponent {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3715007626062472349L;

		public PlotPaneTabComponent(PopupEnabled master) {
			super(master);
		}
		
		public PlotPaneTabComponent(PopupEnabled master, boolean closeButton) {
			super(master, closeButton);
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand().equals("close")) {
				JTabbedPane pane = this.getMasterPane();
				
				if (pane == null) {
					return;
				}
				
				int i = pane.indexOfTabComponent(this);
				if (i != -1) {
					pane.remove(i);
					System.gc();
				}
			} else if (ae.getActionCommand().equals("add")) {
				this.master.showPopup(((JButton) ae.getSource()).getLocation());//.getLocationOnScreen());
			}
		}	
	}
	
	@Override
	public void showPopup(Point locationOnScreen) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void addTab(String title, Component component) {
		super.addTab(title, component);
		this.setTabComponentAt(this.getTabCount() - 1, new PlotPaneTabComponent(this));
	}
	
	public void addCloseableTab(String title, Component component) {
		super.addTab(title, component);
		this.setTabComponentAt(this.getTabCount() - 1, new PlotPaneTabComponent(this, true));
	}
	
	public void addExtendableTab(String title, Component component) {
		super.addTab(title, component);
		this.setTabComponentAt(this.getTabCount() - 1, new PlotPaneTabComponent(this, false));
	}
}
