package utils.visualization;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;

public class IconButton extends JButton implements ActionListener, PropertyChangeListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4983351024865088274L;
	
	private class ButtonHighlightThread implements Runnable {
		private double factor;
		private int interval;
		private boolean done;
		
		private ButtonHighlightThread() {
			super();
			this.factor = 1.0d;
			this.interval = 120;
			this.done = false;
		}
		
		@Override
		public void run() {
			while (!this.done) {
				Color base = IconButton.this.defaultBackgroundColor;
				IconButton.this.setBackground(new Color((int) Math.rint(base
						.getRed() * this.factor), (int) Math.rint(base.getGreen()
						* this.factor), (int) Math.rint(base.getBlue()
						* this.factor)));

				this.factor -= .2d;
				if (this.factor < 0) {
					this.factor = 1.0d;
				}
				
				try {
					Thread.sleep(this.interval);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
			
			IconButton.this.setBackground(IconButton.this.defaultBackgroundColor);
		}
		
		private void requestCancel() {
			this.done = true;
		}
	}
	
	private ButtonIconFactory.ButtonStates state;
	private Color defaultBackgroundColor;
	private String propertyChangeCommand;
	private boolean runHighlightThreadUponActivation;
	private ButtonHighlightThread highlighter;
	
	public IconButton(ButtonIconFactory.ButtonStates state) {
		super(null, null);
		this.state = state;
		this.defaultBackgroundColor = this.getBackground();
		this.runHighlightThreadUponActivation = false;
	}

	public IconButton(Icon icon, ButtonIconFactory.ButtonStates state) {
		super(null, icon);
		this.state = state;
		this.defaultBackgroundColor = this.getBackground();
		this.runHighlightThreadUponActivation = false;
	}

	public IconButton(String text, ButtonIconFactory.ButtonStates state) {
		super(text, null);
		this.state = state;
		this.defaultBackgroundColor = this.getBackground();
		this.runHighlightThreadUponActivation = false;
	}

	public IconButton(Action a, ButtonIconFactory.ButtonStates state) {
		super();
		super.setAction(a);
		this.state = state;
		this.defaultBackgroundColor = this.getBackground();
		this.runHighlightThreadUponActivation = false;
	}

	public IconButton(String text, Icon icon, ButtonIconFactory.ButtonStates state) {
		super();
		// Create the model
		super.setModel(new DefaultButtonModel());
		// initialize
		super.init(text, icon);
		this.state = state;
		this.defaultBackgroundColor = this.getBackground();
		this.runHighlightThreadUponActivation = false;
	}
	
	public ButtonIconFactory.ButtonStates getState() {
		return this.state;
	}
	
	public void setState(ButtonIconFactory.ButtonStates state) {
		this.state = state;
	}
	
	public Color getDefaultBackgroundColor() {
		return this.defaultBackgroundColor;
	}

	public String getPropertyChangeCommand() {
		return this.propertyChangeCommand;
	}
	
	public void setPropertyChangeCommand(String propertyChangeCommand) {
		this.propertyChangeCommand = propertyChangeCommand;
	}
	
	public void setHighlightThreadState(boolean runHighlightThreadUponActivation) {
		this.runHighlightThreadUponActivation = runHighlightThreadUponActivation;
	}
	
	public boolean isRunningHighlightthreadUponActivation() {
		return this.runHighlightThreadUponActivation;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (this.propertyChangeCommand != null) {
			if (pce.getPropertyName().equals(this.propertyChangeCommand)) {
				boolean enabled = (Boolean) pce.getNewValue();
				this.setEnabled(enabled);
				if (enabled && this.runHighlightThreadUponActivation) {
					this.addActionListener(this);
					this.highlighter = new ButtonHighlightThread();
					new Thread(this.highlighter).start();
				} else if (!enabled && this.highlighter != null) {
					this.highlighter.requestCancel();
					this.highlighter = null;
					this.removeActionListener(this);
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (this.highlighter != null) {
			this.highlighter.requestCancel();
			this.highlighter = null;
		}
		
		this.removeActionListener(this);
	}
}