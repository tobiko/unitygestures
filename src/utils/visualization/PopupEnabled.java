package utils.visualization;

import java.awt.Point;

public interface PopupEnabled {
	public void showPopup(Point locationOnScreen);
}
