package utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TCPCommunicationUtilities {
	// just some methods ported from the MPUConnection class
	public static int bytesToInt(byte buffer[], ByteOrder endianness) {
		if (buffer.length == 4) {
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer).order(endianness);
			return byteBuffer.asIntBuffer().get();
		} else {
			throw new NumberFormatException(
					"Byte array should contain 4 bytes.");
		}
	}

	public static long bytesToUint(byte buffer[], ByteOrder endianness) {
		if (buffer.length == 4) {
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer).order(endianness);
			// TODO: what?
			return 4294967295L & (long) byteBuffer.asIntBuffer().get();
		} else {
			throw new NumberFormatException(
					"Byte array should contain 4 bytes.");
		}
	}

	public static float bytesToFloat(byte buffer[], ByteOrder endianess) {
		if (buffer.length == 4) {
			ByteBuffer byteBuffer = ByteBuffer.wrap(buffer).order(endianess);
			return byteBuffer.asFloatBuffer().get();
		} else {
			throw new NumberFormatException(
					"Byte array should contain 4 bytes.");
		}
	}
}
