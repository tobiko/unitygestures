package fileio.export;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import streamservice.tracks.Track;
import utils.visualization.ScrollableFileChooserTextPreview;

/**
 * this class handles the export of recorded data, currently raw data export and export in terms of a kinematic chain according
 * to the bvh format are supported. file import is not supported yet.
 */
public class ExportService {
	/**
	 * Just the {@link FileFilter} applied in the {@link JFileChooser} used to obtain the output file.
	 */
	public static class ExportFileFilter extends FileFilter {
		/**
		 * id of the file type, either raw or bvh. is used to determine the kind of output stream.
		 */
		private String id;
		/**
		 * short description of the file type.
		 */
		private String description;
		/**
		 * valid file extensions (.raw or .bvh)
		 */
		private String extension;
		/**
		 * constructs a new file filter for the respective file extension
		 * @param description description of the format (displayed in the chooser dialog)
		 * @param extension valid file extension
		 * @param id identifier of the file type
		 */
		public ExportFileFilter(String description, String extension, String id) {
			super();
			this.description = description;
			this.extension = extension;
			this.id = id;
		}
		
		@Override
		public String getDescription() {
			return this.description;
		}
		
		@Override
		public boolean accept(File file) {
			if (file != null) {
				if (file.getName().endsWith(this.extension)) {
					return true;
				}
			}
			return false;
		}
		
		public String getID() {
			return this.id;
		}
	}
	/**
	 * sets up and assigns the file filters to a certain {@link JFileChooser}.
	 * @param fc, the {@link JFileChooser} to which the filters shall be assigned.
	 */
	private static void setupFileFilters(JFileChooser fc) {
		for (int i = 0; i < RawFileSaveService.getDescriptions().length; i++) {
			fc.addChoosableFileFilter(new ExportFileFilter(
					RawFileSaveService.getDescriptions()[i],
					RawFileSaveService.getExtensions()[i], "raw"));
		}
		
		for (int i = 0; i < BVHFileSaveService.getDescriptions().length; i++) {
			fc.addChoosableFileFilter(new ExportFileFilter(
					BVHFileSaveService.getDescriptions()[i],
					BVHFileSaveService.getExtensions()[i], "bvh"));
		}
	}
	/**
	 * this method shows a file dialog, i.e. a {@link JFileChooser} for the data export.
	 * @param accesory, if true, a text preview is displayed on the right side of the chooser
	 * @param baseDirectory, directory were the file selection starts
	 * @return an object array, holding the assigned filter at index 0 and the chosen file at
	 * index 1. if no file was selected this array is null.
	 */
	public static Object[] getFile(boolean accesory, String baseDirectory) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(baseDirectory));
		
		fc.setApproveButtonText("select file");
		fc.setDialogTitle("output file");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		ExportService.setupFileFilters(fc);
		
		if (accesory) fc.setAccessory(new ScrollableFileChooserTextPreview(fc));
		
		int state = fc.showOpenDialog(null);
		
		if (state == JFileChooser.APPROVE_OPTION) {
			return new Object[] {fc.getSelectedFile(), fc.getFileFilter()};
		} else {
			return null;
		}
	}
	/**
	 * runs a certain save service for the selected track. Depending on the user selection, either
	 * raw data (via a {@link RawFileSaveService}) or bvh data (via a {@link BVHFileSaveService}) is saved.
	 * @param data, this object array contains the {@link ExportFileFilter} and the {@link File}, obtained via
	 * the {@link ExportService#getFile(boolean, String)} method.
	 * @param track the to be saved track-
	 */
	public static void runSaveService(Object[] data, Track track) {
		if (data != null) {
			if ((data[0] instanceof File) && (data[1] instanceof ExportFileFilter)) {
				File file = (File) data[0];
				String id = ((ExportFileFilter) data[1]).getID();
				
				if (id.equals("raw")) {
					try {
						new RawFileSaveService().save(track, file);
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
				} else if (id.equals("bvh")) {
					try {
						new BVHFileSaveService().save(track, file);
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
				}
			}
		}
	}
	
}
