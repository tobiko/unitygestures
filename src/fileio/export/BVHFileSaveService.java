package fileio.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import kinematics.Joint;
import kinematics.KinematicModelService;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import streamservice.frames.KinematicFrame;
import streamservice.stream.StreamService;
import streamservice.torrents.KinematicTorrent;
import streamservice.tracks.Track;

import com.spookengine.maths.Euler;
import com.spookengine.maths.FastMath;
import com.spookengine.maths.Vec3;
import com.spookengine.scenegraph.Node;

/**
 * instances of this class are used to save bvh data from a certain track.
 * saving is done asynchronously via a {@link KinematicTorrent}, this class implements
 * the necessary listener.
 */
public class BVHFileSaveService implements KinematicTorrent.Listener {
	/**
	 * just a constant for measure conversion.
	 */
	private final static float INCH2CM = 2.54F;
	/**
	 * the {@link StreamService} hosting the to be saved track.
	 */
	private StreamService streamService;
	/**
	 * writer for the text output.
	 */
	private BufferedWriter writer;
	/**
	 * for number formatting...
	 */
	private DecimalFormat format;
	/**
	 * for even more number formatting...
	 */
	private DecimalFormat extendedFormat;
	/**
	 * creates a new {@link BVHFileSaveService} and initializes the global variables.
	 */
	public BVHFileSaveService() {
		super();
		this.streamService = GlobalSettingsAndVariables.defaultStreamService;
		// ahhh, so much occasions were i freaked out when some data was saved comma-separated, hence the
		// formats are initialized with an US locale.
		this.format = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		this.format.applyPattern("###0.00");
		this.extendedFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		this.extendedFormat.applyPattern("###0.00000");
	}
	/**
	 * saves a single joint in a bvh-compatible way, the kinematic chain stored in the {@link KinematicModelService} associated
	 * with a certain track is already well suited to be stored in bvh format, however, the pure text stuff (channels, parenthesis)
	 * is done here. this method is called recursively until the whole hierarchy is saved.
	 * @param out, the writer for the text output
	 * @param depth, indicates the number of tabs until the opening bracket
	 * @param joint, the actual joint which shall be saved
	 * @throws IOException
	 */
	private void saveJoint(BufferedWriter out, int depth, Joint joint)
			throws IOException {
		String indent = "";
		// offset for the bracket
		for (int i = 0; i < depth; ++i) {
			indent = indent + "\t";
		}
		// formatting depends on the number of child nodes
		if (!(joint.getChildren().isEmpty())) {
			out.write(indent + "JOINT " + joint.name);
			out.newLine();
			out.write(indent + "{");
			out.newLine();
			// offsets...
			out.write(indent
					+ "\tOFFSET\t"
					+ this.format.format(joint.getLocalTransform()
							.getPosition().x() * BVHFileSaveService.INCH2CM)
					+ "\t"
					+ this.format.format(joint.getLocalTransform()
							.getPosition().z() * BVHFileSaveService.INCH2CM)
					+ "\t"
					+ this.format.format(-joint.getLocalTransform()
							.getPosition().y() * BVHFileSaveService.INCH2CM));
			out.newLine();
			// and channels
			out.write(indent + "\tCHANNELS 3 Zrotation Xrotation Yrotation");
			out.newLine();
		} else {
			out.write(indent + "End Site");
			out.newLine();
			out.write(indent + "{");
			out.newLine();

			out.write(indent
					+ "\tOFFSET\t"
					+ this.format.format(joint.getLocalTransform()
							.getPosition().x() * BVHFileSaveService.INCH2CM)
					+ "\t"
					+ this.format.format(joint.getLocalTransform()
							.getPosition().z() * BVHFileSaveService.INCH2CM)
					+ "\t"
					+ this.format.format(-joint.getLocalTransform()
							.getPosition().y() * BVHFileSaveService.INCH2CM));
			out.newLine();
		}
		// invoke the method recursivly until all joints are stored
		for (Node child : joint.getChildren()) {
			if (child instanceof Joint) {
				this.saveJoint(out, depth + 1, (Joint) child);
			}
		}
		// closing bracket
		out.write(indent + "}");
		out.newLine();
	}
	/**
	 * writes a single data frame. according to the bvh format, angles are stored in terms of Euler angles (RPY sequence).
	 * the necessary conversions take place here. again, this method operates in a recursive manner until all data is stored.
	 * @param out writer for the text output
	 * @param joint to be saved joint
	 * @param frame current data frame (instance of {@link KinematicFrame})
	 * @throws IOException
	 */
	private void saveFrame(BufferedWriter out, Joint joint, KinematicFrame frame)
			throws IOException {
		Joint skeletonJoint;
		if (!(joint.getChildren().isEmpty())) {
			Euler euler = new Euler();
			if (joint.sensorId >= 0) {
				skeletonJoint = frame.skeleton.findChild(joint.sensorId);

				skeletonJoint.getLocalTransform().getRotationYPR(euler);
			}

			out.write(this.format.format(-FastMath.toDegrees(euler.roll))
					+ "\t"
					+ this.format.format(FastMath.toDegrees(euler.pitch))
					+ "\t" + this.format.format(FastMath.toDegrees(euler.yaw))
					+ "\t");

			for (Node child : joint.getChildren()) {
				if (child instanceof Joint) {
					this.saveFrame(out, (Joint) child, frame);
			
				}
			}
		}
	}

	/**
	 * this method initializes the saving procedure, by a call to the {@link StreamService#startKinematicTorrent(Track, streamservice.torrents.KinematicTorrent.Listener)}
	 * method.
	 * @param track the to be saved track.
	 * @param file the file destination for the output.
	 * @throws IOException
	 */
	public void save(Track track, File file) throws IOException {
		this.writer = new BufferedWriter(new FileWriter(file));
		this.streamService.startKinematicTorrent(track, this);
	}

	@Override
	public void start(Track track) {
		try {
			this.writer.write("HIERARCHY");
			this.writer.newLine();

			Joint root = track.getActor().getRoot();
			this.writer.write("ROOT " + root.name);
			this.writer.newLine();
			this.writer.write("{");
			this.writer.newLine();
			this.writer.write("\tOFFSET\t"
					+ this.format.format(BVHFileSaveService.INCH2CM * root.getWorldTransform()
							.getPosition().x())
					+ "\t"
					+ this.format.format(BVHFileSaveService.INCH2CM * root.getWorldTransform()
							.getPosition().z())
					+ "\t"
					+ this.format.format(-BVHFileSaveService.INCH2CM
							* root.getWorldTransform().getPosition().y()));
			this.writer.newLine();
			this.writer
					.write("\tCHANNELS 6 Xposition Yposition Zposition Zrotation Xrotation Yrotation");
			this.writer.newLine();

			for (Node child : root.getChildren()) {
				if (child instanceof Joint)
					saveJoint(this.writer, 1, (Joint) child);
			}
			this.writer.write("}");
			this.writer.newLine();
			this.writer.newLine();

			this.writer.write("MOTION");
			this.writer.newLine();
			this.writer.write("Frames: " + track.getFrameCount());
			this.writer.newLine();
			this.writer.write("Frame Time: "
					+ this.extendedFormat.format(1.0F / track.getFps()));
			this.writer.newLine();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void next(Track track, KinematicFrame frame) {
		try {
			Vec3 pos = frame.skeleton.getRoot().getWorldTransform()
					.getPosition();
			this.writer.write(this.format.format(BVHFileSaveService.INCH2CM * pos.x()) + "\t"
					+ this.format.format(BVHFileSaveService.INCH2CM * pos.z()) + "\t"
					+ this.format.format(-BVHFileSaveService.INCH2CM * pos.y()) + "\t");
			saveFrame(this.writer, frame.skeleton.getRoot(), frame);
			this.writer.newLine();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void finished(Track track) {
		try {
			this.writer.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			MainFrame.defaultConsole.write(ioe.getLocalizedMessage() + System.getProperty("line.separator"));
		}
		MainFrame.defaultConsole.write("bvh file stored..." + System.getProperty("line.separator"));
	}
	/**
	 * @return the description displayed in the file chooser for bvh files.
	 */
	public static String[] getDescriptions() {
		return new String[] {"biovision motion hierarchy format."};
	}
	/**
	 * @return the file extension for bvh files.
	 */
	public static String[] getExtensions() {
		return new String[] { ".bvh" };
	}
}
