package fileio.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import main.GlobalSettingsAndVariables;
import main.MainFrame;
import streamservice.frames.RawFrame;
import streamservice.stream.StreamService;
import streamservice.torrents.RawTorrent;
import streamservice.tracks.Track;

/**
 * instances of this class are used to save raw data from a certain track.
 * saving is done asynchronously via a {@link RawTorrent}, this class implements
 * the necessary listener.
 */
public class RawFileSaveService implements RawTorrent.Listener {
	/**
	 * the {@link StreamService} hosting the to be saved track.
	 */
	private StreamService streamService;
	/**
	 * output is written via this writer.
	 */
	private BufferedWriter writer;
	/**
	 * constructs a new {@link RawFileSaveService}.
	 */
	public RawFileSaveService() {
		super();
		this.streamService = GlobalSettingsAndVariables.defaultStreamService;
	}
	/**
	 * this method initializes the saving procedure, by a call to the {@link StreamService#startRawTorrent(Track, streamservice.torrents.RawTorrent.Listener)}
	 * method.
	 * @param track the to be saved track.
	 * @param file the file destination for the output.
	 * @throws IOException
	 */
	public void save(Track track, File file) throws IOException {
		this.writer = new BufferedWriter(new FileWriter(file));
		this.streamService.startRawTorrent(track, this);
	}

	@Override
	public void start(int frameCount, int fps) {
		try {
			this.writer.write("SensorId\t" + "FrameCount\t" + "AccelerationX\t"
					+ "AccelerationY\t" + "AccelerationZ\t" + "AngularRateX\t"
					+ "AngularRateY\t" + "AngularRateZ\t" + "MagneticX\t"
					+ "MagneticY\t" + "MagneticZ\t" + "RawQuatX\t"
					+ "RawQuatY\t" + "RawQuatZ\t" + "RawQuatW\t" + "QuatX\t"
					+ "QuatY\t" + "QuatZ\t" + "QuatW\t" + "QErrorX\t"
					+ "QErrorY\t" + "QErrorZ\t" + "QErrorW\t"
					+ "QAzimuthError\t" + "NMag\t" + "NMagZero\t" + "Alpha\t"
					+ "State\t" + "Annotation"
					+ System.getProperty("line.separator"));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void next(RawFrame frame) {
		try {
			this.writer.write(frame.toString());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	@Override
	public void finished() {
		try {
			this.writer.close();
		} catch (IOException ioe) {
			MainFrame.defaultConsole.write(ioe.getLocalizedMessage() + System.getProperty("line.separator"));
		}
		MainFrame.defaultConsole.write("raw file stored..." + System.getProperty("line.separator"));
	}
	/**
	 * @return the description displayed in the file chooser for raw files.
	 */
	public static String[] getDescriptions() {
		return new String[] {"raw quaternion data."};
	}
	/**
	 * @return the file extension for raw files.
	 */
	public static String[] getExtensions() {
		return new String[] { ".raw" };
	}
}
