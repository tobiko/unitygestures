package kinematics;

import java.io.File;

/**
 * this interface is implemented by classes who can create a kinematic model (see the kinematics.model package).
 */
public abstract interface KinematicModelService {
	/**
	 * should return the models' name
	 * @return the name of the model
	 */
	public abstract String getName();
	/**
	 * should create a kinematic model, which is a bunch of {@link Joint} objects, organized in a kinematic chain.
	 * @return a kinematic model.
	 */
	public abstract KinematicModel create();
	/**
	 * should create a kinematic model, which is a bunch of {@link Joint} objects, organized in a kinematic chain.
	 * offset data should be parsed from the file instead of being hard-coded.
	 * @param file, file with offset data
	 * @return a kinematic model
	 */
	public abstract KinematicModel create(File file);
}
