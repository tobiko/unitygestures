package kinematics;

import scene.Scene;
import streamservice.frames.KinematicFrame;
import walkingalgorithm.WalkingAlgorithm;

import com.spookengine.scenegraph.Node;
import com.spookengine.scenegraph.Spatial;
import com.spookengine.scenegraph.Trfm;

/**
 * instances of this class represent joints in the various kinematic models.
 * it is an extension of the {@link Spatial} class provided by the spookengine, which
 * provides local and world transformations (transformation matrices for translations and
 * rotations). please note that not necessarily every joint is assigned to a certain sensor,
 * instead, some joints are interpolated using data from other joints within the kinematic
 * chain. further, some joints might be used for extrapolating other joints (actually the parent in the
 * local hierarchy).
 * Interpolation is done within the {@link KinematicFrame} class, actually it consists rotating
 * the respective joint around the rotation axis of the parent, the rotation is weighted by the
 * {@link Joint#interpolationFactor}. Extrapolation works the other way round and is governed by
 * the {@link Joint#extrapolationFactor}.
 */
public class Joint extends Spatial {
	/**
	 * id of the sensor assigned to this joint, -1 in case of no assignment.
	 */
	public int sensorId;
	/**
	 * the radius of the bone used in the visualization of this joint, see the {@link Scene} class,
	 * especially the private visualizeSkeleton method.
	 */
	public float radius;
	/**
	 * this flag indicates if a joint has contact to the floor, this affects the visualization in the {@link Scene}
	 * class and is used in the {@link WalkingAlgorithm}.
	 */
	public boolean isContact;
	/**
	 * This factor governs the strength of the extrapolation of missing parent data.
	 */
	public float interpolationFactor = 0.0f;
	/**
	 * this factor determines the interpolation strength from parent transforms.
	 */
	public float extrapolationFactor = 0.0f;
	/**
	 * the name of this joint, also used to find it within the hierarchy.
	 */
	public String name;
	/**
	 * creates a new joint with the given name, initially the sensorId equal -1.
	 * @param name the name of the joint
	 */
	public Joint(String name) {
		super(name);
		this.name = name;
		this.sensorId = -1;
	}
	/**
	 * fetches a child of this joint based on the sensorId. this method works
	 * recursively and can be conveniently called on the root joint.
	 * @param sensorId the to be found sensorId
	 * @return a joint with the sensorId or null
	 */
	public Joint findChild(int sensorId) {
		if (this.sensorId == sensorId) {
			return this;
		}
		for (Node child : this.children) {
			if (child instanceof Joint) {
				Joint found = ((Joint) child).findChild(sensorId);
				if (found != null) {
					return found;
				}
			}
		}
		return null;
	}
	/**
	 * equal is based on the joints' name.
	 */
	public boolean equals(Object obj) {
		return ((obj instanceof Joint) && (((Joint) obj).name.equals(this.name)));
	}
	/**
	 * for safety reasons, some classes only work with copies of the actual kinematic model,
	 * for instance the {@link KinematicFrame} class, in these cases the clone method is called
	 * to obtain a copy.
	 */
	public Joint clone() {
		Joint clone = new Joint(this.name);
		clone.sensorId = this.sensorId;
		clone.radius = this.radius;
		clone.interpolationFactor = this.interpolationFactor;
		clone.extrapolationFactor = this.extrapolationFactor;

		clone.localTransform.setTo(this.localTransform);
		clone.worldTransform.setTo(this.worldTransform);

		if (this.bounds != null) {
			clone.setBounds(this.bounds.clone());
		}
		return clone;
	}
	/**
	 * as noted above, for visualization purposes only a copy of the kinematic model
	 * is used, to clone a whole hierarchy, this method can be used.
	 */
	public Joint cloneTree(Node parent) {
		Joint clone = this.clone();

		if (parent != null) {
			parent.attachChild(clone);
		}

		for (int i = 0; i < this.children.size(); ++i) {
			((Node) this.children.get(i)).cloneTree(clone);
		}
		return clone;
	}

	public String toString() {
		return this.name;
	}

	public Trfm getLocalTransform() {
		return this.localTransform;
	}

	public Trfm getWorldTransform() {
		return this.worldTransform;
	}
}