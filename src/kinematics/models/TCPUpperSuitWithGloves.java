package kinematics.models;

import java.io.File;
import java.util.HashMap;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import streamservice.frames.KinematicFrame;
import kinematics.Joint;
import kinematics.KinematicModel;
import kinematics.KinematicModelService;
import kinematics.models.ioutils.DistanceDataParser;
/**
 * Implementation of {@link KinematicModelService} interface that provides a kineamtic model
 * matching the combination of the suit with both gloves in the tcp configuration.
 * this class produces a kinematic chain of joints that represent the respective body part.
 * sensor addresses are assigned here, but can be changed via the gui.
 */
public class TCPUpperSuitWithGloves implements KinematicModelService {
	// XXX debug
	/**
	 * if true, no sensors are assigned to the spine trunk (hips, spine1, head), this values can also be
	 * changed via the gui.
	 */
	public final static boolean ignoreSpineTrunk = false;
	/**
	 * name of the model, as it is displayed in the gui.
	 */
	public static String modelName = "TCP: Upper Suit and Two 12-Sensor Gloves";
	// XXX these static values are used to initialize the scaling factors within the constructor
	public static float baseHandScalingFactor = .6f;//.75f;//1.0f;
	public static float baseArmScalingFactor = 1.0f;
	public static float baseUpperBodyScalingFactor = 1.0f;
	public static float baseLegScalingFactor = 1.0f;
	/**
	 * base radius of the joints, affects also the placement of the joint, this was intended as a convenient
	 * scaling constant.
	 */
	private float handScalingFactor;
	private float armScalingFactor;
	private float upperBodyScalingFactor;
	private float legScalingFactor;
	/**
	 * usual interpolation factor used to interpolate rotations of joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of the parent joint, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float interpolationFactor;
	/**
	 * usual extrapolation factor used to interpolate rotations of parent joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of a joint to its parent, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float extrapolationFactor;
	/**
	 * the number of models created by this service (used as id in the returned {@link KinematicModel}).
	 */
	int instanceNumber;

	public TCPUpperSuitWithGloves() {
		super();
//		this.handScalingFactor = 1.0f;
		this.handScalingFactor      = TCPUpperSuitWithGloves.baseHandScalingFactor;
		this.armScalingFactor       = TCPUpperSuitWithGloves.baseArmScalingFactor;
		this.legScalingFactor       = TCPUpperSuitWithGloves.baseLegScalingFactor;
		this.upperBodyScalingFactor = TCPUpperSuitWithGloves.baseUpperBodyScalingFactor;
		
		this.interpolationFactor = 0.5f;
		this.extrapolationFactor = 0.5f;
		this.instanceNumber = 0;
	}

	public String getName() {
		return TCPUpperSuitWithGloves.modelName;
	}
	/**
	 * creates the model. for the combined the model the mapping works in the following way (14.08.2014):</p>
	 * <table>
	 * <col width="50%"/>
	 * <col width="50%"/>
	 * <thead> <tr><th>Joint</th><th>sensorId</th></tr> <thead>
	 * <tbody>
     * <tr><td>Hips</td><td>31</td></tr>
     * <tr><td>Spine1</td><td>30</td></tr>
     * <tr><td>Head</td><td>29</td></tr>
     * <tr><td>LeftShoulder</td><td>25</td></tr>
     * <tr><td>LeftArm</td><td>24</td></tr>
     * <tr><td>LeftForeArm</td><td>23</td></tr>
     * <tr><td>RightShoulder</td><td>28</td></tr>
     * <tr><td>RightArm</td><td>27</td></tr>
     * <tr><td>RightForeArm</td><td>26</td></tr>
     * <tr><td>---</td><td>---</td></tr>
     * <tr><td>RightHand</td><td>12</td></tr>
     * <tr><td>RightThumbKnuckle1</td><td>13</td></tr>
     * <tr><td>RightThumbKnuckle2</td><td>14</td></tr>
     * <tr><td>RightThumbTip</td><td>15</td></tr>
     * <tr><td>RightIndexKnuckle1</td><td>16</td></tr>
     * <tr><td>RightIndexKnuckle2</td><td>17</td></tr>
     * <tr><td>RightMiddleKnuckle1</td><td>18</td></tr>
     * <tr><td>RightMiddleKnuckle2</td><td>19</td></tr>
     * <tr><td>RightRingKnuckle1</td><td>20</td></tr>
     * <tr><td>RightRingKnuckle2</td><td>21</td></tr>
     * <tr><td>RightLittleKnuckle1</td><td>22</td></tr>
     * <tr><td>---</td><td>---</td></tr>
     * <tr><td>LeftHand</td><td>0</td></tr>
     * <tr><td>LeftThumbKnuckle1</td><td>1</td></tr>
     * <tr><td>LeftThumbKnuckle2</td><td>2</td></tr>
     * <tr><td>LeftThumbTip</td><td>3</td></tr>
     * <tr><td>LeftIndexKnuckle1</td><td>4</td></tr>
     * <tr><td>LeftIndexKnuckle2</td><td>5</td></tr>
     * <tr><td>LeftMiddleKnuckle1</td><td>6</td></tr>
     * <tr><td>LeftMiddleKnuckle2</td><td>7</td></tr>
     * <tr><td>LeftRingKnuckle1</td><td>8</td></tr>
     * <tr><td>LeftRingKnuckle2</td><td>9</td></tr>
     * <tr><td>LeftLittleKnuckle1</td><td>10</td></tr>
  	 * </tbody>
	 * </table>
	 * </p>
	 * please note: legs are included, but since there is no sensor data available, they are not animated
	 */
	public KinematicModel create() {
		KinematicModel defaultModel = new KinematicModel(
				"TCP: Upper Suit and Two 12-Sensor Gloves "
						+ this.instanceNumber);
		this.instanceNumber += 1;

		Joint hips = new Joint("Hips");
		// XXX
		if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) hips.sensorId = 29;//= 31;
		hips.radius = 3.0F * this.upperBodyScalingFactor;

		Joint leftUpLeg = new Joint("LeftUpLeg");
		hips.attachChild(leftUpLeg);
		leftUpLeg.getLocalTransform().moveTo(3.0F * this.legScalingFactor, 0.0F, 0.0F).update();
		leftUpLeg.radius = 2.0F * this.legScalingFactor;

		Joint leftLeg = new Joint("LeftLeg");
		leftUpLeg.attachChild(leftLeg);
		leftLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F * this.legScalingFactor).update();
		leftLeg.radius = 2.0F * this.legScalingFactor;

		Joint leftFoot = new Joint("LeftFoot");
		leftLeg.attachChild(leftFoot);
		leftFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F * this.legScalingFactor).update();
		leftFoot.radius = 0.0F * this.legScalingFactor;

		Joint leftFootHeel = new Joint("LeftFootHeel");
		leftFoot.attachChild(leftFootHeel);
		leftFootHeel.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor).update();
		leftFootHeel.radius = 1.0F * this.legScalingFactor;

		Joint leftFootHeelOutside = new Joint("LeftFootHeelOutside");
		leftFoot.attachChild(leftFootHeelOutside);
		leftFootHeelOutside.getLocalTransform().moveTo(0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor)
				.update();
		leftFootHeelOutside.radius = 1.0F * this.legScalingFactor;

		Joint leftFootToe = new Joint("LeftFootToe");
		leftFootHeel.attachChild(leftFootToe);
		leftFootToe.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F).update();
		leftFootToe.radius = 1.0F * this.legScalingFactor;

		Joint leftFootToeOutside = new Joint("LeftFootToeOutside");
		leftFootHeelOutside.attachChild(leftFootToeOutside);
		leftFootToeOutside.getLocalTransform().moveTo(0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F)
				.update();
		leftFootToeOutside.radius = 1.0F * this.legScalingFactor;

		Joint rightUpLeg = new Joint("RightUpLeg");
		hips.attachChild(rightUpLeg);
		rightUpLeg.getLocalTransform().moveTo(-3.0F * this.legScalingFactor, 0.0F, 0.0F).update();
		rightUpLeg.radius = 2.0F * this.legScalingFactor;

		Joint rightLeg = new Joint("RightLeg");
		rightUpLeg.attachChild(rightLeg);
		rightLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F * this.legScalingFactor).update();
		rightLeg.radius = 2.0F * this.legScalingFactor;

		Joint rightFoot = new Joint("RightFoot");
		rightLeg.attachChild(rightFoot);
		rightFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F * this.legScalingFactor).update();
		rightFoot.radius = 0.0F;

		Joint rightFootHeel = new Joint("RightFootHeel");
		rightFoot.attachChild(rightFootHeel);
		rightFootHeel.getLocalTransform().moveTo(0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor).update();
		rightFootHeel.radius = 1.0F * this.legScalingFactor;

		Joint rightFootHeelOutside = new Joint("RightFootHeelOutside");
		rightFoot.attachChild(rightFootHeelOutside);
		rightFootHeelOutside.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor)
				.update();
		rightFootHeelOutside.radius = 1.0F * this.legScalingFactor;

		Joint rightFootToe = new Joint("RightFootToe");
		rightFootHeel.attachChild(rightFootToe);
		rightFootToe.getLocalTransform().moveTo(0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F).update();
		rightFootToe.radius = 1.0F * this.legScalingFactor;

		Joint rightFootToeOutside = new Joint("RightFootToeOutside");
		rightFootHeelOutside.attachChild(rightFootToeOutside);
		rightFootToeOutside.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F)
				.update();
		rightFootToeOutside.radius = 1.0F * this.legScalingFactor;

		Joint spine = new Joint("Spine");
		hips.attachChild(spine);
		spine.getLocalTransform().moveTo(0.0F, 1.0F * this.upperBodyScalingFactor, 3.0F * this.upperBodyScalingFactor).update();
		spine.radius = 1.0F * this.upperBodyScalingFactor;

		Joint spine1 = new Joint("Spine1");
		spine.attachChild(spine1);
		// XXX
		// XXX: like the head top: currently it is always disabled
		if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) spine1.sensorId = 31;//30;
		spine1.extrapolationFactor = this.extrapolationFactor;
		spine1.getLocalTransform().moveTo(0.0F, 0.0F, 3.0F * this.upperBodyScalingFactor).update();
		spine1.radius = 0.0F;

		Joint neck = new Joint("Neck");
		spine1.attachChild(neck);
		neck.interpolationFactor = this.interpolationFactor;
		neck.getLocalTransform().moveTo(0.0F, -1.0F * this.upperBodyScalingFactor, 15.0F * this.upperBodyScalingFactor).update();
		neck.radius = 0.0F;

		Joint head = new Joint("Head");
		neck.attachChild(head);
		// XXX
		// XXX: like the spine1: currently it is always disabled
		if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) head.sensorId = 30;//29;
		head.extrapolationFactor = this.extrapolationFactor;
		head.getLocalTransform().moveTo(0.0F, 0.0F, 6.5F * this.upperBodyScalingFactor).update();
		head.radius = 0.0F;

		Joint headTop = new Joint("HeadTop");
		head.attachChild(headTop);
		headTop.getLocalTransform().moveTo(0.0F, 0.0F, 4.0F * this.upperBodyScalingFactor).update();
		headTop.radius = 3.0F * this.upperBodyScalingFactor;

		Joint leftShoulder = new Joint("LeftShoulder");
		spine1.attachChild(leftShoulder);
		leftShoulder.sensorId = 25;
		leftShoulder.getLocalTransform().moveTo(3.0F * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, 13.0F * this.upperBodyScalingFactor).update();
		leftShoulder.radius = 0.0F;

		Joint leftArm = new Joint("LeftArm");
		leftShoulder.attachChild(leftArm);
		leftArm.sensorId = 24;
		leftArm.getLocalTransform().moveTo(5.0F * this.armScalingFactor, 0.0F, 0.0F).update();
		leftArm.radius = 1.5F * this.armScalingFactor;

		Joint leftForeArm = new Joint("LeftForeArm");
		leftArm.attachChild(leftForeArm);
		leftForeArm.sensorId = 23;
		leftForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F * this.armScalingFactor).update();
		leftForeArm.radius = 1.5F * this.armScalingFactor;

		Joint leftHand = new Joint("LeftHand");
		leftForeArm.attachChild(leftHand);
		leftHand.sensorId = 0;
		leftHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F * this.armScalingFactor).update();
		leftHand.radius = 1.0F * this.armScalingFactor;

		Joint leftCmc = new Joint("LeftCarpometacarpal");
		leftHand.attachChild(leftCmc);
		leftCmc.sensorId = 1;
		leftCmc.getLocalTransform()
				.moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		leftCmc.radius = (0.5F * this.handScalingFactor);

		Joint leftThumb1 = new Joint("LeftThumbKnuckle1");
		leftCmc.attachChild(leftThumb1);
		leftThumb1.sensorId = 2;
		leftThumb1.getLocalTransform()
				.moveTo(0.0F, -4.5F * this.handScalingFactor, -3.0F * this.handScalingFactor).update();
		leftThumb1.radius = (1.5F * this.handScalingFactor);

		Joint leftThumb2 = new Joint("LeftThumbKnuckle2");
		leftThumb1.attachChild(leftThumb2);
		leftThumb2.sensorId = 3;
		leftThumb2.getLocalTransform()
				.moveTo(0.0F, -2.5F * this.handScalingFactor, -2.5F * this.handScalingFactor).update();
		leftThumb2.radius = (1.0F * this.handScalingFactor);

		Joint leftThumbTip = new Joint("LeftThumbTip");
		leftThumb2.attachChild(leftThumbTip);
//		leftThumbTip.sensorId = 3;
		leftThumbTip.interpolationFactor = this.interpolationFactor;
		leftThumbTip.getLocalTransform()
				.moveTo(0.0F, -2.5F * this.handScalingFactor, -2.0F * this.handScalingFactor).update();
		leftThumbTip.radius = 0.0F;

		Joint leftIndex1 = new Joint("LeftIndexKnuckle1");
		leftHand.attachChild(leftIndex1);
		leftIndex1.sensorId = 4;
		leftIndex1.getLocalTransform()
				.moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		leftIndex1.radius = (1.5F * this.handScalingFactor);

		Joint leftIndex2 = new Joint("LeftIndexKnuckle2");
		leftIndex1.attachChild(leftIndex2);
		leftIndex2.sensorId = 5;
		leftIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		leftIndex2.radius = (1.0F * this.handScalingFactor);

		Joint leftIndex3 = new Joint("LeftIndexKnuckle3");
		leftIndex2.attachChild(leftIndex3);
		leftIndex3.interpolationFactor = this.interpolationFactor;
		leftIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -3.0F * this.handScalingFactor)
				.update();
		leftIndex3.radius = (0.5F * this.handScalingFactor);

		Joint leftIndexTip = new Joint("LeftIndexTip");
		leftIndex3.attachChild(leftIndexTip);
		leftIndexTip.interpolationFactor = this.interpolationFactor;
		leftIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		leftIndexTip.radius = 0.0F;

		Joint leftMiddle1 = new Joint("LeftMiddleKnuckle1");
		leftHand.attachChild(leftMiddle1);
		leftMiddle1.sensorId = 6;
		leftMiddle1.getLocalTransform()
				.moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		leftMiddle1.radius = (1.5F * this.handScalingFactor);

		Joint leftMiddle2 = new Joint("LeftMiddleKnuckle2");
		leftMiddle1.attachChild(leftMiddle2);
		leftMiddle2.sensorId = 7;
		leftMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		leftMiddle2.radius = (1.0F * this.handScalingFactor);

		Joint leftMiddle3 = new Joint("LeftMiddleKnuckle3");
		leftMiddle2.attachChild(leftMiddle3);
		leftMiddle3.interpolationFactor = this.interpolationFactor;
		leftMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		leftMiddle3.radius = (0.5F * this.handScalingFactor);

		Joint leftMiddleTip = new Joint("LeftMiddleTip");
		leftMiddle3.attachChild(leftMiddleTip);
		leftMiddleTip.interpolationFactor = this.interpolationFactor;
		leftMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		leftMiddleTip.radius = 0.0F;

		Joint leftRing1 = new Joint("LeftRingKnuckle1");
		leftHand.attachChild(leftRing1);
		leftRing1.sensorId = 8;
		leftRing1.getLocalTransform()
				.moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		leftRing1.radius = (1.5F * this.handScalingFactor);

		Joint leftRing2 = new Joint("LeftRingKnuckle2");
		leftRing1.attachChild(leftRing2);
		leftRing2.sensorId = 9;
		leftRing2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		leftRing2.radius = (1.0F * this.handScalingFactor);

		Joint leftRing3 = new Joint("LeftRingKnuckle3");
		leftRing2.attachChild(leftRing3);
		leftRing3.interpolationFactor = this.interpolationFactor;
		leftRing3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		leftRing3.radius = (0.5F * this.handScalingFactor);

		Joint leftRingTip = new Joint("LeftRingTip");
		leftRing3.attachChild(leftRingTip);
		leftRingTip.interpolationFactor = this.interpolationFactor;
		leftRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		leftRingTip.radius = 0.0F;

		Joint leftLittle1 = new Joint("LeftLittleKnuckle1");
		leftHand.attachChild(leftLittle1);
		leftLittle1.sensorId = 10;
		leftLittle1.getLocalTransform()
				.moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		leftLittle1.radius = (1.5F * this.handScalingFactor);

		Joint leftLittle2 = new Joint("LeftLittleKnuckle2");
		leftLittle1.attachChild(leftLittle2);
//		XXX
//		leftLittle2.sensorId = 11;
		leftLittle2.interpolationFactor = this.interpolationFactor;
		leftLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		leftLittle2.radius = (1.0F * this.handScalingFactor);

		Joint leftLittle3 = new Joint("LeftLittleKnuckle3");
		leftLittle2.attachChild(leftLittle3);
		leftLittle3.interpolationFactor = this.interpolationFactor;
		leftLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		leftLittle3.radius = (0.5F * this.handScalingFactor);

		Joint leftLittleTip = new Joint("LeftLittleTip");
		leftLittle3.attachChild(leftLittleTip);
		leftLittleTip.interpolationFactor = this.interpolationFactor;
		leftLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		leftLittleTip.radius = 0.0F;

		Joint rightShoulder = new Joint("RightShoulder");
		spine1.attachChild(rightShoulder);
		rightShoulder.sensorId = 28;
		rightShoulder.getLocalTransform().moveTo(-3.0F * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, 13.0F * this.upperBodyScalingFactor).update();
		rightShoulder.radius = 0.0F;

		Joint rightArm = new Joint("RightArm");
		rightShoulder.attachChild(rightArm);
		rightArm.sensorId = 27;
		rightArm.getLocalTransform().moveTo(-5.0F * this.armScalingFactor, 0.0F, 0.0F).update();
		rightArm.radius = 1.5F * this.armScalingFactor;

		Joint rightForeArm = new Joint("RightForeArm");
		rightArm.attachChild(rightForeArm);
		rightForeArm.sensorId = 26;
		rightForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F * this.armScalingFactor).update();
		rightForeArm.radius = 1.5F * this.armScalingFactor;

		Joint rightHand = new Joint("RightHand");
		rightForeArm.attachChild(rightHand);
		rightHand.sensorId = 12;
		rightHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F * this.armScalingFactor).update();
		rightHand.radius = 1.0F * this.armScalingFactor;

		Joint rightCmc = new Joint("RightCarpometacarpal");
		rightHand.attachChild(rightCmc);
		rightCmc.sensorId = 13;
		rightCmc.getLocalTransform()
				.moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		rightCmc.radius = (0.5F * this.handScalingFactor);

		Joint rightThumb1 = new Joint("RightThumbKnuckle1");
		rightCmc.attachChild(rightThumb1);
		rightThumb1.sensorId = 14;
		rightThumb1.getLocalTransform()
				.moveTo(0.0F, -4.5F * this.handScalingFactor, -3.0F * this.handScalingFactor).update();
		rightThumb1.radius = (1.5F * this.handScalingFactor);

		Joint rightThumb2 = new Joint("RightThumbKnuckle2");
		rightThumb1.attachChild(rightThumb2);
		rightThumb2.sensorId = 15;
		rightThumb2.getLocalTransform()
				.moveTo(0.0F, -2.5F * this.handScalingFactor, -2.5F * this.handScalingFactor).update();
		rightThumb2.radius = (1.0F * this.handScalingFactor);

		Joint rightThumbTip = new Joint("RightThumbTip");
		rightThumb2.attachChild(rightThumbTip);
//		rightThumbTip.sensorId = 15;
		rightThumbTip.interpolationFactor = this.interpolationFactor;
		rightThumbTip.getLocalTransform()
				.moveTo(0.0F, -2.5F * this.handScalingFactor, -2.0F * this.handScalingFactor).update();
		rightThumbTip.radius = 0.0F;

		Joint rightIndex1 = new Joint("RightIndexKnuckle1");
		rightHand.attachChild(rightIndex1);
		rightIndex1.sensorId = 16;
		rightIndex1.getLocalTransform()
				.moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		rightIndex1.radius = (1.5F * this.handScalingFactor);

		Joint rightIndex2 = new Joint("RightIndexKnuckle2");
		rightIndex1.attachChild(rightIndex2);
		rightIndex2.sensorId = 17;
		rightIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		rightIndex2.radius = (1.0F * this.handScalingFactor);

		Joint rightIndex3 = new Joint("RightIndexKnuckle3");
		rightIndex2.attachChild(rightIndex3);
		rightIndex3.interpolationFactor = this.interpolationFactor;
		rightIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -3.0F * this.handScalingFactor)
				.update();
		rightIndex3.radius = (0.5F * this.handScalingFactor);

		Joint rightIndexTip = new Joint("RightIndexTip");
		rightIndex3.attachChild(rightIndexTip);
		rightIndexTip.interpolationFactor = this.interpolationFactor;
		rightIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		rightIndexTip.radius = 0.0F;

		Joint rightMiddle1 = new Joint("RightMiddleKnuckle1");
		rightHand.attachChild(rightMiddle1);
		rightMiddle1.sensorId = 18;
		rightMiddle1.getLocalTransform()
				.moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		rightMiddle1.radius = (1.5F * this.handScalingFactor);

		Joint rightMiddle2 = new Joint("RightMiddleKnuckle2");
		rightMiddle1.attachChild(rightMiddle2);
		rightMiddle2.sensorId = 19;
		rightMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		rightMiddle2.radius = (1.0F * this.handScalingFactor);

		Joint rightMiddle3 = new Joint("RightMiddleKnuckle3");
		rightMiddle2.attachChild(rightMiddle3);
		rightMiddle3.interpolationFactor = this.interpolationFactor;
		rightMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		rightMiddle3.radius = (0.5F * this.handScalingFactor);

		Joint rightMiddleTip = new Joint("RightMiddleTip");
		rightMiddle3.attachChild(rightMiddleTip);
		rightMiddleTip.interpolationFactor = this.interpolationFactor;
		rightMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		rightMiddleTip.radius = 0.0F;

		Joint rightRing1 = new Joint("RightRingKnuckle1");
		rightHand.attachChild(rightRing1);
		rightRing1.sensorId = 20;
		rightRing1.getLocalTransform()
				.moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		rightRing1.radius = (1.5F * this.handScalingFactor);

		Joint rightRing2 = new Joint("RightRingKnuckle2");
		rightRing1.attachChild(rightRing2);
		rightRing2.sensorId = 21;
		rightRing2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor)
				.update();
		rightRing2.radius = (1.0F * this.handScalingFactor);

		Joint rightRing3 = new Joint("RightRingKnuckle3");
		rightRing2.attachChild(rightRing3);
		rightRing3.interpolationFactor = this.interpolationFactor;
		rightRing3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		rightRing3.radius = (0.5F * this.handScalingFactor);

		Joint rightRingTip = new Joint("RightRingTip");
		rightRing3.attachChild(rightRingTip);
		rightRingTip.interpolationFactor = this.interpolationFactor;
		rightRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		rightRingTip.radius = 0.0F;

		Joint rightLittle1 = new Joint("RightLittleKnuckle1");
		rightHand.attachChild(rightLittle1);
		rightLittle1.sensorId = 22;
		rightLittle1.getLocalTransform()
				.moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		rightLittle1.radius = (1.5F * this.handScalingFactor);

		Joint rightLittle2 = new Joint("RightLittleKnuckle2");
		rightLittle1.attachChild(rightLittle2);
//		XXX
//		rightLittle2.sensorId = 23;
		rightLittle2.interpolationFactor = this.interpolationFactor;
		rightLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor)
				.update();
		rightLittle2.radius = (1.0F * this.handScalingFactor);

		Joint rightLittle3 = new Joint("RightLittleKnuckle3");
		rightLittle2.attachChild(rightLittle3);
		rightLittle3.interpolationFactor = this.interpolationFactor;
		rightLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		rightLittle3.radius = (0.5F * this.handScalingFactor);

		Joint rightLittleTip = new Joint("RightLittleTip");
		rightLittle3.attachChild(rightLittleTip);
		rightLittleTip.interpolationFactor = this.interpolationFactor;
		rightLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor)
				.update();
		rightLittleTip.radius = 0.0F;

		defaultModel.attachChild(hips);

		defaultModel.localToWorldTree();

		return defaultModel;
	}

	@Override
	public KinematicModel create(File file) {
		KinematicModel defaultModel = new KinematicModel(
				"TCP: Upper Suit and Two 12-Sensor Gloves "
						+ this.instanceNumber);
		this.instanceNumber += 1;

		HashMap<String, DescriptiveStatistics> distanceData = DistanceDataParser.parseFile(file);
		String key;
		
		Joint hips = new Joint("Hips");
		// XXX
		if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) hips.sensorId = 31;
		hips.radius = 3.0F * this.upperBodyScalingFactor;

		float hipDistance = 0.0F;
		float additionalSpineLength = 0.0F;
		key = "hip_right-hip_left";
		if (distanceData.containsKey(key)) {
			hipDistance = (float) distanceData.get(key).getMean();
		}
		key = "hip_center-hip_left";
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.acos(hipDistance * .5F / distance);
			additionalSpineLength = ((float) Math.sin(angle)) * distance;
		}
		
		Joint leftUpLeg = new Joint("LeftUpLeg");
		hips.attachChild(leftUpLeg);
		key = "hip_center-hip_left";
		if (distanceData.containsKey(key)) {
//			XXX the distance value obtained from the kinect does not reflect a straight line, but the
//			hypotenuse in a right triangle
//			float distance = (float) distanceData.get(key).getMean();
//			rightUpLeg.getLocalTransform().moveTo(-distance, 0.0F, 0.0F).update();
//			float distance = (float) distanceData.get(key).getMean();
//			float adjacent = (float) Math.cos(Math.toRadians(45.0d)) * distance;
//			leftUpLeg.getLocalTransform().moveTo(adjacent, 0.0F, 0.0F).update();
			leftUpLeg.getLocalTransform().moveTo(hipDistance * .5F * this.legScalingFactor, 0.0F, 0.0F).update();
		} else {
			leftUpLeg.getLocalTransform().moveTo(3.0F * this.legScalingFactor, 0.0F, 0.0F).update();
		}
		leftUpLeg.radius = 2.0F;

		Joint leftLeg = new Joint("LeftLeg");
		leftUpLeg.attachChild(leftLeg);
		key = "hip_left-knee_left";
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
			leftLeg.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.legScalingFactor).update();
		} else {
			leftLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F * this.legScalingFactor).update();
		}
		leftLeg.radius = 2.0F * this.legScalingFactor;

		Joint leftFoot = new Joint("LeftFoot");
		leftLeg.attachChild(leftFoot);
		key = "knee_left-ankle_left";
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
			leftFoot.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.legScalingFactor).update();
		} else {
			leftFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F * this.legScalingFactor).update();
		}
		leftFoot.radius = 0.0F;

		Joint leftFootHeel = new Joint("LeftFootHeel");
		leftFoot.attachChild(leftFootHeel);
		leftFootHeel.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor).update();
		leftFootHeel.radius = 1.0F * this.legScalingFactor;

		Joint leftFootHeelOutside = new Joint("LeftFootHeelOutside");
		leftFoot.attachChild(leftFootHeelOutside);
		leftFootHeelOutside.getLocalTransform().moveTo(0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor)
				.update();
		leftFootHeelOutside.radius = 1.0F * this.legScalingFactor;

		Joint leftFootToe = new Joint("LeftFootToe");
		leftFootHeel.attachChild(leftFootToe);
		leftFootToe.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F).update();
		leftFootToe.radius = 1.0F * this.legScalingFactor;

		Joint leftFootToeOutside = new Joint("LeftFootToeOutside");
		leftFootHeelOutside.attachChild(leftFootToeOutside);
		leftFootToeOutside.getLocalTransform().moveTo(0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F)
				.update();
		leftFootToeOutside.radius = 1.0F * this.legScalingFactor;

		Joint rightUpLeg = new Joint("RightUpLeg");
		hips.attachChild(rightUpLeg);
		key = "hip_center-hip_right";
		if (distanceData.containsKey(key)) {
//			XXX the distance value obtained from the kinect does not reflect a straight line, but the
//			hypotenuse in a right triangle
//			float distance = (float) distanceData.get(key).getMean();
//			rightUpLeg.getLocalTransform().moveTo(-distance, 0.0F, 0.0F).update();
//			float distance = (float) distanceData.get(key).getMean();
//			float adjacent = (float) Math.cos(Math.toRadians(45.0d)) * distance;
//			rightUpLeg.getLocalTransform().moveTo(-adjacent, 0.0F, 0.0F).update();
			rightUpLeg.getLocalTransform().moveTo(-hipDistance * .5F * this.legScalingFactor, 0.0F, 0.0F).update();
		} else {
			rightUpLeg.getLocalTransform().moveTo(-3.0F * this.legScalingFactor, 0.0F, 0.0F).update();
		}
		rightUpLeg.radius = 2.0F * this.legScalingFactor;

		Joint rightLeg = new Joint("RightLeg");
		rightUpLeg.attachChild(rightLeg);
		key = "hip_right-knee_right";
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
			rightLeg.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.legScalingFactor).update();
		} else {
			rightLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F * this.legScalingFactor).update();
		}
		rightLeg.radius = 2.0F * this.legScalingFactor;

		Joint rightFoot = new Joint("RightFoot");
		rightLeg.attachChild(rightFoot);
		key = "knee_right-ankle_right";
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
			rightFoot.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.legScalingFactor).update();
		} else {
			rightFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F * this.legScalingFactor).update();	
		}
		rightFoot.radius = 0.0F;

		Joint rightFootHeel = new Joint("RightFootHeel");
		rightFoot.attachChild(rightFootHeel);
		rightFootHeel.getLocalTransform().moveTo(0.5F, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor).update();
		rightFootHeel.radius = 1.0F * this.legScalingFactor;

		Joint rightFootHeelOutside = new Joint("RightFootHeelOutside");
		rightFoot.attachChild(rightFootHeelOutside);
		rightFootHeelOutside.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, 1.5F * this.legScalingFactor, -3.5F * this.legScalingFactor)
				.update();
		rightFootHeelOutside.radius = 1.0F * this.legScalingFactor;

		Joint rightFootToe = new Joint("RightFootToe");
		rightFootHeel.attachChild(rightFootToe);
		rightFootToe.getLocalTransform().moveTo(0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F).update();
		rightFootToe.radius = 1.0F * this.legScalingFactor;

		Joint rightFootToeOutside = new Joint("RightFootToeOutside");
		rightFootHeelOutside.attachChild(rightFootToeOutside);
		rightFootToeOutside.getLocalTransform().moveTo(-0.5F * this.legScalingFactor, -5.5F * this.legScalingFactor, 0.0F)
				.update();
		rightFootToeOutside.radius = 1.0F * this.legScalingFactor;
		/// upper body begins here
		Joint spine = new Joint("Spine");
		hips.attachChild(spine);
		key = "spine-hip_center";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(3.0d / 1.0d);
			float adjacent = (float) Math.abs(Math.cos(angle) * distance);
			float opposite = (float) Math.abs(Math.sin(angle) * distance);
			spine.getLocalTransform().moveTo(0.0F, adjacent * this.upperBodyScalingFactor, opposite * this.upperBodyScalingFactor).update();
		} else {
			spine.getLocalTransform().moveTo(0.0F, 1.0F * this.upperBodyScalingFactor, 3.0F * this.upperBodyScalingFactor).update();
		}
		spine.radius = 1.0F * this.upperBodyScalingFactor;
		// XXX some helper variables to keep the original proportions
		double defSpineLength = Math.sqrt(0.0d * 0.0d + 0.0d * 0.0d + 3.0d * 3.0d);
		double defSpine1Length = Math.sqrt(0.0d * 0.0d + 1.0d * 1.0d + 15.0d * 15.0d);
		double defTotalSpineLength = defSpine1Length + defSpineLength;
		float spineFraction = (float) (defSpineLength / defTotalSpineLength);
		float spine1Fraction = (float) (defSpine1Length / defTotalSpineLength);
		
		Joint spine1 = new Joint("Spine1");
		spine.attachChild(spine1);
		// XXX
		// XXX: currently always disabled
		//if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) spine1.sensorId = 30;
		spine1.extrapolationFactor = this.extrapolationFactor;
		key = "shoulder_center-spine";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean() * spineFraction;
			spine1.getLocalTransform().moveTo(0.0F, 0.0F, distance * this.upperBodyScalingFactor).update();
		} else {
			spine1.getLocalTransform().moveTo(0.0F, 0.0F, 3.0F * this.upperBodyScalingFactor).update();
		}
		spine1.radius = 0.0F;

		Joint neck = new Joint("Neck");
		spine1.attachChild(neck);
		neck.interpolationFactor = this.interpolationFactor;
		key = "shoulder_center-spine";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) (distanceData.get(key).getMean() + additionalSpineLength) * spine1Fraction;
			float angle = (float) Math.atan(15.0d / -1.0d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.abs(Math.cos(angle) * distance);
			float opposite = (float) Math.abs(Math.sin(angle) * distance);
			neck.getLocalTransform().moveTo(0.0F, -adjacent * this.upperBodyScalingFactor, opposite * this.upperBodyScalingFactor).update();
		} else {
			neck.getLocalTransform().moveTo(0.0F, -1.0F * this.upperBodyScalingFactor, 15.0F * this.upperBodyScalingFactor).update();
		}
		neck.radius = 0.0F;

		Joint head = new Joint("Head");
		neck.attachChild(head);
		// XXX
		// XXX: currently always disabled
		// if (!TCPUpperSuitWithGloves.ignoreSpineTrunk) head.sensorId = 29;
		head.extrapolationFactor = this.extrapolationFactor;
		key = "head-shoulder_center";
		if (distanceData.containsKey(key)) {
			// XXX this had to be adapted since it should reflect the distance between chin and neck and not head-center and neck...
//			head.getLocalTransform().moveTo(0.0F, 0.0F, (float) distanceData.get(key).getMean()).update();
			head.getLocalTransform().moveTo(0.0F, 0.0F, (float) distanceData.get(key).getMean() * .66f * this.upperBodyScalingFactor).update();
		} else {
			head.getLocalTransform().moveTo(0.0F, 0.0F, 6.5F * this.upperBodyScalingFactor).update();
		}
		head.radius = 0.0F;

		Joint headTop = new Joint("HeadTop");
		head.attachChild(headTop);
		headTop.getLocalTransform().moveTo(0.0F, 0.0F, 4.0F * this.upperBodyScalingFactor).update();
		headTop.radius = 3.0F * this.upperBodyScalingFactor;

		key = "shoulder_left-shoulder_right";
		float shoulderwidth = 0.0F;
		if (distanceData.containsKey(key)) {
			shoulderwidth = (float) distanceData.get(key).getMean();
		}
		key = "shoulder_center-shoulder_left";
		float leftShoulderFraction1 = 0.0F;
		float leftShoulderFraction2 = 0.0F;
		float spineOffSet = 0.0F;
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
//			leftShoulderFraction1 = 3.0F / 8.0F * distance;
//			leftShoulderFraction2 = 5.0F / 8.0F * distance;
			leftShoulderFraction1 = 3.0F / 8.0F * shoulderwidth * .5F;
			leftShoulderFraction2 = 5.0F / 8.0F * shoulderwidth * .5F;
			float hypotenuse = distance;
			float adjacent = shoulderwidth * .5F;
			float angle = (float) Math.acos(adjacent / hypotenuse);
			spineOffSet = ((float) Math.sin(angle)) * hypotenuse;
		}
		
		
		Joint leftShoulder = new Joint("LeftShoulder");
		spine1.attachChild(leftShoulder);
		leftShoulder.sensorId = 25;
		key = "shoulder_center-spine";
		if (distanceData.containsKey(key)) {
			// unstlye, but seems more or less appropriate
//			float distance = (float) distanceData.get(key).getMean() * spine1Fraction;
//			float angle = (float) Math.atan(leftShoulderFraction1 / distance);
//			float hypotenuse = leftShoulderFraction1 / ((float) Math.sin(angle));
//			leftShoulder.getLocalTransform().moveTo(leftShoulderFraction1, -1.0F, hypotenuse).update();
			float distance = (float) (distanceData.get(key).getMean() + additionalSpineLength) * spine1Fraction - spineOffSet;
			float angle = (float) Math.atan(leftShoulderFraction1 / distance);
			float hypotenuse = leftShoulderFraction1 / ((float) Math.sin(angle));
			leftShoulder.getLocalTransform().moveTo(leftShoulderFraction1 * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, hypotenuse * this.upperBodyScalingFactor).update();
		} else {
			leftShoulder.getLocalTransform().moveTo(3.0F * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, 13.0F * this.upperBodyScalingFactor).update();
		}
		leftShoulder.radius = 0.0F;

		Joint leftArm = new Joint("LeftArm");
		leftShoulder.attachChild(leftArm);
		leftArm.sensorId = 24;
		if (distanceData.containsKey(key)) {
			leftArm.getLocalTransform().moveTo(leftShoulderFraction2 * this.armScalingFactor, 0.0F, 0.0F).update();
		} else {	
			leftArm.getLocalTransform().moveTo(5.0F * this.armScalingFactor, 0.0F, 0.0F).update();
		}
		leftArm.radius = 1.5F * this.armScalingFactor;

		Joint leftForeArm = new Joint("LeftForeArm");
		leftArm.attachChild(leftForeArm);
		leftForeArm.sensorId = 23;
		key = "shoulder_left-elbow_left";
		if (distanceData.containsKey(key)) {
			leftForeArm.getLocalTransform().moveTo(0.0F, 0.0F, (float) -distanceData.get(key).getMean() * this.armScalingFactor).update();
		} else {
			leftForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F * this.armScalingFactor).update();
		}
		leftForeArm.radius = 1.5F * this.armScalingFactor;
		// left hand goes here...
		Joint leftHand = new Joint("LeftHand");
		leftForeArm.attachChild(leftHand);
		leftHand.sensorId = 0;
		key = "elbow_left-wrist_left";
		if (distanceData.containsKey(key)) {
			leftHand.getLocalTransform().moveTo(0.0F, 0.0F, (float) -distanceData.get(key).getMean() * this.armScalingFactor).update();
		} else {
			leftHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F * this.armScalingFactor).update();
		}
		leftHand.radius = 1.0F * this.armScalingFactor;
		// left thumb
		Joint leftCmc = new Joint("LeftCarpometacarpal");
		leftHand.attachChild(leftCmc);
		leftCmc.sensorId = 1;
		key = "LeftHand-LeftCarpometacarpal";
		if (distanceData.containsKey(key)) {
//			// y -> adjacent
//			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-1.0d / -1.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			leftCmc.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			leftCmc.getLocalTransform().moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		} else {
			leftCmc.getLocalTransform().moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		}
		leftCmc.radius = (0.5F * this.handScalingFactor);

		Joint leftThumb1 = new Joint("LeftThumbKnuckle1");
		leftCmc.attachChild(leftThumb1);
		leftThumb1.sensorId = 2;
		key = "LeftCarpometacarpal-LeftThumbKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-3.0d / -4.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			leftThumb1.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			leftThumb1.getLocalTransform().moveTo(0.0F, -4.5F * this.handScalingFactor, -3.0F * this.handScalingFactor).update();
		}
		leftThumb1.radius = (1.5F * this.handScalingFactor);

		Joint leftThumb2 = new Joint("LeftThumbKnuckle2");
		leftThumb1.attachChild(leftThumb2);
		leftThumb2.sensorId = 3;
		key = "LeftThumbKnuckle1-LeftThumbKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-2.5d / -2.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			leftThumb2.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			leftThumb2.getLocalTransform().moveTo(0.0F, -2.5F * this.handScalingFactor, -2.5F * this.handScalingFactor).update();
		}
		leftThumb2.radius = (1.0F * this.handScalingFactor);

		Joint leftThumbTip = new Joint("LeftThumbTip");
		leftThumb2.attachChild(leftThumbTip);
//		leftThumbTip.sensorId = 3;
		leftThumbTip.interpolationFactor = this.interpolationFactor;
		key = "LeftThumbKnuckle2-LeftThumbTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-2.0d / -2.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			leftThumbTip.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			leftThumbTip.getLocalTransform().moveTo(0.0F, -2.5F * this.handScalingFactor, -2.0F * this.handScalingFactor).update();
		}
		leftThumbTip.radius = 0.0F;
		// left index finger
		Joint leftIndex1 = new Joint("LeftIndexKnuckle1");
		leftHand.attachChild(leftIndex1);
		leftIndex1.sensorId = 4;
		key = "LeftHand-LeftIndexKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			leftIndex1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			leftIndex1.getLocalTransform().moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		} else {
			leftIndex1.getLocalTransform().moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		}
		leftIndex1.radius = (1.5F * this.handScalingFactor);

		Joint leftIndex2 = new Joint("LeftIndexKnuckle2");
		leftIndex1.attachChild(leftIndex2);
		leftIndex2.sensorId = 5;
		key = "LeftIndexKnuckle1-LeftIndexKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		leftIndex2.radius = (1.0F * this.handScalingFactor);

		Joint leftIndex3 = new Joint("LeftIndexKnuckle3");
		leftIndex2.attachChild(leftIndex3);
		leftIndex3.interpolationFactor = this.interpolationFactor;
		key = "LeftIndexKnuckle2-LeftIndexKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -3.0F * this.handScalingFactor).update();
		}
		leftIndex3.radius = (0.5F * this.handScalingFactor);

		Joint leftIndexTip = new Joint("LeftIndexTip");
		leftIndex3.attachChild(leftIndexTip);
		leftIndexTip.interpolationFactor = this.interpolationFactor;
		key = "LeftIndexKnuckle3-LeftIndexTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		leftIndexTip.radius = 0.0F;

		Joint leftMiddle1 = new Joint("LeftMiddleKnuckle1");
		leftHand.attachChild(leftMiddle1);
		leftMiddle1.sensorId = 6;
		key = "LeftHand-LeftMiddleKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			leftMiddle1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			leftMiddle1.getLocalTransform().moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		} else {
			leftMiddle1.getLocalTransform().moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		}
		
		leftMiddle1.radius = (1.5F * this.handScalingFactor);

		Joint leftMiddle2 = new Joint("LeftMiddleKnuckle2");
		leftMiddle1.attachChild(leftMiddle2);
		leftMiddle2.sensorId = 7;
		key = "LeftMiddleKnuckle1-LeftMiddleKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		leftMiddle2.radius = (1.0F * this.handScalingFactor);

		Joint leftMiddle3 = new Joint("LeftMiddleKnuckle3");
		leftMiddle2.attachChild(leftMiddle3);
		leftMiddle3.interpolationFactor = this.interpolationFactor;
		key = "LeftMiddleKnuckle2-LeftMiddleKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		leftMiddle3.radius = (0.5F * this.handScalingFactor);

		Joint leftMiddleTip = new Joint("LeftMiddleTip");
		leftMiddle3.attachChild(leftMiddleTip);
		leftMiddleTip.interpolationFactor = this.interpolationFactor;
		key = "LeftMiddleKnuckle3-LeftMiddleTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		leftMiddleTip.radius = 0.0F;

		Joint leftRing1 = new Joint("LeftRingKnuckle1");
		leftHand.attachChild(leftRing1);
		leftRing1.sensorId = 8;
		key = "LeftHand-LeftRingKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			leftRing1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			leftRing1.getLocalTransform().moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		} else {
			leftRing1.getLocalTransform().moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		}
		
		leftRing1.radius = (1.5F * this.handScalingFactor);

		Joint leftRing2 = new Joint("LeftRingKnuckle2");
		leftRing1.attachChild(leftRing2);
		leftRing2.sensorId = 9;
		key = "LeftRingKnuckle1-LeftRingKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftRing2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftRing2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		leftRing2.radius = (1.0F * this.handScalingFactor);

		Joint leftRing3 = new Joint("LeftRingKnuckle3");
		leftRing2.attachChild(leftRing3);
		leftRing3.interpolationFactor = this.interpolationFactor;
		key = "LeftRingKnuckle2-LeftRingKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftRing3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftRing3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		leftRing3.radius = (0.5F * this.handScalingFactor);

		Joint leftRingTip = new Joint("LeftRingTip");
		leftRing3.attachChild(leftRingTip);
		leftRingTip.interpolationFactor = this.interpolationFactor;
		key = "LeftRingKnuckle3-LeftRingTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		leftRingTip.radius = 0.0F;

		Joint leftLittle1 = new Joint("LeftLittleKnuckle1");
		leftHand.attachChild(leftLittle1);
		leftLittle1.sensorId = 10;
		key = "LeftHand-LeftLittleKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			leftLittle1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			leftLittle1.getLocalTransform().moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		} else {
			leftLittle1.getLocalTransform().moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		}
		leftLittle1.radius = (1.5F * this.handScalingFactor);

		Joint leftLittle2 = new Joint("LeftLittleKnuckle2");
		leftLittle1.attachChild(leftLittle2);
//		XXX
//		leftLittle2.sensorId = 11;
		leftLittle2.interpolationFactor = this.interpolationFactor;
		key = "LeftLittleKnuckle1-LeftLittleKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		leftLittle2.radius = (1.0F * this.handScalingFactor);

		Joint leftLittle3 = new Joint("LeftLittleKnuckle3");
		leftLittle2.attachChild(leftLittle3);
		leftLittle3.interpolationFactor = this.interpolationFactor;
		key = "LeftLittleKnuckle2-LeftLittleKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		leftLittle3.radius = (0.5F * this.handScalingFactor);

		Joint leftLittleTip = new Joint("LeftLittleTip");
		leftLittle3.attachChild(leftLittleTip);
		leftLittleTip.interpolationFactor = this.interpolationFactor;
		key = "LeftLittleKnuckle3-LeftLittleTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			leftLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			leftLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		leftLittleTip.radius = 0.0F;

		key = "shoulder_center-shoulder_right";
		float rightShoulderFraction1 = 0.0F;
		float rightShoulderFraction2 = 0.0F;
		if (distanceData.containsKey(key)) {
			float distance = (float) distanceData.get(key).getMean();
//			rightShoulderFraction1 = 3.0F / 8.0F * distance;
//			rightShoulderFraction2 = 5.0F / 8.0F * distance;
			rightShoulderFraction1 = 3.0F / 8.0F * shoulderwidth * .5F;
			rightShoulderFraction2 = 5.0F / 8.0F * shoulderwidth * .5F;
			float hypotenuse = distance;
			float adjacent = shoulderwidth * .5F;
			float angle = (float) Math.acos(adjacent / hypotenuse);
			spineOffSet = ((float) Math.sin(angle)) * hypotenuse;
		}
		
		Joint rightShoulder = new Joint("RightShoulder");
		spine1.attachChild(rightShoulder);
		rightShoulder.sensorId = 28;
		key = "shoulder_center-spine";
		if (distanceData.containsKey(key)) {
			// unstlye, but seems more or less appropriate
//			float distance = (float) distanceData.get(key).getMean() * spine1Fraction;
//			float angle = (float) Math.atan(leftShoulderFraction1 / distance);
//			float hypotenuse = rightShoulderFraction1 / ((float) Math.sin(angle));
//			rightShoulder.getLocalTransform().moveTo(-rightShoulderFraction1, -1.0F, hypotenuse).update();
			float distance = (float) (distanceData.get(key).getMean() + additionalSpineLength) * spine1Fraction - spineOffSet;
			float angle = (float) Math.atan(leftShoulderFraction1 / distance);
			float hypotenuse = rightShoulderFraction1 / ((float) Math.sin(angle));
			rightShoulder.getLocalTransform().moveTo(-rightShoulderFraction1 * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, hypotenuse * this.upperBodyScalingFactor).update();
		} else {
			rightShoulder.getLocalTransform().moveTo(-3.0F * this.upperBodyScalingFactor, -1.0F * this.upperBodyScalingFactor, 13.0F * this.upperBodyScalingFactor).update();
		}
		rightShoulder.radius = 0.0F;

		Joint rightArm = new Joint("RightArm");
		rightShoulder.attachChild(rightArm);
		rightArm.sensorId = 27;
		if (distanceData.containsKey(key)) {
			rightArm.getLocalTransform().moveTo(-rightShoulderFraction2 * this.armScalingFactor, 0.0F, 0.0F).update();
		} else {
			rightArm.getLocalTransform().moveTo(-5.0F * this.armScalingFactor, 0.0F, 0.0F).update();
		}
		rightArm.radius = 1.5F * this.armScalingFactor;

		Joint rightForeArm = new Joint("RightForeArm");
		rightArm.attachChild(rightForeArm);
		rightForeArm.sensorId = 26;
		key = "shoulder_right-elbow_right";
		if (distanceData.keySet().contains(key)) {
			rightForeArm.getLocalTransform().moveTo(0.0F, 0.0F, (float) -distanceData.get(key).getMean() * this.armScalingFactor).update();
		} else {
			rightForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F * this.armScalingFactor).update();
		}
		rightForeArm.radius = 1.5F;

		Joint rightHand = new Joint("RightHand");
		rightForeArm.attachChild(rightHand);
		rightHand.sensorId = 12;
		key = "elbow_right-wrist_right";
		if (distanceData.keySet().contains(key)) {
			rightHand.getLocalTransform().moveTo(0.0F, 0.0F, (float) -distanceData.get(key).getMean() * this.armScalingFactor).update();
		} else {
			rightHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F * this.armScalingFactor).update();
		}
//		rightHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F * this.armScalingFactor).update();
		rightHand.radius = 1.0F * this.armScalingFactor;
		// right hand goes here...
		Joint rightCmc = new Joint("RightCarpometacarpal");
		rightHand.attachChild(rightCmc);
		rightCmc.sensorId = 13;
		key = "RightHand-RightCarpometacarpal";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			rightCmc.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			rightCmc.getLocalTransform().moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		} else {
			rightCmc.getLocalTransform().moveTo(0.0F, -1.5F * this.handScalingFactor, -1.0F * this.handScalingFactor).update();
		}
		rightCmc.radius = (0.5F * this.handScalingFactor);

		Joint rightThumb1 = new Joint("RightThumbKnuckle1");
		rightCmc.attachChild(rightThumb1);
		rightThumb1.sensorId = 14;
		key = "RightCarpometacarpal-RightThumbKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-3.0d / -4.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			rightThumb1.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			rightThumb1.getLocalTransform().moveTo(0.0F, -4.5F * this.handScalingFactor, -3.0F * this.handScalingFactor).update();
		}
		rightThumb1.radius = (1.5F * this.handScalingFactor);

		Joint rightThumb2 = new Joint("RightThumbKnuckle2");
		rightThumb1.attachChild(rightThumb2);
		rightThumb2.sensorId = 15;
		key = "RightThumbKnuckle1-RightThumbKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-2.5d / -2.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			rightThumb2.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			rightThumb2.getLocalTransform().moveTo(0.0F, -2.5F * this.handScalingFactor, -2.5F * this.handScalingFactor).update();
		}
		rightThumb2.radius = (1.0F * this.handScalingFactor);

		Joint rightThumbTip = new Joint("RightThumbTip");
		rightThumb2.attachChild(rightThumbTip);
//		rightThumbTip.sensorId = 15;
		rightThumbTip.interpolationFactor = this.interpolationFactor;
		key = "RightThumbKnuckle2-RightThumbTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			float angle = (float) Math.atan(-2.0d / -2.5d);
			System.out.println(distance + " : " + Math.toDegrees(angle));
			float adjacent = (float) Math.cos(angle) * distance;
			float opposite = (float) Math.sin(angle) * distance;
			rightThumbTip.getLocalTransform().moveTo(0.0F, -adjacent * this.handScalingFactor, -opposite * this.handScalingFactor).update();
		} else {
			rightThumbTip.getLocalTransform().moveTo(0.0F, -2.5F * this.handScalingFactor, -2.0F * this.handScalingFactor).update();
		}
		rightThumbTip.radius = 0.0F;

		Joint rightIndex1 = new Joint("RightIndexKnuckle1");
		rightHand.attachChild(rightIndex1);
		rightIndex1.sensorId = 16;
		key = "RightHand-RightIndexKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			rightIndex1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			rightIndex1.getLocalTransform().moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		} else {
			rightIndex1.getLocalTransform().moveTo(0.0F, -3.5F * this.handScalingFactor, -6.5F * this.handScalingFactor).update();
		}
		rightIndex1.radius = (1.5F * this.handScalingFactor);

		Joint rightIndex2 = new Joint("RightIndexKnuckle2");
		rightIndex1.attachChild(rightIndex2);
		rightIndex2.sensorId = 17;
		key = "RightIndexKnuckle1-RightIndexKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		rightIndex2.radius = (1.0F * this.handScalingFactor);

		Joint rightIndex3 = new Joint("RightIndexKnuckle3");
		rightIndex2.attachChild(rightIndex3);
		rightIndex3.interpolationFactor = this.interpolationFactor;
		key = "RightIndexKnuckle2-RightIndexKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -3.0F * this.handScalingFactor).update();
		}
		rightIndex3.radius = (0.5F * this.handScalingFactor);

		Joint rightIndexTip = new Joint("RightIndexTip");
		rightIndex3.attachChild(rightIndexTip);
		rightIndexTip.interpolationFactor = this.interpolationFactor;
		key = "RightIndexKnuckle3-RightIndexTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		rightIndexTip.radius = 0.0F;

		Joint rightMiddle1 = new Joint("RightMiddleKnuckle1");
		rightHand.attachChild(rightMiddle1);
		rightMiddle1.sensorId = 18;
		key = "RightHand-RightMiddleKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			rightMiddle1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			rightMiddle1.getLocalTransform().moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		} else {
			rightMiddle1.getLocalTransform().moveTo(0.0F, -1.0F * this.handScalingFactor, -7.0F * this.handScalingFactor).update();
		}
		rightMiddle1.radius = (1.5F * this.handScalingFactor);

		Joint rightMiddle2 = new Joint("RightMiddleKnuckle2");
		rightMiddle1.attachChild(rightMiddle2);
		rightMiddle2.sensorId = 19;
		key = "RightMiddleKnuckle1-RightMiddleKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		rightMiddle2.radius = (1.0F * this.handScalingFactor);

		Joint rightMiddle3 = new Joint("RightMiddleKnuckle3");
		rightMiddle2.attachChild(rightMiddle3);
		rightMiddle3.interpolationFactor = this.interpolationFactor;
		key = "RightMiddleKnuckle2-RightMiddleKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		rightMiddle3.radius = (0.5F * this.handScalingFactor);

		Joint rightMiddleTip = new Joint("RightMiddleTip");
		rightMiddle3.attachChild(rightMiddleTip);
		rightMiddleTip.interpolationFactor = this.interpolationFactor;
		key = "RightMiddleKnuckle3-RightMiddleTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		rightMiddleTip.radius = 0.0F;

		Joint rightRing1 = new Joint("RightRingKnuckle1");
		rightHand.attachChild(rightRing1);
		rightRing1.sensorId = 20;
		key = "RightHand-RightRingKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			rightRing1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			rightRing1.getLocalTransform().moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		} else {
			rightRing1.getLocalTransform().moveTo(0.0F, 1.0F * this.handScalingFactor, -6.0F * this.handScalingFactor).update();
		}
		rightRing1.radius = (1.5F * this.handScalingFactor);

		Joint rightRing2 = new Joint("RightRingKnuckle2");
		rightRing1.attachChild(rightRing2);
		rightRing2.sensorId = 21;
		key = "RightRingKnuckle1-RightRingKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightRing2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightRing2.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F * this.handScalingFactor).update();
		}
		rightRing2.radius = (1.0F * this.handScalingFactor);

		Joint rightRing3 = new Joint("RightRingKnuckle3");
		rightRing2.attachChild(rightRing3);
		rightRing3.interpolationFactor = this.interpolationFactor;
		key = "RightRingKnuckle2-RightRingKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightRing3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightRing3.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		rightRing3.radius = (0.5F * this.handScalingFactor);

		Joint rightRingTip = new Joint("RightRingTip");
		rightRing3.attachChild(rightRingTip);
		rightRingTip.interpolationFactor = this.interpolationFactor;
		key = "RightRingKnuckle3-RightRingTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		rightRingTip.radius = 0.0F;

		Joint rightLittle1 = new Joint("RightLittleKnuckle1");
		rightHand.attachChild(rightLittle1);
		rightLittle1.sensorId = 22;
		key = "RightHand-RightLittleKnuckle1";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
//			float distance = (float) distanceData.get(key).getMean();
//			float angle = (float) Math.atan(-6.5d / -3.5d);
//			System.out.println(distance + " : " + Math.toDegrees(angle));
//			float adjacent = (float) Math.cos(angle) * distance;
//			float opposite = (float) Math.sin(angle) * distance;
//			rightLittle1.getLocalTransform().moveTo(0.0F, -adjacent * this.baseRadius, -opposite * this.baseRadius).update();
			rightLittle1.getLocalTransform().moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		} else {
			rightLittle1.getLocalTransform().moveTo(0.0F, 3.0F * this.handScalingFactor, -5.0F * this.handScalingFactor).update();
		}
		rightLittle1.radius = (1.5F * this.handScalingFactor);

		Joint rightLittle2 = new Joint("RightLittleKnuckle2");
		rightLittle1.attachChild(rightLittle2);
//		XXX
//		rightLittle2.sensorId = 23;
		rightLittle2.interpolationFactor = this.interpolationFactor;
		key = "RightLittleKnuckle1-RightLittleKnuckle2";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -4.0F * this.handScalingFactor).update();
		}
		rightLittle2.radius = (1.0F * this.handScalingFactor);

		Joint rightLittle3 = new Joint("RightLittleKnuckle3");
		rightLittle2.attachChild(rightLittle3);
		rightLittle3.interpolationFactor = this.interpolationFactor;
		key = "RightLittleKnuckle2-RightLittleKnuckle3";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		rightLittle3.radius = (0.5F * this.handScalingFactor);

		Joint rightLittleTip = new Joint("RightLittleTip");
		rightLittle3.attachChild(rightLittleTip);
		rightLittleTip.interpolationFactor = this.interpolationFactor;
		key = "RightLittleKnuckle3-RightLittleTip";
		if (distanceData.containsKey(key)) {
			// y -> adjacent
			// z -> opposite
			float distance = (float) distanceData.get(key).getMean();
			rightLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -distance * this.handScalingFactor).update();
		} else {
			rightLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -2.5F * this.handScalingFactor).update();
		}
		rightLittleTip.radius = 0.0F;

		defaultModel.attachChild(hips);

		defaultModel.localToWorldTree();

		return defaultModel;
	}
	
	public String toString() {
		return this.getName();
	}
}