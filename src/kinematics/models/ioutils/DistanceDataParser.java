package kinematics.models.ioutils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFileChooser;

import main.GlobalSettingsAndVariables;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import utils.visualization.ScrollableFileChooserTextPreview;

public class DistanceDataParser {

	public final static double centimeterToInch = 0.393700787d;
	
	public final static double kinectScalingFactor = 100.0d;
	
	public final static double leapScalingFactor = 0.25d;//0.1d;
	
	private static double scalingFactor = 1.0d;
	
	
//	public final static String[] jointNames = new String[] {
//		"head",
//		"shoulder_center",
//		"spine",
//		"hip_center",
//		"shoulder_left",
//		"elbow_left",
//		"wrist_left",
//		"hand_left",
//		"shoulder_right",
//		"elbow_right",
//		"wrist_right",
//		"hand_right"
//	};
	
	public final static String[][] relevantJointPairings = new String[][] {
		// left leg
		{ "hip_center", "hip_left"   },
		{ "hip_left"  , "knee_left"  },
		{ "knee_left" , "ankle_left" },
		{ "ankle_left", "foot_left"  },
		// right leg
		{ "hip_center" , "hip_right"   },
		{ "hip_right"  , "knee_right"  },
		{ "knee_right" , "ankle_right" },
		{ "ankle_right", "foot_right"  },
		// upper body
		{ "head"           , "shoulder_center" },
		{ "shoulder_center", "spine"           },
		{ "spine"          , "hip_center"      },
		{ "shoulder_center", "shoulder_left"   },
		{ "shoulder_left"  , "elbow_left"      },
		{ "elbow_left"     , "wrist_left"      },
		{ "shoulder_center", "shoulder_right"  },
		{ "shoulder_right" , "elbow_right"     },
		{ "elbow_right"    , "wrist_right"     },
		{ "shoulder_left"  , "shoulder_right"  },
		{ "hip_right"      , "hip_left"        },
		// left hand
		{ "LeftHand"           , "LeftCarpometacarpal" },
		{ "LeftCarpometacarpal", "LeftThumbKnuckle1"   },
		{ "LeftThumbKnuckle1"  , "LeftThumbKnuckle2"   },
		{ "LeftThumbKnuckle2"  , "LeftThumbTip"        },
		{ "LeftHand"           , "LeftIndexKnuckle1"   },
		{ "LeftIndexKnuckle1"  , "LeftIndexKnuckle2"   },
		{ "LeftIndexKnuckle2"  , "LeftIndexKnuckle3"   },
		{ "LeftIndexKnuckle3"  , "LeftIndexTip"        },
		{ "LeftHand"           , "LeftMiddleKnuckle1"  },
		{ "LeftMiddleKnuckle1" , "LeftMiddleKnuckle2"  },
		{ "LeftMiddleKnuckle2" , "LeftMiddleKnuckle3"  },
		{ "LeftMiddleKnuckle3" , "LeftMiddleTip"       },
		{ "LeftHand"           , "LeftRingKnuckle1"    },
		{ "LeftRingKnuckle1"   , "LeftRingKnuckle2"    },
		{ "LeftRingKnuckle2"   , "LeftRingKnuckle3"    },
		{ "LeftRingKnuckle3"   , "LeftRingTip"         },
		{ "LeftHand"           , "LeftLittleKnuckle1"  },
		{ "LeftLittleKnuckle1" , "LeftLittleKnuckle2"  },
		{ "LeftLittleKnuckle2" , "LeftLittleKnuckle3"  },
		{ "LeftLittleKnuckle3" , "LeftLittleTip"       },
		// right hand
		{ "RightHand"           , "RightCarpometacarpal" },
		{ "RightCarpometacarpal", "RightThumbKnuckle1"   },
		{ "RightThumbKnuckle1"  , "RightThumbKnuckle2"   },
		{ "RightThumbKnuckle2"  , "RightThumbTip"        },
		{ "RightHand"           , "RightIndexKnuckle1"   },
		{ "RightIndexKnuckle1"  , "RightIndexKnuckle2"   },
		{ "RightIndexKnuckle2"  , "RightIndexKnuckle3"   },
		{ "RightIndexKnuckle3"  , "RightIndexTip"        },
		{ "RightHand"           , "RightMiddleKnuckle1"  },
		{ "RightMiddleKnuckle1" , "RightMiddleKnuckle2"  },
		{ "RightMiddleKnuckle2" , "RightMiddleKnuckle3"  },
		{ "RightMiddleKnuckle3" , "RightMiddleTip"       },
		{ "RightHand"           , "RightRingKnuckle1"    },
		{ "RightRingKnuckle1"   , "RightRingKnuckle2"    },
		{ "RightRingKnuckle2"   , "RightRingKnuckle3"    },
		{ "RightRingKnuckle3"   , "RightRingTip"         },
		{ "RightHand"           , "RightLittleKnuckle1"  },
		{ "RightLittleKnuckle1" , "RightLittleKnuckle2"  },
		{ "RightLittleKnuckle2" , "RightLittleKnuckle3"  },
		{ "RightLittleKnuckle3" , "RightLittleTip"       }
	};
	
	public final static String[][] symmetricDistances = new String[][] {
		// legs
		{ "hip_center-hip_left" , "hip_center-hip_right"   },
		{ "hip_left-knee_left"  , "hip_right-knee_right"   },
		{ "knee_left-ankle_left", "knee_right-ankle_right" },
		{ "ankle_left-foot_left", "ankle_right-foot_right" },
		// arms
		{ "shoulder_center-shoulder_left", "shoulder_center-shoulder_right" },
		{ "shoulder_left-elbow_left"     , "shoulder_right-elbow_right"     },
		{ "elbow_left-wrist_left"        , "elbow_right-wrist_right"        }
	};
	
	
	/**
	 * Das hier ist ein nuetzliches regex pattern, das ich auf stackoverflow
	 * gefunden hab:
	 * http://stackoverflow.com/questions/4731055/whitespace-matching-regex-java
	 * Damit kann man in einem string alle Arten von whitespaces (Leerzeichen etc.)
	 * in einem {@link String} erfassen und ersetzen z.B. mit der {@link String#replaceAll(String, String)}
	 * Methode.
	 */
	private final static String whitespacePattern = "["
			+ "\\u0009" // CHARACTER TABULATION
			+ "\\u000A" // LINE FEED (LF)
			+ "\\u000B" // LINE TABULATION
			+ "\\u000C" // FORM FEED (FF)
			+ "\\u000D" // CARRIAGE RETURN (CR)
			+ "\\u0020" // SPACE
			+ "\\u0085" // NEXT LINE (NEL)
			+ "\\u00A0" // NO-BREAK SPACE
			+ "\\u1680" // OGHAM SPACE MARK
			+ "\\u180E" // MONGOLIAN VOWEL SEPARATOR
			+ "\\u2000" // EN QUAD
			+ "\\u2001" // EM QUAD
			+ "\\u2002" // EN SPACE
			+ "\\u2003" // EM SPACE
			+ "\\u2004" // THREE-PER-EM SPACE
			+ "\\u2005" // FOUR-PER-EM SPACE
			+ "\\u2006" // SIX-PER-EM SPACE
			+ "\\u2007" // FIGURE SPACE
			+ "\\u2008" // PUNCTUATION SPACE
			+ "\\u2009" // THIN SPACE
			+ "\\u200A" // HAIR SPACE
			+ "\\u2028" // LINE SEPARATOR
			+ "\\u2029" // PARAGRAPH SEPARATOR
			+ "\\u202F" // NARROW NO-BREAK SPACE
			+ "\\u205F" // MEDIUM MATHEMATICAL SPACE
			+ "\\u3000" // IDEOGRAPHIC SPACE
			+ "]+"      // plus damit alle sukkzessiven whitespaces erfasst werden
	;
	
	public static HashMap<String, DescriptiveStatistics> parseFile(File file) {
		HashMap<String, DescriptiveStatistics> distances = null;
		
		if (file.isDirectory()) {
			distances = DistanceDataParser.parseDirectory(file);
		} else {
			distances = DistanceDataParser.parseSingleFile(file);
		}
		
		if (GlobalSettingsAndVariables.enforceSymmetry) {
			DistanceDataParser.enforceSymmetry(distances);
		}
		
		return distances;
	}
	
	private static void enforceSymmetry(HashMap<String, DescriptiveStatistics> distances) {
		for (String[] mappings : DistanceDataParser.symmetricDistances) {
			if (distances.containsKey(mappings[0]) && distances.containsKey(mappings[1])) {
				DescriptiveStatistics firstStats = distances.get(mappings[0]);
				DescriptiveStatistics secondStats = distances.get(mappings[0]);
				DescriptiveStatistics completeStats = new DescriptiveStatistics(firstStats);
				for (double value : secondStats.getValues()) {
					completeStats.addValue(value);
				}
				distances.put(mappings[0], completeStats);
				distances.put(mappings[1], completeStats);
			}
		}
	}
	
	public static HashMap<String, DescriptiveStatistics> parseSingleFile(File file) {
		HashMap<String, DescriptiveStatistics> distances = new HashMap<String, DescriptiveStatistics>();
		
		for (String[] pairing : DistanceDataParser.relevantJointPairings) {
			distances.put(pairing[0] + "-" + pairing[1], new DescriptiveStatistics());
		}
		
		if (file.getName().endsWith(".leapdata")) {
			DistanceDataParser.scalingFactor = DistanceDataParser.leapScalingFactor;
		} else if (file.getName().endsWith(".skeletaldata")) {
			DistanceDataParser.scalingFactor = DistanceDataParser.kinectScalingFactor;
		}
		
		try { 
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String line;
			Vector<String> lines = new Vector<String>();
			
			while (reader.ready()) {
				line = reader.readLine();
				if (line.startsWith("#")) {
					if (lines.size() > 0) {
						DistanceDataParser.processStringVector(distances, lines);
					}
					
					lines.clear();
				} else {
					lines.add(line);
				}
			}
			
			if (lines.size() > 0) {
				DistanceDataParser.processStringVector(distances, lines);
			}
			
			reader.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return distances;
	}
	
	public static HashMap<String, DescriptiveStatistics> parseDirectory(File dir) {
		HashMap<String, DescriptiveStatistics> distances = new HashMap<String, DescriptiveStatistics>();
		
		for (String[] pairing : DistanceDataParser.relevantJointPairings) {
			distances.put(pairing[0] + "-" + pairing[1], new DescriptiveStatistics());
		}
		
		for (File file : dir.listFiles()) {
			if (!file.isDirectory()) {
				
				if (file.getName().endsWith(".leapdata")) {
					DistanceDataParser.scalingFactor = DistanceDataParser.leapScalingFactor;
				} else if (file.getName().endsWith(".skeletaldata")) {
					DistanceDataParser.scalingFactor = DistanceDataParser.kinectScalingFactor;
				}
				
				try { 
					BufferedReader reader = new BufferedReader(new FileReader(file));
					
					String line;
					Vector<String> lines = new Vector<String>();
					
					while (reader.ready()) {
						line = reader.readLine();
						if (line.startsWith("#")) {
							if (lines.size() > 0) {
								DistanceDataParser.processStringVector(distances, lines);
							}
							
							lines.clear();
						} else {
							lines.add(line);
						}
					}
					
					if (lines.size() > 0) {
						DistanceDataParser.processStringVector(distances, lines);
					}
					
					reader.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		
		return distances;
	}
	
	private static void processStringVector(HashMap<String, DescriptiveStatistics> distances, Vector<String> lines) {
		for (String[] pairing : DistanceDataParser.relevantJointPairings) {
			String key = pairing[0] + "-" + pairing[1];
			String firstString = null;
			String secondString = null;
			for (String line : lines) {
				if (line.startsWith(pairing[0] + ":")) {
					firstString = line;
				}
				
				if (line.startsWith(pairing[1] + ":")) {
					secondString = line;
				}
			}
			
			if (firstString != null && secondString != null) {
				distances.get(key).addValue(DistanceDataParser.getDistance(firstString, secondString));
			}
		}
	}
	
	private static double getDistance(String firstString, String secondString) {
		double[] firstValues  = DistanceDataParser.getValuesFromLine(firstString);
		double[] secondValues = DistanceDataParser.getValuesFromLine(secondString);
		
		double distance = 0.0d;
		
		for (int i = 0; i < firstValues.length; i++) {
			distance += Math.pow(firstValues[i] - secondValues[i], 2.0d);
		}
		
		return Math.sqrt(distance);
	}
	
	private static double[] getValuesFromLine(String line) {
		String[] stringValues = line.split(":")[1].replaceAll(DistanceDataParser.whitespacePattern, "\t").split("\t");
		double[] values = new double[stringValues.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = Double.parseDouble(stringValues[i]) * DistanceDataParser.scalingFactor * DistanceDataParser.centimeterToInch;
		}
		
		return values;
	}
	
	public static File getFile(boolean accesory, String baseDirectory) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(baseDirectory));
		
		fc.setApproveButtonText("select file");
		fc.setDialogTitle("data file");
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		if (accesory) fc.setAccessory(new ScrollableFileChooserTextPreview(fc));
		
		int state = fc.showOpenDialog(null);
		
		if (state == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		} else {
			return null;
		}
	}
	
	public static void main(String[] args) {
//		RightThumbKnuckle2: -72.569328308 304.864471436 26.378957748
//		RightThumbKnuckle2: -72.569328308 304.864471436 26.378957748
//		RightThumbTip: -91.469268799 313.184020996 17.647577286
//		shoulder_right:-0.6703927 0.2534725 2.037878
//		String sample = "RightThumbTip: -91.469268799 313.184020996 17.647577286";
		String sample = "shoulder_right:-0.6703927 0.2534725 2.037878";
//		DistanceDataParser.getValuesFromLine(sample);
		String[] stringValues = sample.split(":" + DistanceDataParser.whitespacePattern)[1].replaceAll(DistanceDataParser.whitespacePattern, "\t").split("\t");
		for (String s : stringValues) {
			System.out.println("obtained: " + s);
		}
	}
	
}
