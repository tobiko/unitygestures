package kinematics.models;

import java.io.File;

import streamservice.frames.KinematicFrame;
import kinematics.Joint;
import kinematics.KinematicModel;
import kinematics.KinematicModelService;
/**
 * Implementation of {@link KinematicModelService} interface that provides a kineamtic model
 * matching the suit in the tcp configuration.
 * this class produces a kinematic chain of joints that represent the respective body part.
 * sensor addresses are assigned here, but can be changed via the gui.
 */
public class TCPSuit implements KinematicModelService {
	/**
	 * name of the model, as it is displayed in the gui.
	 */
	public static String modelName = "TCP: Standard Suit";
	/**
	 * usual extrapolation factor used to interpolate rotations of parent joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of a joint to its parent, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float extrapolationFactor;
	/**
	 * usual interpolation factor used to interpolate rotations of joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of the parent joint, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float interpolationFactor;
	/**
	 * the number of models created by this service (used as id in the returned {@link KinematicModel}).
	 */
	int instanceNumber;

	public TCPSuit() {
		super();
		this.extrapolationFactor = 0.5f;
		this.interpolationFactor = 0.5f;
		this.instanceNumber = 0;
	}

	public String getName() {
		return TCPSuit.modelName;
	}
	/**
	 * creates the model. for the suit the mapping works in the following way (14.08.2014):</p>
	 * <table>
	 * <col width="50%"/>
	 * <col width="50%"/>
	 * <thead> <tr><th>Joint</th><th>sensorId</th></tr> <thead>
	 * <tbody>
     * <tr><td>Hips</td><td>31</td></tr>
     * <tr><td>Spine1</td><td>30</td></tr>
     * <tr><td>Head</td><td>29</td></tr>
     * <tr><td>LeftShoulder</td><td>25</td></tr>
     * <tr><td>LeftArm</td><td>24</td></tr>
     * <tr><td>LeftForeArm</td><td>23</td></tr>
     * <tr><td>RightShoulder</td><td>28</td></tr>
     * <tr><td>RightArm</td><td>27</td></tr>
     * <tr><td>RightForeArm</td><td>26</td></tr>
  	 * </tbody>
	 * </table>
	 * </p>
	 * please note: legs are included, but since there is no sensor data available, they are not animated
	 */
	public KinematicModel create() {
		KinematicModel defaultModel = new KinematicModel("TCP Suit "
				+ this.instanceNumber);
		this.instanceNumber += 1;
		
		Joint hips = new Joint("Hips");
		hips.sensorId = 31;
		hips.radius = 3.0F;

		Joint leftUpLeg = new Joint("LeftUpLeg");
		hips.attachChild(leftUpLeg);
		leftUpLeg.getLocalTransform().moveTo(3.0F, 0.0F, 0.0F).update();
		leftUpLeg.radius = 2.0F;

		Joint leftLeg = new Joint("LeftLeg");
		leftUpLeg.attachChild(leftLeg);
		leftLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F).update();
		leftLeg.radius = 2.0F;

		Joint leftFoot = new Joint("LeftFoot");
		leftLeg.attachChild(leftFoot);
		leftFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F).update();
		leftFoot.radius = 0.0F;

		Joint leftFootHeel = new Joint("LeftFootHeel");
		leftFoot.attachChild(leftFootHeel);
		leftFootHeel.getLocalTransform().moveTo(-0.5F, 1.5F, -3.5F).update();
		leftFootHeel.radius = 1.0F;

		Joint leftFootHeelOutside = new Joint("LeftFootHeelOutside");
		leftFoot.attachChild(leftFootHeelOutside);
		leftFootHeelOutside.getLocalTransform().moveTo(0.5F, 1.5F, -3.5F)
				.update();
		leftFootHeelOutside.radius = 1.0F;

		Joint leftFootToe = new Joint("LeftFootToe");
		leftFootHeel.attachChild(leftFootToe);
		leftFootToe.getLocalTransform().moveTo(-0.5F, -5.5F, 0.0F).update();
		leftFootToe.radius = 1.0F;

		Joint leftFootToeOutside = new Joint("LeftFootToeOutside");
		leftFootHeelOutside.attachChild(leftFootToeOutside);
		leftFootToeOutside.getLocalTransform().moveTo(0.5F, -5.5F, 0.0F)
				.update();
		leftFootToeOutside.radius = 1.0F;

		Joint rightUpLeg = new Joint("RightUpLeg");
		hips.attachChild(rightUpLeg);
		rightUpLeg.getLocalTransform().moveTo(-3.0F, 0.0F, 0.0F).update();
		rightUpLeg.radius = 2.0F;

		Joint rightLeg = new Joint("RightLeg");
		rightUpLeg.attachChild(rightLeg);
		rightLeg.getLocalTransform().moveTo(0.0F, 0.0F, -17.5F).update();
		rightLeg.radius = 2.0F;

		Joint rightFoot = new Joint("RightFoot");
		rightLeg.attachChild(rightFoot);
		rightFoot.getLocalTransform().moveTo(0.0F, 0.0F, -15.5F).update();
		rightFoot.radius = 0.0F;

		Joint rightFootHeel = new Joint("RightFootHeel");
		rightFoot.attachChild(rightFootHeel);
		rightFootHeel.getLocalTransform().moveTo(0.5F, 1.5F, -3.5F).update();
		rightFootHeel.radius = 1.0F;

		Joint rightFootHeelOutside = new Joint("RightFootHeelOutside");
		rightFoot.attachChild(rightFootHeelOutside);
		rightFootHeelOutside.getLocalTransform().moveTo(-0.5F, 1.5F, -3.5F)
				.update();
		rightFootHeelOutside.radius = 1.0F;

		Joint rightFootToe = new Joint("RightFootToe");
		rightFootHeel.attachChild(rightFootToe);
		rightFootToe.getLocalTransform().moveTo(0.5F, -5.5F, 0.0F).update();
		rightFootToe.radius = 1.0F;

		Joint rightFootToeOutside = new Joint("RightFootToeOutside");
		rightFootHeelOutside.attachChild(rightFootToeOutside);
		rightFootToeOutside.getLocalTransform().moveTo(-0.5F, -5.5F, 0.0F)
				.update();
		rightFootToeOutside.radius = 1.0F;

		Joint spine = new Joint("Spine");
		hips.attachChild(spine);
		spine.getLocalTransform().moveTo(0.0F, 1.0F, 3.0F).update();
		spine.radius = 1.0F;

		Joint spine1 = new Joint("Spine1");
		spine.attachChild(spine1);
		spine1.sensorId = 30;
		spine1.extrapolationFactor = this.extrapolationFactor;
		spine1.getLocalTransform().moveTo(0.0F, 0.0F, 3.0F).update();
		spine1.radius = 0.0F;

		Joint neck = new Joint("Neck");
		spine1.attachChild(neck);
		neck.interpolationFactor = this.interpolationFactor;
		neck.getLocalTransform().moveTo(0.0F, -1.0F, 15.0F).update();
		neck.radius = 0.0F;

		Joint head = new Joint("Head");
		neck.attachChild(head);
		head.sensorId = 29;
		head.extrapolationFactor = this.extrapolationFactor;
		head.getLocalTransform().moveTo(0.0F, 0.0F, 6.5F).update();
		head.radius = 0.0F;

		Joint headTop = new Joint("HeadTop");
		head.attachChild(headTop);
		headTop.getLocalTransform().moveTo(0.0F, 0.0F, 4.0F).update();
		headTop.radius = 3.0F;

		Joint leftShoulder = new Joint("LeftShoulder");
		spine1.attachChild(leftShoulder);
		leftShoulder.sensorId = 25;
		leftShoulder.getLocalTransform().moveTo(3.0F, -1.0F, 13.0F).update();
		leftShoulder.radius = 0.0F;

		Joint leftArm = new Joint("LeftArm");
		leftShoulder.attachChild(leftArm);
		leftArm.sensorId = 24;
		leftArm.getLocalTransform().moveTo(5.0F, 0.0F, 0.0F).update();
		leftArm.radius = 1.5F;

		Joint leftForeArm = new Joint("LeftForeArm");
		leftArm.attachChild(leftForeArm);
		leftForeArm.sensorId = 23;
		leftForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F).update();
		leftForeArm.radius = 1.5F;

		Joint leftHand = new Joint("LeftHand");
		leftForeArm.attachChild(leftHand);
		leftHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F).update();
		leftHand.radius = 1.0F;

		Joint rightShoulder = new Joint("RightShoulder");
		spine1.attachChild(rightShoulder);
		rightShoulder.sensorId = 28;
		rightShoulder.getLocalTransform().moveTo(-3.0F, -1.0F, 13.0F).update();
		rightShoulder.radius = 0.0F;

		Joint rightArm = new Joint("RightArm");
		rightShoulder.attachChild(rightArm);
		rightArm.sensorId = 27;
		rightArm.getLocalTransform().moveTo(-5.0F, 0.0F, 0.0F).update();
		rightArm.radius = 1.5F;

		Joint rightForeArm = new Joint("RightForeArm");
		rightArm.attachChild(rightForeArm);
		rightForeArm.sensorId = 26;
		rightForeArm.getLocalTransform().moveTo(0.0F, 0.0F, -12.0F).update();
		rightForeArm.radius = 1.5F;

		Joint rightHand = new Joint("RightHand");
		rightForeArm.attachChild(rightHand);
		rightHand.getLocalTransform().moveTo(0.0F, 0.0F, -9.5F).update();
		rightHand.radius = 1.0F;

		defaultModel.attachChild(hips);

		defaultModel.localToWorldTree();

		return defaultModel;
	}
	
	@Override
	public KinematicModel create(File file) {
		// TODO Auto-generated method stub
		return this.create();
	}

	public String toString() {
		return this.getName();
	}
}