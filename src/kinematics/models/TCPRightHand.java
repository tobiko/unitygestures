package kinematics.models;

import java.io.File;

import streamservice.frames.KinematicFrame;
import kinematics.Joint;
import kinematics.KinematicModel;
import kinematics.KinematicModelService;
/**
 * Implementation of {@link KinematicModelService} interface that provides a kineamtic model
 * matching the right hand in the tcp configuration.
 * this class produces a kinematic chain of joints that represent the respective body part.
 * sensor addresses are assigned here, but can be changed via the gui.
 */
public class TCPRightHand implements KinematicModelService {
	/**
	 * name of the model, as it is displayed in the gui.
	 */
	public static String modelName = "TCP: Right Hand 12 Sensor";
	/**
	 * usual interpolation factor used to interpolate rotations of joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of the parent joint, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float interpolationFactor;
	/**
	 * the number of models created by this service (used as id in the returned {@link KinematicModel}).
	 */
	int instanceNumber;

	public TCPRightHand() {
		super();
		this.interpolationFactor = 0.5f;
		this.instanceNumber = 0;
	}

	public String getName() {
		return TCPRightHand.modelName;
	}
	
	/**
	 * creates the model. for the right hand the mapping works in the following way (14.08.2014):</p>
	 * <table>
	 * <col width="50%"/>
	 * <col width="50%"/>
	 * <thead> <tr><th>Joint</th><th>sensorId</th></tr> <thead>
	 * <tbody>
     * <tr><td>RightHand</td><td>12</td></tr>
     * <tr><td>RightThumbKnuckle1</td><td>13</td></tr>
     * <tr><td>RightThumbKnuckle2</td><td>14</td></tr>
     * <tr><td>RightThumbTip</td><td>15</td></tr>
     * <tr><td>RightIndexKnuckle1</td><td>16</td></tr>
     * <tr><td>RightIndexKnuckle2</td><td>17</td></tr>
     * <tr><td>RightMiddleKnuckle1</td><td>18</td></tr>
     * <tr><td>RightMiddleKnuckle2</td><td>19</td></tr>
     * <tr><td>RightRingKnuckle1</td><td>20</td></tr>
     * <tr><td>RightRingKnuckle2</td><td>21</td></tr>
     * <tr><td>RightLittleKnuckle1</td><td>22</td></tr>
  	 * </tbody>
	 * </table>
	 */
	public KinematicModel create() {
		KinematicModel defaultModel = new KinematicModel(
				"TCP: Right Hand 12 Sensor " + this.instanceNumber);
		defaultModel.getLocalTransform().moveTo(15.0F, 0.0F, 0.0F).update();
		this.instanceNumber += 1;

		Joint rightHand = new Joint("RightHand");
		rightHand.sensorId = 12;
		rightHand.radius = 4.0F;

		Joint rightCmc = new Joint("RightCarpometacarpal");
		rightHand.attachChild(rightCmc);
		rightCmc.getLocalTransform().moveTo(0.0F, -3.0F, -2.0F).update();
		rightCmc.radius = 1.0F;

		Joint rightThumb1 = new Joint("RightThumbKnuckle1");
		rightCmc.attachChild(rightThumb1);
		rightThumb1.sensorId = 13;
		rightThumb1.getLocalTransform().moveTo(0.0F, -9.0F, -6.0F).update();
		rightThumb1.radius = 3.0F;

		Joint rightThumb2 = new Joint("RightThumbKnuckle2");
		rightThumb1.attachChild(rightThumb2);
		rightThumb2.sensorId = 14;
		rightThumb2.getLocalTransform().moveTo(0.0F, -5.0F, -5.0F).update();
		rightThumb2.radius = 2.0F;

		Joint rightThumbTip = new Joint("RightThumbTip");
		rightThumb2.attachChild(rightThumbTip);
		rightThumbTip.sensorId = 15;
		rightThumbTip.interpolationFactor = this.interpolationFactor;
		rightThumbTip.getLocalTransform().moveTo(0.0F, -5.0F, -4.0F).update();
		rightThumbTip.radius = 0.0F;

		Joint rightIndex1 = new Joint("RightIndexKnuckle1");
		rightHand.attachChild(rightIndex1);
		rightIndex1.sensorId = 16;
		rightIndex1.getLocalTransform().moveTo(0.0F, -7.0F, -13.0F).update();
		rightIndex1.radius = 3.0F;

		Joint rightIndex2 = new Joint("RightIndexKnuckle2");
		rightIndex1.attachChild(rightIndex2);
		rightIndex2.sensorId = 17;
		rightIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		rightIndex2.radius = 2.0F;

		Joint rightIndex3 = new Joint("RightIndexKnuckle3");
		rightIndex2.attachChild(rightIndex3);
		rightIndex3.interpolationFactor = this.interpolationFactor;
		rightIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -6.0F).update();
		rightIndex3.radius = 1.0F;

		Joint rightIndexTip = new Joint("RightIndexTip");
		rightIndex3.attachChild(rightIndexTip);
		rightIndexTip.interpolationFactor = this.interpolationFactor;
		rightIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		rightIndexTip.radius = 0.0F;

		Joint rightMiddle1 = new Joint("RightMiddleKnuckle1");
		rightHand.attachChild(rightMiddle1);
		rightMiddle1.sensorId = 18;
		rightMiddle1.getLocalTransform().moveTo(0.0F, -2.0F, -14.0F).update();
		rightMiddle1.radius = 3.0F;

		Joint rightMiddle2 = new Joint("RightMiddleKnuckle2");
		rightMiddle1.attachChild(rightMiddle2);
		rightMiddle2.sensorId = 19;
		rightMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		rightMiddle2.radius = 2.0F;

		Joint rightMiddle3 = new Joint("RightMiddleKnuckle3");
		rightMiddle2.attachChild(rightMiddle3);
		rightMiddle3.interpolationFactor = this.interpolationFactor;
		rightMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		rightMiddle3.radius = 1.0F;

		Joint rightMiddleTip = new Joint("RightMiddleTip");
		rightMiddle3.attachChild(rightMiddleTip);
		rightMiddleTip.interpolationFactor = this.interpolationFactor;
		rightMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		rightMiddleTip.radius = 0.0F;

		Joint rightRing1 = new Joint("RightRingKnuckle1");
		rightHand.attachChild(rightRing1);
		rightRing1.sensorId = 20;
		rightRing1.getLocalTransform().moveTo(0.0F, 2.0F, -12.0F).update();
		rightRing1.radius = 3.0F;

		Joint rightRing2 = new Joint("RightRingKnuckle2");
		rightRing1.attachChild(rightRing2);
		rightRing2.sensorId = 21;
		rightRing2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		rightRing2.radius = 2.0F;

		Joint rightRing3 = new Joint("RightRingKnuckle3");
		rightRing2.attachChild(rightRing3);
		rightRing3.interpolationFactor = this.interpolationFactor;
		rightRing3.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		rightRing3.radius = 1.0F;

		Joint rightRingTip = new Joint("RightRingTip");
		rightRing3.attachChild(rightRingTip);
		rightRingTip.interpolationFactor = this.interpolationFactor;
		rightRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		rightRingTip.radius = 0.0F;

		Joint rightLittle1 = new Joint("RightLittleKnuckle1");
		rightHand.attachChild(rightLittle1);
		rightLittle1.sensorId = 22;
		rightLittle1.getLocalTransform().moveTo(0.0F, 6.0F, -10.0F).update();
		rightLittle1.radius = 3.0F;

		Joint rightLittle2 = new Joint("RightLittleKnuckle2");
		rightLittle1.attachChild(rightLittle2);
//		XXX
//		rightLittle2.sensorId = 23;
		rightLittle2.interpolationFactor = this.interpolationFactor;
		rightLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		rightLittle2.radius = 2.0F;

		Joint rightLittle3 = new Joint("RightLittleKnuckle3");
		rightLittle2.attachChild(rightLittle3);
		rightLittle3.interpolationFactor = this.interpolationFactor;
		rightLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		rightLittle3.radius = 1.0F;

		Joint rightLittleTip = new Joint("RightLittleTip");
		rightLittle3.attachChild(rightLittleTip);
		rightLittleTip.interpolationFactor = this.interpolationFactor;
		rightLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		rightLittleTip.radius = 0.0F;

		defaultModel.attachChild(rightHand);

		defaultModel.localToWorldTree();

		return defaultModel;
	}
	
	@Override
	public KinematicModel create(File file) {
		// TODO Auto-generated method stub
		return this.create();
	}

	public String toString() {
		return this.getName();
	}
}