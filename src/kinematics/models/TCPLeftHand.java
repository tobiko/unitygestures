package kinematics.models;

import java.io.File;

import streamservice.frames.KinematicFrame;
import kinematics.Joint;
import kinematics.KinematicModel;
import kinematics.KinematicModelService;
/**
 * Implementation of {@link KinematicModelService} interface that provides a kineamtic model
 * matching the left hand in the tcp configuration.
 * this class produces a kinematic chain of joints that represent the respective body part.
 * sensor addresses are assigned here, but can be changed via the gui.
 */
public class TCPLeftHand implements KinematicModelService {
	/**
	 * name of the model, as it is displayed in the gui.
	 */
	public static String modelName = "TCP: Left Hand 12 Sensor";
	/**
	 * usual interpolation factor used to interpolate rotations of joints which are not associated
	 * with a certain sensor. this works by applying the rotation axis of the parent joint, for details see
	 * the {@link KinematicFrame} class.
	 */
	private float interpolationFactor;
	/**
	 * the number of models created by this service (used as id in the returned {@link KinematicModel}).
	 */
	private int instanceNumber;

	public TCPLeftHand() {
		super();
		this.interpolationFactor = 0.5f;
		this.instanceNumber = 0;
	}

	public String getName() {
		return TCPLeftHand.modelName;
	}
	/**
	 * creates the model. for the left hand the mapping works in the following way (14.08.2014):</p>
	 * <table>
	 * <col width="50%"/>
	 * <col width="50%"/>
	 * <thead> <tr><th>Joint</th><th>sensorId</th></tr> <thead>
	 * <tbody>
     * <tr><td>LeftHand</td><td>0</td></tr>
     * <tr><td>LeftThumbKnuckle1</td><td>1</td></tr>
     * <tr><td>LeftThumbKnuckle2</td><td>2</td></tr>
     * <tr><td>LeftThumbTip</td><td>3</td></tr>
     * <tr><td>LeftIndexKnuckle1</td><td>4</td></tr>
     * <tr><td>LeftIndexKnuckle2</td><td>5</td></tr>
     * <tr><td>LeftMiddleKnuckle1</td><td>6</td></tr>
     * <tr><td>LeftMiddleKnuckle2</td><td>7</td></tr>
     * <tr><td>LeftRingKnuckle1</td><td>8</td></tr>
     * <tr><td>LeftRingKnuckle2</td><td>9</td></tr>
     * <tr><td>LeftLittleKnuckle1</td><td>10</td></tr>
  	 * </tbody>
	 * </table>
	 */
	public KinematicModel create() {
		KinematicModel defaultModel = new KinematicModel(
				"TCP: Left Hand 12 Sensor " + this.instanceNumber);
		defaultModel.getLocalTransform().moveTo(15.0F, 0.0F, 0.0F).update();
		this.instanceNumber += 1;

		Joint leftHand = new Joint("LeftHand");
		leftHand.sensorId = 0;
		leftHand.radius = 4.0F;

		Joint leftCmc = new Joint("LeftCarpometacarpal");
		leftHand.attachChild(leftCmc);
		leftCmc.getLocalTransform().moveTo(0.0F, -3.0F, -2.0F).update();
		leftCmc.radius = 1.0F;

		Joint leftThumb1 = new Joint("LeftThumbKnuckle1");
		leftCmc.attachChild(leftThumb1);
		leftThumb1.sensorId = 1;
		leftThumb1.interpolationFactor = this.interpolationFactor;
		leftThumb1.getLocalTransform().moveTo(0.0F, -9.0F, -6.0F).update();
		leftThumb1.radius = 3.0F;

		Joint leftThumb2 = new Joint("LeftThumbKnuckle2");
		leftThumb1.attachChild(leftThumb2);
		leftThumb2.sensorId = 2;
		leftThumb2.getLocalTransform().moveTo(0.0F, -5.0F, -5.0F).update();
		leftThumb2.radius = 2.0F;

		Joint leftThumbTip = new Joint("LeftThumbTip");
		leftThumb2.attachChild(leftThumbTip);
		leftThumbTip.sensorId = 3;
		leftThumbTip.interpolationFactor = this.interpolationFactor;
		leftThumbTip.getLocalTransform().moveTo(0.0F, -5.0F, -4.0F).update();
		leftThumbTip.radius = 0.0F;

		Joint leftIndex1 = new Joint("LeftIndexKnuckle1");
		leftHand.attachChild(leftIndex1);
		leftIndex1.sensorId = 4;
		leftIndex1.getLocalTransform().moveTo(0.0F, -7.0F, -13.0F).update();
		leftIndex1.radius = 3.0F;

		Joint leftIndex2 = new Joint("LeftIndexKnuckle2");
		leftIndex1.attachChild(leftIndex2);
		leftIndex2.sensorId = 5;
		leftIndex2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		leftIndex2.radius = 2.0F;

		Joint leftIndex3 = new Joint("LeftIndexKnuckle3");
		leftIndex2.attachChild(leftIndex3);
		leftIndex3.interpolationFactor = this.interpolationFactor;
		leftIndex3.getLocalTransform().moveTo(0.0F, 0.0F, -6.0F).update();
		leftIndex3.radius = 1.0F;

		Joint leftIndexTip = new Joint("LeftIndexTip");
		leftIndex3.attachChild(leftIndexTip);
		leftIndexTip.interpolationFactor = this.interpolationFactor;
		leftIndexTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		leftIndexTip.radius = 0.0F;

		Joint leftMiddle1 = new Joint("LeftMiddleKnuckle1");
		leftHand.attachChild(leftMiddle1);
		leftMiddle1.sensorId = 6;
		leftMiddle1.getLocalTransform().moveTo(0.0F, -2.0F, -14.0F).update();
		leftMiddle1.radius = 3.0F;

		Joint leftMiddle2 = new Joint("LeftMiddleKnuckle2");
		leftMiddle1.attachChild(leftMiddle2);
		leftMiddle2.sensorId = 7;
		leftMiddle2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		leftMiddle2.radius = 2.0F;

		Joint leftMiddle3 = new Joint("LeftMiddleKnuckle3");
		leftMiddle2.attachChild(leftMiddle3);
		leftMiddle3.interpolationFactor = this.interpolationFactor;
		leftMiddle3.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		leftMiddle3.radius = 1.0F;

		Joint leftMiddleTip = new Joint("LeftMiddleTip");
		leftMiddle3.attachChild(leftMiddleTip);
		leftMiddleTip.interpolationFactor = this.interpolationFactor;
		leftMiddleTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		leftMiddleTip.radius = 0.0F;

		Joint leftRing1 = new Joint("LeftRingKnuckle1");
		leftHand.attachChild(leftRing1);
		leftRing1.sensorId = 8;
		leftRing1.getLocalTransform().moveTo(0.0F, 2.0F, -12.0F).update();
		leftRing1.radius = 3.0F;

		Joint leftRing2 = new Joint("LeftRingKnuckle2");
		leftRing1.attachChild(leftRing2);
		leftRing2.sensorId = 9;
		leftRing2.getLocalTransform().moveTo(0.0F, 0.0F, -10.0F).update();
		leftRing2.radius = 2.0F;

		Joint leftRing3 = new Joint("LeftRingKnuckle3");
		leftRing2.attachChild(leftRing3);
		leftRing3.interpolationFactor = this.interpolationFactor;
		leftRing3.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		leftRing3.radius = 1.0F;

		Joint leftRingTip = new Joint("LeftRingTip");
		leftRing3.attachChild(leftRingTip);
		leftRingTip.interpolationFactor = this.interpolationFactor;
		leftRingTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		leftRingTip.radius = 0.0F;

		Joint leftLittle1 = new Joint("LeftLittleKnuckle1");
		leftHand.attachChild(leftLittle1);
		leftLittle1.sensorId = 10;
		leftLittle1.getLocalTransform().moveTo(0.0F, 6.0F, -10.0F).update();
		leftLittle1.radius = 3.0F;

		Joint leftLittle2 = new Joint("LeftLittleKnuckle2");
		leftLittle1.attachChild(leftLittle2);
//		XXX
//		leftLittle2.sensorId = 11;
		leftLittle2.interpolationFactor = this.interpolationFactor;
		leftLittle2.getLocalTransform().moveTo(0.0F, 0.0F, -8.0F).update();
		leftLittle2.radius = 2.0F;

		Joint leftLittle3 = new Joint("LeftLittleKnuckle3");
		leftLittle2.attachChild(leftLittle3);
		leftLittle3.interpolationFactor = this.interpolationFactor;
		leftLittle3.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		leftLittle3.radius = 1.0F;

		Joint leftLittleTip = new Joint("LeftLittleTip");
		leftLittle3.attachChild(leftLittleTip);
		leftLittleTip.interpolationFactor = this.interpolationFactor;
		leftLittleTip.getLocalTransform().moveTo(0.0F, 0.0F, -5.0F).update();
		leftLittleTip.radius = 0.0F;

		defaultModel.attachChild(leftHand);

		defaultModel.localToWorldTree();

		return defaultModel;
	}

	@Override
	public KinematicModel create(File file) {
		// TODO Auto-generated method stub
		return this.create();
	}
	
	public String toString() {
		return this.getName();
	}
}