package kinematics;

import com.spookengine.maths.Mat3;
import com.spookengine.scenegraph.Node;
import com.spookengine.scenegraph.Spatial;

/**
 * this container class maintains a bunch of joints which are organized in a
 * kinematic chain. this class itself extends the {@link Spatial} class from
 * the spookengine, which allows convenient rendering.
 */
public class KinematicModel extends Spatial {
	/**
	 * constructs a kinematic model with the assigned name.
	 * @param name name of the model.
	 */
	public KinematicModel(String name) {
		super(name);
	}
	/**
	 * returns the root node of the kinematic chain.
	 * @return the root node of the kinematic chain
	 */
	public Joint getRoot() {
		return ((Joint) this.children.get(0));
	}
	/**
	 * finds a certain joint by index, internally the recursive {@link Joint#findChild(int)} method is called on
	 * the root joint of the hierarchy.
	 * @param sensorId to be found sensor index
	 * @return the joint with the respective sensor index, or null if the sensor index has not been assigned.
	 */
	public Joint findChild(int sensorId) {
		if (!(this.children.isEmpty())) {
			return ((Joint) this.children.get(0)).findChild(sensorId);
		}
		return null;
	}
	/**
	 * for safety reasons the visualization works only on a copy of the actual kinematic model, this copy is
	 * obtained here.
	 */
	public KinematicModel clone() {
		KinematicModel clone = new KinematicModel(this.name);
		clone.localTransform.setTo(this.localTransform);
		clone.worldTransform.setTo(this.worldTransform);

		if (this.bounds != null) {
			clone.setBounds(this.bounds.clone());
		}
		return clone;
	}
	/**
	 * this method obtains a copy of the whole kinematic chain. internally the {@link Joint#cloneTree(Node)} of the
	 * root node is called.
	 */
	public KinematicModel cloneTree(Node parent) {
		KinematicModel clone = this.clone();

		if (parent != null) {
			parent.attachChild(clone);
		}

		for (int i = 0; i < this.children.size(); ++i) {
			((Node) this.children.get(i)).cloneTree(clone);
		}
		return clone;
	}

	public boolean equals(Object obj) {
		if (obj instanceof KinematicModel) {
			return (obj.hashCode() == hashCode());
		}

		return false;
	}

	public String toString() {
		return this.name;
	}
	
	public String getQuaternionString(Joint joint) {
		Mat3 m1 = joint.getLocalTransform().getRotation();
		float w  = (float) Math.sqrt(1.0 + m1.m[0][0] + m1.m[1][1] + m1.m[2][2]) / 2.0f;
		float w4 = (4.0f * w);
		float x  = (m1.m[2][1] - m1.m[1][2]) / w4 ;
		float y  = (m1.m[0][2] - m1.m[2][0]) / w4 ;
		float z  = (m1.m[1][0] - m1.m[0][1]) / w4 ;
		
		StringBuilder sb = new StringBuilder();
		sb.append(joint.name + "\t" + String.valueOf(x) + "\t"
				+ String.valueOf(y) + "\t"
				+ String.valueOf(z) + "\t"
				+ String.valueOf(w) + "nn");
				//+ System.getProperty("line.separator"));
		for (Node child : joint.getChildren()) {
			if (child instanceof Joint) sb.append(this.getQuaternionString((Joint) child));
		}

		return sb.toString();
	}

	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + getHashCode(this.name);
		return hash;
	}

	public static int getHashCode(Object object) {
		return ((object == null) ? 0 : object.hashCode());
	}
}