package kinematics.onlineeditor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTree;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import kinematics.Joint;
import kinematics.KinematicModel;
import streamservice.tracks.Track;

import com.spookengine.scenegraph.Node;
/**
 * instances of this class provide an ui element which allows editing (e.g. reassignment of sensor addresses) of a
 * certain kinematic model which is associated with a datastream ({@link Track}). the editor uses a {@link JTree} to
 * visualize the kinematic chain.
 */
public class ModelPanel extends JPanel implements TreeSelectionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 107101284733735480L;
	/**
	 * custom tree icon, see the {@link TreeIconProvider}.
	 */
	private static Icon TREE_CLOSED_ICON;
	/**
	 * custom tree icon, see the {@link TreeIconProvider}.
	 */
	private static Icon TREE_OPEN_ICON;
	/**
	 * custom tree icon, see the {@link TreeIconProvider}.
	 */
	private static Icon TREE_END_ICON;
	/**
	 * this class provides an editor dialog for a certain joint, it is activated upon clicking on a certain
	 * joint within the tree-view.
	 */
	private class TreeMouseAdapter extends MouseAdapter implements ActionListener {
		/**
		 * the actual editor dialog, the sensor id can be changed via a {@link JSpinner}.
		 */
		private class JointEditorDialog extends JDialog implements ChangeListener {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5892423467269421825L;
			/**
			 * to be edited {@link Joint}.
			 */
			private Joint joint;
			/**
			 * the user selected node (which might be updated after the user changes values).
			 */
			private DefaultMutableTreeNode node;
			/**
			 * sensor id editor.
			 */
			private JSpinner sensorSpinner;
			/**
			 * sets up the dialog for a certain {@link Joint} and the node which is used to visualize
			 * it within the tree view.
			 * @param joint the to be edited {@link Joint}
			 * @param node the tree-node which represents the joint
			 */
			private JointEditorDialog(Joint joint, DefaultMutableTreeNode node) {
				super();
				this.joint = joint;
				this.node = node;
				this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				this.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
				this.setTitle("Edit Bone Properties for: " + this.joint.name);
				
				this.setupContentPane();
				
				Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
				Point upperLeft = new Point(0, 0);
				
//				this.setSize((int) (d.width * .5D), (int) (d.height * .25D));
				this.pack();
				Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
						+ d.height / 2);
				this.setLocation(new Point(center.x - this.getWidth() / 2, center.y
						- this.getHeight() / 2));
				
				this.setVisible(true);
			}
			/**
			 * builds the content pane of the dialog and places components.
			 */
			private void setupContentPane() {
				SpinnerModel sensorIndexModel = new SpinnerNumberModel(this.joint.sensorId, -1, 31, 1);
				this.sensorSpinner = new JSpinner(sensorIndexModel);
				this.sensorSpinner.addChangeListener(this);
				
				JLabel sensorLabel = new JLabel("sensor ID:");
				
				JPanel contentPanel = new JPanel();
				GroupLayout mainLayout = new GroupLayout(contentPanel);
				
				mainLayout.setAutoCreateContainerGaps(true);
				mainLayout.setAutoCreateGaps(true);
				
				mainLayout.linkSize(sensorLabel);
				mainLayout.linkSize(this.sensorSpinner);
				
				mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
					.addGroup(mainLayout.createSequentialGroup()
						.addComponent(sensorLabel)
						.addComponent(this.sensorSpinner)
					)
				);
				
				mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
					.addGroup(mainLayout.createParallelGroup()
						.addComponent(sensorLabel)
						.addComponent(this.sensorSpinner)
					)
				);
				
				contentPanel.setLayout(mainLayout);
				contentPanel.setBorder(BorderFactory.createTitledBorder("Editing Options"));
				this.setContentPane(contentPanel);
			}

			@Override
			public void stateChanged(ChangeEvent ce) {
				if (ce.getSource().equals(this.sensorSpinner)) {
					this.joint.sensorId = ((SpinnerNumberModel) this.sensorSpinner.getModel()).getNumber().intValue();
					this.node.setUserObject(this.joint.name + "[ Id = " + String.valueOf(this.joint.sensorId) + " ]");
				}
			}
		}
		/**
		 * the mouse event is stored to obtain the {@link TreePath} at the clicked position: if it is a valid node,
		 * than editing is possible.
		 */
		private MouseEvent lastMouseEvent;
		
		public void mouseReleased(MouseEvent me) {
			if (me.getButton() != MouseEvent.BUTTON3) {
				return;
			} else {
				this.lastMouseEvent = me;
				this.showPopup(me.getPoint());
			}
		}
		/**
		 * shows the option dialog at the clicked position. currently only sensor editing is available.
		 * @param locationOnScreen
		 */
		private void showPopup(Point locationOnScreen) {
			JPopupMenu optionMenu = new JPopupMenu("options");
			
			JMenuItem editItem = new JMenuItem("edit sensor ids");
			editItem.setToolTipText("change sensor id mapping");
			editItem.setActionCommand("popup_edit_properties");
			editItem.addActionListener(this);
			optionMenu.add(editItem);
			
			JMenuItem closeItem = new JMenuItem("nevermind");
			closeItem.setToolTipText("do nothing");
			optionMenu.addSeparator();
			optionMenu.add(closeItem);
			
			optionMenu.pack();
			
			optionMenu.show(ModelPanel.this.tree, locationOnScreen.x, locationOnScreen.y);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand().equals("popup_edit_properties")) {
				if (this.lastMouseEvent == null) {
					return;
				}
				
				TreePath selectionPath = ModelPanel.this.tree.getPathForLocation(this.lastMouseEvent.getX(), this.lastMouseEvent.getY());
				if (selectionPath != null) {
					final DefaultMutableTreeNode node = ((DefaultMutableTreeNode) selectionPath.getLastPathComponent());
					// XXX unstyle!!!!
					String jointName = node.getUserObject().toString().split("\\[")[0];
					final Joint joint = (Joint) ModelPanel.this.model.getRoot().findChild(jointName);
					
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							new JointEditorDialog(joint, node);
						}
					});
				}
			}
		}
	}
	/**
	 * simple renderer that provides some custom icons for the {@link JTree}, the actual icons are loaded in the
	 * static constructor of this class.
	 */
	private class TreeIconProvider extends DefaultTreeCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6156002430661551621L;

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value,
				boolean selected, boolean expanded, boolean leaf, int row,
				boolean hasFocus) {
			super.getTreeCellRendererComponent(tree, value, selected, expanded,
					leaf, row, hasFocus);
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			if (node.getChildCount() > 0) {
				if (expanded) {
					this.setIcon(ModelPanel.TREE_OPEN_ICON);
				} else {
					this.setIcon(ModelPanel.TREE_CLOSED_ICON);
				}
			} else {
				this.setIcon(ModelPanel.TREE_END_ICON);
			}
			return this;
		}
	}
	/**
	 * the kinematic model of the assigned track, this model can be edited with this class.
	 */
	private KinematicModel model;
	/**
	 * the track enclosing the to be edited model as well as the data.
	 */
	private Track track;
	/**
	 * the tree-like visualization of the kinematic chain.
	 */
	private JTree tree;
	/**
	 * creates a new editor for the model associated with the assigned {@link Track}.
	 * @param track the {@link Track} containing the to be edited model.
	 */
	public ModelPanel(Track track) {
		super();
		this.track = track;
		this.model = track.getActor();
		
		DefaultMutableTreeNode root = this.setupTree(
				new DefaultMutableTreeNode(this.model.getRoot().name), (Joint) this.model.getRoot());
		this.tree = new JTree(root);
		this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.tree.setCellRenderer(new TreeIconProvider());
		this.tree.addTreeSelectionListener(this);
		this.tree.addMouseListener(new TreeMouseAdapter());
		
		JScrollPane treeView = new JScrollPane(this.tree);
		treeView.getVerticalScrollBar().setUnitIncrement(20);
		
		GroupLayout mainLayout = new GroupLayout(this);
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup().addComponent(treeView));
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup().addComponent(treeView));
		this.setLayout(mainLayout);
	}
	/**
	 * builds up the tree view recursively.
	 * @param root the root node of the tree view
	 * @param joint the to be added joint
	 * @return a tree-like structure representing the kinematic chain.
	 */
	private DefaultMutableTreeNode setupTree(DefaultMutableTreeNode root, Joint joint) {
		for (Node child : joint.getChildren()) {
			if (child instanceof Joint) {
				root.add(this.setupTree(new DefaultMutableTreeNode(((Joint) child).name + "[ Id = " + String.valueOf(((Joint) child).sensorId) + " ]"), (Joint) child));
			}
		}
		
		return root;
	}
	
	public KinematicModel getModel() {
		return this.model;
	}
	
	public Track getTrack() {
		return this.track;
	}

	@Override
	public void valueChanged(TreeSelectionEvent tse) {}
	
	static {
		try {
			ModelPanel.TREE_CLOSED_ICON = new ImageIcon((ImageIO.read(ModelPanel.class.getResourceAsStream("/kinematics/onlineeditor/icons/tree_closed_icon.gif")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ModelPanel.TREE_OPEN_ICON   = new ImageIcon((ImageIO.read(ModelPanel.class.getResourceAsStream("/kinematics/onlineeditor/icons/tree_open_icon.gif")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
			ModelPanel.TREE_END_ICON    = new ImageIcon((ImageIO.read(ModelPanel.class.getResourceAsStream("/kinematics/onlineeditor/icons/tree_end_icon.gif")).getScaledInstance(24, 24, BufferedImage.SCALE_SMOOTH)));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
