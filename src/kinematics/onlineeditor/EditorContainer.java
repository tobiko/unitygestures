package kinematics.onlineeditor;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import kinematics.KinematicModel;

import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import streamservice.stream.StreamService;
import streamservice.stream.events.TrackActorMapEvent;
import streamservice.stream.events.TracksCreatedEvent;
import streamservice.stream.events.TracksDestroyedEvent;
import streamservice.tracks.Track;
import utils.visualization.EditableTabbedPane;
import eventhandling.EventBus;

/**
 * a simple container for the joint editor in the gui. it listens for the creation and destruction of tracks and
 * adds or removes editors accordingly. the actual editor functionality is realized in the {@link ModelPanel} class.
 */
public class EditorContainer extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2312713236123764559L;
	/**
	 * Listener for the {@link EventBus} which is invoked whenever tracks
	 * are created, this is usually the case if network devices are detected and
	 * connected to the {@link StreamService}.
	 */
	private class OnTracksCreated implements LookupListener {
		/**
		 * {@link TracksCreatedEvent}s will be stored in this collection.
		 */
		private Lookup.Result<TracksCreatedEvent> results;
		/**
		 * listener which obtains {@link TracksCreatedEvent}s from the {@link EventBus}.
		 */
		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(TracksCreatedEvent.class);
			this.results.addLookupListener(this);
		}
		/**
		 * invoked when new {@link TracksCreatedEvent}s have been created. in this case, the according {@link Track}
		 * is fetched and a new {@link ModelPanel} is created (which allows editing of the {@link KinematicModel}),
		 * which is then added to the {@link EditorContainer}.
		 */
		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksCreatedEvent> it = this.results.allInstances().iterator();

			if (it.hasNext()) {
				TracksCreatedEvent evt = (TracksCreatedEvent) it.next();

				for (Track track : evt.tracks) {
					ModelPanel modelPanel = new ModelPanel(track);
					EditorContainer.this.modelPanels.add(modelPanel);
					EditorContainer.this.mainPane.addTab(track.getActor().toString() + "[" + track.getName() + "]", modelPanel);
				}
			}
		}
	}
	/**
	 * Listener for the {@link EventBus} which is invoked whenever tracks
	 * are destroyed, this is usually the case if network devices are
	 * disconnected from the {@link StreamService}.
	 */
	private class OnTracksDestroyed implements LookupListener {
		/**
		 * {@link TracksDestroyedEvent}s will be stored in this collection.
		 */
		private Lookup.Result<TracksDestroyedEvent> results;
		/**
		 * listener which obtains {@link TracksDestroyedEvent}s from the {@link EventBus}.
		 */
		public void listen() {
			this.results = EventBus.getInstance().getLookup()
					.lookupResult(TracksDestroyedEvent.class);
			this.results.addLookupListener(this);
		}
		/**
		 * invoked when new {@link TracksDestroyedEvent}s have been created. in this case, the according {@link ModelPanel}
		 * is fetched and removed from the {@link EditorContainer}.
		 */
		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksDestroyedEvent> it = this.results
					.allInstances().iterator();

			if (it.hasNext()) {
				TracksDestroyedEvent evt = (TracksDestroyedEvent) it.next();

				for (Track track : evt.tracks) {
					String title = track.getActor().toString() + "[" + track.getName() + "]";
					int index = EditorContainer.this.mainPane.indexOfTab(title);
					
					if (index != -1) {
						EditorContainer.this.mainPane.remove(index);
					}
					
					for (int i = 0; i < EditorContainer.this.modelPanels.size(); i++) {
						ModelPanel modelPanel = EditorContainer.this.modelPanels.get(i);
						if (modelPanel.getTrack() == track) {
							EditorContainer.this.modelPanels.remove(modelPanel);
							break;
						}
					}
				}
			}
		}
	}
	private class OnTrackActorMap implements LookupListener {
		private Lookup.Result<TrackActorMapEvent> result;

		public void listen() {
			this.result = EventBus.getInstance().getLookup()
					.lookupResult(TrackActorMapEvent.class);
			this.result.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TrackActorMapEvent> it = this.result.allInstances().iterator();
			while (it.hasNext()) {
				TrackActorMapEvent evt = (TrackActorMapEvent) it.next();
				Track track = evt.track;
				// actor name might have changed...
				//String title = track.getActor().toString() + "[" + track.getName() + "]";
				//int index = EditorContainer.this.mainPane.indexOfTab(title);
				int index = -1;
				for (int i = 0; i < EditorContainer.this.mainPane.getTabCount(); i++) {
					if (EditorContainer.this.mainPane.getTitleAt(i).endsWith("[" + track.getName() + "]")) {
						index = i;
						break;
					}
				}
				
				if (index != -1) {
					EditorContainer.this.mainPane.remove(index);
				}
				
				for (int i = 0; i < EditorContainer.this.modelPanels.size(); i++) {
					ModelPanel modelPanel = EditorContainer.this.modelPanels.get(i);
					if (modelPanel.getTrack().equals(track)) {
						EditorContainer.this.modelPanels.remove(modelPanel);
						break;
					}
				}
				
				ModelPanel modelPanel = new ModelPanel(track);
				EditorContainer.this.modelPanels.add(modelPanel);
				EditorContainer.this.mainPane.addTab(track.getActor().toString() + "[" + track.getName() + "]", modelPanel);
			}
		}
	}
	/**
	 * listener which responds in case of creation of a new {@link Track}
	 */
	private final OnTracksCreated onTracksCreated = new OnTracksCreated();
	/**
	 * this listener is invoked if a certain {@link Track} is destroyed.
	 */
	private final OnTracksDestroyed onTracksDestroyed = new OnTracksDestroyed();
	/**
	 * this listener is invoked if the actor of a certain {@link Track} is changed.
	 */
	private final OnTrackActorMap onTrackActorMap = new OnTrackActorMap();
	/**
	 * a list of currently active editors, i.e. {@link ModelPanel}s.
	 */
	private ArrayList<ModelPanel> modelPanels;
	/**
	 * the {@link JTabbedPane} where the editors are stored.
	 */
	private EditableTabbedPane mainPane;
	/**
	 * builds a new, empty container.
	 */
	public EditorContainer() {
		super();
		
		this.modelPanels = new ArrayList<ModelPanel>();
		this.mainPane = new EditableTabbedPane();
		
		this.onTracksCreated.listen();
		this.onTracksDestroyed.listen();
		this.onTrackActorMap.listen();
		
		GroupLayout mainLayout = new GroupLayout(this);
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup().addComponent(this.mainPane));
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup().addComponent(this.mainPane));
		this.setLayout(mainLayout);
		
		this.setBorder(BorderFactory.createTitledBorder("Kinematic Model Editors"));
	}
}
