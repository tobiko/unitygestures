package kinematics;

import kinematics.models.TCPLeftHand;
import kinematics.models.TCPRightHand;
import kinematics.models.TCPSuit;
import kinematics.models.TCPUpperSuitWithGloves;
/**
 * Just a simple factory class to obtain kinematic models, created by {@link KinematicModelService}
 * instances. Of course it would be more elegant to realize this via reflection, but this lazy approach
 * works as well.
 */
public class KinematicModelProvider {
	/**
	 * available models by name, these ids are also used in the GUI, a new model have to be included
	 * here as well as in the method below.
	 */
	public static String[] availableModelNames = {
		TCPLeftHand.modelName,
		TCPRightHand.modelName,
		TCPSuit.modelName,
		TCPUpperSuitWithGloves.modelName
	};
	/**
	 * simple factory method which provides kinematic models ({@link KinematicModelService}s) according to the
	 * assigned name.
	 * @param modelName name of the to be obtained model.
	 * @return the model indicated by the assigned name, null if no model with this name is found.
	 */
	public static KinematicModelService getServiceByName(String modelName) {
		if (modelName.equals(TCPLeftHand.modelName)) {
			return new TCPLeftHand();
		} else if (modelName.equals(TCPRightHand.modelName)) {
			return new TCPRightHand();
		} else if (modelName.equals(TCPSuit.modelName)) {
			return new TCPSuit();
		} else if (modelName.equals(TCPUpperSuitWithGloves.modelName)) {
			return new TCPUpperSuitWithGloves();
		} else {
			return null;
		}
	}
	
}
