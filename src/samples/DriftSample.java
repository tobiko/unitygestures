package samples;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import main.GlobalSettingsAndVariables;
import scene.Scene;
import streamservice.onlinedatamanipulation.AvailableManipulations;
import streamservice.onlinedatamanipulation.ExtWristDrift;

public class DriftSample implements ISample {

	private final static long TIME_TO_RUN = 120L * 1000L;
	private final static long TIME_DELAY = 10L * 1000L;
	
	private JPanel panel;
	private JProgressBar progressBar;
	
	@Override
	public void init(Scene scene) {
		this.panel = new JPanel();
		
		this.progressBar = new JProgressBar(
				0, 100);
		this.progressBar.setValue(0);
		this.progressBar.setStringPainted(true);
		this.progressBar.setIndeterminate(false);
		
		JLabel progLabel = new JLabel("trial progress:");
		
		GroupLayout layout = new GroupLayout(this.panel);
		layout.setVerticalGroup(layout.createSequentialGroup()
			.addGroup(layout.createParallelGroup()
				.addComponent(progLabel)
				.addComponent(this.progressBar)
			)
		);
		layout.setHorizontalGroup(layout.createParallelGroup()
			.addGroup(layout.createSequentialGroup()
				.addComponent(progLabel)
				.addComponent(this.progressBar)
			)
		);
		this.panel.setLayout(layout);
		
		ExtWristDrift model = new ExtWristDrift();
		model.initTimedControl(this.progressBar, DriftSample.TIME_TO_RUN, DriftSample.TIME_DELAY);
		if (model != null) {
			model.setScene(scene);
		}
		if (GlobalSettingsAndVariables.defaultDataManipulator != null) {
			GlobalSettingsAndVariables.defaultDataManipulator.shutDown();
		}
		GlobalSettingsAndVariables.defaultDataManipulator = model;
		GlobalSettingsAndVariables.defaultDataManipulatorName = model.getName();
	}

	@Override
	public boolean usesUI() {
		return true;
	}

	@Override
	public JPanel getUIComponent() {
		return this.panel;
	}
	
	@Override
	public void shutDown() {
		if (GlobalSettingsAndVariables.defaultDataManipulator != null) {
			GlobalSettingsAndVariables.defaultDataManipulator.shutDown();
		}
		GlobalSettingsAndVariables.defaultDataManipulator = null;
		GlobalSettingsAndVariables.defaultDataManipulatorName = AvailableManipulations.availableManipulators[0];
	}
}
