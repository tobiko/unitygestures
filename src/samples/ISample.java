package samples;

import javax.swing.JPanel;

import scene.Scene;

public interface ISample {

	public void init(Scene scene);
	
	public boolean usesUI();
	
	public JPanel getUIComponent();
	
	public void shutDown();
	
}
