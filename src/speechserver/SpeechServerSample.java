package speechserver;

import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import utils.TextAreaOutputStream;

public class SpeechServerSample extends JPanel implements ActionListener, PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4785582978142903041L;

	private class SpeechServerSampleInternalFrameListener implements InternalFrameListener {

		@Override
		public void internalFrameOpened(InternalFrameEvent ife) {}

		@Override
		public void internalFrameClosing(InternalFrameEvent ife) {
			if (SpeechServerSample.this.connector != null) {
				SpeechServerSample.this.connector.stopBackgroundThread();
			}
		}

		@Override
		public void internalFrameClosed(InternalFrameEvent ife) {}

		@Override
		public void internalFrameIconified(InternalFrameEvent ife) {}

		@Override
		public void internalFrameDeiconified(InternalFrameEvent ife) {}

		@Override
		public void internalFrameActivated(InternalFrameEvent ife) {}

		@Override
		public void internalFrameDeactivated(InternalFrameEvent ife) {}
		
	}
	
	private PropertyChangeSupport pcs;
	private TextAreaOutputStream  trafficDisplay;
	
	private SpeechServerConnector connector;
	private SpeechServerSampleInternalFrameListener frameListener;
	
	public SpeechServerSample() {
		super();
		this.pcs = new PropertyChangeSupport(this);
		this.pcs.addPropertyChangeListener(this);
		this.frameListener = new SpeechServerSampleInternalFrameListener();
		this.connector = new SpeechServerConnector(this.pcs);
		
		this.initContentPane();
	}
	
	private void initContentPane() {
		JTextArea messageArea = new JTextArea(5, 20);
		messageArea.setMargin(new Insets(5, 5, 5, 5));
		messageArea.setEditable(false);
		messageArea.setBorder(BorderFactory.createTitledBorder("Speech Server Traffic"));

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JScrollPane(messageArea), BorderLayout.CENTER);
		
		this.trafficDisplay = new TextAreaOutputStream(messageArea, 200);
		
		JButton disconnectButton = new JButton("close connection");
		disconnectButton.setActionCommand("stop_server");
		disconnectButton.addActionListener(this);
		disconnectButton.setEnabled(true);
		
		JPanel controlPanel = new JPanel();
		GroupLayout controlLayout = new GroupLayout(controlPanel);
		controlLayout.setAutoCreateContainerGaps(true);
		controlLayout.setAutoCreateGaps(true);
		
		controlLayout.setVerticalGroup(controlLayout.createSequentialGroup()
			.addGroup(controlLayout.createParallelGroup()
				.addComponent(disconnectButton)
			)
		);
		
		controlLayout.setHorizontalGroup(controlLayout.createParallelGroup()
			.addGroup(controlLayout.createSequentialGroup()
				.addGap(0, 100, Short.MAX_VALUE)
				.addComponent(disconnectButton)
			)
		);
		
		controlPanel.setLayout(controlLayout);
		controlPanel.setBorder(BorderFactory.createTitledBorder("Controls"));
		
		GroupLayout mainLayout = new GroupLayout(this);
		mainLayout.setAutoCreateContainerGaps(true);
		mainLayout.setAutoCreateGaps(true);
		
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
			.addComponent(panel)
			.addComponent(controlPanel)
		);
		
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
			.addComponent(panel)
			.addComponent(controlPanel)
		);
		
		this.setLayout(mainLayout);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("stop_server")) {
			if (this.trafficDisplay != null) {
				this.trafficDisplay.write("closing connection..." + System.getProperty("line.separator"));
			}
			
			if (this.connector != null) {
				this.connector.stopBackgroundThread();
			}
			
			if (ae.getSource() instanceof JButton) {
				((JButton) ae.getSource()).setEnabled(false);
			}
		}
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getPropertyName().equals("text_from_speechserver")) {
			if (pce.getNewValue() instanceof String) {
				if (this.trafficDisplay == null) {
					System.out.println(pce.getNewValue());
				} else {
					this.trafficDisplay.write(((String) pce.getNewValue()) + System.getProperty("line.separator"));
				}
			}
		}
	}
	
	public SpeechServerSampleInternalFrameListener getFrameListener() {
		return this.frameListener;
	}
}
