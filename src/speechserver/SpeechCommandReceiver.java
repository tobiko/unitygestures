package speechserver;

public interface SpeechCommandReceiver {

	public String processSpeechCommand(String command);
	
}
