package speechserver;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import main.GlobalSettingsAndVariables;

public class SpeechServerConnector implements PropertyChangeListener {
	
	public final static int speechServerReceiverPortNumber = 5721;
	
	public final static int speechServerSenderPortNumber = 5723;
	
	private class SocketListener extends SwingWorker<Void, Void> {

		private ServerSocket serverSocket;
		
		private boolean serverReady;
		
		private boolean ready;
		
		private SocketListener() {
			super();
			this.ready = false;
			this.initServerSocket();
			
			int status = -1;
			
			if (this.serverReady) {
				SpeechServerConnector.this.localPCS.firePropertyChange(
						"server_socket_ready", true, false);
				status = this.startSpeechRecognitionServer();
			}
			
			if (status == 0) {
				this.ready = true;
			}
		}
		
		private void initServerSocket() {
			boolean ready = true;
			
			try {
				this.serverSocket = new ServerSocket(SpeechServerConnector.speechServerReceiverPortNumber);
				this.serverSocket.setSoTimeout(100);
			} catch (IOException ioe) {
				ioe.printStackTrace();
				ready = false;
			}
			
			this.serverReady = ready;
		}
		
		private int startSpeechRecognitionServer() {
			int exitValue = -1;
			
			Runtime runtime = Runtime.getRuntime();
			try {
				Process cSharpVerbalServer = runtime.exec(GlobalSettingsAndVariables.speechServerBatchFilePath);
				exitValue = cSharpVerbalServer.waitFor();
				System.out.println("process exit with value: " + String.valueOf(exitValue));
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
			
			return exitValue;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			while (this.ready) {
				try {
					Socket clientSocket = null;
					try {
						clientSocket = this.serverSocket.accept();
					} catch (IOException ioe) {
//						System.out.println("connection time-out...");
						continue;
					}
					System.out.println("client initialized...");
					BufferedReader in = new BufferedReader(
							new InputStreamReader(clientSocket.getInputStream()));
					String inputLine;//, outputLine;

					while ((inputLine = in.readLine()) != null) {
						System.out.println(inputLine);
						SpeechServerConnector.this.localPCS.firePropertyChange(
								"server_socket_received", null, inputLine);
					}

					in.close();
				} catch (IOException e) {
					System.out
							.println("Exception caught when trying to listen on port "
									+ SpeechServerConnector.speechServerReceiverPortNumber + " or listening for a connection");
					System.out.println(e.getMessage());
				}
			}

			try {
				this.serverSocket.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			System.out.println("server thread ended...");
			SpeechServerConnector.this.localPCS.firePropertyChange(
					"server_socket_closed", true, false);
			
			this.serverReady = false;
			return null;
		}
		
		private void stop() {
			this.ready = false;
			SpeechServerConnector.this.sendMessage("Control: closing");
		}	
	}
	
	private SocketListener socketListener;
	
	private PropertyChangeSupport pcs;
	private PropertyChangeSupport localPCS;
	
	public SpeechServerConnector(PropertyChangeSupport pcs) {
		super();
		this.pcs = pcs;
		this.localPCS = new PropertyChangeSupport(this);
		this.localPCS.addPropertyChangeListener(this);
		
		this.socketListener = new SocketListener();
		this.socketListener.execute();
	}
	
	public void stopBackgroundThread() {
		if (SpeechServerConnector.this.socketListener.ready) {
			SpeechServerConnector.this.socketListener.stop();
			while (SpeechServerConnector.this.socketListener.serverReady) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getPropertyName().equals("server_socket_received")) {
			if (!pce.getNewValue().toString().startsWith("received speech:")) {
				return;
			}
			
			final String receivedText = pce.getNewValue().toString();//.split("received speech: ")[1].trim();
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					SpeechServerConnector.this.pcs.firePropertyChange(
							"text_from_speechserver", null, receivedText);
				}
			});
		}
	}
	
	public void sendMessage(String message) {		
		try {
			Socket socket = new Socket();
			
			try {
				socket.connect(new InetSocketAddress("127.0.0.1", SpeechServerConnector.speechServerSenderPortNumber), 20);
			} catch (SocketTimeoutException ste) {
				System.out.println("server seems to be down...");
			}
			
			PrintWriter writer = new PrintWriter(socket.getOutputStream());
			writer.print(message);
			writer.flush();
			writer.close();
			socket.close();
		} catch (ConnectException ce) {
			ce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
}