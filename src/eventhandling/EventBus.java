package eventhandling;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Iterator;

import network.connections.tcp.TCPSuitConnector;
import network.connections.tcp.events.SuitsConnectedEvent;

import org.openide.util.Lookup;
import org.openide.util.LookupListener;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

import streamservice.StreamPanel;
import streamservice.stream.StreamService;

/**
 * in the first design of this program i stuck to my usual approach of inter-service communication without explicit referencing:
 * the usage of a {@link PropertyChangeSupport} in combination with various {@link PropertyChangeListener}s. however, the idea
 * of a central event buffer seems more elegant. the applied classes are not part of the standard JDK but come from the UI
 * framework shipping with the NetBeans IDE. It would be nice to port the necessary functions instead of including large parts of
 * the framework...
 * 
 * Using this event buffer, classes can register for certain types of events via the {@link LookupListener} interface.
 * Further, classes can invoke the buffer via the add method. Similar to the {@link PropertyChangeSupport} approach, invoker
 * and listener not necessarily know of each other, but the overhead for the listening and invoking procedures is much smaller
 * in my opinion.
 * 
 * Just one example for the usage of this class: Upon establishing a connection to a network device, the {@link TCPSuitConnector}
 * puts some {@link SuitsConnectedEvent}s on this buffer. The {@link StreamPanel} hosts various listeners, for instance one that
 * listens for such events. Consequently it is invoked and also invokes the {@link StreamService} in the same manner. All these
 * classes are independent of each other and the events are sorted out by there class type instead of some kind of propagation id.
 * quite neat i think...
 */
public class EventBus {
	/**
	 * the event buffer used by all classes which want to put events in the queue.
	 */
	private static EventBus instance;
	/**
	 * this object allows to add and remove content from the queue, its part of the
	 * org.openide.util.lookup package.
	 */
	private final InstanceContent content;
	/**
	 * the actual lookup for the events, i.e. the event types. this lookup can be used
	 * to fetch data from the queue, e.g. via 
	 * EventBus.getInstance().getLookup().lookupResult(ClassOfInterest.class).
	 */
	private final Lookup lookup;
	/**
	 * constructor for the event buffer instance, since it is a singleton, this constructor
	 * is private and only called once by the {@link EventBus#getInstance()} method.
	 */
	private EventBus() {
		super();
		this.content = new InstanceContent();
		this.lookup = new AbstractLookup(this.content);
	}
	/**
	 * Returns and if necessary creates the {@link EventBus} singleton.
	 * @return the {@link EventBus} instance.
	 */
	public static EventBus getInstance() {
		if (EventBus.instance == null) {
			EventBus.instance = new EventBus();
		}
		return EventBus.instance;
	}
	/**
	 * returns the lookup to fetch some events.
	 * @return the {@link Lookup} associated with this event buffer.
	 */
	public Lookup getLookup() {
		return this.lookup;
	}
	/**
	 * puts an event into the queue.
	 * @param evt the to be posed event, can be any kind of object.
	 */
	public void add(Object evt) {
		System.out.println("event fired...");
		this.content.add(evt);
	}
	/**
	 * removes events from the queue.
	 * @param evt the to be removed event.
	 */
	public void remove(Object evt) {
		this.content.remove(evt);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	/**
	 * removes all instances of a certain class from the queue.
	 * @param currentClass the class of the to be removed events.
	 */
	public void removeAll(Class currentClass) {
		Iterator it = this.lookup.lookupAll(currentClass).iterator();
		while (it.hasNext())
			this.content.remove(it.next());
	}
	/**
	 * to make sure that this remains a singleton, the clone operation always throws an {@link Exception}.
	 */
	public Object clone() {
		throw new UnsupportedOperationException("The event bus is a singleton...");
	}
}
