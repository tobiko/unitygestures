package scene;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import kinematics.Joint;
import kinematics.KinematicModel;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import streamservice.frames.FrameBuffer;
import streamservice.frames.FrameBufferFactory;
import streamservice.frames.KinematicFrame;
import streamservice.tracks.Track;

import com.spookengine.core.Geom;
import com.spookengine.core.Trimesh;
import com.spookengine.core.camera.Cam;
import com.spookengine.core.camera.CameraMan;
import com.spookengine.core.events.Task;
import com.spookengine.core.events.TaskScheduler;
import com.spookengine.core.lights.LightBulb;
import com.spookengine.core.lights.LightMan;
import com.spookengine.maths.Euler;
import com.spookengine.maths.FastMath;
import com.spookengine.maths.Mat3;
import com.spookengine.maths.Vec2;
import com.spookengine.maths.Vec3;
import com.spookengine.platform.desktop.JOGLRenderer3;
import com.spookengine.platform.desktop.ObjLoader;
import com.spookengine.platform.desktop.TextureLoader;
import com.spookengine.scenegraph.Node;
import com.spookengine.scenegraph.Spatial;
import com.spookengine.scenegraph.Visual;
import com.spookengine.scenegraph.appearance.App;
import com.spookengine.scenegraph.appearance.Colour;
import com.spookengine.scenegraph.appearance.Material;

public class Scene {

	private static final float METER_TO_INCH = 39.370079F;
	public boolean demoMode = false;
	private FrameBufferFactory frameBufferFactory;
	private FrameBuffer frameBuffer;
	private float size;
	private Visual root;
	private LightBulb sun;
	public boolean followSkeleton = false;
	private Spatial lookat;
	private Spatial camGroup;
	private CameraMan camMan;
	private Spatial stageGroup;
	private Visual actorsGroup;
	private Geom floor;
	private Map<Track, KinematicModel> skeletons;
	private Vec3 worldUpAxis;
	private Vec3 worldDirectionAxis;
	private Vec3 worldAxesCrossProduct;
	private float theta;
	private float phi;
	private Vec3 tmpV = new Vec3();
	private Vec3 boneDir = new Vec3();
	private Vec3 axis = new Vec3();
	private Vec3 zeros = new Vec3();
	public boolean lines;

	public Scene() {
		super();
		this.lines = false;
		this.size = 787.40161F;

		this.skeletons = new HashMap<Track, KinematicModel>();

		this.worldUpAxis = new Vec3();
		this.worldDirectionAxis = new Vec3();
		this.worldAxesCrossProduct = new Vec3();

		this.init();

		this.frameBufferFactory = GlobalSettingsAndVariables.defaultFrameBufferFactory;
		this.frameBuffer = this.frameBufferFactory.createFrameBuffer(
				"SceneBuffer", 1);

		TaskScheduler.getInstance("Scene").schedule(3, new BillboardTask());
		TaskScheduler.getInstance("Scene").schedule(3, new UpdateTask());
		TaskScheduler.getInstance("Scene").schedule(3, new PrettyCameraTask());
	}
	
	public void init() {
		JOGLRenderer3.clearColour = new Vec3(
				0.75F, 0.75F, 0.75F);
		
		this.root = new Visual("root");

		this.lookat = new Spatial("lookat");
		this.lookat.getLocalTransform().moveTo(0.0F, 0.0F, 40.0F).update();
		this.root.attachChild(this.lookat);

		this.camGroup = new Spatial("camera_group");
		this.camGroup.getLocalTransform().moveBy(0.0F, -120.0F, 0.0F).update();
		this.lookat.attachChild(this.camGroup);

		this.camMan = new CameraMan("camera_man", new Cam(
				Cam.Projection.PERSPECTIVE));
		this.camMan.getCamera().setFarClip(2000.0F);
		this.camGroup.attachChild(this.camMan);

		this.sun = new LightBulb(0.7F, 0.7F, 0.7F);
		this.sun.hasAmbience = true;
		LightMan lightMan = new LightMan("sun", this.sun);
		lightMan.getLocalTransform().moveBy(0.0F, 1.0F, 0.5F).update();
		this.camGroup.attachChild(lightMan);

		this.stageGroup = new Spatial("stage");
		this.root.attachChild(this.stageGroup);

//		this.stageGroup.attachChild(createFloor(2.0F));
		this.stageGroup.attachChild(createFloor(2000.0F));
		
		this.actorsGroup = new Visual("actors");
		this.actorsGroup.getLocalAppearance().override(64);
		this.actorsGroup.getLocalAppearance().addLight(this.sun);
		this.stageGroup.attachChild(this.actorsGroup);

		if (!this.skeletons.isEmpty()) {
			for (KinematicModel skeleton : this.skeletons.values()) {
				Spatial spatial = new Spatial("SkeletonRoot");
				spatial.attachChild(skeleton);
				this.actorsGroup.attachChild(spatial);
			}
		}
	}

	public void recreate() {
		synchronized (this.floor.lock) {
			this.floor.getLocalAppearance().removeTexture(0);
			try {
				BufferedImage texImg = ImageIO.read(super.getClass().getResourceAsStream("/scene/textures/red_floor.png"));
				this.floor.getLocalAppearance().addTexture(
						TextureLoader.getInstance().loadTexture(texImg));
			} catch (IOException ex) {
				MainFrame.defaultConsole.write("Failed to load floor texture..." + System.getProperty("line.separator"));
				ex.printStackTrace();
			}
		}
	}

	public void recalculateFloorUnits(float floorUnits) {
		synchronized (this.floor.lock) {
			float texRepeat = this.size / floorUnits * Scene.METER_TO_INCH;
			Vec2[] texCoordArray = { new Vec2(0.0F, 0.0F),
					new Vec2(texRepeat, 0.0F), new Vec2(0.0F, texRepeat),
					new Vec2(texRepeat, texRepeat) };

			this.floor.getTrimesh().removeTexCoords(0);
			this.floor.getTrimesh().addTexCoords(texCoordArray);
		}
	}

	private Visual createFloor(float floorUnits) {
		this.floor = new Geom("floor", Trimesh.Quad(this.size, this.size,
				this.size / floorUnits * Scene.METER_TO_INCH));
		this.floor.getLocalTransform()
				.rotateToRPY(0.0F, -FastMath.toRadians(90.0F), 0.0F).update();
		try {
			BufferedImage texImg = ImageIO.read(super.getClass().getResourceAsStream("/scene/textures/red_floor.png"));
			this.floor.getLocalAppearance().override(App.COLOUR);
			this.floor.getLocalAppearance().addTexture(
					TextureLoader.getInstance().loadTexture(texImg));
			this.floor.getLocalAppearance().colour = new Colour(new float[] {
					0.65F, 0.65F, 0.65F });
		} catch (IOException ex) {
			MainFrame.defaultConsole.write("Failed to load floor texture..." + System.getProperty("line.separator"));
			ex.printStackTrace();
		}

		return this.floor;
	}

	public Visual getRoot() {
		return this.root;
	}

	public Cam getCamera() {
		return this.camMan.getCamera();
	}

	public void removeSkeleton(Track track) {
		KinematicModel skeleton = (KinematicModel) this.skeletons.remove(track);

		if (skeleton != null)
			this.actorsGroup.detachChild(skeleton);
	}

	public void removeAllSkeletons() {
		for (KinematicModel skeleton : this.skeletons.values()) {
			this.actorsGroup.detachChild(skeleton);
		}

		this.skeletons.clear();
	}

	public void addSkeleton(Track track, KinematicModel skeleton) {
		if (!(this.skeletons.containsKey(track))) {
			this.skeletons.put(track, skeleton);

			this.visualiseSkeleton(skeleton.getRoot());
			this.actorsGroup.attachChild(skeleton);
		}
	}

	private void visualiseSkeleton(Joint joint) {
		float diameter = 2.0F * joint.radius;
		Geom visualJoint = new Geom("ContactRadius", Trimesh.Ellipse(diameter,
				diameter, 16, false));
		visualJoint.getLocalAppearance().override(App.MATERIAL);
		visualJoint.getLocalAppearance().material = new Material(new float[] {
				0.06F, 0.05F, 0.02F }, new float[] { 0.06F, 0.05F, 0.02F });
		
		joint.attachChild(visualJoint);
		Spatial parent;
		float length;
		float angle;
		
		if ((joint.getParent() != null) && (joint.getParent() instanceof Joint)) {
			parent = (Joint) joint.getParent();

			if (parent != null) {
				this.tmpV.setTo(joint.getLocalTransform().getPosition());
				length = this.tmpV.length();

				this.boneDir.setTo(this.tmpV).norm();
				angle = (float) (-Math.acos(this.boneDir.dot(0.0F, 0.0F, 1.0F)));
				this.axis.setTo(this.boneDir).cross(0.0F, 0.0F, 1.0F).norm();

				if (this.axis.equals(this.zeros))
					this.axis.setTo(0.0F, 1.0F, 0.0F);
				try {
					Visual bone;
					if (this.lines)
						bone = ObjLoader.getInstance().loadModel(
								"/scene/models/",
								"bone_zup2.obj");
					else
						bone = ObjLoader.getInstance()
								.loadModel("/scene/models/",
										"bone_zup.obj");
					bone.name = "BoneGeom";
					bone.getLocalTransform().scaleTo(length)
							.rotateTo(angle, this.axis).update();
					parent.attachChild(bone);
				} catch (IOException ex) {
					System.out.println("Unable to load contact point model...");
					ex.printStackTrace();
				}
			}
		}
		joint.getLocalTransform().update();

		Object[] children = joint.getChildren().toArray();
		int numberOfChildren = children.length;
		for (int i = 0; i < numberOfChildren; i++) {
			Object child = children[i];
			if (child instanceof Joint) {
				this.visualiseSkeleton((Joint) child);
			}
		}
	}

	public void rotate(float dy, float dx) {
		this.theta -= 3.141593F * dx;
		this.phi -= 3.141593F * dy;

		this.lookat.getLocalTransform().rotateToRPY(0.0F, this.phi, this.theta)
				.update();
//		Vec3 pos = this.camGroup.getLocalTransform().getPosition();
//		MainFrame.defaultConsole.write("cam: " + String.valueOf(Math.toDegrees(this.theta)) + " " + String.valueOf(Math.toDegrees(this.phi)) + System.getProperty("line.separator"));
//		MainFrame.defaultConsole.write("pos (camgroup): " + String.valueOf(pos.x()) + " " + String.valueOf(pos.y()) + " " + String.valueOf(pos.z()) + System.getProperty("line.separator"));
//		pos = this.lookat.getLocalTransform().getPosition();
//		MainFrame.defaultConsole.write("pos (lookat): " + String.valueOf(pos.x()) + " " + String.valueOf(pos.y()) + " " + String.valueOf(pos.z()) + System.getProperty("line.separator"));
//		cam: 180.23829664871974 -70.12859847289074
//		pos (camgroup): 0.0 -30.0 0.0
//		pos (lookat): -0.6489346 -9.91447 70.52961

	}

	public void translate(float dy, float dx) {
		this.worldUpAxis.setTo(this.camMan.getWorldUp());
		this.worldDirectionAxis.setTo(this.camMan.getWorldDir());
		this.worldAxesCrossProduct.setTo(this.worldUpAxis);
		this.worldAxesCrossProduct.cross(this.worldDirectionAxis);

		Vec3 dUp = this.worldUpAxis.mult(dy);
		Vec3 dLeft = this.worldAxesCrossProduct.mult(dx);

		this.lookat.getLocalTransform().moveBy(dUp.add(dLeft)).update();
	}

	public void zoom(float z) {
		this.camGroup.getLocalTransform().moveBy(0.0F, z, 0.0F).update();
	}
	
	public void toHeadCenter() {
		Node headTopNode = this.root.findChild("HeadTop");
		Node hipNode = this.root.findChild("Hips");
		if (headTopNode != null && hipNode != null) {
			Joint headTop = (Joint) headTopNode;
			Joint hip = (Joint) hipNode;
			Vec3 pos = headTop.getWorldTransform().getPosition();
			
			Euler angle = new Euler();
			// XXX not super sure if the hip is the best choice, but i thought the orientation towards the body center would be
			// more useful than some kind of gaze extrapolation
//			hip.getWorldTransform().getRotationPRY(angle);
			hip.getLocalTransform().getRotationPRY(angle);
			this.theta = angle.yaw;
			this.phi = angle.pitch;
//			Mat3 m = hip.getLocalTransform().getRotation().invert();
//			Trfm t = new Trfm();
//			t.setTo(hip.getLocalTransform());
//			t.rotateTo(m);
//			t.getRotationPRY(angle);
//			this.theta = angle.yaw;
//			this.phi = angle.pitch;
			
//			this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
//					.rotateToRPY(0.0F, this.phi, this.theta).update();
//			this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
			
//			junk
//			this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
//			.rotateToRPY(0.0F, this.phi, this.theta).update();
//			this.lookat.getLocalTransform().rotateToRPY(0.0F, FastMath.toRadians(-70.0F), FastMath.toRadians(180.0F)).update();
//			this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
			
			angle = new Euler();
			this.lookat.getLocalTransform().getRotationPRY(angle);
			// XXX experimental
			Joint leftHand = (Joint) this.root.findChild("LeftHand");
			Joint rightHand = (Joint) this.root.findChild("RightHand");
			if (leftHand != null && rightHand != null) {
				// XXX
//				matrix to quaternion
//				qw= sqrt(1 + m00 + m11 + m22) /2
//				qx = (m21 - m12)/( 4 *qw)
//				qy = (m02 - m20)/( 4 *qw)
//				qz = (m10 - m01)/( 4 *qw)
//				Mat3 m = hip.getLocalTransform().getRotation();
//				float qw = (float) Math.sqrt(1.0f + m.m[0][0] + m.m[1][1] + m.m[2][2]) / 2.0f;
//				float w4 = (4.0f * qw);
//				float qx = (m.m[2][1] - m.m[1][2]) / w4 ;
//				float qy = (m.m[0][2] - m.m[2][0]) / w4 ;
//				float qz = (m.m[1][0] - m.m[0][1]) / w4 ;
//				// invert...
//				qw *= -1.0f;
//				qy *= -1.0f;
//				qz *= -1.0f;
//				// to matrix
//				float xx = qx * qx;
//				float xy = qx * qy;
//				float xz = qx * qz;
//				float xw = qx * qw;
//
//				float yy = qy * qy;
//				float yz = qy * qz;
//				float yw = qy * qw;
//
//				float zz = qz * qz;
//				float zw = qz * qw;
//				
//				float m00 = 1 - 2 * (yy + zz);
//				float m01 = 2 * (xy - zw);
//				float m02 = 2 * (xz + yw);
//
//				float m10 = 2 * (xy + zw);
//				float m11 = 1 - 2 * (xx + zz);
//				float m12 = 2 * (yz - xw);
//
//				float m20 = 2 * (xz - yw);
//				float m21 = 2 * (yz + xw);
//				float m22 = 1 - 2 * (xx + yy);
//				Mat3 mf = new Mat3(m00, m01, m02, m10, m11, m12, m20, m21, m22);
//				Trfm t = new Trfm();
//				t.setTo(hip.getLocalTransform());
//				t.rotateTo(mf);
//				t.getRotationPRY(angle);
//				this.theta = angle.yaw;
//				this.phi = angle.pitch;
				// XXX
				
				Vec3 lPos = new Vec3(leftHand.getWorldTransform().getPosition());
				Vec3 rPos = new Vec3(rightHand.getWorldTransform().getPosition());
				Vec3 dVec = new Vec3(lPos.x() - rPos.x(), lPos.y() - rPos.y(), lPos.z() - rPos.z());
				dVec = rPos.add(dVec.x() * .5f, dVec.y() * .5f, dVec.z() * .5f);
				
				// XXX
//				Vec3 ddVec = new Vec3(dVec).norm();
//				Vec3 ppos = new Vec3(pos).norm();
//				Vec3 rotAxis = new Vec3(ddVec).cross(ppos);
//				float rotAngle = (float) Math.acos(new Vec3(ddVec).dot(ppos) / (ddVec.length() * ppos.length()));
//				this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
//					.rotateTo(rotAngle, rotAxis).update();
				// XXX
				
				// XXX
//				Vec3 diff = new Vec3(dVec).sub(pos);
//				float yaw = (float) (Math.atan2(diff.x(), diff.z()) * 180.0 / Math.PI);
//
//				float padj = (float) (Math.sqrt(Math.pow(diff.x(), 2) + Math.pow(diff.z(), 2))); 
//				float pitch = (float) (Math.atan2(padj, diff.y()) *180.0 / Math.PI);
//				this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
//					.rotateToRPY(0.0f, pitch, yaw).update();
				
				// XXX
				
//				this.camMan.getCamera().lookAt(dVec);
//				this.lookat.getLocalTransform().moveTo(dVec.x(), dVec.y(), dVec.z())
//						.rotateToRPY(0.0F, this.phi, this.theta).update();
				
				this.lookat.getLocalTransform().moveTo(dVec.x(), dVec.y(), dVec.z())
					.rotateToRPY(0.0F, FastMath.toRadians(-70.0F), FastMath.toRadians(180.0F)).update();
				
//				this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z()).update();
//				this.camMan.getCamera().lookAt(dVec);
				this.camGroup.getLocalTransform().moveTo(0.0F, -60.0F, 0.0F).update();
			}
			
			this.theta = angle.yaw;
			this.phi = angle.pitch;
		} else {
			this.theta = FastMath.toRadians(180.0F);
			this.phi = FastMath.toRadians(-70.0F);

			this.lookat.getLocalTransform().moveTo(0.0F, -10.0F, 70.0F)
					.rotateToRPY(0.0F, this.phi, this.theta).update();
			this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
		}
	}
	
//	bck 290914
//	public void toHeadCenter() {
//		Node headTopNode = this.root.findChild("HeadTop");
//		Node hipNode = this.root.findChild("Hips");
//		if (headTopNode != null && hipNode != null) {
//			Joint headTop = (Joint) headTopNode;
//			Joint hip = (Joint) hipNode;
//			Vec3 pos = headTop.getWorldTransform().getPosition();
//			
//			Euler angle = new Euler();
//			// XXX not super sure if the hip is the best choice, but i thought the orientation towards the body center would be
//			// more useful than some kind of gaze extrapolation
////			hip.getWorldTransform().getRotationPRY(angle);
//			hip.getLocalTransform().getRotationPRY(angle);
//			this.theta = angle.yaw;
//			this.phi = angle.pitch;
//			Mat3 m = hip.getLocalTransform().getRotation().invert();
//			Trfm t = new Trfm();
//			t.setTo(hip.getLocalTransform());
//			t.rotateTo(m);
//			t.getRotationPRY(angle);
//			this.theta = angle.yaw;
//			this.phi = angle.pitch;
//			
//			this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
//					.rotateToRPY(0.0F, this.phi, this.theta).update();
////			this.lookat.getLocalTransform().moveTo(pos.x(), pos.y(), pos.z())
////			.rotateTo(m).update();
////			this.lookat.getLocalTransform().rotateToRPY(0.0F, FastMath.toRadians(-70.0F), FastMath.toRadians(180.0F)).update();
//			this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
//			
//			angle = new Euler();
//			this.lookat.getLocalTransform().getRotationPRY(angle);
//			// XXX experimental
//			Joint leftHand = (Joint) this.root.findChild("LeftHand");
//			Joint rightHand = (Joint) this.root.findChild("RightHand");
//			if (leftHand != null && rightHand != null) {
//				Vec3 lPos = new Vec3(leftHand.getWorldTransform().getPosition());
//				Vec3 rPos = new Vec3(rightHand.getWorldTransform().getPosition());
//				Vec3 dVec = new Vec3(lPos.x() - rPos.x(), lPos.y() - rPos.y(), lPos.z() - rPos.z());
//				dVec = rPos.add(dVec.x() * .5f, dVec.y() * .5f, dVec.z() * .5f);
////				this.camMan.getCamera().lookAt(dVec);
////				this.lookat.getLocalTransform().moveTo(dVec.x(), dVec.y(), dVec.z())
////						.rotateToRPY(0.0F, this.phi, this.theta).update();
////				this.lookat.getLocalTransform().rotateToRPY(0.0F, FastMath.toRadians(-70.0F), FastMath.toRadians(180.0F)).update();
////				this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
//			}
//			
//			this.theta = angle.yaw;
//			this.phi = angle.pitch;
//		} else {
//			this.theta = FastMath.toRadians(180.0F);
//			this.phi = FastMath.toRadians(-70.0F);
//
//			this.lookat.getLocalTransform().moveTo(0.0F, -10.0F, 70.0F)
//					.rotateToRPY(0.0F, this.phi, this.theta).update();
//			this.camGroup.getLocalTransform().moveTo(0.0F, -30.0F, 0.0F).update();
//		}
//	}

	public void toFront() {
		this.theta = 0.0F;
		this.phi = 0.0F;

		this.lookat.getLocalTransform().moveTo(0.0F, 0.0F, 40.0F)
				.rotateToRPY(0.0F, this.phi, this.theta).update();
		this.camGroup.getLocalTransform().moveTo(0.0F, -120.0F, 0.0F).update();
	}

	public void toTop() {
		this.theta = 0.0F;
		this.phi = (-FastMath.toRadians(90.0F));

		this.lookat.getLocalTransform().moveTo(0.0F, 0.0F, 40.0F)
				.rotateToRPY(0.0F, this.phi, this.theta).update();
		this.camGroup.getLocalTransform().moveTo(0.0F, -120.0F, 0.0F).update();
	}

	public void toSide() {
		this.theta = (-FastMath.toRadians(90.0F));
		this.phi = 0.0F;

		this.lookat.getLocalTransform().moveTo(0.0F, 0.0F, 40.0F)
				.rotateToRPY(0.0F, this.phi, this.theta).update();
		this.camGroup.getLocalTransform().moveTo(0.0F, -120.0F, 0.0F).update();
	}

	public void centerSkeleton() {
		if (!(this.skeletons.isEmpty())) {
			KinematicModel skeleton = (KinematicModel) this.skeletons.values()
					.iterator().next();
			if (skeleton != null) {
				this.lookat
						.getLocalTransform()
						.moveTo(skeleton.getRoot().getWorldTransform()
								.getPosition())
						.rotateToRPY(0.0F, this.phi, this.theta).update();
				this.camGroup.getLocalTransform().moveTo(0.0F, -120.0F, 0.0F)
						.update();
			}
		}
	}

	private class BillboardTask extends Task {
		private void billboard(Spatial node) {
			Spatial contactRadius = (Spatial) node.findChild("ContactRadius");
			Mat3 inverse;
			if (contactRadius != null) {
				inverse = new Mat3(((Spatial) node.getParent())
						.getWorldTransform().getRotation());
				contactRadius.getLocalTransform().rotateTo(inverse.invert());
				contactRadius
						.getLocalTransform()
						.getRotation()
						.mult(Scene.this.camMan.getWorldTransform()
								.getRotation());
				contactRadius.getLocalTransform().update();
			}

			for (Node child : node.getChildren())
				if (child instanceof Spatial)
					billboard((Spatial) child);
		}

		public Task.TaskState perform(float f) {
			for (KinematicModel skeleton : Scene.this.skeletons.values()) {
				if (skeleton != null) {
					billboard(skeleton.getRoot());
				}
			}
			return Task.TaskState.CONTINUE_RUN;
		}
	}

	public class PrettyCameraTask extends Task {
		public Task.TaskState whilePaused(float tpf) {
			if (Scene.this.demoMode) {
				this.state = Task.TaskState.CONTINUE_RUN;
			}
			return Task.TaskState.PAUSE;
		}

		public Task.TaskState perform(float f) {
			Scene.this.theta += FastMath.toRadians(1.0F);
			Scene.this.lookat.getLocalTransform()
					.rotateToRPY(0.0F, Scene.this.phi, Scene.this.theta)
					.update();

			if (Scene.this.demoMode) {
				return Task.TaskState.CONTINUE_RUN;
			}
			return Task.TaskState.PAUSE;
		}
	}

	private class UpdateTask extends Task {
		private KinematicFrame currFrame;
		private Material black;
		private Material red;

		private UpdateTask() {
			this.black = new Material(new float[] { 0.06F, 0.05F, 0.02F },
					new float[] { 0.06F, 0.05F, 0.02F });

			this.red = new Material(new float[] { 0.8F, 0.25F, 0.1F },
					new float[] { 0.8F, 0.25F, 0.1F });
		}

		private void update(Spatial node) {
			if (node instanceof Joint) {
				Joint frameJoint = (Joint) this.currFrame.skeleton
						.findChild(node.name);
//				TODO: remove?
//				if (frameJoint.name.equals("RightForeArm")) {
//					System.out.println("right forearm found...");
//					if (frameJoint.findChild("distance_indicator") != null) {
//						System.out.println("distance indicator is here...");
//					}
//					if (frameJoint.findChild("ContactRadius") != null) {
//						System.out.println("radius is here...");
//					}
//				}
				Visual contactRadius = (Visual) node.findChild("ContactRadius");
				if (frameJoint.isContact)
					contactRadius.getLocalAppearance().material = this.red;
				else {
					contactRadius.getLocalAppearance().material = this.black;
				}

				node.getLocalTransform().setTo(frameJoint.getLocalTransform());
				node.getLocalTransform().update();
			}

			for (int j = 0; j < node.getChildren().size(); ++j) {
				Node child = (Node) node.getChildren().get(j);
				if (child instanceof Spatial)
					update((Spatial) child);
			}
		}

		public Task.TaskState perform(float tpf) {
			if (!GlobalSettingsAndVariables.animate) {
				return Task.TaskState.CONTINUE_RUN;
			}
			
			for (Track track : Scene.this.skeletons.keySet()) {
				this.currFrame = Scene.this.frameBuffer.pop(track, false);

				KinematicModel skeleton = (KinematicModel) Scene.this.skeletons
						.get(track);
				if ((skeleton != null) && (this.currFrame != null)) {
					update(skeleton.getRoot());
				}

				if ((skeleton != null) && (Scene.this.followSkeleton)) {
					Scene.this.lookat
							.getLocalTransform()
							.moveTo(skeleton.getRoot().getWorldTransform()
									.getPosition()).update();
				}
			}
			return Task.TaskState.CONTINUE_RUN;
		}
	}
}
