package scene;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Iterator;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.JPanel;

import kinematics.Joint;
import kinematics.KinematicModel;
import main.GlobalSettingsAndVariables;

import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import streamservice.stream.StreamService;
import streamservice.stream.events.TrackActorMapEvent;
import streamservice.stream.events.TracksDestroyedEvent;
import streamservice.stream.events.TracksSelectedEvent;
import streamservice.tracks.SuitTrack;
import streamservice.tracks.Track;
import walkingalgorithm.WalkingAlgorithm;

import com.jogamp.opengl.util.FPSAnimator;
import com.spookengine.core.events.TaskScheduler;
import com.spookengine.maths.Vec3;
import com.spookengine.platform.desktop.JOGLRenderer3;

import eventhandling.EventBus;

public class SceneContainer extends JPanel implements GLEventListener,
		MouseListener, MouseMotionListener, MouseWheelListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4055929036937984953L;
	
	private class OnTrackActorMap implements LookupListener {
		private Lookup.Result<TrackActorMapEvent> result;

		public void listen() {
			this.result = EventBus.getInstance().getLookup()
					.lookupResult(TrackActorMapEvent.class);
			this.result.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TrackActorMapEvent> it = this.result.allInstances().iterator();
			while (it.hasNext()) {
				TrackActorMapEvent evt = (TrackActorMapEvent) it.next();

				if ((evt.actor != null) && (evt.track.isSelected)) {
					SceneContainer.this.scene.removeSkeleton(evt.track);

					KinematicModel actor = evt.actor;
					KinematicModel skeleton = actor.cloneTree(null);

					WalkingAlgorithm walkingAlgorithm = new WalkingAlgorithm();
					Joint contactJoint = walkingAlgorithm
							.findContactJoint(skeleton.getRoot());
					Vec3 contactPos = contactJoint.getWorldTransform()
							.getPosition();
					skeleton.getRoot().getLocalTransform()
							.moveTo(0.0F, 0.0F, -contactPos.z()).update();

					SceneContainer.this.scene.addSkeleton(evt.track, skeleton);
				}
			}
		}
	}

	private class OnTrackDestroyed implements LookupListener {
		private Lookup.Result<TracksDestroyedEvent> result;

		public void listen() {
			this.result = EventBus.getInstance().getLookup()
					.lookupResult(TracksDestroyedEvent.class);
			this.result.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksDestroyedEvent> it = this.result.allInstances().iterator();
			while (it.hasNext()) {
				TracksDestroyedEvent evt = (TracksDestroyedEvent) it.next();

				for (Track track : evt.tracks)
					SceneContainer.this.scene.removeSkeleton(track);
			}
		}
	}

	private class OnTrackSelected implements LookupListener {
		private Lookup.Result<TracksSelectedEvent> result;

		public void listen() {
			this.result = EventBus.getInstance().getLookup()
					.lookupResult(TracksSelectedEvent.class);
			this.result.addLookupListener(this);
		}

		public void resultChanged(LookupEvent le) {
			Iterator<? extends TracksSelectedEvent> it = this.result.allInstances().iterator();
			while (it.hasNext()) {
				TracksSelectedEvent evt = (TracksSelectedEvent) it.next();

				SceneContainer.this.scene.removeAllSkeletons();
				for (Track track : evt.tracks) {
					KinematicModel actor = track.getActor();
					KinematicModel skeleton = actor.cloneTree(null);

					WalkingAlgorithm walkingAlgorithm = new WalkingAlgorithm();
					walkingAlgorithm.walk(skeleton);

					if (track instanceof SuitTrack)
						// XXX; originally this one was true...
						SceneContainer.this.playback.performWalkingAlgorithm = false;
					else {
						SceneContainer.this.playback.performWalkingAlgorithm = false;
					}
					SceneContainer.this.scene.addSkeleton(track, skeleton);
				}
			}
		}
	}
	
	public Scene scene;
	public StreamService playback;
	public int width;
	public int height;
	private OnTrackSelected onTrackSelected;
	private OnTrackActorMap onTrackActorMap;
	private OnTrackDestroyed onTrackDestroyed;
	private Point prevPos;
	private JPanel scenePanel;
	
	public SceneContainer() {
		super();
		this.playback = GlobalSettingsAndVariables.defaultStreamService;

		this.initComponents();

		GLProfile glp = GLProfile.getDefault();
		GLCapabilities glCaps = new GLCapabilities(glp);

		glCaps.setNumSamples(3);
		glCaps.setSampleBuffers(true);

		GLJPanel glCanvas = new GLJPanel(glCaps);
		this.scenePanel.add(glCanvas, "Center");

		glCanvas.addGLEventListener(this);
		this.scenePanel.addMouseListener(this);
		this.scenePanel.addMouseMotionListener(this);
		this.scenePanel.addMouseWheelListener(this);
		try {
			this.scene = new Scene();
		} catch (Exception ex) {
			System.err.println("Error initializing scene...");
			ex.printStackTrace();
		}

		this.onTrackSelected = new OnTrackSelected();
		this.onTrackActorMap = new OnTrackActorMap();
		this.onTrackDestroyed = new OnTrackDestroyed();
		this.onTrackSelected.listen();
		this.onTrackActorMap.listen();
		this.onTrackDestroyed.listen();

		FPSAnimator animator = new FPSAnimator(glCanvas, 25);
		animator.start();
	}

	private void initComponents() {
		this.scenePanel = new JPanel();

		this.setLayout(new BorderLayout());

		this.scenePanel.setLayout(new BorderLayout());
		this.add(this.scenePanel, "Center");
	}
	
	public Scene getScene() {
		return this.scene;
	}

	public void init(GLAutoDrawable drawable) {
		GL2 gl2 = drawable.getGL().getGL2();
		
		JOGLRenderer3.clearColour = new Vec3(0.75F, 0.75F, 0.75F);
		JOGLRenderer3.getInstance("Scene", gl2).onSurfaceCreated();
		
		this.scene.recreate();
	}

	public void dispose(GLAutoDrawable drawable) {
		// FIXME: hmmm, shouldn't some resource-freeing take place here?
	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl2 = drawable.getGL().getGL2();

		TaskScheduler.getInstance("Scene").update();
		JOGLRenderer3.getInstance("Scene", gl2).onDrawFrame(
				this.scene.getRoot(), this.scene.getCamera());
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl2 = drawable.getGL().getGL2();

		this.width = width;
		this.height = height;
		JOGLRenderer3.getInstance("Scene", gl2).onSurfaceChanged(width, height);
	}

	public void mouseClicked(MouseEvent me) {
		this.requestFocus();
	}

	public void mouseEntered(MouseEvent me) {
		this.requestFocus();
	}

	public void mouseExited(MouseEvent me) {}

	public void mousePressed(MouseEvent me) {
		this.prevPos = me.getPoint();
	}

	public void mouseReleased(MouseEvent me) {}

	public void mouseMoved(MouseEvent me) {}

	public void mouseDragged(MouseEvent me) {
		if (me.isControlDown()) {
			Point currPos = me.getPoint();
			float dy = (currPos.y - this.prevPos.y) / (float) this.width;
			float dx = (currPos.x - this.prevPos.x) / (float) this.height;
			
			if ((me.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK)
				this.scene.translate(100.0F * dy, 100.0F * dx);
			else if ((me.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) {
				this.scene.rotate(dy, dx);
			}

			this.prevPos = currPos;
		}
	}

	public void mouseWheelMoved(MouseWheelEvent mwe) {
		if (mwe.isControlDown())
			this.scene.zoom(-10 * mwe.getWheelRotation());
	}

	public void refreshSkeletons() {
		this.scene.removeAllSkeletons();
		for (Track track : this.playback.getSelectedTracks()) {
			KinematicModel actor = track.getActor();
			KinematicModel skeleton = actor.cloneTree(null);

			WalkingAlgorithm walkingAlgorithm = new WalkingAlgorithm();
			walkingAlgorithm.walk(skeleton);

			if (track instanceof SuitTrack)
				// XXX; originally this one was true...
				this.playback.performWalkingAlgorithm = false;
			else {
				this.playback.performWalkingAlgorithm = false;
			}
			this.scene.addSkeleton(track, skeleton);
		}
	}

	static {
		System.setProperty("sun.awt.noerasebackground", "true");
		GLProfile.initSingleton();
	}
}