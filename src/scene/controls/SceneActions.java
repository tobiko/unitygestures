package scene.controls;

import javax.swing.JOptionPane;

import com.spookengine.scenegraph.Visual;

import scene.SceneContainer;

public class SceneActions {

	private SceneContainer container;
	// TODO: are they really used?
	public enum Options {
		CENTER_VIEW,
		FRONT_VIEW,
		SIDE_VIEW,
		TOP_VIEW,
		TOGGLE_FLOOR;
	}
	
	private final static String[] optionLabels = {
		"center view",
		"front view",
		"side view",
		"top view",
		"head-centered view",
		"toggle floor",
		"nevermind"
	};
	
	public SceneActions(SceneContainer container) {
		super();
		this.container = container;
	}
	
	public void centerView() {
		this.container.getScene().centerSkeleton();
	}
	
	public void frontView() {
		this.container.getScene().toFront();
	}
	
	public void sideView() {
		this.container.getScene().toSide();
	}
	
	public void topView() {
		this.container.getScene().toTop();
	}
	
	public void headCenteredView() {
		this.container.getScene().toHeadCenter();
	}
	
	public void toggleFloor() {
		((Visual) this.container.getScene().getRoot().findChild("floor")).isHidden = !(((Visual) this.container
				.getScene().getRoot().findChild("floor")).isHidden);
	}
	
	public void showAndProcessOptions() {
		String response = (String)JOptionPane.showInputDialog(
                null,
                "select option:",
                "viewport options",
                JOptionPane.PLAIN_MESSAGE,
                null,
                SceneActions.optionLabels,
				SceneActions.optionLabels[0]);
		
		if (response != null) {
			if (response.equals(SceneActions.optionLabels[0])) {
				this.centerView();
			} else if (response.equals(SceneActions.optionLabels[1])) {
				this.frontView();
			} else if (response.equals(SceneActions.optionLabels[2])) {
				this.sideView();
			} else if (response.equals(SceneActions.optionLabels[3])) {
				this.topView();
			} else if (response.equals(SceneActions.optionLabels[4])) {
				this.headCenteredView();
			} else if (response.equals(SceneActions.optionLabels[5])) {
				this.toggleFloor();
			}
		}
	}
}
