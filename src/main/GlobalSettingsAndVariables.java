package main;

import java.io.File;
import java.io.PrintStream;

import javax.swing.JTextArea;

import kinematics.KinematicModelService;
import kinematics.models.TCPUpperSuitWithGloves;
import streamservice.frames.FrameBufferFactoryImplementation;
import streamservice.frames.KinematicFrame;
import streamservice.onlinedatamanipulation.AvailableManipulations;
import streamservice.onlinedatamanipulation.IDataManipulator;
import streamservice.stream.StreamService;
import utils.TextAreaOutputStream;

// TODO: load and print options
public class GlobalSettingsAndVariables {
	/**
	 * just a debug flag, the main effect is that fake data streaming is possible if it is true.
	 */
	public final static boolean debug = true;
	/**
	 * if true only a sparse, hardcoded set of ips is checked for devices.
	 * the ips, or at least there suffices are specified below
	 */
	public final static boolean useSparseNetworkSearch = true;
	/**
	 * in case of sparse network search only a few ips are checked namely those defined here.
	 * Please note the ips are assumed to be constructed as 192.168.11.* where the asterisk indicates one
	 * of the values defined in this array. This should only be used if the router does not assign ips dynamically,
	 * i.e. if you have defined an ip / mac address mapping on the router.
	 */
	public final static int[] sparseIPSearchRange = new int[] {
		101,
		// mac: 00.08.EE.07.F7.92 -> currently (29.9.14) the 'hand' device
		102,
		// mac: 00.08.EE.07.F7.8E -> currently (29.9.14) the 'suit' device
		103
	};
	/**
	 * if this flag is true, than the speech server might be activated via the GUI, otherwise, this
	 * option is blocked.
	 */
	public final static boolean enableSpeechServer = true;
	/**
	 * if this flag is true, than the unityserver might be activated via the GUI, otherwise, this
	 * option is blocked.
	 */
	public final static boolean enableUnityServer = true;
	/**
	 * if this flag is true, detected devices i.e. ILMPUs will be streamed into the same actor model.
	 * this requires some synchronization of data streams from different ILMPUs which causes lags.
	 * however, if there is only one device, there will be nearly no overhead induced by the fusion.
	 */
	public final static boolean fuseConnections = true;
	/**
	 * if this flag is true, than the reading of fused connection is synchronized, this is not a good
	 * idea since the devices are not synchronized, hence reading in fixed intervals causes data loss.
	 */
	public final static boolean recordFusedConnectionsAtOnce = false;
	/**
	 * the default frame rate used to record from the devices, this value determines the update interval
	 * of the data reader threads.
	 */
	public final static int defaultFPS = 60;
	/**
	 * default location for externally obtained kinematic (distance) data of the joints, e.g. from the kinect or the leap sensor
	 */
	public final static String defaultKinematicDataPath = "kinematic_reference_data";
	/**
	 * if true, it is assured that the limbs (except the hands) are of equal length. To do so the length of the left upper arm,
	 * for instance is computed as the mean of both the left and right upper arm data.
	 */
	public final static boolean enforceSymmetry = true;
	/**
	 * just the default location for recorded data.
	 */
	public final static String defaultOutputPath = "records";
	/**
	 * default location for fake, i.e. file input for the fake server.
	 */
	public final static String fakeServerDataDirectory = "fakeserverdata";
	/**
	 * location of the batch file used to start the speech server binary. please note that the location of the
	 * actual binary is hard-coded in the batch file.
	 */
	public final static String speechServerBatchFilePath = "external_binaries" + File.separator + "speechserver" + File.separator + "speechserver.bat";
	/**
	 * location of the batch file used to start the kinect measurement service. please note that the location of the
	 * actual binary is hard-coded in the batch file.
	 */
	public final static String kinectBatchFilePath = "external_binaries" + File.separator + "kinectskeletonmeasurement" + File.separator + "kinect.bat";
	/**
	 * location of the batch file used to start the leap hand measurement service. please note that the location of the
	 * actual binary is hard-coded in the batch file... as well as the java library path and the other shit which is usually handled by
	 * the IDE, really, running just the class file is a pain in the ass.
	 */
	public final static String leapBatchFilePath = "external_binaries" + File.separator + "leaphandmeasurement" + File.separator + "leap.bat";
	/**
	 * if false, the scene will not be updated i.e. the visualization freezes
	 */
	public static boolean animate = true;
	/**
	 * string identifier for the default kinematic model.
	 */
	public static String defaultKinematicModelName = TCPUpperSuitWithGloves.modelName;
	/**
	 * an instance of the default kinematic model. this is basically due to paranoia reasons to prevent the
	 * unlikely case that a track is started without a valid actor model.
	 */
	public static KinematicModelService defaultKinematicModelService;
	/**
	 * the default {@link StreamService} instance used to control data recording and replay. since this object has to be
	 * accessible from many classes, it is stored here. actually i do not see a reason why this should not be a
	 * singletone, since in the current setup there is no real reason to use another {@link StreamService} than this one.
	 */
	public static StreamService defaultStreamService;
	/**
	 * {@link KinematicFrame}s used for the visualization are stored in a dedicated buffer. these buffers are created
	 * and maintained via this factory instance.
	 */
	public static FrameBufferFactoryImplementation defaultFrameBufferFactory;
	/**
	 * string identifier of the default data manipulator.
	 */
	public static String defaultDataManipulatorName = "NONE";
	/**
	 * the current data manipulator. data manipulation takes place in the {@link StreamService#onTick(network.connections.SuitConnection, streamservice.frames.DataFrame)}
	 * method.
	 */
	public static IDataManipulator defaultDataManipulator = null;
	/**
	 * this textarea hosts the error console, it is questionable to redirect the default error stream, however, i thought if might
	 * be useful to redirect it to some gui component, so the software can be monitored directly from the gui.
	 */
	public static JTextArea errorConsoleContainer;
	/**
	 * this output stream is used to redirect the default error stream (via the {@link System#setErr(PrintStream)} method).
	 */
	public static TextAreaOutputStream errorConsole;
	/**
	 * a print stream encapsulating the output stream from above, this print stream is assigned as the error stream within the
	 * static constructor of this class.
	 */
	public static PrintStream errorStream;
	
	static {
		GlobalSettingsAndVariables.defaultFrameBufferFactory = new FrameBufferFactoryImplementation();
		GlobalSettingsAndVariables.defaultKinematicModelService = new TCPUpperSuitWithGloves();
		
		GlobalSettingsAndVariables.defaultStreamService = new StreamService();
		GlobalSettingsAndVariables.defaultDataManipulator = AvailableManipulations
				.getManipulatorInstanceByName(GlobalSettingsAndVariables.defaultDataManipulatorName);
		
		GlobalSettingsAndVariables.errorConsoleContainer = new JTextArea();
		GlobalSettingsAndVariables.errorConsoleContainer.setEditable(false);
		GlobalSettingsAndVariables.errorConsole = new TextAreaOutputStream(GlobalSettingsAndVariables.errorConsoleContainer);
		GlobalSettingsAndVariables.errorStream = new PrintStream(GlobalSettingsAndVariables.errorConsole);
		System.setErr(GlobalSettingsAndVariables.errorStream);
	}
}