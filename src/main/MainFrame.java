package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import kinematics.onlineeditor.EditorContainer;
import network.connections.fake.FakeDataProvider;
import network.connections.tcp.TCPSuitConnector;
import samples.DriftSample;
import samples.ISample;
import scene.Scene;
import scene.SceneContainer;
import scene.controls.SceneActions;
import speechserver.SpeechCommandReceiver;
import speechserver.SpeechServerConnector;
import streamservice.StreamPanel;
import unityserver.UnityServerConnector;
import utils.TextAreaOutputStream;
import utils.visualization.BinaryIconButton;
import utils.visualization.ButtonIconFactory;
import utils.visualization.ButtonIconFactory.ButtonStates;
import utils.visualization.EditableTabbedPane;
import utils.visualization.IconButton;
import utils.visualization.SettingsEditor;

/**
 * the main application container, all the option and control panels are stored here. the separation
 * between view and controls is somewhat dirty, since one of the main logging consoles is stored here
 * as well. further, the speechserver interface is also realized within this class.
 */
public class MainFrame extends JFrame implements ActionListener, SpeechCommandReceiver {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2630322136338626255L;

	/**
	 * since there are no german texts to display in this application this is rather useless.
	 */
	public final static char[] umlaute = new char[] {
		//ae
		'\u00E4',
		//oe
		'\u00F6',
		//ue
		'\u00FC',
		//AE
		'\u00C4',
		//OE
		'\u00D6',
		//UE
		'\u00DC'
	};
	
	/**
	 * for {@link PropertyChangeListener} communication some hardcoded property names are useful.
	 * this string indicates an event which enables or disables menubar elements.
	 * @me: not used anymore
	 */
	public final static String MENU_BAR_ELEMENT_PCN  = "menu_bar_item_enable";
	/**
	 * for {@link PropertyChangeListener} communication some hardcoded property names are useful.
	 * this string indicates an event fired during the search for network devices, used for instance
	 * by the {@link TCPSuitConnector}.
	 */
	public final static String CONNECTION_SEARCH_PCN = "tcp_connection_search";
	/**
	 * for {@link PropertyChangeListener} communication some hardcoded property names are useful.
	 * since some button actions are only available if a device is active (e.g. live data or recording),
	 * respective state changes are forwarded with this id.
	 */
	public final static String BUTTON_STATE_PCN = "button_state_change";
	/**
 	 * the default print stream of the application. in hindsight it would have be more clever to reassign
 	 * system.out to this stream...
 	 */
	public static TextAreaOutputStream defaultConsole;
	/**
	 * just the default text font name.
	 */
	public static String defaultFontName = "Arial";
	/**
	 * this flag is set to true in case the speech server is started.
	 */
	public static boolean speechServerActive;
	/**
	 * this flag is set to true in case the unity server is started.
	 */
	public static boolean unityServerActive;
	/**
	 * this listener kicks in if the window is closed and does some clean-up (close open device connections,
	 * disconnect and shut down the speechserver etc.).
	 */
	private class FrameWindowListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent we) {
			if (MainFrame.this.deviceConnector != null) {
				MainFrame.this.deviceConnector.disconnectSuitsFromAction();
			}
			
			if (MainFrame.this.speechServerConnector != null) {
				MainFrame.this.speechServerConnector.stopBackgroundThread();
			}
			
			if (MainFrame.unityServerConnector != null) {
				MainFrame.unityServerConnector.stopBackgroundThread();
			}
		}
	}
	/**
	 * speech server traffic is handled by this {@link PropertyChangeListener}. if text is received, it is either
	 * forwarded to the console or processed in terms of a command (see the {@link MainFrame#processSpeechCommand(String)}
	 * method).
	 */
	private class SpeechServerConnectionPropertyChangeListener implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent pce) {
			if (pce.getPropertyName().equals("text_from_speechserver")) {
				if (pce.getNewValue() instanceof String) {
					String message = pce.getNewValue().toString();
//					System.out.println("text_from_speechserver: " + message);
					MainFrame.defaultConsole.write("text_from_speechserver: " + message + System.getProperty("line.separator"));
					if (message.startsWith("received speech:")) {
						String possibleCommand = message.split("received speech: ")[1].trim();
//						System.out.println("command: " + possibleCommand + "...");
						MainFrame.defaultConsole.write("command: " + possibleCommand + "..." + System.getProperty("line.separator"));
						
						String response = MainFrame.this.processSpeechCommand(possibleCommand);
						MainFrame.this.speechServerConnector.sendMessage("Repeat: " + response);
					}
				}
			}
		}
	}
	
	/**
	 * unity server traffic is handled by this {@link PropertyChangeListener}.
	 */
	private class UnityServerConnectionPropertyChangeListener implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent pce) {
			if (pce.getPropertyName().equals("received_unity_text")) {
				if (pce.getNewValue() instanceof String) {
					String message = pce.getNewValue().toString();
					MainFrame.defaultConsole.write("text_from_unity: " + message + System.getProperty("line.separator"));
				}
			}
		}
	}
	
	/**
	 * this {@link PropertyChangeSupport} is used to invoke property changes from various classes.
	 */
	private PropertyChangeSupport mainPCS;
	/**
	 * this class handles the tcp/ip communication with the speechserver asynchronously.
	 */
	private SpeechServerConnector speechServerConnector;
	/**
	 * this class handles the tcp/ip communication with unity.
	 */
	public static UnityServerConnector unityServerConnector;
	// utils
	/**
	 * this is the host for the 3D openGL visualization of the kinematic model.
	 */
	private SceneContainer sceneContainer;
	/**
	 * this class maintains possible viewport options like centering of the scene and stuff like that.
	 */
	private SceneActions sceneActions;
	/**
	 * this {@link JTextArea} is used to display the data received via the {@link MainFrame#defaultConsole}.
	 */
	private JTextArea consoleContainer;
	/**
	 * the main view is sorted in two tabbed panes, this pane hosts the 3D scene, and other higher level visualization stuff.
	 */
	private EditableTabbedPane mainPane;
	/**
	 * the main view is sorted in two tabbed panes, this pane hosts various consoles and other diagnostic stuff.
	 */
	private EditableTabbedPane consolePane;
	// network stuff
	/**
	 * this object realizes the communication with the network devices (ILMPUs).
	 */
	private TCPSuitConnector deviceConnector;
	/**
	 * in case of the debugging mode, fake data streaming is enabled. this is realized in terms of simulated network communication.
	 * the raw data is obtained from previous records. this class provides the necessary methods to simulate this kind of fake traffic.
	 */
	private FakeDataProvider fakeDeviceConnector;
	// XXX unstyle
	/**
	 * due to some poor design decisions, the integration of speech-based and mouse-based control input is rather cumbersome.
	 * most of the crucial buttons are stored in this vector to invoke them in case of speechserver commands. it would be nicer,
	 * if the buttons would implement the speechsever listener ({@link SpeechCommandReceiver}) and respond directly... 
	 */
	private Vector<JButton> invokeableButtons;
	private ISample currentSample;
	/**
	 * the default constructor, sets up and initializes the main application frame.
	 */
	public MainFrame() {
		super("The marvelous world of motion tracking");
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		this.setResizable(true);
		this.addWindowListener(new FrameWindowListener());
		this.mainPCS = new PropertyChangeSupport(this);
		this.mainPCS.addPropertyChangeListener(new SpeechServerConnectionPropertyChangeListener());
		this.mainPCS.addPropertyChangeListener(new UnityServerConnectionPropertyChangeListener());
		this.speechServerConnector = null;
		MainFrame.speechServerActive = false;
		// build the content pane...
		this.setupContentPane();
		// and the menu
		this.setJMenuBar(this.generateMenu());
		// initialize connectors...
		this.deviceConnector = new TCPSuitConnector();
		this.deviceConnector.assignPropertyChangeSupport(this.mainPCS);
		if (GlobalSettingsAndVariables.debug) {
			this.fakeDeviceConnector = new FakeDataProvider();
			this.fakeDeviceConnector.assignPropertyChangeSupport(this.mainPCS);
		}
		
		this.setVisible(true);
	}
	/**
	 * builds the main content pane. as it was noted above, the ui consists of two
	 * {@link JTabbedPane}s, namely the {@link MainFrame#mainPane} and the
	 * {@link MainFrame#consolePane}. Both are created here, as well as the simple
	 * kinematic model editor.
	 */
	private void setupContentPane() {
		this.consoleContainer = new JTextArea();
		this.consoleContainer.setEditable(false);
		MainFrame.defaultConsole = new TextAreaOutputStream(this.consoleContainer);
		
		this.sceneContainer = new SceneContainer();
		this.sceneActions = new SceneActions(this.sceneContainer);
		
		this.mainPane = new EditableTabbedPane();
		this.mainPane.addExtendableTab("Scene", this.sceneContainer);
		JSplitPane nestedJsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.mainPane, new EditorContainer());
		nestedJsp.setOneTouchExpandable(true);
		nestedJsp.setDividerLocation(Short.MAX_VALUE);
		
		JPanel contolPanel = this.generateGlobalControlPanel();
		
		this.consolePane = new EditableTabbedPane();
		JScrollPane consoleScrollPane = new JScrollPane(this.consoleContainer);
		consoleScrollPane.getVerticalScrollBar().setUnitIncrement(20);
		this.consolePane.addExtendableTab("Console", consoleScrollPane);
		
		JScrollPane errorConsoleScrollPane = new JScrollPane(GlobalSettingsAndVariables.errorConsoleContainer);
		errorConsoleScrollPane.getVerticalScrollBar().setUnitIncrement(20);
		this.consolePane.addTab("OpenGL Console", errorConsoleScrollPane);
		
		StreamPanel streamPanel = new StreamPanel();
		this.consolePane.addTab("Tracks", streamPanel);
		
//		JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.mainPane, this.consolePane);
		JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, nestedJsp, this.consolePane);
		jsp.setOneTouchExpandable(true);
		jsp.setDividerLocation(Short.MAX_VALUE);
		
		JPanel mainPanel = new JPanel();
		GroupLayout mainLayout = new GroupLayout(mainPanel);
		mainLayout.setAutoCreateContainerGaps(true);
		mainLayout.setAutoCreateGaps(true);
		
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
			.addComponent(contolPanel)
			.addComponent(jsp)
		);
		
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
			.addComponent(contolPanel)
			.addComponent(jsp)
		);
		
		mainPanel.setLayout(mainLayout);
		
		this.setContentPane(mainPanel);
	}
	/**
	 * The different global option buttons are hosted within a dedicated ui component which is
	 * created here.
	 * @return a {@link JPanel} containing all the buttons.
	 */
	private JPanel generateGlobalControlPanel() {
		this.invokeableButtons = new Vector<JButton>();
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		IconButton perspectiveButton = new IconButton(
				ButtonIconFactory.PERSPECTIVE_OPTIONS,
				ButtonIconFactory.ButtonStates.PERSPECTIVE_OPTIONS);
		perspectiveButton.setToolTipText(ButtonIconFactory.getToolTipForState(ButtonIconFactory.ButtonStates.PERSPECTIVE_OPTIONS));
		perspectiveButton.addActionListener(this);
		controlPanel.add(perspectiveButton);
		
		IconButton findButton = new IconButton(
				ButtonIconFactory.FIND_ICON,
				ButtonIconFactory.ButtonStates.FIND_STATE);
		findButton.setToolTipText(ButtonIconFactory.getToolTipForState(ButtonIconFactory.ButtonStates.FIND_STATE));
		findButton.addActionListener(this);
		controlPanel.add(findButton);
		
		IconButton alignButton = new IconButton(ButtonIconFactory.ALIGN_ICON,
				ButtonIconFactory.ButtonStates.ALIGN_STATE);
		alignButton.setToolTipText(ButtonIconFactory.getToolTipForState(ButtonIconFactory.ButtonStates.ALIGN_STATE));
		alignButton.addActionListener(this);
		alignButton.setEnabled(false);
		alignButton.setHighlightThreadState(true);
		alignButton.setPropertyChangeCommand(MainFrame.BUTTON_STATE_PCN);
		this.mainPCS.addPropertyChangeListener(alignButton);
		controlPanel.add(alignButton);
		this.invokeableButtons.add(alignButton);
		
		BinaryIconButton liveButton = new BinaryIconButton(
				ButtonIconFactory.STOP_LIVE_ICON,
				ButtonIconFactory.ButtonStates.STOP_LIVE_STATE,
				ButtonIconFactory.START_LIVE_ICON,
				ButtonIconFactory.ButtonStates.START_LIVE_STATE, false);
		liveButton.setEnabled(false);
		liveButton.setPropertyChangeCommand(MainFrame.BUTTON_STATE_PCN);
		this.mainPCS.addPropertyChangeListener(liveButton);
		controlPanel.add(liveButton);
		this.invokeableButtons.add(liveButton);
		
		BinaryIconButton recordButton = new BinaryIconButton(
				ButtonIconFactory.STOP_RECORD_ICON,
				ButtonIconFactory.ButtonStates.STOP_RECORD_STATE,
				ButtonIconFactory.START_RECORD_ICON,
				ButtonIconFactory.ButtonStates.START_RECORD_STATE, false);
		recordButton.setEnabled(false);
		recordButton.setPropertyChangeCommand(MainFrame.BUTTON_STATE_PCN);
		this.mainPCS.addPropertyChangeListener(recordButton);
		controlPanel.add(recordButton);
		this.invokeableButtons.add(recordButton);
		
		recordButton.setReferenceButton(liveButton);
		liveButton.setReferenceButton(recordButton);
		
		IconButton disconnectButton = new IconButton(ButtonIconFactory.DISCONNECT_ICON,
				ButtonIconFactory.ButtonStates.DISCONNECT_STATE);
		disconnectButton.setToolTipText(ButtonIconFactory.getToolTipForState(ButtonIconFactory.ButtonStates.DISCONNECT_STATE));
		disconnectButton.addActionListener(this);
		disconnectButton.setEnabled(false);
		disconnectButton.setPropertyChangeCommand(MainFrame.BUTTON_STATE_PCN);
		this.mainPCS.addPropertyChangeListener(disconnectButton);
		controlPanel.add(disconnectButton);
		
		BinaryIconButton speechServerButton = new BinaryIconButton(
			ButtonIconFactory.CONNECT_SPEECH_SERVER,
			ButtonIconFactory.ButtonStates.CONNECT_TO_SPEECHSERVER,
			ButtonIconFactory.DISCONNECT_SPEECH_SERVER,
			ButtonIconFactory.ButtonStates.DISCONNECT_FROM_SPEECHSERVER, true);
		speechServerButton.setEnabled(GlobalSettingsAndVariables.enableSpeechServer);
		speechServerButton.setActionCommand("speechserver");
		speechServerButton.addActionListener(this);
		controlPanel.add(speechServerButton);
		
		BinaryIconButton unityServerButton = new BinaryIconButton(
				ButtonIconFactory.CONNECT_UNITY_SERVER,
				ButtonIconFactory.ButtonStates.CONNECT_TO_UNITYSERVER,
				ButtonIconFactory.DISCONNECT_UNITY_SERVER,
				ButtonIconFactory.ButtonStates.DISCONNECT_FROM_UNITYSERVER, true);
		unityServerButton.setEnabled(GlobalSettingsAndVariables.enableUnityServer);
		unityServerButton.setActionCommand("unityserver");
		unityServerButton.addActionListener(this);
		controlPanel.add(unityServerButton);
		
		BinaryIconButton toggleAnimationButton = new BinaryIconButton(
			ButtonIconFactory.TOGGLE_VIEW_ICON,
			ButtonIconFactory.ButtonStates.DISABLE_VIEW_STATE,
			ButtonIconFactory.TOGGLE_VIEW_ICON,
			ButtonIconFactory.ButtonStates.ENABLE_VIEW_STATE, true);
		controlPanel.add(toggleAnimationButton);
		
		controlPanel.setMinimumSize(new Dimension(controlPanel.getMinimumSize().width,     5 * perspectiveButton.getMinimumSize().height / 2));
		controlPanel.setPreferredSize(new Dimension(controlPanel.getPreferredSize().width, 5 * perspectiveButton.getPreferredSize().height / 2));
		controlPanel.setMaximumSize(new Dimension(controlPanel.getMaximumSize().width,     5 * perspectiveButton.getMaximumSize().height / 2));
		
		controlPanel.setBorder(BorderFactory.createTitledBorder("options"));
		
		return controlPanel;
	}
	/**
	 * the main menu which allows to edit the default settings and stuff like this.
	 * @return the main menu bar of the frame.
	 */
	private JMenuBar generateMenu() {
		JMenuBar mainMenu = new JMenuBar();
//		FIXME
//		JMenu igsTCPConnectionMenu = new JMenu("IGS Device Connection");
//		
//		JMenuItem searchConnectionsItem = new JMenuItem("Search Connections");
//		searchConnectionsItem.setActionCommand("search_connections");
//		searchConnectionsItem.addActionListener(this);
//		
//		igsTCPConnectionMenu.add(searchConnectionsItem);
//		
//		JMenuItem defaultConnectionItem = new JMenuItem("Check Connection");
//		// XXX
//		defaultConnectionItem.setEnabled(false);
//		this.mainPCS.addPropertyChangeListener(new ComponentStatePropertyChangeLister(defaultConnectionItem, MainFrame.MENU_BAR_ELEMENT_PCN));
//		defaultConnectionItem.setActionCommand("default_connection");
//		defaultConnectionItem.addActionListener(this);
//		
//		igsTCPConnectionMenu.add(defaultConnectionItem);
//		
//		mainMenu.add(igsTCPConnectionMenu);
//		
//		JMenu sampleMenu = new JMenu("Tests and Samples");
//		
//		JMenuItem speechServerSampleItem = new JMenuItem("Speech Server Sample");
//		speechServerSampleItem.setActionCommand("samples_speech_server");
//		speechServerSampleItem.addActionListener(this);
//		
//		sampleMenu.add(speechServerSampleItem);
//		
//		JMenuItem fakeDataConnectionSampleItem = new JMenuItem("Fake Data Sample");
//		fakeDataConnectionSampleItem.setActionCommand("samples_fake_server");
//		fakeDataConnectionSampleItem.addActionListener(this);
//		
//		sampleMenu.add(fakeDataConnectionSampleItem);
//		
//		mainMenu.add(sampleMenu);
//		
//		JMenu toolMenu = new JMenu("Tools and Utilities");
//		
//		JMenuItem speechServerItem = new JMenuItem("Start Speechserver");
//		speechServerItem.setActionCommand("speech_server_switch");
//		speechServerItem.addActionListener(this);
//		
//		toolMenu.add(speechServerItem);
//		
//		mainMenu.add(toolMenu);
		
		JMenu settingsMenu = new JMenu("Settings");
		JMenuItem settingsItem = new JMenuItem("edit global settings");
		settingsItem.setActionCommand("menu:settings");
		settingsItem.addActionListener(this);
		settingsMenu.add(settingsItem);

		mainMenu.add(settingsMenu);
		
		JMenu toolMenu = new JMenu("Tools");
		JMenuItem kinectItem = new JMenuItem("run kinect skeleton measurement");
		kinectItem.setActionCommand("menu:kinect");
		kinectItem.addActionListener(this);
		toolMenu.add(kinectItem);
		JMenuItem leapItem = new JMenuItem("run leap hand measurement");
		leapItem.setActionCommand("menu:leap");
		leapItem.addActionListener(this);
		toolMenu.add(leapItem);

		mainMenu.add(toolMenu);
		
		JMenu experimentsMenu = new JMenu("Experiments");
		JMenuItem driftSample = new JMenuItem("Continuous Motion Sample");
		driftSample.addActionListener(this);
		driftSample.setActionCommand("experiments:drift_sample");
		driftSample.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.SHIFT_MASK, true));
		experimentsMenu.add(driftSample);
		
		mainMenu.add(experimentsMenu);
		
		return mainMenu;
	}
	
//	public void addComponentAsInternalFrame(JComponent component, String title, InternalFrameListener listener) {
//		JInternalFrame frame = new JInternalFrame(title, true, true, true, true);
//		frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
//		//useless as it as set in the constructor, just for paranoia reasons.
//		frame.setResizable(true);
//		
//		frame.getContentPane().add(component);
//		component.setFocusable(true);
//		
//		if (listener != null) {
//			frame.addInternalFrameListener(listener);
//		}
//		
//		Dimension d = this.getContentPane().getSize();
//		Point upperLeft = this.getContentPane().getLocation();
//		
//		frame.setSize((int) (d.width * .75D), (int) (d.height * .75D));
//		Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
//				+ d.height / 2);
//		frame.setLocation(new Point(center.x - frame.getWidth() / 2, center.y
//				- frame.getHeight() / 2));
//	
//		frame.setVisible(true);
//		
//		this.desktop.add(frame);
//		try {
//			frame.setSelected(true);
//		} catch (PropertyVetoException pve) {
//			pve.printStackTrace();
//		}
//	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() instanceof IconButton) {
			IconButton button = (IconButton) ae.getSource();
			
			switch (button.getState()) {
				case FIND_STATE:
					this.findDevicesAction();
					break;
				case PERSPECTIVE_OPTIONS:
					this.sceneActions.showAndProcessOptions();
					break;
				case ALIGN_STATE:
					this.alignDevicesAction();
					break;
				case DISCONNECT_STATE:
					this.disconnectDevicesAction();
					break;
					// TODO: speechserver?
				default:
					break;
			}
		} else {
			String command = ae.getActionCommand();
			if (command.startsWith("menu:")) {
				String subCommand = command.split("menu:")[1];
				if (subCommand.equals("settings")) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							new SettingsEditor(MainFrame.this);
						}
					});
				} else if (subCommand.equals("kinect")) {
					this.startKinectSkeleonMeasurement();
				} else if (subCommand.equals("leap")) {
					this.startLeapHandMeasurement();
				}
			} else if (command.startsWith("experiments:")) {
				String subCommand = command.split("experiments:")[1];
				if (subCommand.equals("drift_sample")) {
					if (this.currentSample != null) {
						MainFrame.defaultConsole.write("stop sample" + System.getProperty("line.separator"));
						this.currentSample.shutDown();
						if (this.currentSample.usesUI()) {
							this.consolePane.remove(this.currentSample.getUIComponent());
						}
						this.currentSample = null;
					} else {
						MainFrame.defaultConsole.write("run sample" + System.getProperty("line.separator"));
						this.currentSample = new DriftSample();
						this.currentSample.init(this.sceneContainer.scene);
						if (this.currentSample.usesUI()) {
							this.consolePane.addTab("Sample", this.currentSample.getUIComponent());
//							this.consolePane.setSelectedIndex(this.consolePane.getTabCount() - 1);
						}
					}
				}
			} else if (command.equals("speechserver")) {
				if (ae.getSource() instanceof BinaryIconButton) {
					BinaryIconButton button = (BinaryIconButton) ae.getSource();
					if (!button.getSelectionState()) {
						this.shutDownSpeechServer();
					} else {
						this.startSpeechServer();
					}
					
					button.setSelectionState(!button.getSelectionState());
				}
			} else if (command.equals("unityserver")) {
				if (ae.getSource() instanceof BinaryIconButton) {
					BinaryIconButton button = (BinaryIconButton) ae.getSource();
					if (!button.getSelectionState()) {
						this.shutDownUnityServer();
					} else {
						this.startUnityServer();
					}
					
					button.setSelectionState(!button.getSelectionState());
				}
			}
		}
	}
	/**
	 * this method is called from the {@link ActionListener} to initialize the network scan (or to invoke the fake data stream).
	 * the scan is performed by the {@link MainFrame#deviceConnector}, more precisely, by the {@link TCPSuitConnector#findSuits()}
	 * method which invokes a bunch of threads that look for devices in a certain IP range.
	 */
	private void findDevicesAction() {
		if (GlobalSettingsAndVariables.debug) {
			Object[] options = { "search network", "fake data" };
			int answer = JOptionPane
					.showOptionDialog(
							this,
							"do you want to search the network for devices, or do you want to use fake data?",
							"connection type", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							options,
							options[0]);
			if (answer == 0) {
				this.deviceConnector.findSuits();
			} else {
				// XXX: fake data
				this.fakeDeviceConnector.findSuits();
			}
		} else {
			this.deviceConnector.findSuits();
		}
	}
	/**
	 * this method is called from the {@link ActionListener} to align found devices, actually this encapsulates a call
	 * to the {@link TCPSuitConnector#alignSuitsFromAction()} method.
	 */
	private void alignDevicesAction() {
		this.deviceConnector.alignSuitsFromAction();
	}
	/**
	 * disconnects network devices and closes the respective sockets. this is realized by a call to the
	 * {@link TCPSuitConnector#disconnectSuitsFromAction()} method.
	 */
	private void disconnectDevicesAction() {
		this.deviceConnector.disconnectSuitsFromAction();
		if (this.fakeDeviceConnector != null) {
			this.fakeDeviceConnector.disconnectSuitsFromAction();
		}
	}
	
	@Override
	public String processSpeechCommand(String command) {
		System.out.println("processing: " + command);
		if (command.equals("center view")) {
			this.sceneActions.centerView();
			return "switched to centered view";
		} else if (command.equals("side view")) {
			this.sceneActions.sideView();
			return "switched to side view";
		} else if (command.equals("top view")) {
			this.sceneActions.topView();
			return "switched to top view";
		} else if (command.equals("front view")) {
			this.sceneActions.frontView();
			return "switched to front view";
		} else if (command.equals("toogle live")) {
			if (!GlobalSettingsAndVariables.defaultStreamService.getSelectedTracks().isEmpty()) {
//				GlobalSettingsAndVariables.defaultPlaybackService.toggleLive();
				this.checkButtons(ButtonIconFactory.ButtonStates.START_LIVE_STATE);
				return "toggled live preview";
			} else {
				return "no active tracks";
			}
		} else if (command.equals("toggle record")) {
			if (!GlobalSettingsAndVariables.defaultStreamService.getSelectedTracks().isEmpty()) {
//				GlobalSettingsAndVariables.defaultPlaybackService.toggleRecord();
				this.checkButtons(ButtonIconFactory.ButtonStates.START_RECORD_STATE);
				return "toggled live recording";
			} else {
				return "no active tracks";
			}
		} else if (command.equals("align")) {
			// XXX
			if (this.deviceConnector.hasDevices()) {
				this.deviceConnector.alignSuitsFromAction();
				this.checkButtons(ButtonIconFactory.ButtonStates.ALIGN_STATE);
				return "aligned devices";
			} else {
				return "no network devices";
			}
		}
		
		return command;
	}
	/**
	 * As it was noted above the current integration of mouse and speech commands is stupid at best.
	 * In case of a valid speech command an {@link ActionEvent} is emulated on the respective button.
	 * This method is called from the {@link MainFrame#processSpeechCommand(String)} method.
	 * @param state the {@link ButtonStates} state corresponding to the speech command.
	 */
	private void checkButtons(ButtonIconFactory.ButtonStates state) {
		for (JButton button : this.invokeableButtons) {
			if (button instanceof IconButton) {
				IconButton iButton = (IconButton) button;
				if (iButton.getState() == state) {
					iButton.actionPerformed(new ActionEvent(iButton, ActionEvent.ACTION_PERFORMED, iButton.getActionCommand()));
				}
			} else if (button instanceof BinaryIconButton) {
				BinaryIconButton iButton = (BinaryIconButton) button;
				if (iButton.getSelectedState() == state || iButton.getDeselectedState() == state) {
					iButton.actionPerformed(new ActionEvent(iButton, ActionEvent.ACTION_PERFORMED, iButton.getActionCommand()));
				}
			}
		}
	}
	/**
	 * starts the speechserver by creating a new instance of {@link SpeechServerConnector}.
	 */
	private void startSpeechServer() {
		if (this.speechServerConnector == null) {
			this.speechServerConnector = new SpeechServerConnector(this.mainPCS);
		}
		this.mainPCS.firePropertyChange("speech_server_state", false, true);
		MainFrame.speechServerActive = true;
	}
	/**
	 * shuts down the speechserver in case there is an active instance of {@link SpeechServerConnector}.
	 */
	private void shutDownSpeechServer() {
		if (this.speechServerConnector != null) {
			this.speechServerConnector.stopBackgroundThread();
			this.speechServerConnector = null;
		}
		this.mainPCS.firePropertyChange("speech_server_state", true, false);
		MainFrame.speechServerActive = false;
	}
	/**
	 * starts the unityserver by creating a new instance of {@link UnityServerConnector}.
	 */
	private void startUnityServer() {
		if (MainFrame.unityServerConnector == null) {
			MainFrame.unityServerConnector = new UnityServerConnector(this.mainPCS);
		}
		MainFrame.unityServerActive = true;
	}
	/**
	 * shuts down the unityServerConnector in case there is an active instance of {@link UnityServerConnector}.
	 */
	private void shutDownUnityServer() {
		if (MainFrame.unityServerConnector != null) {
			MainFrame.unityServerConnector.stopBackgroundThread();
			MainFrame.unityServerConnector = null;
		}
		MainFrame.unityServerActive = false;
	}
	/**
	 * runs the kinect program used to obtain skeletal data
	 */
	private void startKinectSkeleonMeasurement() {
		int exitValue = -1;
		
		Runtime runtime = Runtime.getRuntime();
		try {
			File dir = new File(GlobalSettingsAndVariables.defaultKinematicDataPath);
			Process cSharpKinectApp = runtime.exec(GlobalSettingsAndVariables.kinectBatchFilePath + " " + dir.getAbsolutePath());
			exitValue = cSharpKinectApp.waitFor();
			System.out.println("process exit with value: " + String.valueOf(exitValue));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}
	/**
	 * runs the leap program used to obtain hand data
	 */
	private void startLeapHandMeasurement() {
		int exitValue = -1;
		
		Runtime runtime = Runtime.getRuntime();
		try {
			File dir = new File(GlobalSettingsAndVariables.defaultKinematicDataPath);
			Process javaLeapApp = runtime.exec(GlobalSettingsAndVariables.leapBatchFilePath + " " + dir.getAbsolutePath());
			exitValue = javaLeapApp.waitFor();
			System.out.println("process exit with value: " + String.valueOf(exitValue));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}
	
	public Scene getScene() {
		return this.sceneContainer.scene;
	}
	/**
	 * just some ui stuff. one of the assets of the Nimbus {@link LookAndFeel} is that properties can
	 * be set dynamically at runtime. some of these defaults are overridden here.
	 */
	private static void setUIDefaults() {
		// wir holen uns die defaults...
		UIDefaults defaults = UIManager.getLookAndFeelDefaults();
		// aus irgendwelchen Gruenden mein momentaner Standardfont, den kann man sich
		// hier ziehen: http://www.fonts2u.com/avalon-quest.font
		String preferredFontName = "Avalon Quest";
		// wir schauen nach obs den Font auf diesem System gibt...
		Font[] fonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		
		boolean defaultFontAvailable = false;
		
		for (Font font : fonts) {
			if (font.getName().equals(preferredFontName)) {
				defaultFontAvailable = true;
				MainFrame.defaultFontName = preferredFontName;
				break;
			}
		}
		
		// und setzen ihn auf die Standardvariante wenn nicht.
		if (!defaultFontAvailable) {
			preferredFontName = MainFrame.defaultFontName;
		}
		
		// hier setzen wir die defaults; am Anfang geben wir die zu
		// setzende Entitaet an, danach kommt der ensprechende Wert
		// zunaechst die Fonts...
		defaults.put("InternalFrame.titleFont", new Font(preferredFontName, Font.BOLD, 14));
		
		defaults.put("TextArea.font"          , new Font(preferredFontName, Font.BOLD, 12));
		defaults.put("TextField.font"         , new Font(preferredFontName, Font.BOLD, 20));
		defaults.put("FormattedTextField.font", new Font(preferredFontName, Font.BOLD, 20));
		defaults.put("Label.font"             , new Font(preferredFontName, Font.BOLD, 20));
		defaults.put("RadioButton.font"       , new Font(preferredFontName, Font.BOLD, 18));
		defaults.put("Button.font"            , new Font(preferredFontName, Font.BOLD, 18));
		defaults.put("ComboBox.font"          , new Font(preferredFontName, Font.BOLD, 18));
		//Tree stuff
		defaults.put("Tree.font"              , new Font(preferredFontName, Font.BOLD, 18));
		File icons = new File("icons");
		if (icons.exists() && icons.isDirectory()) {
			defaults.put("Tree.openIcon"      , new ImageIcon(icons.getAbsolutePath() + File.separator + "tree_open_icon.gif"));
			defaults.put("Tree.closedIcon"    , new ImageIcon(icons.getAbsolutePath() + File.separator + "tree_closed_icon.gif"));
			defaults.put("Tree.leafIcon"      , new ImageIcon(icons.getAbsolutePath() + File.separator + "tree_end_icon.gif"));
		}
		
		defaults.put("Menu.font"              , new Font(preferredFontName, Font.BOLD, 14));
		defaults.put("MenuItem.font"          , new Font(preferredFontName, Font.BOLD, 13));
		
		// dann ein paar Farben...
		java.awt.Color gold = new java.awt.Color(180, 160, 105);
		java.awt.Color karmin = new java.awt.Color(165, 30, 55);
		java.awt.Color anthra = new java.awt.Color(50, 65, 75);
		
		defaults.put("nimbusSelectionBackground", gold);
		defaults.put("controlText", karmin);
		defaults.put("nimbusBorder", anthra);
	
		// zum Schluss Eigenschaften von JInternalFrame Objekten.
		defaults.put("InternalFrameTitlePane.background", Color.BLACK);
		defaults.put("InternalFrameTitlePane.disabled", Color.BLACK);
		defaults.put("InternalFrameTitlePane.foreground", Color.BLACK);
		
		defaults.put("InternalFrame.foreground", Color.BLACK);
		defaults.put("InternalFrame.background", Color.BLACK);
	}
	/**
	 * the journey starts at the main...
	 * @param args command line arguments, not used
	 */
	public static void main(String[] args) {
		try {
			javax.swing.UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			System.err.println("Nimbus is missing...");
		}
		
		MainFrame.setUIDefaults();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				new MainFrame();
			}
		});
	}
}
