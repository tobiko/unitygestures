package unityserver;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import main.MainFrame;

public class UnityServerConnector implements PropertyChangeListener {

	public final static int RECEIVER_PORT = 5850;
	
	public final static int SENDER_PORT   = 5852;
	
	private class SocketListener extends SwingWorker<Void, Void> {

		private ServerSocket serverSocket;
		
		private boolean serverReady;
		
		private boolean ready;
		
		private SocketListener() {
			super();
			this.ready = false;
			this.initServerSocket();
			this.ready = this.serverReady;
			
			if (this.serverReady) {
				UnityServerConnector.this.localPCS.firePropertyChange(
						"server_socket_ready", true, false);
			}
		}
		
		private void initServerSocket() {
			boolean ready = true;
			
			try {
				this.serverSocket = new ServerSocket(UnityServerConnector.RECEIVER_PORT);
				this.serverSocket.setSoTimeout(100);
			} catch (IOException ioe) {
				ioe.printStackTrace();
				ready = false;
			}
			
			this.serverReady = ready;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			MainFrame.defaultConsole.write("unity server thread started..." + System.getProperty("line.separator"));
			while (this.ready) {
				try {
//					LeapServer.defaultConsole.write("initialize client..." + System.getProperty("line.separator"));
					Socket clientSocket = null;
					try {
						clientSocket = this.serverSocket.accept();
					} catch (IOException ioe) {
						continue;
					}
					MainFrame.defaultConsole.write("client initialized..." + System.getProperty("line.separator"));
					BufferedReader in = new BufferedReader(
							new InputStreamReader(clientSocket.getInputStream()));
					String inputLine;//, outputLine;

					while ((inputLine = in.readLine()) != null) {
						MainFrame.defaultConsole.write(inputLine + System.getProperty("line.separator"));
						UnityServerConnector.this.localPCS.firePropertyChange(
								"server_socket_received", null, inputLine);
					}

					in.close();
				} catch (IOException e) {
					MainFrame.defaultConsole.write("Exception caught when trying to listen on port "
									+ UnityServerConnector.RECEIVER_PORT + " or while waiting for a connection" + System.getProperty("line.separator"));
					MainFrame.defaultConsole.write(e.getMessage() + System.getProperty("line.separator"));
				}
			}

			try {
				this.serverSocket.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			MainFrame.defaultConsole.write("unity server thread ended..." + System.getProperty("line.separator"));
			UnityServerConnector.this.localPCS.firePropertyChange(
					"server_socket_closed", true, false);
			
			this.serverReady = false;
			return null;
		}
		
		private void stop() {
			this.ready = false;
			UnityServerConnector.this.sendMessage("Control: closing");
		}	
	}
	
	private SocketListener socketListener;
	
	private PropertyChangeSupport pcs;
	private PropertyChangeSupport localPCS;
	
	private boolean forwardData;
	
	public UnityServerConnector(PropertyChangeSupport pcs) {
		super();
		this.pcs = pcs;
		this.localPCS = new PropertyChangeSupport(this);
		this.localPCS.addPropertyChangeListener(this);
		this.forwardData = false;
		this.socketListener = new SocketListener();
		this.socketListener.execute();
	}
	
	public void stopBackgroundThread() {
		if (UnityServerConnector.this.socketListener.ready) {
			UnityServerConnector.this.socketListener.stop();
			while (UnityServerConnector.this.socketListener.serverReady) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getPropertyName().equals("server_socket_received")) {
			if (pce.getNewValue().toString().startsWith("greetings from unity...")) {
				this.forwardData = true;
			} else if (pce.getNewValue().toString().startsWith("farewell from unity...")) {
				this.forwardData = false;
			}
			
			final String receivedText = pce.getNewValue().toString();
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					UnityServerConnector.this.pcs.firePropertyChange(
							"received_unity_text", null, receivedText);
				}
			});
		}
	}
	
	public void sendFrame(String message) {
//		MainFrame.defaultConsole.write("send request..." + System.getProperty("line.separator"));
//		MainFrame.defaultConsole.write(message + System.getProperty("line.separator"));
		if (this.forwardData) {
//			MainFrame.defaultConsole.write(message + System.getProperty("line.separator"));
			this.sendMessage(message);
		}
	}
	
	public void sendMessage(String message) {		
		try {
			Socket socket = new Socket();
			
			try {
				socket.connect(new InetSocketAddress("127.0.0.1", UnityServerConnector.SENDER_PORT), 20);
			} catch (SocketTimeoutException ste) {
				System.out.println("client seems to be down...");
			}
			
			PrintWriter writer = new PrintWriter(socket.getOutputStream());
			writer.print(message);
			writer.flush();
			writer.close();
			socket.close();
		} catch (ConnectException ce) {
			ce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
