package network.connections.fake;

import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import praktikum.preprocessing.MarkFrames;
import main.GlobalSettingsAndVariables;
import main.MainFrame;
import network.connections.SuitConnection;
import network.connections.TCPSuitConnectionProvider;
import network.connections.tcp.events.SuitsConnectedEvent;
import network.connections.tcp.events.SuitsDisconnectedEvent;
import streamservice.frames.DataFrame;
import streamservice.frames.RawFrame;
import streamservice.frames.RawReading;
import streamservice.stream.StreamService;
import utils.visualization.ScrollableFileChooserTextPreview;

import com.spookengine.maths.Mat3;

import eventhandling.EventBus;

public class FakeDataProvider implements TCPSuitConnectionProvider {

	private class FakeDeviceConnection extends SuitConnection {

		private ArrayList<DataFrame> dataFrames;
		
		private ArrayList<RawFrame> rawDataFrames;
		
		private boolean cancelled;
		
		public FakeDeviceConnection(String name, File file) {
			super(name, GlobalSettingsAndVariables.defaultFPS);
//			FakeDataProvider.getSensorCount(file.getAbsolutePath());
			this.dataFrames = new ArrayList<DataFrame>();
			this.rawDataFrames = new ArrayList<RawFrame>();
			FakeDataProvider.obtainData(file.getAbsolutePath(), this);
			this.cancelled = false;
		}
	}
	
	private class FakeDeviceConnectionTask extends TimerTask {

		private StreamService playback;
		private FakeDeviceConnection device;
		
		private int currentFrame;
		
		private FakeDeviceConnectionTask(FakeDeviceConnection device) {
			super();
			this.device = device;
			this.playback = GlobalSettingsAndVariables.defaultStreamService;
			this.currentFrame = 0;
		}
		
		@Override
		public void run() {
			if (this.device == null) {
				return;
			}
			
			if (this.device.cancelled) {
				return;
			}
			
			RawFrame rawDataFrame = this.device.rawDataFrames.get(this.currentFrame);
			DataFrame dataFrame = this.device.dataFrames.get(this.currentFrame);
			
			//System.out.println(currentFrame);
			
			
			
			this.playback.setRawFrame(rawDataFrame);
			this.playback.onTick(this.device, dataFrame);
			
			this.currentFrame++;
			if (this.currentFrame >= this.device.dataFrames.size()) {
				this.currentFrame = 0;
			}
		}
	}
	
	private HashMap<FakeDeviceConnection, Timer> deviceToTimer;
	private ArrayList<FakeDeviceConnection> devices;
	private PropertyChangeSupport pcs;
	
	public FakeDataProvider() {
		super();
		this.devices = new ArrayList<FakeDataProvider.FakeDeviceConnection>();
		this.deviceToTimer = new HashMap<FakeDataProvider.FakeDeviceConnection, Timer>();
	}
	
	@Override
	public void assignPropertyChangeSupport(PropertyChangeSupport pcs) {
		this.pcs = pcs;
	}
	
	@Override
	public void findSuits() {
		this.connectSuits(null);
	}

	@Override
	public void disconnectSuits(List<? extends SuitConnection> devices) {
		for (SuitConnection device : devices) {
			if (device instanceof FakeDeviceConnection) {
				FakeDeviceConnection fakeDevice = (FakeDeviceConnection) device;
				fakeDevice.cancelled = true;
				// FIXME: ???
				MainFrame.defaultConsole.write("disconnecting suit "
						+ fakeDevice.getName() + "..."
						+ System.getProperty("line.separator"));
				EventBus.getInstance().add(new SuitsDisconnectedEvent(devices));
			}
		}
		devices.clear();
		
		if (this.pcs != null) {
			this.pcs.firePropertyChange(MainFrame.BUTTON_STATE_PCN, true, false);
		}
	}

	@Override
	public void disconnectSuitsFromAction() {
		if (!this.deviceToTimer.keySet().isEmpty()) {
			this.disconnectSuits(this.devices);
		}
	}

	@Override
	public void connectSuits(List<? extends SuitConnection> paramList) {
		File file = this.getFile(true, GlobalSettingsAndVariables.fakeServerDataDirectory);
		
		if (file != null) {
			String name = file.getName().split("\\.")[0] + "_fake_data";
			FakeDeviceConnection device = new FakeDeviceConnection(name, file);
			Timer timer = new Timer();
	        timer.scheduleAtFixedRate(new FakeDeviceConnectionTask(device), 0L, 1000 / device.getFps());
	        this.devices.add(device);
	        this.deviceToTimer.put(device, timer);
	        
	        EventBus.getInstance().removeAll(SuitsConnectedEvent.class);
	        ArrayList<SuitConnection> devices = new ArrayList<SuitConnection>();
	        devices.add(device);
	        EventBus.getInstance().add(new SuitsConnectedEvent(devices));

	        if (this.pcs != null) {
				this.pcs.firePropertyChange(MainFrame.BUTTON_STATE_PCN, false, true);
			}
		}
	}

	@Override
	public void alignSuits(List<? extends SuitConnection> paramList) {
		return;
	}

	@Override
	public void alignSuitsFromAction() {
		return;
	}

	@Override
	public void tagAction() {
		return;
	}

	@Override
	public void setRaw(boolean paramBoolean) {
		// allways raw
		return;
	}

	private File getFile(boolean accesory, String baseDirectory) {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(baseDirectory));
		
		fc.setApproveButtonText("Load File");
		fc.setDialogTitle("Data File");
		fc.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "raw data.";
			}
			
			@Override
			public boolean accept(File file) {
				if (file != null) {
					if (file.getName().endsWith(".raw")) {
						return true;
					}
					
					if (file.isDirectory()) {
						return true;
					}
				}
				return false;
			}
		});
		
		if (accesory) fc.setAccessory(new ScrollableFileChooserTextPreview(fc));
		
		int state = fc.showOpenDialog(null);
		
		if (state == JFileChooser.APPROVE_OPTION) {
			if (!fc.getSelectedFile().isDirectory() &&
				fc.getSelectedFile().exists()) {
				return fc.getSelectedFile();	
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static void obtainData(String fileName, FakeDeviceConnection device) {
		int sensorCount = FakeDataProvider.getSensorCount(fileName);
		
		
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			// discard header...
			String line = reader.readLine();
			String[] tokens = line.split("\t");
			
			RawFrame rawDataFrame = null;
			DataFrame dataFrame = null;
			int frame = -1;
			int counter = 0;
			
			while (reader.ready()) {
				line = reader.readLine();
				line = line.replaceAll(",", "\\.");
//				System.out.println(line);
				tokens = line.split("\t");
				
				int cFrame = Integer.parseInt(tokens[1]);
				
				if (cFrame != frame) {
					if (rawDataFrame != null) {
						device.rawDataFrames.add(rawDataFrame);
					}
					
					if (dataFrame != null) {
						device.dataFrames.add(dataFrame);
					}
					
					rawDataFrame = new RawFrame();
					dataFrame = new DataFrame(DataFrame.LocalOrWorld.WORLD);
					counter++;
				}
				
				frame = cFrame;
				
				RawReading reading = new RawReading(
//						SensorID	sensorCount	FrameCount
						Integer.parseInt(tokens[0]), sensorCount, counter,
//						accelerationX	accelerationY	accelerationZ
						Float.parseFloat(tokens[2]), Float.parseFloat(tokens[3]), Float.parseFloat(tokens[4]),
//						angularRateX	angularRateY	angularRateZ
						Float.parseFloat(tokens[5]), Float.parseFloat(tokens[6]), Float.parseFloat(tokens[7]),
//						magneticX	magneticY	magneticZ
						Float.parseFloat(tokens[8]), Float.parseFloat(tokens[9]), Float.parseFloat(tokens[10]),
//						rawQuatX	rawQuatY	rawQuatZ	rawQuatW
						Float.parseFloat(tokens[11]), Float.parseFloat(tokens[12]), Float.parseFloat(tokens[13]), Float.parseFloat(tokens[14]),
//						quaternionX	quaternionY	quaternionZ	quaternionW
						Float.parseFloat(tokens[15]), Float.parseFloat(tokens[16]), Float.parseFloat(tokens[17]), Float.parseFloat(tokens[18]),
//						qErrorX	qErrorY	qErrorZ	qErrorW	
						Float.parseFloat(tokens[19]), Float.parseFloat(tokens[20]), Float.parseFloat(tokens[21]), Float.parseFloat(tokens[22]),
//						qAzimuthError
						Float.parseFloat(tokens[23]),
//						nMag	nMagZero	alpha
						Float.parseFloat(tokens[24]), Float.parseFloat(tokens[25]), Float.parseFloat(tokens[26]),
//						state
						Float.parseFloat(tokens[27]));
				rawDataFrame.addReading(reading);
				


				float qx = Float.parseFloat(tokens[15]);
				float qy = Float.parseFloat(tokens[16]);
				float qz = Float.parseFloat(tokens[17]);
				float qw = Float.parseFloat(tokens[18]);
//				System.out.println(counter + "\t" + tokens[0] + "\t" + tokens[15] + "\t" + tokens[16] + "\t" + tokens[17] + "\t" + tokens[18]);
				
				dataFrame.addRotation(Integer.parseInt(tokens[0]), new Mat3(
						1.0F - (2.0F * qy * qy) - (2.0F * qz * qz), 2.0F * qx
								* qy - (2.0F * qz * qw), 2.0F * qx * qz + 2.0F
								* qy * qw, 2.0F * qx * qy + 2.0F * qz * qw,
						1.0F - (2.0F * qx * qx) - (2.0F * qz * qz), 2.0F * qy
								* qz - (2.0F * qx * qw), 2.0F * qx * qz
								- (2.0F * qy * qw), 2.0F * qy * qz + 2.0F * qx
								* qw, 1.0F - (2.0F * qx * qx)
								- (2.0F * qy * qy)));
				
				
			}
			
			
			MarkFrames.init(fileName , device.rawDataFrames.size());
		
			
			if (rawDataFrame != null) {
				device.rawDataFrames.add(rawDataFrame);
				
			}
			
			if (dataFrame != null) {
				device.dataFrames.add(dataFrame);
			}
			
			reader.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private static int getSensorCount(String fileName) {
		int count = -1;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			// discard header...
			String line = reader.readLine();
			String[] tokens = line.split("\t");
			
			Vector<Integer> uniqueIds = new Vector<Integer>();
			
			while (reader.ready()) {
				line = reader.readLine();
				tokens = line.split("\t");
				
				int sensorID = Integer.parseInt(tokens[0]);
				if (!uniqueIds.contains(sensorID)) {
					uniqueIds.add(sensorID);
				}
			}
			
			reader.close();
			count = uniqueIds.size();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return count;
	}
	
	public static void main(String[] args) {
		String test = "0	7263	-0,16504	0,03672	0,51903	0,00035	0,00009	0,00010	0,05372	0,02673	0,54762	0,13335	0,09468	-0,68961	0,70548	-0,38513	0,65514	-0,50428	0,41008	-0,00778	-0,00503	0,00739	0,99993	1,00000	1,10068	1,17053	0,00016	0,00000";
		test = test.replaceAll(",", "\\.");
		System.out.println(test);
		String[] tokens = test.split("\t");
		System.out.println(Float.parseFloat(tokens[2]));
	}
	
}
