package network.connections;

import java.beans.PropertyChangeSupport;
import java.util.List;

public abstract interface TCPSuitConnectionProvider {
	public abstract void assignPropertyChangeSupport(PropertyChangeSupport pcs);
	
	public abstract void findSuits();

	public abstract void disconnectSuits(
			List<? extends SuitConnection> paramList);

	public abstract void disconnectSuitsFromAction();

	public abstract void connectSuits(List<? extends SuitConnection> paramList);

	public abstract void alignSuits(List<? extends SuitConnection> paramList);

	public abstract void alignSuitsFromAction();

	public abstract void tagAction();

	public abstract void setRaw(boolean paramBoolean);
}