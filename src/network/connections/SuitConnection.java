package network.connections;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.UUID;

public class SuitConnection {
	public boolean tagNext = false;

	private UUID id = UUID.randomUUID();
	private String name;
	private int fps;
	protected boolean daqConnected = false;
	protected DatagramSocket daqSocket;

	public SuitConnection(String name, int fps) {
		this.name = name;
		this.fps = fps;
	}

	public String getName() {
		return this.name;
	}

	public int getFps() {
		return this.fps;
	}

	public boolean isDaqConnected() {
		return this.daqConnected;
	}

	public void daqConnect(String ip, int port) throws SocketException,
			UnknownHostException, IOException {
		if (this.daqSocket != null) {
			this.daqSocket.disconnect();
			this.daqSocket.close();
		}

		this.daqSocket = new DatagramSocket(port, InetAddress.getByName(ip));

		byte[] buf = new byte[2048];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		this.daqSocket.receive(packet);

		this.daqConnected = true;
	}

	public byte[] daqReceive() throws IOException {
		byte[] buf = new byte[232];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		this.daqSocket.receive(packet);

		if (packet.getLength() > 0) {
			return buf;
		}
		return null;
	}

	public boolean equals(Object obj) {
		if (obj instanceof SuitConnection) {
			SuitConnection that = (SuitConnection) obj;

			return (that.hashCode() == hashCode());
		}

		return false;
	}

	public int hashCode() {
		int hash = 7;
		hash = 67 * hash + this.id.hashCode();
		hash = 67 * hash + this.name.hashCode();
		return hash;
	}

	public String toString() {
		return this.name;
	}
}
