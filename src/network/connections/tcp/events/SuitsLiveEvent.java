package network.connections.tcp.events;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;

public class SuitsLiveEvent {
	public List<? extends SuitConnection> suits;

	public SuitsLiveEvent() {
		super();
		this.suits = new ArrayList<SuitConnection>();
	}

	public SuitsLiveEvent(List<? extends SuitConnection> suits) {
		this.suits = suits;
	}
}
