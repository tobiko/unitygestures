package network.connections.tcp.events;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;

public class SuitsFoundEvent {
	public List<? extends SuitConnection> suits;

	public SuitsFoundEvent() {
		super();
		this.suits = new ArrayList<SuitConnection>();
	}

	public SuitsFoundEvent(List<? extends SuitConnection> suits) {
		this.suits = suits;
	}
}