package network.connections.tcp.events;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;

public class SuitsDisconnectedEvent {
	public List<? extends SuitConnection> suits;

	public SuitsDisconnectedEvent() {
		super();
		this.suits = new ArrayList<SuitConnection>();
	}

	public SuitsDisconnectedEvent(List<? extends SuitConnection> suits) {
		this.suits = suits;
	}
}
