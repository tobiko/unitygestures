package network.connections.tcp.events;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;

public class SuitsConnectedEvent {
	public List<? extends SuitConnection> suits;

	public SuitsConnectedEvent() {
		super();
		this.suits = new ArrayList<SuitConnection>();
	}

	public SuitsConnectedEvent(List<? extends SuitConnection> suits) {
		this.suits = suits;
	}
}
