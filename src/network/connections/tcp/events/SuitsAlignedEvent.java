package network.connections.tcp.events;

import java.util.ArrayList;
import java.util.List;

import network.connections.SuitConnection;

public class SuitsAlignedEvent {
	public List<? extends SuitConnection> suits;

	public SuitsAlignedEvent() {
		super();
		this.suits = new ArrayList<SuitConnection>();
	}

	public SuitsAlignedEvent(List<? extends SuitConnection> suits) {
		this.suits = suits;
	}
}
