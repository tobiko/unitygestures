package network.connections.tcp;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import main.GlobalSettingsAndVariables;
import main.MainFrame;
import network.connections.SuitConnection;
import network.connections.TCPSuitConnectionProvider;
import network.connections.tcp.events.SuitsAlignedEvent;
import network.connections.tcp.events.SuitsConnectedEvent;
import network.connections.tcp.events.SuitsDisconnectedEvent;
import network.connections.tcp.events.SuitsFoundEvent;
import streamservice.frames.DataFrame;
import streamservice.frames.RawFrame;
import streamservice.stream.StreamService;
import eventhandling.EventBus;

import com.spookengine.maths.Mat3;

public class TCPSuitConnectorBackup080814 implements TCPSuitConnectionProvider {

	private final static int HEADER =  1431655765;
	private final static int FOOTER = -1431655766;
	private final static int CTRL_PORT = 29293;
	private final static int DATA_PORT = 29294;
	private final static int MAX_NUMBER_OF_CONNECTION_ATTEMPTS = 60;
	
	private class ConnectionScannerDialog extends JDialog implements ActionListener, PropertyChangeListener {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1400271846939804078L;
		
		private JTextArea taskOutput;
		private JProgressBar progressBar;
		private PeddlerPool peddlerPool;
		
		private ConnectionScannerDialog(PeddlerPool peddlerPool, PropertyChangeSupport pcs) {
			super();
			this.peddlerPool = peddlerPool;
			this.setTitle("connection initialization...");
			this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			this.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
			this.setAlwaysOnTop(true);
			
			pcs.addPropertyChangeListener(this);
			
			this.progressBar = new JProgressBar(
					0, TCPSuitConnectorBackup080814.MAX_NUMBER_OF_CONNECTION_ATTEMPTS);
			this.progressBar.setValue(0);
			this.progressBar.setStringPainted(true);
			this.progressBar.setIndeterminate(false);
			
			this.taskOutput = new JTextArea(5, 20);
			this.taskOutput.setMargin(new Insets(5, 5, 5, 5));
			this.taskOutput.setEditable(false);

			JButton cancelButton = new JButton("cancel");
			cancelButton.addActionListener(this);
			cancelButton.setActionCommand("cancel");
			
			JScrollPane ouputPane = new JScrollPane(this.taskOutput);
			
			JPanel panel = new JPanel();
			GroupLayout mainLayout = new GroupLayout(panel);
			mainLayout.setAutoCreateContainerGaps(true);
			mainLayout.setAutoCreateGaps(true);
			
			mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
				.addComponent(this.progressBar)
				.addComponent(ouputPane)
				.addComponent(cancelButton)
			);
			
			mainLayout.setHorizontalGroup(mainLayout.createParallelGroup()
				.addComponent(this.progressBar)
				.addComponent(ouputPane)
				.addGroup(mainLayout.createSequentialGroup()
					.addGap(0, 100, Short.MAX_VALUE)
					.addComponent(cancelButton)	
				)
			);
			
			panel.setLayout(mainLayout);
			
			this.setContentPane(panel);
			
			Dimension d = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			Point upperLeft = new Point(0, 0);
			
			this.setSize((int) (d.width * .5D), (int) (d.height * .25D));
			Point center = new Point(upperLeft.x + d.width / 2, upperLeft.y
					+ d.height / 2);
			this.setLocation(new Point(center.x - this.getWidth() / 2, center.y
					- this.getHeight() / 2));
			
			this.setVisible(true);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getActionCommand().equals("cancel")) {
				this.peddlerPool.requestCancel();
			}
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent pce) {
			if (pce.getPropertyName().equals(MainFrame.CONNECTION_SEARCH_PCN)) {
				if (pce.getNewValue() instanceof String) {
					this.taskOutput.append(((String) pce.getNewValue()) + System.getProperty("line.separator"));
					this.taskOutput.setCaretPosition(this.taskOutput.getDocument().getLength());
				} else if (pce.getNewValue() instanceof Integer) {
					this.progressBar.setValue(((Integer) pce.getNewValue()).intValue());
				} else if (pce.getNewValue() instanceof Boolean) {
					this.dispose();
				}
			}
		}
	}
	
	private class PeddlerPool implements Runnable {
		private PropertyChangeSupport pcs;
		private boolean externalCancel;
		
		public PeddlerPool() {
			super();
			this.externalCancel = false;
			this.pcs = new PropertyChangeSupport(this);
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					new ConnectionScannerDialog(PeddlerPool.this, PeddlerPool.this.pcs);
				}
			});
		}

		public void run() {
			int attempts = 0;
			boolean run = true;
			while (run) {
				if (this.externalCancel) {
					break;
				}
				attempts++;
				int threads = 5;
				int ips = 50;
				int ipsPerThread = ips / threads;
				int ipOffset = 100;

				ExecutorService executor = Executors
						.newFixedThreadPool(threads);
				List<Future<List<Socket>>> futures = new ArrayList<Future<List<Socket>>>(threads);
				for (int i = 0; i < threads; i++) {
					futures.add(executor
							.submit(new TCPSuitConnectorBackup080814.PeddlerThread(ipOffset
									+ i * ipsPerThread, ipOffset + ipsPerThread
									+ i * ipsPerThread)));
				}

				TCPSuitConnectorBackup080814.this.found = new ArrayList<SuitConnection>();
				
				if (GlobalSettingsAndVariables.fuseConnections) {
					Vector<Socket> sockets = new Vector<Socket>();
					
					for (Future<List<Socket>> future : futures) {
						try {
							for (Socket sock : future.get()) {
								sockets.add(sock);
							}
						} catch (InterruptedException ex) {
							ex.printStackTrace();
						} catch (ExecutionException ex) {
							ex.printStackTrace();
						} finally {
						}
					}
					
					if (!sockets.isEmpty()) {
						Socket[] socketArray = new Socket[sockets.size()];
						for (int i = 0; i < socketArray.length; i++) {
							socketArray[i] = sockets.remove(0);
						}
						
						SuitConnection suitConn = new TCPSuitConnection(
								"TCP -- fused connection",
								GlobalSettingsAndVariables.defaultFPS, socketArray);
						TCPSuitConnectorBackup080814.this.found.add(suitConn);
					}
				} else {
					for (Future<List<Socket>> future : futures) {
						try {
							for (Socket sock : future.get()) {
								SuitConnection suitConn = new TCPSuitConnection(
										"TCP " + sock.getInetAddress().getHostName(),
										GlobalSettingsAndVariables.defaultFPS, new Socket[] {sock});
								TCPSuitConnectorBackup080814.this.found.add(suitConn);
							}
						} catch (InterruptedException ex) {
							ex.printStackTrace();
						} catch (ExecutionException ex) {
							ex.printStackTrace();
						} finally {
						}
					}
				}
				
				if (!(TCPSuitConnectorBackup080814.this.found.isEmpty())) {
					TCPSuitConnectorBackup080814.this
							.connectSuits(TCPSuitConnectorBackup080814.this.found);

					EventBus.getInstance().removeAll(SuitsFoundEvent.class);
					EventBus.getInstance().add(
							new SuitsFoundEvent(TCPSuitConnectorBackup080814.this.found));
					break;
				}

				if (attempts == TCPSuitConnectorBackup080814.MAX_NUMBER_OF_CONNECTION_ATTEMPTS)
					break;
				try {
					this.pcs.firePropertyChange(MainFrame.CONNECTION_SEARCH_PCN, -1, attempts);
					this.pcs.firePropertyChange(MainFrame.CONNECTION_SEARCH_PCN, null, "done with attempt no." + String.valueOf(attempts) + ", waiting for 2 seconds...");
					Thread.sleep(2000L);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
			TCPSuitConnectorBackup080814.this.isFinding = false;

			this.pcs.firePropertyChange(MainFrame.CONNECTION_SEARCH_PCN, false, true);
		}
		
		private void requestCancel() {
			this.pcs.firePropertyChange(MainFrame.CONNECTION_SEARCH_PCN, null, "received cancel request...");
			this.externalCancel = true;
		}
	}

	private class PeddlerThread implements Callable<List<Socket>> {
		private final int ipMin;
		private final int ipMax;

		public PeddlerThread(int ipMin, int ipMax) {
			this.ipMin = ipMin;
			this.ipMax = ipMax;
		}

		public List<Socket> call() throws Exception {
			List<Socket> res = new ArrayList<Socket>();

			for (int i = this.ipMin; i < this.ipMax; ++i) {
				String ip = "192.168.11." + i;
				InetSocketAddress addr = new InetSocketAddress(ip, TCPSuitConnectorBackup080814.CTRL_PORT);
				try {
					Socket sock = new Socket();
					sock.connect(addr, 200);

					res.add(sock);
				} catch (UnknownHostException ex) {
				} catch (IOException ex) {
				}
			}
			for (int i = this.ipMin; i < this.ipMax; ++i) {
				String ip = "127.8.0." + i;
				InetSocketAddress addr = new InetSocketAddress(ip, TCPSuitConnectorBackup080814.CTRL_PORT);
				try {
					Socket sock = new Socket();
					sock.connect(addr, 200);

					res.add(sock);
				} catch (UnknownHostException ex) {
				} catch (IOException ex) {
				}
			}

			MainFrame.defaultConsole.write("returning "
					+ String.valueOf(res.size())
					+ " sockets ( searched from ip " + "192.168.11."
					+ String.valueOf(this.ipMin) + " to ip " + "192.168.11."
					+ String.valueOf(this.ipMax) + " )..."
					+ System.getProperty("line.separator"));
			return res;
		}
	}

	private class TCPSuitConnectionTask extends TimerTask {
		// FIXME: actually i don't see why these values should be globals...
//		private StreamService playback;
//		private TCPSuitConnection suit;
//		private Socket dataSock;
//		private BufferedInputStream dataIn;
//		private int bytesRead;
//		private byte[] sensorsBitmaskBuffer = new byte[4];
//		private byte[] timestampBuffer = new byte[4];
//		private boolean[] sensorsPresent = new boolean[32];
//		private int sensorCount;
//		private int s;
//		private int nSensors;
//		private int bitmask;
//		private int bytesMatch;
//		private int b;
//		private int messageSize;
//		private float qx;
//		private float qy;
//		private float qz;
//		private float qw;
//		private byte[] packet;
//		private DataFrame dataFrame;
//		private RawFrame rawFrame;

//		public TCPSuitConnectionTask(TCPSuitConnection suit) throws IOException {
//			super();
//			this.playback = GlobalSettingsAndVariables.defaultPlaybackService;
//			this.suit = suit;
//
//			suit.send(new int[] { TCPSuitConnector.HEADER, 16777216, 16777216, TCPSuitConnector.FOOTER });
//
//			byte[] res = suit.receive(8);
//			int retries = 0;
//			while (retries < 5) {
//				if ((TCPSuitConnector.this.bytesToInt(
//						Arrays.copyOfRange(res, 0, 4), ByteOrder.LITTLE_ENDIAN) == TCPSuitConnector.HEADER)
//						&& (TCPSuitConnector.this.bytesToInt(
//								Arrays.copyOfRange(res, 4, 8),
//								ByteOrder.LITTLE_ENDIAN) == 1)) {
//					break;
//				}
//
//				++retries;
//				try {
//					Thread.sleep(200L);
//				} catch (InterruptedException ex) {
//					ex.printStackTrace();
//				}
//			}
//
//			if (retries < 5) {
//				InetSocketAddress addr = new InetSocketAddress(suit.getIp(),
//						TCPSuitConnector.DATA_PORT);
//				this.dataSock = new Socket();
//				this.dataSock.connect(addr);
//
//				this.dataIn = new BufferedInputStream(
//						this.dataSock.getInputStream());
//
//				MainFrame.defaultConsole.write("Data connection established to "
//						+ suit.getIp().toString() + "..." + System.getProperty("line.separator"));
//			} else {
//				MainFrame.defaultConsole.write("Connection timeout! Did not get response from TCP server when trying to open data stream." + System.getProperty("line.separator"));
//			}
//		}
		
		private StreamService playback;
		private TCPSuitConnection suit;
		private Socket[] dataSockets;
		private BufferedInputStream[] dataInputStreams;
		private int bytesRead;
		private byte[] sensorsBitmaskBuffer = new byte[4];
		private byte[] timestampBuffer = new byte[4];
		private boolean[] sensorsPresent = new boolean[32];
		private int sensorCount;
		private int s;
		private int nSensors;
		private int bitmask;
		private int bytesMatch;
		private int b;
		private int messageSize;
		private float qx;
		private float qy;
		private float qz;
		private float qw;
		private byte[] packet;
		private DataFrame dataFrame;
		private RawFrame rawFrame;
		
		public TCPSuitConnectionTask(TCPSuitConnection suit) throws IOException {
			super();
			this.playback = GlobalSettingsAndVariables.defaultStreamService;
			this.suit = suit;

			this.dataInputStreams = new BufferedInputStream[suit.getNumberOfConnections()];
			this.dataSockets = new Socket[suit.getNumberOfConnections()];
			
			for (int socketIndex = 0; socketIndex < suit.getNumberOfConnections(); socketIndex++) {
				suit.send(new int[] { TCPSuitConnectorBackup080814.HEADER, 16777216, 16777216, TCPSuitConnectorBackup080814.FOOTER }, socketIndex);

				byte[] res = suit.receive(8, socketIndex);
				int retries = 0;
				while (retries < 5) {
					if ((TCPSuitConnectorBackup080814.this.bytesToInt(
							Arrays.copyOfRange(res, 0, 4), ByteOrder.LITTLE_ENDIAN) == TCPSuitConnectorBackup080814.HEADER)
							&& (TCPSuitConnectorBackup080814.this.bytesToInt(
									Arrays.copyOfRange(res, 4, 8),
									ByteOrder.LITTLE_ENDIAN) == 1)) {
						break;
					}

					++retries;
					try {
						Thread.sleep(200L);
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
				}

				if (retries < 5) {
					InetSocketAddress addr = new InetSocketAddress(suit.getIp(socketIndex),
							TCPSuitConnectorBackup080814.DATA_PORT);
					this.dataSockets[socketIndex] = new Socket();
					this.dataSockets[socketIndex].connect(addr);

					this.dataInputStreams[socketIndex] = new BufferedInputStream(
							this.dataSockets[socketIndex].getInputStream());

					MainFrame.defaultConsole.write("Data connection established to "
							+ suit.getIp(socketIndex).toString() + "..." + System.getProperty("line.separator"));
				} else {
					MainFrame.defaultConsole.write("Connection timeout! Did not get response from TCP server when trying to open data stream." + System.getProperty("line.separator"));
				}
			}
		}

//		public void run() {
//			try {
//				this.dataFrame = new DataFrame(DataFrame.LocalOrWorld.WORLD);
//				this.rawFrame = new RawFrame();
//
//				this.bytesMatch = 0;
//				this.b = 0;
//				do {
//					try {
//						this.b = this.dataIn.read();
//					} catch (IOException ex) {
//						ex.printStackTrace();
//						break;
//					}
//					if (this.b == 85)
//						this.bytesMatch += 1;
//				} while ((this.b != -1) && (this.bytesMatch < 4));
//
//				if (this.bytesMatch == 4) {
//					this.nSensors = 0;
//					this.bytesRead = this.dataIn
//							.read(this.sensorsBitmaskBuffer);
//					if (this.bytesRead == 4) {
//						this.bitmask = TCPSuitConnector.this.bytesToInt(
//								this.sensorsBitmaskBuffer,
//								ByteOrder.LITTLE_ENDIAN);
//						for (int i = 0; i < 32; ++i) {
//							this.sensorsPresent[i] = false;
//							if ((this.bitmask & 1 << i) != 0) {
//								this.sensorsPresent[i] = true;
//								this.nSensors += 1;
//							}
//						}
//
//					}
//
//					this.bytesRead = this.dataIn.read(this.timestampBuffer);
//
//					this.messageSize = (this.nSensors * 104 + 4);
//					this.packet = new byte[this.messageSize];
//
//					while (this.dataIn.available() < this.messageSize)
//						;
//					this.dataIn.read(this.packet);
//
//					if (TCPSuitConnector.this.bytesToInt(Arrays
//							.copyOfRange(this.packet, this.messageSize - 4,
//									this.messageSize), ByteOrder.LITTLE_ENDIAN) == TCPSuitConnector.FOOTER) {
//						this.sensorCount = 0;
//						for (int i = 0; i < 32; ++i) {
//							if (this.sensorsPresent[i]) {
//								this.s = (this.sensorCount * 104 + 52);
//								this.qx = TCPSuitConnector.this.bytesToFloat(
//										Arrays.copyOfRange(this.packet, this.s,
//												this.s += 4),
//										ByteOrder.LITTLE_ENDIAN);
//								this.qy = TCPSuitConnector.this.bytesToFloat(
//										Arrays.copyOfRange(this.packet, this.s,
//												this.s += 4),
//										ByteOrder.LITTLE_ENDIAN);
//								this.qz = TCPSuitConnector.this.bytesToFloat(
//										Arrays.copyOfRange(this.packet, this.s,
//												this.s += 4),
//										ByteOrder.LITTLE_ENDIAN);
//								this.qw = TCPSuitConnector.this.bytesToFloat(
//										Arrays.copyOfRange(this.packet, this.s,
//												this.s += 4),
//										ByteOrder.LITTLE_ENDIAN);
//								this.dataFrame.addRotation(i, new Mat3(1.0F
//										- (2.0F * this.qy * this.qy)
//										- (2.0F * this.qz * this.qz), 2.0F
//										* this.qx * this.qy
//										- (2.0F * this.qz * this.qw), 2.0F
//										* this.qx * this.qz + 2.0F * this.qy
//										* this.qw, 2.0F * this.qx * this.qy
//										+ 2.0F * this.qz * this.qw, 1.0F
//										- (2.0F * this.qx * this.qx)
//										- (2.0F * this.qz * this.qz), 2.0F
//										* this.qy * this.qz
//										- (2.0F * this.qx * this.qw), 2.0F
//										* this.qx * this.qz
//										- (2.0F * this.qy * this.qw), 2.0F
//										* this.qy * this.qz + 2.0F * this.qx
//										* this.qw, 1.0F
//										- (2.0F * this.qx * this.qx)
//										- (2.0F * this.qy * this.qy)));
//
//								if (TCPSuitConnector.this.enableRaw)
//									// XXX: originally, the sensor id assigned here was i + 1 instead of i
//									this.rawFrame.fromPacket(this.packet,
//											i, this.sensorCount,
//											TCPSuitConnector.this.frameCount);
//								this.sensorCount += 1;
//							}
//						}
//						
//						
//						TCPSuitConnector.this.tagAction();
//						if (this.suit.tagNext) {
//							this.suit.tagNext = false;
//							this.dataFrame.tag();
//						}
//						if (TCPSuitConnector.this.enableRaw) {
//							this.playback.setRawFrame(this.rawFrame);
//						}
//						this.playback.onTick(this.suit, this.dataFrame);
//						TCPSuitConnector.this.frameCount += 1;
//					} else {
//						System.out.println("dropped package...");
//					}
//				}
//			} catch (IOException ex) {
//				ex.printStackTrace();
//			}
//		}
		
		public void run() {
			try {
				this.dataFrame = new DataFrame(DataFrame.LocalOrWorld.WORLD);
				this.rawFrame = new RawFrame();

				for (int socketIndex = 0; socketIndex < this.dataSockets.length; socketIndex++) {
					this.bytesMatch = 0;
					this.b = 0;
					do {
						try {
							this.b = this.dataInputStreams[socketIndex].read();
						} catch (IOException ex) {
							ex.printStackTrace();
							break;
						}
						if (this.b == 85)
							this.bytesMatch += 1;
					} while ((this.b != -1) && (this.bytesMatch < 4));

					if (this.bytesMatch == 4) {
						this.nSensors = 0;
						this.bytesRead = this.dataInputStreams[socketIndex]
								.read(this.sensorsBitmaskBuffer);
						if (this.bytesRead == 4) {
							this.bitmask = TCPSuitConnectorBackup080814.this.bytesToInt(
									this.sensorsBitmaskBuffer,
									ByteOrder.LITTLE_ENDIAN);
							for (int i = 0; i < 32; ++i) {
								this.sensorsPresent[i] = false;
								if ((this.bitmask & 1 << i) != 0) {
									this.sensorsPresent[i] = true;
									this.nSensors += 1;
								}
							}

						}

						this.bytesRead = this.dataInputStreams[socketIndex].read(this.timestampBuffer);

						this.messageSize = (this.nSensors * 104 + 4);
						this.packet = new byte[this.messageSize];

						while (this.dataInputStreams[socketIndex].available() < this.messageSize)
							;
						this.dataInputStreams[socketIndex].read(this.packet);

						if (TCPSuitConnectorBackup080814.this.bytesToInt(Arrays
								.copyOfRange(this.packet, this.messageSize - 4,
										this.messageSize), ByteOrder.LITTLE_ENDIAN) == TCPSuitConnectorBackup080814.FOOTER) {
							this.sensorCount = 0;
							for (int i = 0; i < 32; ++i) {
								if (this.sensorsPresent[i]) {
									this.s = (this.sensorCount * 104 + 52);
									this.qx = TCPSuitConnectorBackup080814.this.bytesToFloat(
											Arrays.copyOfRange(this.packet, this.s,
													this.s += 4),
											ByteOrder.LITTLE_ENDIAN);
									this.qy = TCPSuitConnectorBackup080814.this.bytesToFloat(
											Arrays.copyOfRange(this.packet, this.s,
													this.s += 4),
											ByteOrder.LITTLE_ENDIAN);
									this.qz = TCPSuitConnectorBackup080814.this.bytesToFloat(
											Arrays.copyOfRange(this.packet, this.s,
													this.s += 4),
											ByteOrder.LITTLE_ENDIAN);
									this.qw = TCPSuitConnectorBackup080814.this.bytesToFloat(
											Arrays.copyOfRange(this.packet, this.s,
													this.s += 4),
											ByteOrder.LITTLE_ENDIAN);
									this.dataFrame.addRotation(i, new Mat3(1.0F
											- (2.0F * this.qy * this.qy)
											- (2.0F * this.qz * this.qz), 2.0F
											* this.qx * this.qy
											- (2.0F * this.qz * this.qw), 2.0F
											* this.qx * this.qz + 2.0F * this.qy
											* this.qw, 2.0F * this.qx * this.qy
											+ 2.0F * this.qz * this.qw, 1.0F
											- (2.0F * this.qx * this.qx)
											- (2.0F * this.qz * this.qz), 2.0F
											* this.qy * this.qz
											- (2.0F * this.qx * this.qw), 2.0F
											* this.qx * this.qz
											- (2.0F * this.qy * this.qw), 2.0F
											* this.qy * this.qz + 2.0F * this.qx
											* this.qw, 1.0F
											- (2.0F * this.qx * this.qx)
											- (2.0F * this.qy * this.qy)));

									if (TCPSuitConnectorBackup080814.this.enableRaw)
										// XXX: originally, the sensor id assigned here was i + 1 instead of i
										this.rawFrame.fromPacket(this.packet,
												i, this.sensorCount,
												TCPSuitConnectorBackup080814.this.frameCount);
									this.sensorCount += 1;
								}
							}
							
							
//							TCPSuitConnector.this.tagAction();
//							if (this.suit.tagNext) {
//								this.suit.tagNext = false;
//								this.dataFrame.tag();
//							}
//							if (TCPSuitConnector.this.enableRaw) {
//								this.playback.setRawFrame(this.rawFrame);
//							}
//							this.playback.onTick(this.suit, this.dataFrame);
//							TCPSuitConnector.this.frameCount += 1;
						} else {
//							System.out.println("dropped package...");
							MainFrame.defaultConsole.write("dropped package..." + System.getProperty("line.separator"));
						}
					}
				}
				
				TCPSuitConnectorBackup080814.this.tagAction();
				if (this.suit.tagNext) {
					this.suit.tagNext = false;
					this.dataFrame.tag();
				}
				if (TCPSuitConnectorBackup080814.this.enableRaw) {
					this.playback.setRawFrame(this.rawFrame);
				}
				this.playback.onTick(this.suit, this.dataFrame);
				TCPSuitConnectorBackup080814.this.frameCount += 1;
				
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private int frameCount;
	private List<SuitConnection> found;
	private boolean enableRaw;
	private boolean isFinding;
	private final Map<SuitConnection, Timer> suitToThread;

	private PropertyChangeSupport pcs;
	
	public TCPSuitConnectorBackup080814() {
		super();
		this.frameCount = 0;
		this.found = new ArrayList<SuitConnection>();
		this.enableRaw = true;
		this.isFinding = false;
		this.suitToThread = new HashMap<SuitConnection, Timer>();
	}

	protected int bytesToInt(byte[] buf, ByteOrder endianness) {
		if (buf.length == 4) {
			ByteBuffer bb = ByteBuffer.wrap(buf).order(endianness);
			return bb.asIntBuffer().get();
		}

		throw new NumberFormatException("Byte array should contain 4 bytes.");
	}

	protected long bytesToUint(byte[] buf, ByteOrder endianness) {
		if (buf.length == 4) {
			ByteBuffer bb = ByteBuffer.wrap(buf).order(endianness);
			return (0xFFFFFFFF & bb.asIntBuffer().get());
		}

		throw new NumberFormatException("Byte array should contain 4 bytes.");
	}

	protected float bytesToFloat(byte[] buf, ByteOrder endianess) {
		if (buf.length == 4) {
			ByteBuffer bb = ByteBuffer.wrap(buf).order(endianess);
			return bb.asFloatBuffer().get();
		}

		throw new NumberFormatException("Byte array should contain 4 bytes.");
	}
	
	@Override
	public void assignPropertyChangeSupport(PropertyChangeSupport pcs) {
		this.pcs = pcs;
	}

	public void findSuits() {
		if (!this.isFinding) {
			Thread peddlerPool = new Thread(new PeddlerPool());
			peddlerPool.start();
			this.isFinding = true;
		}
	}

	public void connectSuits(List<? extends SuitConnection> suits) {
		for (SuitConnection suit : suits) {
			try {
				Timer timer = new Timer();
				timer.scheduleAtFixedRate(new TCPSuitConnectionTask(
						(TCPSuitConnection) suit), 0L, 1000 / suit.getFps());
				this.suitToThread.put(suit, timer);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		if (!(suits.isEmpty())) {
			alignSuits(suits);
			System.out.println("aligning suits...");
			EventBus.getInstance().removeAll(SuitsConnectedEvent.class);
			EventBus.getInstance().add(new SuitsConnectedEvent(suits));
			if (this.pcs != null) {
				this.pcs.firePropertyChange(MainFrame.BUTTON_STATE_PCN, false, true);
			}
		}
	}

//	public void disconnectSuits(List<? extends SuitConnection> suits) {
//		for (SuitConnection suit : suits)
//			if (suit instanceof TCPSuitConnection) {
//				TCPSuitConnection tcpSuit = (TCPSuitConnection) suit;
//				try {
//					tcpSuit.send(new int[] { TCPSuitConnector.HEADER, 16777216, 16777216,
//							TCPSuitConnector.FOOTER });
//
//					MainFrame.defaultConsole.write("disconnecting suit "
//							+ tcpSuit.getName() + "..."
//							+ System.getProperty("line.separator"));
//					EventBus.getInstance().add(
//							new SuitsDisconnectedEvent(suits));
//				} catch (IOException ex) {
//				}
//			}
//		suits.clear();
//		
//		if (this.pcs != null) {
//			this.pcs.firePropertyChange(MainFrame.BUTTON_STATE_PCN, true, false);
//		}
//	}
	
	public void disconnectSuits(List<? extends SuitConnection> suits) {
		for (SuitConnection suit : suits)
			if (suit instanceof TCPSuitConnection) {
				TCPSuitConnection tcpSuit = (TCPSuitConnection) suit;
				try {
					for (int socketIndex = 0; socketIndex < tcpSuit.getNumberOfConnections(); socketIndex++) {
						tcpSuit.send(new int[] { TCPSuitConnectorBackup080814.HEADER, 16777216, 16777216,
								TCPSuitConnectorBackup080814.FOOTER }, socketIndex);
					}

					MainFrame.defaultConsole.write("disconnecting suit "
							+ tcpSuit.getName() + "..."
							+ System.getProperty("line.separator"));
					EventBus.getInstance().add(
							new SuitsDisconnectedEvent(suits));
				} catch (IOException ex) {
				}
			}
		suits.clear();
		
		if (this.pcs != null) {
			this.pcs.firePropertyChange(MainFrame.BUTTON_STATE_PCN, true, false);
		}
	}

	public void disconnectSuitsFromAction() {
		if (!(this.found.isEmpty()))
			disconnectSuits(this.found);
	}

//	public void alignSuits(List<? extends SuitConnection> suits) {
//		for (SuitConnection suit : suits) {
//			if (suit instanceof TCPSuitConnection) {
//				TCPSuitConnection tcpSuit = (TCPSuitConnection) suit;
//				try {
//					ByteBuffer msg = ByteBuffer.allocate(528);
//					msg.putInt(TCPSuitConnector.HEADER);
//					msg.putInt(-1);
//					msg.putInt(134217728);
//					for (int i = 0; i < 32; ++i) {
//						msg.putFloat(0.0F);
//						msg.putFloat(0.0F);
//						msg.putFloat(0.0F);
//						msg.putFloat(1.0F);
//					}
//					msg.putInt(TCPSuitConnector.FOOTER);
//					tcpSuit.send(msg.array());
//
//					tcpSuit.send(new int[] { TCPSuitConnector.HEADER, 150994944, -1,
//							TCPSuitConnector.FOOTER });
//					MainFrame.defaultConsole.write("Aligning suit " + suit.getName()
//							+ "..." + System.getProperty("line.separator"));
//				} catch (IOException ex) {
//					ex.printStackTrace();
//				}
//			}
//		}
//
//		if (!(suits.isEmpty())) {
//			EventBus.getInstance().removeAll(SuitsAlignedEvent.class);
//			EventBus.getInstance().add(new SuitsAlignedEvent(suits));
//		}
//	}
	
	public void alignSuits(List<? extends SuitConnection> suits) {
		for (SuitConnection suit : suits) {
			if (suit instanceof TCPSuitConnection) {
				TCPSuitConnection tcpSuit = (TCPSuitConnection) suit;
				try {
					for (int socketIndex = 0; socketIndex < tcpSuit.getNumberOfConnections(); socketIndex++) {
						ByteBuffer msg = ByteBuffer.allocate(528);
						msg.putInt(TCPSuitConnectorBackup080814.HEADER);
						msg.putInt(-1);
						msg.putInt(134217728);
						for (int i = 0; i < 32; ++i) {
							msg.putFloat(0.0F);
							msg.putFloat(0.0F);
							msg.putFloat(0.0F);
							msg.putFloat(1.0F);
						}
						msg.putInt(TCPSuitConnectorBackup080814.FOOTER);
						tcpSuit.send(msg.array(), socketIndex);

						tcpSuit.send(new int[] { TCPSuitConnectorBackup080814.HEADER, 150994944, -1,
								TCPSuitConnectorBackup080814.FOOTER }, socketIndex);
					}
					MainFrame.defaultConsole.write("Aligning suit " + suit.getName()
							+ "..." + System.getProperty("line.separator"));
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}

		if (!(suits.isEmpty())) {
			EventBus.getInstance().removeAll(SuitsAlignedEvent.class);
			EventBus.getInstance().add(new SuitsAlignedEvent(suits));
		}
	}

	public void alignSuitsFromAction() {
		if (!(this.found.isEmpty()))
			alignSuits(this.found);
	}

	public void tagAction() {
		for (SuitConnection suit : this.found)
			suit.tagNext = true;
	}

	public void setRaw(boolean enableRaw) {
		this.enableRaw = enableRaw;
	}
}