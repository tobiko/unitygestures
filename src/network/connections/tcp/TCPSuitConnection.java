package network.connections.tcp;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import network.connections.SuitConnection;

//public class TCPSuitConnection extends SuitConnection {
//	private Socket ctrlSock;
//	private DataOutputStream ctrlOut;
//	private BufferedInputStream ctrlIn;
//
//	public TCPSuitConnection(String name, int fps, Socket ctrlSock) {
//		super(name, fps);
//		try {
//			this.ctrlSock = ctrlSock;
//			this.ctrlOut = new DataOutputStream(ctrlSock.getOutputStream());
//			this.ctrlIn = new BufferedInputStream(ctrlSock.getInputStream());
//		} catch (IOException ex) {
//			Logger.getLogger(TCPSuitConnection.class.getName()).log(
//					Level.SEVERE, null, ex);
//		}
//	}
//
//	public String getIp() {
//		return this.ctrlSock.getInetAddress().getHostName();
//	}
//
//	public void send(int[] msg) throws IOException {
//		ByteBuffer bb = ByteBuffer.allocate(4 * msg.length);
//		IntBuffer ib = bb.asIntBuffer();
//		ib.put(msg);
//
//		this.ctrlOut.write(bb.order(ByteOrder.BIG_ENDIAN).array());
//	}
//
//	public void send(byte[] msg) throws IOException {
//		ByteBuffer bb = ByteBuffer.allocate(msg.length);
//
//		this.ctrlOut.write(bb.order(ByteOrder.BIG_ENDIAN).array());
//	}
//
//	public byte[] receive(int size) throws IOException {
//		byte[] res = new byte[size];
//
//		this.ctrlIn.read(res);
//		return res;
//	}
//}

public class TCPSuitConnection extends SuitConnection {
	private Socket[] controlSockets;
	private DataOutputStream[] controlOutput;
	private BufferedInputStream[] controlInput;

	public TCPSuitConnection(String name, int fps, Socket[] controlSockets) {
		super(name, fps);
		try {
			this.controlSockets = controlSockets;
			this.controlOutput = new DataOutputStream[this.controlSockets.length];
			this.controlInput  = new BufferedInputStream[this.controlSockets.length];
			
			for (int i = 0; i < this.controlSockets.length; i++) {
				this.controlOutput[i] = new DataOutputStream(this.controlSockets[i].getOutputStream());
				this.controlInput[i]  = new BufferedInputStream(this.controlSockets[i].getInputStream());
			}
		} catch (IOException ex) {
			Logger.getLogger(TCPSuitConnection.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}

	public String getIp(int socketIndex) {
		return this.controlSockets[socketIndex].getInetAddress().getHostName();
	}

	public void send(int[] msg, int socketIndex) throws IOException {
		ByteBuffer bb = ByteBuffer.allocate(4 * msg.length);
		IntBuffer ib = bb.asIntBuffer();
		ib.put(msg);

		this.controlOutput[socketIndex].write(bb.order(ByteOrder.BIG_ENDIAN).array());
	}

	public void send(byte[] msg, int socketIndex) throws IOException {
		ByteBuffer bb = ByteBuffer.allocate(msg.length);

		this.controlOutput[socketIndex].write(bb.order(ByteOrder.BIG_ENDIAN).array());
	}

	public byte[] receive(int size, int socketIndex) throws IOException {
		byte[] res = new byte[size];

		this.controlInput[socketIndex].read(res);
		return res;
	}
	
	public int getNumberOfConnections() {
		return this.controlSockets.length;
	}
	
	public void disconnect(int socketIndex) throws IOException {
		this.controlOutput[socketIndex].close();
		this.controlSockets[socketIndex].close();
	}
}