%% Evaluation of the network during the learning phase
% 
% -How many patterns are learned for each gesture 
%  (groups of patterns)?
% -Are these groups of patterns disjoint? Patterns should
%  mainly exclusive win while showing a gesture.


%% Initialisation

clear; close all; clc;
%% Reading in of the csv-file
% ...which contains information about the pattern (profit-frequency)
% while a gesture (or non-gesture) is learned.
filename = 'csvTestData.dat';

disp(sprintf('Collecting csv-data...'))

% approach: 
% csv file contains only numeric values:

M = csvread(filename)

disp(sprintf('Collecting finished.'))


%% Analyze the collected data

% Not necessary when data is analysed in java!

disp(sprintf('Create analysis of the data...'))

disp(sprintf('Analysis completed.'))


%% Display the results

disp(sprintf('Showing results of the analysis...'))

%create a figure-window with an own title
figure('Name','Evaluation','NumberTitle','off');

%current test-data
analysedData = [2 2 3 1 3 2;
     2 5 6 1 2 2;
     2 8 9 3 5 4;
     2 11 12 7 14 12];
 
%get the dimensions of analysedData and use them to adjust the barplot
[rows,columns] = size(analysedData);

barGraph = bar(analysedData);%create the barplot

numberOfDifferentColors = columns;%number of colors that will be used for the different bars
colormap(summer(numberOfDifferentColors));%set of colors
%grid on;%shows a grid in the background of the plot

%set the range of values of the x- & y-axis
ylim([0 1.1*max(ylim)]);
xlim('auto');


legendDescription = cell(1,6);%cell array, contains description of the barplot-colors
legendDescription{1}='Geste 1, Trial 1';
legendDescription{2}='Geste 1, Trial 2';
legendDescription{3}='Geste 1, Trial 3';
legendDescription{4}='Geste 2, Trial 1';
legendDescription{5}='Geste 2, Trial 2';
legendDescription{6}='Geste 2, Trial 3';

legend(barGraph,legendDescription,'Location','EastOutside');%creates a legend outside the barplot

%label the x- & y-axis
xlabel('Motion Pattern Index');
ylabel('Win-count at setup 1...6');

disp(sprintf('Evaluation completed.'))
